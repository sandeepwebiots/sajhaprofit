@extends('layouts.home.master')
@section('title') About us | CRM @endsection
@section('content')



<div class="wrapper">
    <!-- Header Middle -->

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home">
        <div class="container-fluid text-center set-fluid">
            <div class="row">
                <div class="inner-conraimer-details">

                    <div class="col-md-12 pr-left-right">
                        <div class="banner-bg">
                            <img src="{{url('assets/home/images/team/7.jpg')}}" alt="payment.jpg" class="img-responsive">
                        <div class="center-content">
                        <h1 class="text-uppercase ">About Us</h1>
                        <div class="typed_text_bg">
                            <div id="typed-strings" style="display: none;">
                                <p>Want to work with a professional & competent team?</p>
                                <p>We have best team is client–centric believes <br> in collaborative culture.<br> Be a Part of It.</p>
                            </div>
                            <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                        </div>
                    </div>
                        </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu">
                            <li><a href="#"> HOME </a></li>
                            <li><a href="#"> <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                            <li> <a href="#"> ABOUT US </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Our About -->
    <section class="ulockd-about2">
        <div class="container-fluid">
            @foreach($about_us as $about)
            <div class="row" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <div class="col-sm-12 col-md-4">
                    <div class="ulockd-about-box ulockd-mrgn1230">
                        <div class="ab-thumb">
                            <img class="img-responsive img-whp" src="{{url('assets/home/images/banners/about')}}/{{ $about->image }}" alt="5.jpg">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8">
                    <div class="ulock-about center-text">
                        <h3 class="title-bottom text-thm2 ulockd-mrgn630 ulockd-mrgn120">{{ $about->title }}</h3>
                        <p>{{ $about->content }}<b></b>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- <div class="row" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <div class="col-sm-12 col-md-5">
                    <div class="ulockd-about-box ulockd-mrgn1230">
                        <div class="ab-thumb">
                            <img class="img-responsive img-whp" src="{{url('assets/home/images/banners/about/1.jpg')}}" alt="2.jpg">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-7">
                    <div class="ulock-about center-text">
                        <h3 class="title-bottom text-thm2 ulockd-mrgn630 ulockd-mrgn120">What we do</h3>
                        <p>Sajha profit is a group of passionate professionals who believe in providing Investment advice,
                            trading recommendations, IPOs and Mutual fund suggestions and other services related to the
                            Indian Equity and commodity market. With Innovative approach we always try to provide best
                            value for the Rupee to the clients.</p>
                    </div>
                </div>
            </div>

            <div class="row" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <div class="col-sm-12 col-md-5">
                    <div class="ulockd-about-box ulockd-mrgn1230">
                        <div class="ab-thumb">
                            <img class="img-responsive img-whp" src="{{url('assets/home/images/banners/about/5.jpg')}}" alt="5.jpg">
                        </div>
                    </div>

                </div>
                <div class="col-sm-12 col-md-7">
                    <div class="ulock-about center-text">
                        <h3 class="title-bottom text-thm2 ulockd-mrgn630 ulockd-mrgn120">Innovative</h3>
                        <p>Our team is tech savvy & always works on an innovative approach and does research on
                            economic reforms, market conditions , world economy & its impact on the Indian Equity and
                            Commodity market. </p>
                    </div>
                </div>
            </div>


            <div class="row" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <div class="col-sm-12 col-md-5">
                    <div class="ulockd-about-box ulockd-mrgn1230">
                        <div class="ab-thumb">
                            <img class="img-responsive img-whp" src="{{url('assets/home/images/banners/about/3.jpg')}}" alt="2.jpg">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-7">
                    <div class="ulock-about center-text">
                        <h3 class="title-bottom text-thm2 ulockd-mrgn630 ulockd-mrgn120">Passionate</h3>
                        <p>Sajha profit has the best passionate team of professionals, which always thrives making strategies
                            that give maximum profit & less risk. Our team relentlessly works to ensure the clients
                            always get maximum benefit from the research analysis done by our team. </p>
                    </div>
                </div>
            </div>
            <div class="row" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <div class="col-sm-12 col-md-5">
                    <div class="ulockd-about-box ulockd-mrgn1230">
                        <div class="ab-thumb">
                            <img class="img-responsive img-whp" src="{{url('assets/home/images/banners/about/4.jpg')}}" alt="5.jpg">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-7">
                    <div class="ulock-about center-text">
                        <h3 class="title-bottom text-thm2 ulockd-mrgn630 ulockd-mrgn120">Persistence</h3>
                        <p>We at Sajha profit thrive to get the best results through continuous rigorous research and analysis.
                            Our team is dedicated to implementing innovative ideas so as to take proactive actions to
                            get the best out of the market conditions. </p>
                    </div>

                </div>
            </div>
            <div class="row" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                <div class="col-sm-12 col-md-5">
                    <div class="ulockd-about-box ulockd-mrgn1230">
                        <div class="ab-thumb">
                            <img class="img-responsive img-whp" src="{{url('assets/home/images/banners/about/6.jpg')}}" alt="2.jpg">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-7">
                    <div class="ulock-about center-text">
                        <h3 class="title-bottom text-thm2 ulockd-mrgn630 ulockd-mrgn120">Integrity</h3>
                        <p>Team of Sajha profit always believes in the reputation for absolute integrity. We provide transparent
                            services to the clients to ensure strong & long lasting business relations. One of the main
                            reasons of our success is trust of our clients on us.</p>
                    </div>
                </div>
            </div> -->
            
        </div>
    </section>

</div>

@endsection

@section('script')
<script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}"></script>
@endsection
