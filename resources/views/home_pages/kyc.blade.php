@extends('layouts.home.master')
@section('title') Kyc | Sajha Profit @endsection
@section('style')
<style>
.error {
    color: red !important;
    font-size: 12px;
}
.error-cls {
    color: red;
    font-size: 12px;
}
</style>
@endsection
@section('content')
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <div class="banner-bg">
                            <img src="{{url('assets/home/images/background/city.jpg')}}" alt="explorer">
                        <div class="center-content">
                            <h1 class="text-uppercase">KYC</h1>
                            <div class="typed_text_bg">
                                <div id="typed-strings" style="display: none;">
                                    <p>When everything seems to be going against you,</br>remember that the airplane takes off against the wind,</br>not with it.</p>
                                </div>
                                <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                            </div>
                        </div>
                    </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu">
                            <li><a href="#"> HOME </a></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                            <li> <a href="#"> KYC </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Our Feature Project -->
    <section id="project" class="our-project ulockd_bgp1">
        <div class="container-fluid">
            <div class="row">
                <!--    <div class="col-md-6 col-md-offset-3 text-center">
                        <div class="ulockd-main-title">
                            <h2>Explore</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
                        </div>
                    </div> -->
            </div>
            <div class="row">
                <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="profit-service-lable">
                        <h5 class="text-center">KYC</h5>
                    </div>
                </div>
{{--.........start tabing...............--}}


 {{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}
                <div class="col-md-8 aos-init aos-animate" data-aos="fade-left" data-aos-duration="2000">
                    <ul class="nav nav-tabs nav-set">
                        <li class="active"><a data-toggle="tab" href="#home" aria-expanded="true" class="active show">FORM FOR INDIVIDUAL</a></li>
                        <li ><a data-toggle="tab" href="#menu1" id="second_tab"  aria-expanded="false">FORM FOR NON-INDIVIDUAL</a></li>
                        <li><a data-toggle="tab" href="#menu2" id="third_tab">DOWNLOAD KYC FORM</a></li>
                        <li><a data-toggle="tab" href="#menu3" id="fourth_tab">UPLOAD KYC FORM</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade active show in">
                            <div class="col-md-12 aos-init " data-aos="fade-left" data-aos-duration="2000">
                                <div class="risk-warning ">
                                    <div class="panel-bodis shadoset">
                                        <form class="form-horizontal" method="post" action="{{url('/kyc-Post-individual')}}" name="Individual" id="Individual" enctype="multipart/form-data" multiple>@csrf
                                            @if(Session::has('toasts'))
                                              @foreach(Session::get('toasts') as $toast)
                                                <div class="alert alert-{{ $toast['level'] }}">
                                                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                                                  {{ $toast['message'] }}
                                                </div>
                                              @endforeach
                                            @endif
                                            <div class="col-md-6 col-sm-6 col-12 ">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="Email" name="email" id="email_7" class="form-control form-space">
                                                    <input type="hidden" name="task" value="send">
                                                 @if ($errors->has('email'))
                                                    <span class="error-cls" role="alert" >
                                                    {{ $errors->first('email') }}
                                                    </span>
                                                @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-12 ">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="Contact No." 
                                                    name="Contact_No" id="phone_7" class="form-control" maxlength="11" minlength="11" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                                     @if ($errors->has('Contact_No'))
                                                    <span class="error-cls" role="alert" >
                                                    {{ $errors->first('Contact_No') }}
                                                    </span>
                                                @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <select class="form-control" name="gender">
                                                       
                                                        <option>Male</option>
                                                        <option>Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <select class="form-control" name="married_status">
                                                        
                                                        <option>Married</option>
                                                        <option>Unmarried</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="State" name="state" id="state_7" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <select class="form-control" name="resident_status">
                                                        
                                                        <option>Resident Individual</option>
                                                        <option>Non Resident</option>
                                                        <option>Foreign National</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <input type="date" placeholder="dd/mm/yyyy" name="dob" id="date_7" class="form-control date-picker">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="Pincode" name="pincode" id="pincode_7" maxlength="6" minlength="6" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="Pan No." name="pan_no" maxlength="10" minlength="10" id="pan_no_7" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                                @if ($errors->has('pan_no'))
                                                    <span class="error-cls" role="alert" >
                                                    {{ $errors->first('pan_no') }}
                                                    </span>
                                                @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="UID Adhar No." name="adhar" id="adhar_7" maxlength="18" minlength="18" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                                @if ($errors->has('adhar'))
                                                    <span class="error-cls" role="alert" >
                                                    {{ $errors->first('adhar') }}
                                                    </span>
                                                @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <textarea placeholder="Address" name="address" id="address_7" class="form-control" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 label_small_block">
                                                <div class="form-group">
                                                    <label class="col-sm-12 control-label">Upload Signature: (Upload Only .JPG,.PNG ,.jpeg Format)</label>
                                                    <div class="col-sm-12 form-group">
                                                        <div class="form-control">
                                                            <input type="file" name="file_signature" id="file_signature">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 label_small_block">
                                                <div class="form-group  ">
                                                    <label class="col-sm-12 control-label">Upload Photo:(Upload Only .JPG,.PNG ,.jpeg Format Only)</label>
                                                    <div class="col-sm-12 form-group">
                                                        <div class="form-control">
                                                            <input type="file" name="file_photo" id="file_photo_7">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 label_small_block">
                                                <div class="form-group  ">
                                                    <label class="col-sm-12 control-label">Upload Document: Upload Document in .PDF,.WORD Format Only</label>
                                                    <div class="col-sm-12 form-group">
                                                        <div class="form-control">
                                                            <input type="file" name="file_doc" id="file_doc_7">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 label_small_block">
                                                <div class="form-group">
                                                    <label class="col-sm-12 control-label">Please enter the verication code shown below.<span>*</span></label>

                                                    <div id="captcha-wrap" name="captcha_wrap" class="varify-cls">
                                                        <div class="g-recaptcha" name="g-recaptcha" data-sitekey="6LfaCWMUAAAAAHf7Q6V58Ec5vyU-Iui_o1YYsy3Z"></div>
                                                            @if($errors->has('g-recaptcha-response'))       
                                                            <span class="error-cls" role="alert" >
                                                            <br>
                                                            {{ $errors->first('g-recaptcha-response') }}
                                                            </span>
                                                            @endif
                                                            </div>

                                                    <br>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 ">
                                                <div class="col-sm-12 set-submit-btn">
                                                    <button  class="btn btn-primary set-buttan">SUBMIT</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>  {{--....body pannel div....--}}
                                </div>{{--....risk div close....--}}
                            </div>
                        </div>
                        {{--......second tab.start..........--}}
                                    <div id="menu1" class="tab-pane fade ">
                            <div class="col-md-12 aos-init " data-aos="fade-left" data-aos-duration="2000">
                                <div class="risk-warning ">
                                    <div class="panel-bodis shadoset">
                                        <form class="form-horizontal" method="post" action="{{url('/kyc-Post-Non-Individual')}}" id="quick_inquiry" name="quick_inquiry" enctype="multipart/form-data" multiple>@csrf
                                            <div class="col-md-6 col-sm-6 col-12 ">
                                                <div class="col-sm-12 form-group">
                                                    <input type="email" placeholder="Email" name="email1" id="email1" class="form-control form-space">
                                                    <input type="hidden" name="task" value="send">
                                                @if($errors->has('email1'))       
                                                    <span class="error-cls" role="alert" >
                                                        {{ $errors->first('email1') }}
                                                    </span>
                                                @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-12 ">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="Contact No." name="Contact_No1" id="Contact_No1" class="form-control" maxlength="11" minlength="11" onkeypress="return event.charCode >= 48 && event.charCode <= 57" >
                                                    @if($errors->has('Contact_No1'))      
                                                    <span class="error-cls" role="alert" >
                                                        {{ $errors->first('Contact_No1') }}
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <select class="form-control" name="resident_status">
                                                        <option>INDIA</option>
                                                        <option>UDA</option>
                                                        <option>ENGLAND</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-12 ">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="City" name="city" id="city" class="form-control"   >
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-12 ">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="State" name="state1" id="state1" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="Company Resistation No." name="Company_Resistation_No" id="Company_Resistation_No" class="form-control">
                                                     @if($errors->has('Company_Resistation_No'))      
                                                    <span class="error-cls" role="alert" >
                                                        {{ $errors->first('Company_Resistation_No') }}
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <input type="date" placeholder="Date of incorporation of bussiness" name="date_bussiness" id="date_7" class="form-control date-picker">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <input type="date" placeholder="Date of commencement of bussiness" name="date_commencement" id="date_71" class="form-control date-picker">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="Pincode" name="pincode1" id="pincode1" class="form-control"  onkeypress="return event.charCode >= 48 && event.charCode <= 57"  maxlength="6" minlength="6">
                                                        @if($errors->has('pincode1'))      
                                                    <span class="error-cls" role="alert" >
                                                        {{ $errors->first('pincode1') }}
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <input type="text" placeholder="Pan No." name="pan_no1" id="pan_no" class="form-control"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10" minlength="10">
                                                    @if($errors->has('pan_no1'))      
                                                    <span class="error-cls" role="alert" >
                                                        {{ $errors->first('pan_no1') }}
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-sm-12 form-group">
                                                    <textarea placeholder="Address" name="address" id="address_7" class="form-control" rows="3"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 label_small_block">
                                                <div class="form-group">
                                                    <label class="col-sm-12 control-label">Upload Signature: (Upload Only .JPG,.PNG ,.jpeg Format)</label>
                                                    <div class="col-sm-12 form-group">
                                                        <div class="form-control">
                                                            <input type="file" name="file_signature" id="file_signature">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 label_small_block">
                                                <div class="form-group  ">
                                                    <label class="col-sm-12 control-label">Upload Photo:(Upload Only .JPG,.PNG ,.jpeg Format Only)</label>
                                                    <div class="col-sm-12 form-group">
                                                        <div class="form-control">
                                                            <input type="file" name="file_photo" id="file_photo">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 label_small_block">
                                                <div class="form-group  ">
                                                    <label class="col-sm-12 control-label">Upload Document: Upload Document in .PDF,.WORD Format Only</label>
                                                    <div class="col-sm-12 form-group">
                                                        <div class="form-control">
                                                            <input type="file" name="file_doc" id="file_doc_7">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 label_small_block">
                                                <div class="form-group">
                                                    <label class="col-sm-12 control-label">Please enter the verication code shown below.<span>*</span></label>
                                                     <div id="captcha-wrap" name="captcha_wrap" class="varify-cls">
                                                        <div class="g-recaptcha" name="g-recaptcha1" data-sitekey="6LfaCWMUAAAAAHf7Q6V58Ec5vyU-Iui_o1YYsy3Z"></div>
                                                            @if($errors->has('g-recaptcha1-response'))       
                                                            <span class="error-cls" role="alert" >
                                                            <br>
                                                            {{ $errors->first('g-recaptcha1-response') }}
                                                            </span>
                                                            @endif
                                                            </div>
                                                    <br>
                                                   
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 ">
                                                <div class="col-sm-12 set-submit-btn">
                                                    <button  class="btn btn-primary set-buttan">SUBMIT</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>  {{--....body pannel div....--}}
                                </div>{{--....risk div close....--}}
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                             <div class="set-indi-nonindi text-center" data-aos="fade-left" data-aos-duration="2000">
                                <div class="col-md-4  col-12">
                                    <div class="text-box">
                                        <a href="{{url('/download_individual')}}" > <h4>FORM FOR INDIVIDUAL</h4>
                                            <center><img src="https://www.finetechresearch.com/assetsweb/img/down.jpg" class="img-responsive down"></center></a></div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="text-box">
                                        <a href="{{url('/download_non_individual')}}"> <h4>FORM FOR NON-INDIVIDUAL</h4>
                                            <center><img src="https://www.finetechresearch.com/assetsweb/img/down.jpg" class="img-responsive down"></center></a></div>
                                </div>
                             </div>
                        </div>
                        <div id="menu3" class="tab-pane fade" >
                            <div role="tabpanel" class="tab-pane active" id="Section4" data-aos="fade-left" data-aos-duration="2000">
                     <!--                 @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                            </div>
                        @endif -->
                                <form role="form" action="{{url('/upload_kyc_form')}}" method="post" name="kycupload" enctype="multipart/form-data" id="kycupload" multiple>@csrf   
                                    <div class="form-group">
                                        <label for="exampleInputText1">Full Name&nbsp;:</label>
                                        <input name="name" class="form-control" placeholder="Your Name" id="myname" type="text">
                                        <span id="u_name" class="text-dangar"></span>
                                        @if ($errors->has('name'))
                                                    <span class="error-cls" role="alert" >
                                                    {{ $errors->first('name') }}
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputText2">Mobile Number&nbsp;:</label>
                                        <input name="mobile" class="form-control" id="umobile" placeholder="Mobile Number" type="text" minlength="11" maxlength="11">
                                        <span id="u_mobile" class="text-dangar"></span>
                                        @if ($errors->has('mobile'))
                                                    <span class="error-cls" role="alert" >
                                                    {{ $errors->first('mobile') }}
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address&nbsp;:</label>
                                        <input name="user_email" id="uemail" class="form-control" placeholder="Email address" type="text">
                                        <span id="u_email" class="text-dangar"></span>
                                        @if ($errors->has('user_email'))
                                                    <span class="error-cls" role="alert" >
                                                    {{ $errors->first('user_email') }}
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group ">
                                        <label for="exampleInputText1">Form Type&nbsp;:</label><br>
                                        <select name="utype" class="form-control" id="utype">
                                            <option value="">--Select--</option>
                                            <option value="Non-Individual">Non-Individual</option>
                                            <option value="Individual">Individual</option>
                                        </select>
                                         @if ($errors->has('utype'))
                                                    <span class="error-cls" role="alert" >
                                                    {{ $errors->first('utype') }}
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Upload Kyc Form(Only .xlsx ,.xls or .pdf Format)&nbsp;:</label>
                                        <input name="uploadfile" id="kycuploadfile" type="file">
                                        <span class="text-danger"></span>
                                        <span id="u_kyc" class="text-dangar"></span>
                                        @if ($errors->has('uploadfile'))
                                                    <span class="error-cls" role="alert" >
                                                    {{ $errors->first('uploadfile') }}
                                                    </span>
                                        @endif
                                    </div>
                                      <div class="form-group">
                                        <label class="exampleInputFile">Please enter the verication code shown below.<span>*</span></label>
                                               <!--  <div id="captcha-wrap" name="captcha_wrap" class="varify-cls"> -->
                                            <div class="g-recaptcha" name="g-recaptcha2" data-sitekey="6LfaCWMUAAAAAHf7Q6V58Ec5vyU-Iui_o1YYsy3Z"></div>
                                            @if($errors->has('g-recaptcha2-response'))       
                                                <span class="error-cls" role="alert" >
                                                    <br>
                                                        {{ $errors->first('g-recaptcha2-response') }}
                                                </span>
                                            @endif
                                                   <!--  </div> -->
                                                <br>
                                            <br>
                                    </div>
                                    <button type="submit" class="btn btn-primary set-buttan">SUBMIT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {{--<div class="col-md-8 aos-init" data-aos="fade-left" data-aos-duration="2000">
                    <div class="risk-warning  ">
                        <div class="collection-butaan" >
                            <a href="{{ url('#') }}" target="_blank" class="btn_welcome button--aylen m-r-10 set-f-right dropdown-item">FORM FOR INDIVIDUAL</a>
                            <a href="{{  URL::asset('assets/home/Risk_Profile.pdf')  }}" target="_blank" class="btn_welcome button--aylen m-r-10 set-f-right dropdown-item">FORM FOR NON-INDIVIDUAL</a>
                            <a href="{{  url('../assets/kyc_form/kyc_individual.pdf')  }}" class="btn_welcome button--aylen m-r-10 set-f-right button-set" download>DOWNLOAD KYC FORM</a>
                            <a href="{{  url('../assets/kyc_form/kyc_non_individual.pdf')  }}" class="btn_welcome button--aylen m-r-10 set-f-right button-set" download>UPLOAD KYC FORM</a>
                        </div>
                    </div>
                </div>--}}
            </div>
    </section>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script>
  $("#kycupload").validate({
    rules:
    {
        name:
        {
            required:true
        },
        user_email:
        {
            required:true,
            email:true,
            maxlength:50
        },
        mobile: 'customphone',
        utype:
        {
            required:true
        },
        uploadfile:
        {
            required:true,
            extension:"xlsx|xls|pdf"
        }
    }
  });
$.validator.addMethod('customphone', 
   function (value) 
   {
       if((value.substring(0, 2)==07)||(value.substring(0, 2)==08)||(value.substring(0, 2)==09)||(value.substring(0, 2)==06))
       {
            if(value.length == 11)
                return true;
            else
                return false;
       }
       else
           return false;

       return false;
   },"Your Mobile Numbers shoud be start '06','07','08'or'09' digits. eg- 07xxxxxxxxx<br/>");
</script>
<script>
  $("#Individual").validate({
    rules:
    {
        email:
        {
            required:true,
            email:true,
            maxlength:50
        },
        Contact_No: 'customphone',
        gender:
        {
            required:true
        },
        state:
        {
            required:true,
            maxlength:50
        },
        married_status:
        {
            required:true
        },
        resident_status:
        {
            required:true
        },
        dob:
        {
            required:true  
        },
        pincode:
        {
            required:true,
            maxlength:6  
        },
        pan_no:
        {
            required:true,
            maxlength:10
        },
        adhar:
        {
            required:true,
            maxlength:18
        },
        address:
        {
            required:true,
            maxlength:155
        },
        file_signature: 
        {
            required: true,
            extension: "jpg|jpeg|png|JPEG|JPG|PNG"
        },
        file_photo:
        {
            required:true,
            extension: "jpg|jpeg|png|JPEG|JPG|PNG"
        },
        file_doc:
        { 
          required:true,
           extension: "pdf|PDF|DOC|doc"
        }
    }
  });
$.validator.addMethod('customphone', 
   function (value) 
   {
       if((value.substring(0, 2)==07)||(value.substring(0, 2)==08)||(value.substring(0, 2)==09)||(value.substring(0, 2)==06))
       {
            if(value.length == 11)
                return true;
            else
                return false;
       }
       else
           return false;

       return false;
   },"Your Mobile Numbers shoud be start '06','07','08'or'09' digits. eg- 07xxxxxxxxx<br/>");
</script>
<script>
  $("#quick_inquiry").validate({
    rules:
    {
        email1:
        {
            required:true,
            email:true,
            maxlength:50
        },
        Contact_No1: 'customphone1',
        resident_status:
        {
            required:true
        },
        city:
        {
            required:true,
            maxlength:25
        },
        state1:
        {
            required:true,
            maxlength:25
        },
        Company_Resistation_No:
        {
            required:true,
            maxlength:15
        },
        date_bussiness:
        {
            required:true  
        },
        date_commencement:
        {
            required:true
        },
        pincode1:
        {
            required:true,
            maxlength:6
        },
        pan_no1:
        {
            required:true,
            maxlength:10
        },
        address:
        {
            required:true,
            maxlength:155
        },
        file_signature: 
        {
            required: true,
            extension: "jpg|jpeg|png|JPEG|JPG|PNG"
        },
        file_photo:
        {
            required:true,
            extension: "jpg|jpeg|png|JPEG|JPG|PNG"
        },
        file_doc:
        { 
          required:true,
           extension: "pdf|PDF|DOC|doc"
        }
    }
  });
$.validator.addMethod('customphone1', 
   function (value) 
   {
       if((value.substring(0, 2)==07)||(value.substring(0, 2)==08)||(value.substring(0, 2)==09)||(value.substring(0, 2)==06))
       {
            if(value.length == 11)
                return true;
            else
                return false;
       }
       else
           return false;

       return false;
   },"Your Mobile Numbers shoud be start '06','07','08'or'09' digits. eg- 07xxxxxxxxx<br/>");
</script>
@endsection