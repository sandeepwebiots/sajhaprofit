@extends('layouts.home.master') 

@section('title') Service Details @endsection
@section('style')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

@section('content')

<!-- Home Design Inner Pages -->

<div class="ulockd-inner-home-service">
    <div class="container-fluid text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <div class="banner-bg">
                        <img src="{{url('assets/home/images/team/past.jpg')}}" alt="services">
                    <div class="center-content">
                        <div>
                        <h1 class="text-uppercase">{{ $subservice->name }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer">
                    <ul class="list-inline ulockd-icd-sub-menu">
                        <li><a href="#"> HOME </a></li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                        <li> <a href="#"> SERVICE </a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Our Feature Project -->
<section id="project" class="our-project ulockd_bgp1">
    <div class="container-fluid">
        <div class="row">
            <!-- 	<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-main-title">
						<h2>SERVICE</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
					</div>
				</div> -->
        </div>
        <div class="row">
            <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            <div class="col-md-8">
                <div class="profit-service-lable text-center">
                    <h5>{{ $subservice->name }}</h5>
                </div>
            </div>
            <div class="col-md-8">
                <div class="profit-service-box">
                    <div class="row">

                        <div class="col-md-4">
                            <img src="{{url('assets/home/images/service/')}}/{{ $subservice->image }}" class="img-fluid">
                        </div>

                    </div>
                    <div class="col-md-8">
                        <div class="profit-service-details">
                            <ul>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> Profit {{ $subservice->profit }} /-</li>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> Capital Requirement {{ $subservice->capital }} /-</li>
                                <li><i class="fa fa-hand-o-right" aria-hidden="true"></i> Account Management Facility Available</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="service-details">
                    <img src="{{url('assets/home/images/service/icon-1.png')}}">
                    <div class="service-title">
                        <h5>SERVICES DETAILS</h5>
                    </div>
                    <div class="service-point">
                        
                            {!! $subservice->service_details !!}
                        
                    </div>
                </div>
                <!-- SAMPLE CALL -->
                <div class="service-details">
                    <img src="{{url('assets/home/images/service/icon-2.png')}}">
                    <div class="service-title">
                        <h5>SAMPLE CALL</h5>
                    </div>
                    <div class="service-point">
                        {!! $subservice->sample_call !!}
                    </div>
                </div>
                <!-- UPDATE -->
                <div class="service-details">
                    <img src="{{url('assets/home/images/service/icon-3.png')}}">
                    <div class="service-title">
                        <h5>UPDATE</h5>
                    </div>
                    <div class="service-point">
                        {!! $subservice->updates !!}
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>

@endsection