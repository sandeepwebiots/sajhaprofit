@extends('layouts.home.master')
@section('title') FAQ | Sajhaprofit @endsection
@section('content')



<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home">
    <div class="container-fluid text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <div class="banner-bg">
                        <img src="{{url('assets/home/images/team/faq.jpg')}}" alt="faq" class="img-responsive">
                    <div class="center-content">
                        <h1 class="text-uppercase">FAQ'S</h1>
                        <div class="typed_text_bg">
                            <div id="typed-strings" style="display: none;">
                                <p>The important  thing is not to stop questioning.</p><p>Without a good question, a good answer has no place to go.</p><p><span class="align-setting"><strong class="always">A</strong>-Always<br><strong class="always S-seting">S</strong>-Seek<br><strong class="always">K</strong>-Knowledge</span></p>
                            </div>
                            <span id="typed" class="typed_article_text" style="white-space:pre;"><span class="typed-cursor" style="animation-iteration-count: 0;">|</span></span>
                        </div>
                    </div>
                </div>
           </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer">
                    <ul class="list-inline ulockd-icd-sub-menu">
                        <li><a href="#"> HOME </a></li>
                        <li> <i class="fa fa-chevron-right" aria-hidden="true"></i></li>
                        <li> <a href="#"> FAQ </a> </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Our About Page faq Section -->
<section class="ulockd-ap-faq">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            
            <div class=" col-md-8" data-aos="fade-left" data-aos-duration="2000">
                <div class="ulockd-faq-box">
                    <div class="ulockd-faq-title clearfix">
                        <h2>Frequently Asked <span class="text-thm3">Questions</span></h2>
                    </div>
                    <div class="ulockd-faq-content">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            @foreach($faqs as $faq)
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading{{ $faq->id }}">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $faq->id }}"
                                            aria-expanded="false" aria-controls="collapse{{ $faq->id }}">
                                            <i class="icon-Down-2 icon-1"></i>
                                            <i class="icon-Right-2 icon-2"></i>
                                             {{ $faq->quotation }}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{ $faq->id }}" class="panel-collapse collapse " role="tabpanel"
                                     aria-labelledby="heading{{ $faq->id }}">
                                    <div class="panel-body">
                                        <p>{{ $faq->answer }}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Our Partner -->

<!-- <section class="parallax ulockd_bgi4 bgc-overlay-white75 " data-stellar-background-ratio="0.3"  data-aos="fade-right" data-aos-duration="2000">
    <div class="container">
        
    </div>
</section> -->

@endsection
@section('script')
    <script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}">
    </script>

@endsection
