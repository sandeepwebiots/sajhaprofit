@extends('layouts.home.master')

@section('title') Services @endsection

@section('content')

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home-service">
    <div class="container text-center">
        <div class="row">
            <div class="inner-conraimer-details">
                <div class="col-md-12">
                    <div class="center-content">
                    <h1 class="text-uppercase">Service</h1>
                    <div class="typed_text_bg">
                            <div id="typed-strings">
                                <p>You deserve</p>
                                <p>complete financial advice</p>
                                <p>The only acceptable alternative </p> <p>if you want a plan to live well,<br> and on your terms.</p>
                            </div>
                            <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                        </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="ulockd-icd-layer">
                    <ul class="list-inline ulockd-icd-sub-menu">
                        <li><a href="#"> HOME </a></li>
                        <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                        <li> <a href="#"> SERVICE </a> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Our Feature Project -->
<section id="project" class="our-project ulockd_bgp1">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center"  data-aos="zoom-out" data-aos-duration="2000">
                <div class="ulockd-main-title">
                    <h2>SERVICE</h2>
                    <p>Perfection is not attaina but if we chase perfection we can catch Excellence. Believe you can and you’re halfway there.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            
            <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                <div class="row">
                    <div class="rb_content ulockd-bgthm">
                        <h3 class="title text-center">{{ $service_name }}</h3>
                    </div>
                    @foreach($subservice as $sub_serve)
                    <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-4">
                        <div class="recent_box">
                            <a href="{{ url('service-detial') }}/{{ $sub_serve->id }}">
                                <div class="rb_img">
                                    <img class="img-responsive" src="{{url('assets/home/images/service/')}}/{{ $sub_serve->image }}"
                                        alt="1.jpg">
                                    <div class="over-layer-inside"> <h4 style="text-transform: uppercase;">{{ $sub_serve->name }}</h4> </div>
                                    <div class="over-layer">
                                    	<div>
                                        <p><i class="fa fa-chevron-right" aria-hidden="true"></i> Rs {{
                                            $sub_serve->profit }} Profit Service</p>
                                        <p><i class="fa fa-chevron-right" aria-hidden="true"></i> {{
                                            $sub_serve->capital }} Capital Requirement</p>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}"></script>
@endsection