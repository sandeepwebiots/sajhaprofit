@extends('layouts.home.master')
@section('title') CRM @endsection


@section('style')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style type="text/css">
    .show-disp.active.show , .show-disp.active.show.in {
        display: block !important;
    }
</style>

@endsection

@section('content')

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home">
        <div class="container text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <h1 class="text-uppercase">Technical Analysis</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu">
                            <li><a href="#"> HOME </a></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                            <li> <a href="#"> CHART </a> </li>
                           
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>





<div class="container ulockd-mrgn1250">

    <!-- tab -->
<div class="anyalysis-tab">
<div class="col-md-12 text-center">
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item active">
    <a class="nav-link active show-disp active show" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Stock</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Commodity</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <!-- tab-1 -->

  <div class="tab-pane fade show-disp active show in" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
    <div class="analysis-table">

        <div class="container ulockd-mrgn1250">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Daily Trend</th>
                        <th>Weekly Trend</th>
                        <th>Monthly Trend</th>
                        <th>Details date</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Nifty</td>
                        <td>Buy</td>
                        <td>Sell</td>
                        <td>Buy</td>
                        <td>Detailed Analysis</td>
                        
                    </tr>
                    <tr>
                        <td>Nifty Bank</td>
                        <td>Buy</td>
                        <td>Sell</td>
                        <td>Buy</td>
                        <td>Detailed Analysis</td>
                    </tr>
                    <tr>
                        <td>Nifty IT</td>
                        <td>Sell</td>
                        <td>Sell</td>
                        <td>Buy</td>
                        <td>Detailed Analysis</td>
                    </tr>
                    <tr>
                        <td>Nifty Pharma</td>
                        <td>Buy</td>
                        <td>Sell</td>
                        <td>Buy</td>
                        <td>Detailed Analysis</td>
                    </tr>
                    <tr>
                        <td>Nifty FMCG</td>
                        <td>Buy</td>
                        <td>Sell</td>
                        <td>Buy</td>
                        <td>Detailed Analysis</td>
                    </tr>
                    <tr>
                        <td>Nifty Energy</td>
                        <td>Buy</td>
                        <td>Sell</td>
                        <td>Buy</td>
                        <td>Detailed Analysis</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                     <td>Nifty Auto</td>
                        <td>Buy</td>
                        <td>Sell</td>
                        <td>Buy</td>
                        <td>Detailed Analysis</td>
                    </tr>
                </tfoot>
            </table>
            </div>
        </div>
  </div>
  <!-- end tab-1 -->
  <!-- tab-2 -->
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
     <div class="analysis-table">
<div class="container ulockd-mrgn1250">
<table id="example1" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th></th>
                <th>Daily Trend</th>
                <th>Weekly Trend</th>
                <th>Monthly Trend</th>
                <th>Details date</th>
                
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Nifty</td>
                <td>Buy</td>
                <td>Sell</td>
                <td>Buy</td>
                <td>Detailed Analysis</td>
                
            </tr>
            <tr>
                <td>Nifty Bank</td>
                <td>Buy</td>
                <td>Sell</td>
                <td>Buy</td>
                <td>Detailed Analysis</td>
            </tr>
            <tr>
                <td>Nifty IT</td>
                <td>Sell</td>
                <td>Sell</td>
                <td>Buy</td>
                <td>Detailed Analysis</td>
            </tr>
            <tr>
                <td>Nifty Pharma</td>
                <td>Buy</td>
                <td>Sell</td>
                <td>Buy</td>
                <td>Detailed Analysis</td>
            </tr>
            <tr>
                <td>Nifty FMCG</td>
                <td>Buy</td>
                <td>Sell</td>
                <td>Buy</td>
                <td>Detailed Analysis</td>
            </tr>
            <tr>
                <td>Nifty Energy</td>
                <td>Buy</td>
                <td>Sell</td>
                <td>Buy</td>
                <td>Detailed Analysis</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
             <td>Nifty Auto</td>
                <td>Buy</td>
                <td>Sell</td>
                <td>Buy</td>
                <td>Detailed Analysis</td>
            </tr>
        </tfoot>
    </table>
    </div>
</div>

  </div>
  <!-- end tab-2 -->
  
</div>
</div>
</div>
<!-- end end -->

   
</div>

@endsection


@section('script')

 <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
    $('#example').DataTable();
    $('#example1').DataTable();
} );

</script>
@endsection