@extends('layouts.home.master')

@section('title') Pay | CRM @endsection

@section('style')
<style type="text/css">
	.form-group {
	    padding-bottom: 40px;
	}
	.btn-payment {
		width: 100%;
    	display: inline-block;
	}
	.td.text-center.bg-white {
	    text-transform: capitalize;
	}
    .table tr td{
        text-transform: capitalize !important;
    }
    a.btn-payment {
        color: #fff9f9;

    }
</style>
@endsection

@section('content')


<!-- Home Design Inner Pages -->

<div class="ulockd-inner-price">
        <div class="container-fluid text-center  set-fluid">
            <div class="row">
                <div class="inner-conraimer-details">

                    <div class="col-md-12 pr-left-right">
                        <div class="banner-bg">
                            <img src="{{url('assets/home/images/team/payment.jpg')}}" alt="payment.jpg" class="img-responsive">
                        <div class="center-contents-pricing">
                            <h1 class="text-uppercase">E-payment</h1>
                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="ulockd-icd-layer">
					<ul class="list-inline ulockd-icd-sub-menu">
						<li><a href="#"> HOME </a></li>
						<li><i class="fa fa-chevron-right" aria-hidden="true"></i></li>
						<li> <a href="#"> E-Payment </a> </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Our Pricing Table -->
<!-- Our Pricing Table -->

<section class="invoice_section">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 text-center">
                <div class="ulockd-main-title">
                    <h2>Payment</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 text-center" data-aos="zoom-out" data-aos-duration="2000">
                <div class="container">
                    <div class="card">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                            <div class="table-responsive-md">
                                <table class="table table-striped">
                                    <tbody class="table_body_modify">
                                    <tr>
                                        <td  colspan="12" class="center border-bottom-set heading_1">TAX INVOICE</td>
                                    </tr>
                                    <tr>
                                        <td  colspan="12" class="center heading_2">SajhaProfit Investment Advisors</td>
                                    </tr>
                                    <tr>
                                        <td  colspan="12" class="center bold_title">Address: {{ $company_detail->address }}</td>
                                    </tr>
                                    <tr>
                                        <td  colspan="12" class="center bold_title">Mb:- {{ $company_detail->phone_no }}, Email: {{ $company_detail->mail_info }}</td>
                                    </tr>
                                    <tr>
                                        <td
                                                colspan="12" class="center heading_3">CIN: {{ $company_detail->cin }}<br>
                                            GST No: {{ $company_detail->gst_no }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="w25 td_heading_bold">Bill to</td>
                                        <td class="td_heading_bold" colspan="9">Place of Supply</td>
                                        <td class="td_heading_bold" colspan="1" rowspan="2">INVOICE NO : {{ $invoice_year }}{{ str_pad($invoice_no,6,'0',STR_PAD_LEFT) }} </td>
                                        <td class="td_heading_bold" rowspan="2">Dated <br> {{ $payment->created_at->format('d-m-Y') }}</td>
                                    </tr>
                                    <tr>
                                        <td class="td_heading_bold" rowspan="3">Name:<br><br>Address:</td>
                                        <td colspan="9">{{ $user->user_name }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="9" rowspan="2">Address: {{ $user->address }},{{ $user->city }},{{ $user->state }},{{ $user->zip_code }} </td>
                                        <td colspan="" rowspan="2"></td>
                                        <td colspan="" rowspan="2"></td>
                                    </tr>
                                    <tr></tr>
                                    <tr>
                                        <td class="td_heading_bold">Description of Service </td>
                                        <td class="td_heading_bold" colspan="9" rowspan="2">SAC CODE
                                            9971</td>
                                        <td class="td_heading_bold" colspan="3" rowspan="2">Amount</td>
                                    </tr>
                                    <tr></tr>
                                    <tr>
                                        <td class="td_heading_bold">CONSULATNCY SERVICES</td>
                                        <td colspan="9" rowspan="7">{{ $payment->pack }}</td>
                                        <td colspan="3" rowspan="7">₹ {{ $payment->pack_amount }}</td>
                                    </tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr>
                                        <td colspan="12">TAXABLE VALUE</td>
                                    </tr>
                                    <tr>
                                        <td class="td_heading_bold"> IGST {{ $gst }}%</td>
                                        <td colspan="9" rowspan=""></td>
                                        @if($user->state == 'GUJARAT')
                                        <td colspan="3" rowspan=""></td>
                                        @else
                                        <td colspan="3" rowspan="">₹ {{ $payment->gst_amount }}</td>
                                        @endif
                                    </tr>
                                    @php $per_gst = $gst / 2; @endphp
                                    <tr>
                                        <td class="td_heading_bold"> CGST {{ $per_gst }}%</td>
                                        <td colspan="9" rowspan=""></td>
                                        @if($user->state == 'GUJARAT')
                                        <td colspan="3" rowspan="">₹ {{ $cgst_amount }}</td>
                                        @else
                                        <td colspan="3" rowspan=""></td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td class="td_heading_bold">SGST {{ $per_gst }}%</td>
                                        <td colspan="9" rowspan=""></td>
                                        @if($user->state == 'GUJARAT')
                                        <td colspan="3" rowspan="">₹ {{ $cgst_amount }}</td>
                                        @else
                                        <td colspan="3" rowspan=""></td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td class="td_heading_bold">Total</td>
                                        <td colspan="9"></td>
                                        <td colspan="2">₹ {{ $payment->total_amount }}</td>
                                    </tr>
                                    <tr></tr>
                                    <tr>
                                        <td class="td_heading_bold">Amount Chargeable (in words)</td>
                                    </tr>
                                    <tr>
                                        <td colspan="12" class="td_heading_bold">For SajhaProfit Investment Advisors</td>
                                    </tr>
                                    <tr>
                                        <td class="td_heading_bold">Company's PAN : {{ $company_detail->pan_no }}</td>
                                        <td colspan="4"></td>
                                        <td colspan="3"></td>
                                        <td colspan="3"></td>
                                        <td colspan=""></td>
                                    </tr>
                                    <tr>
                                        <td class="td_heading_bold"><b>Note:</b> Valid subject to realization. </td>
                                        <td colspan="4"></td>
                                        <td colspan="3"></td>
                                        <td colspan="6" rowspan="6" class="td_heading_bold">Authorized Signatory:<img src="{{url('assets/home/images/signature.png')}}" alt="signature_img" class="img-fluid" height="100px" width="100px"></td>
                                    </tr>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr>
                                        <td class="td_heading_bold" rowspan="2">"SajhaProfit Investment Advisors"</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    <div class="container">
        

    <div class="row m-b-70">
        <div class="col-lg-6 col-12">
            <div>
                <h3 class="set-border">Package Detail!!</h3>
                <div class="row">
                    <table class="table">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th class="text-center">Package</th>
                                <th class="text-center">Package Amount</th>
                                <th class="text-center">GST Amount</th>
                                <th class="text-center">Total Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center bg-white">{{ $payment->pack }}</td>
                                <td class="text-center bg-white">₹ {{ $payment->pack_amount }}</td>
                                <td class="text-center bg-white">₹ {{ $payment->gst_amount }}</td>
                                <td class="text-center bg-white">₹ {{ $payment->total_amount }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table">
                        <thead class="bg-dark text-white">
                            <tr>
                                <th class="text-white text-center" colspan="2"><a href="{{ url('pay-now') }}/{{ $payment->id }}" class="btn-payment"> Amount Payable</a></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection