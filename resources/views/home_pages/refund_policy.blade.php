@extends('layouts.home.master')

@section('title')  Refund Policy | Sajhaprofit @endsection

@section('content')
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home-explore">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <div class="banner-bg">
                            <img src="{{url('assets/home/images/background/refund.jpg')}}" alt="explorer">
                        <div class="center-content">
                            <h1 class="text-uppercase"> REFUND POLICY</h1>
                            <div class="typed_text_bg">
                                <div id="typed-strings" style="display: none;">
                                    <p>You can never cross the ocean until you have</br>The courage to lose the sight of shore.</p>
                                </div>
                                <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu text-uppercase">
                            <li><a href="#"> HOME </a></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                            <li> <a href="#"> Refund Policy </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Our Feature Project -->
    <section id="project" class="our-project ulockd_bgp1">
        <div class="container-fluid">
            <div class="row">
                <!-- 	<div class="col-md-6 col-md-offset-3 text-center">
                        <div class="ulockd-main-title">
                            <h2>Explore</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
                        </div>
                    </div> -->
            </div>
            <div class="row">
                <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="profit-service-lable">
                        <h5 class="text-center">Refund Policy</h5>
                    </div>
                </div>
                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="re-heading   ">
                        <h4> *** REFUND POLICY  ***</h4>
                        <p>At Sajha Profit ,  we value our customers and are committed to provide the best services. Our clients should note we do not offer a 100% guarantee on our calls and hence cannot offer any refund on subscription.</p>
                        <p>Once a client has subscribed to service and payment has been made for the same, the service cannot be canceled & there will be no refund. Due to some unforeseen reasons, if the client is not satisfied with our services, they may seek our help in directing them on future calls. We will always put our best effort to increase the satisfaction levels in such cases.</p>
                    </div>
                </div>

                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="re-heading">
                        <h4> We strongly recommend that before making a payment all visitors and potential clients should:</h4>

                    </div>
                </div>

                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="refundpolicy-list">
                        <ul class="refund_policy_listing">
                            <li>Read all information about our company, products & services.</li>
                            <li>Visit all sections on our website.</li>
                            <li>Carefully read terms & conditions.</li>
                            <li>Read our Privacy Policy and Refund Policy.</li>
                            <li>Kindly make the payment after reading all terms and conditions, disclaimers and refund policy.</li>
                            <li>There is no refund policy in any case.</li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="re-heading">
                        <h4>Important</h4>
                        <p>We strongly recommend do not authorize any family member or friend to access your credit cards, debit cards or your account at the payment site. Once the payment is made for membership it is considered that you have read and agree to the terms & conditions of Sajha Profit & Refund Policy.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}">
    </script>

@endsection