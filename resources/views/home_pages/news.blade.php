@extends('layouts.home.master') 

@section('title') News | Sajhaprofit @endsection 

@section('style') 

@endsection 

@section('content')

<div class="wrapper">
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-news">
        <div class="container text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <div class="center-content">
                        <h1 class="text-uppercase">news</h1>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu">
                            <li><a href="#"> HOME </a></li>
                            <li><a href="#"> <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                            <li> <a href="#"> NEWS </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center" data-aos="zoom-out" data-aos-duration="2000">
                    <div class="ulockd-main-title">
                        <h2>Latest <span class="text-thm3">News</span></h2>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p> -->
                    </div>
                </div>
                @foreach($news as $new)
                <div class="col-md-12" data-aos="fade-right" data-aos-duration="2000">
                    <div class="example-1 card">
                        <div class="wrapper-1">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="news-page">
                                        <img class="img-responsive img-whp" src="{{url('assets/dashboard/upload/news')}}/{{ $new->img }}" alt="news.jpg">
                                    </div>
                                </div>
                                <div class="date">
                                    <span class="day">{{ $new->created_at->format('d') }}</span>
                                    <span class="month">{{ $new->created_at->format('M') }}</span>
                                    <span class="year">{{ $new->created_at->format('y') }}</span>
                                </div>
                                <div class="col-md-8">
                                    <div class="">

                                        <!-- <span class="author">{{ $new->sub_title }}</span> -->
                                        <h4 class="title"><a href="{{ url('news-detail') }}/{{ $new->id }}">{{ $new->title }}</a></h4>
                                        <h5 class="author">{{ $new->sub_title }}</h5>
                                        <p class="text">{{ str_limit($new['content'],150) }} <a href="{{ url('news-detail') }}/{{ $new->id }}" class="text-primary">Read more</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="text-center center-block">
                  <ul class="pagination">
                     <li class="active">{{ $news->links() }}</li>
                  </ul>
                </div>
              </div>
        </div>
    </section>
</div>
@endsection