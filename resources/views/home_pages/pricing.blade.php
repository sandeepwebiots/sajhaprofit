@extends('layouts.home.master')

@section('title') Pricing | Sajhaprofit @endsection

@section('content')
	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-price">
		<div class="container-fluid text-center  set-fluid">
			<div class="row">
				<div class="inner-conraimer-details">

					<div class="col-md-12 pr-left-right">
						<div class="banner-bg">
							<img src="{{url('assets/home/images/team/payment.jpg')}}" alt="payment.jpg" class="img-responsive">
						<div class="center-contents-pricing">
							<h1 class="text-uppercase">PRICING</h1>
							<div class="typed_text_bg">
								<div id="typed-strings" style="display: none;">
									<p>No price is too low for a bear or too high for a bull</p>
									<p>No matter how brilliant your mind or strategy,<br>if you're playing a solo game, you'll always lose out to a team.</p>
									<p>With us get guidance to navigate through life’s<br>challenges and toward financial success.</p>
								</div>
								<span id="typed" class="typed_article_text" style="white-space:pre;"></span>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-page">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="ulockd-icd-layer">
						<ul class="list-inline ulockd-icd-sub-menu">
							<li><a href="#"> HOME </a></li>
							<li><i class="fa fa-chevron-right" aria-hidden="true"></i></li>
							<li> <a href="#"> PRICING </a> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Our Pricing Table -->
	<!-- Our Pricing Table -->
	<section class="ulockd-pricing">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>



				<div class="col-md-8" data-aos="zoom-out" data-aos-duration="2000">
					<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 text-center">
						<div class="ulockd-main-title">
							<h2>Our <span class="text-thm3">Pricing</span></h2>
							{{-- <p>We provides all traders & investors various services according to their investments & requirements. Here we are specify all services subscription with its Monthly, Quaterly & Yearly charges .</p> --}}
						</div>
					</div>
				</div>
				<div class=" col-md-8" data-aos="fade-left" data-aos-duration="2000">
					@foreach($service as $serve)
						@if($serve->sub_service->count() > 0)
							@foreach($serve->sub_service as $subserve)
								@if($subserve->package)
									<div data-aos="fade-left" data-aos-duration="2000">
										<h3 class="set-border">{{ $serve->name }}</h3>
									</div>
									@break
								@endif
							@endforeach


							<div class="row m-b-70" data-aos="fade-left" data-aos-duration="2000">
								@foreach($serve->sub_service as $subserve)
									@if($subserve->package)
										<div class="col-lg-4 col-sm-6 col-12 " data-aos="fade-left" data-aos-duration="2000">
											<h4 class="title color-blue">{{ $subserve->name }}</h4>
											<table class="table">
												<!--  <thead class="bg-primary text-white">
                                                     <tr>
                                                         <th>Monthly</th>
                                                         <th>Quaterly</th>
                                                         <th>Half Yearly</th>
                                                         <th>Yearly</th>
                                                         <th>***</th>
                                                     </tr>
                                                 </thead>
                                                 <tbody>
                                                     <tr>
                                                         <td>5000</td>
                                                         <td>13500</td>
                                                         <td>28000</td>
                                                         <td>55000</td>
                                                         <td>NA</td>
                                                     </tr>
                                                     <tr>
                                                         <td><a href="#" class="set-link">Pay</a></td>
                                                         <td><a href="#" class="set-link">Pay</a></td>
                                                         <td><a href="#" class="set-link">Pay</a></td>
                                                         <td><a href="#" class="set-link">Pay</a></td>
                                                         <td><a href="#" class="set-link">Pay</a></td>
                                                     </tr>
                                                 </tbody> -->
												<tr class="bg-primary text-white">
													<th>Package</th>
													<td>Price</td>
													<td>Payment</td>
												</tr>
												@if($subserve->package->monthly_checked == 1)
													<tr>
														<th>Monthly</th>
														<td>{{$subserve->package->price_monthly}}</td>
														@if(Sentinel::check())
															<td>
																<div class="col-setting">
																	<a href="{{ url('pay') }}/{{  $subserve->package->id  }}/monthly" class="btn_pay  button--aylen"> Pay </a>
																</div>
															</td>
														@else
															<td>
																<div class="col-setting">
																	<a href="{{ url('pay') }}/{{  $subserve->package->id  }}/monthly" class="btn_pay  button--aylen"> Pay </a>
																</div>
															</td>
														@endif
													</tr>
												@endif
												@if($subserve->package->quarterly_checked == 1)
													<tr>
														<th>Quarterly</th>
														<td>{{$subserve->package->price_quarterly}}</td>
														@if(Sentinel::check())
															<td>
																<div class="col-setting">
																	<a href="{{ url('pay') }}/{{  $subserve->package->id  }}/quarterly" class="btn_pay  button--aylen"> Pay </a>
																</div>
															</td>
														@else
															<td>
																<div class="col-setting">
																	<a href="{{ url('pay') }}/{{  $subserve->package->id  }}/quarterly" class="btn_pay  button--aylen"> Pay </a>
																</div>
															</td>
														@endif
													</tr>
												@endif
												@if($subserve->package->half_yearly_checked == 1)
													<tr>
														<th>Half Yearly</th>
														<td>{{$subserve->package->price_half_yearly}}</td>
														@if(Sentinel::check())
															<td>
																<div class="col-setting">
																	<a href="{{ url('pay') }}/{{  $subserve->package->id  }}/half_yearly" class=" btn_pay  button--aylen"> Pay </a>
																</div>
															</td>
														@else
															<td>
																<div class="col-setting">
																	<a href="{{ url('pay') }}/{{  $subserve->package->id  }}/half_yearly" class=" btn_pay  button--aylen"> Pay </a>
																</div>
															</td>
														@endif
													</tr>
												@endif
												@if($subserve->package->yearly_checked == 1)
													<tr>
														<th>Yearly</th>
														<td>{{$subserve->package->price_yearly}}</td>
														@if(Sentinel::check())
															<td>
																<div class="col-setting">
																	<a href="{{ url('pay') }}/{{  $subserve->package->id  }}/yearly" class="btn_pay  button--aylen"> Pay </a>
																</div>
															</td>
														@else
															<td>
																<div class="col-setting">
																	<a href="{{ url('pay') }}/{{  $subserve->package->id  }}/yearly" class="btn_pay  button--aylen"> Pay </a>
																</div>
															</td>
														@endif
													</tr>
												@endif
											</table>
										</div>
									@endif
								@endforeach
							</div>
						@endif
					@endforeach
				</div>



			</div>{{-----row div close------------}}
		</div>
	</section>

@endsection
@section('script')
	<script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}"></script>
@endsection