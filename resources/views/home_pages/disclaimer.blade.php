@extends('layouts.home.master')

@section('title') Disclaimer | Sajhaprofit @endsection

@section('style')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

@endsection

@section('content')
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home-explore">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <div class="banner-bg">
                            <img src="{{url('assets/home/images/background/DISCLAIMER2.jpg')}}" alt="explorer">
                        <div class="center-content">
                            <h1 class="text-uppercase"> DISCLAIMER</h1>
                            <div class="typed_text_bg">
                                <div id="typed-strings" style="display: none;">
                                    <p>Never depend on a single income,</br> Make investment to create a second source.</p>
                                </div>
                                <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                            </div>
                        </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu">
                            <li><a href="#"> HOME </a></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                            <li> <a href="#"> Disclaimer </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Our Feature Project -->
    <section id="project" class="our-project ulockd_bgp1">
        <div class="container-fluid">
            <div class="row">
                <!-- 	<div class="col-md-6 col-md-offset-3 text-center">
                        <div class="ulockd-main-title">
                            <h2>Explore</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
                        </div>
                    </div> -->
            </div>
            <div class="row">
                <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="profit-service-lable">
                        <h5 class="text-center"> Disclaimer</h5>
                    </div>
                </div>


                <div class="col-md-8" >
                    <div class="refundpolicy-list diclaration" data-aos="fade-left" data-aos-duration="2000">
                        <ul class="refund_policy_listing ">
                            <li>The information and views in this website & all the services we provide are believed to be reliable, but we do not accept any responsibility (or liability) for errors of fact or opinion. Users have the right to choose the product/s that suits them the most.</li>
                            <li>Investment in equity shares has its own risks. Sincere efforts have been made to present the right investment perspective. The information contained herein is based on analysis and on sources that we consider reliable. We, however, do not vouch for the consistency or the completeness thereof. This material is for personal information and we are not responsible for any loss incurred due to it & take no responsibility whatsoever for any financial profits or loss which may arise from the recommendations above.</li>
                            <li>Sajha Profit does not purport to be an invitation or an offer to buy or sell any financial instrument. Analyst or any person related to Sajha Profit might be holding positions in the stocks recommended.</li>
                            <li>Our Clients (Paid Or Unpaid), Any third party or anyone else have no rights to forward or share our calls or SMS or Reports or Any Information Provided by us to/with anyone which is received directly or indirectly by them. If found so then Serious Legal Actions can be taken.</li>
                            <li>By accessing sajhaprofit.com or any of its associate/group sites, you have read, understood and agree to be legally bound by the terms of the following disclaimer and user agreement</li>
                            <li>sajhaprofit.com has taken due care and caution in compilation of data for its web site. The views and investment tips expressed by investment experts on sajhaprofit.com are their own, and not that of the website or its management. sajhaprofit.com advises users to check with certified experts before taking any investment decision. However, sajhaprofit.com does not guarantee the consistency, adequacy or completeness of any information and is not responsible for any errors or omissions or for the results obtained from the use of such information. sajhaprofit.com especially states that it has no financial liability whatsoever to any user on account of the use of information provided on its website.</li>
                            <li>sajhaprofit.com is not responsible for any errors, omissions or representations on any of our pages or on any links on any of our pages. Sajhaprofit.com does not endorse in anyway any advertisers on our web pages. Please verify the veracity of all information on your own before undertaking any alliance.</li>
                            <li>The information on this website is updated from time to time. sajhaprofit.com however excludes any warranties (whether expressed or implied), as to the quality, consistency, efficacy, completeness, performance, fitness or any of the contents of the website, including (but not limited) to any comments, feedback and advertisements contained within the Site.</li>
                            <li>This website contains material in the form of inputs submitted by users and sajhaprofit.com accepts no responsibility for the content or consistency of such content nor does Sajhaprofit.com make any representations by virtue of the contents of this website in respect of the existence or availability of any goods and services advertised in the contributory sections. sajhaprofit.com makes no warranty that the contents of the website are free from infection by viruses or anything else which has contaminating or destructive properties and shall have no liability in respect thereof.</li>
                            <li>Part of this website contains advertising and other material submitted to us by third parties. Kindly note that those advertisers are responsible for ensuring that material submitted for inclusion on the website complies with all legal requirements. Although acceptance of advertisements on the website is subject to our terms and conditions which are available on request, we do not accept liability in respect of any advertisements.</li>
                            <li>This website will contain articles contributed by several individuals. The views are exclusively their own and do not necessarily represent the views of the website or its management. The linked sites are not under our control and we are not responsible for the contents of any linked site or any link contained in a linked site, or any changes or updates to such sites. Sajhaprofit.com is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by us of the site.</li>
                            <li>There are risks associated with utilizing internet and short messaging system (SMS) based information and research dissemination services. Subscribers are advised to understand that the services can fail due to failure of hardware, software, and Internet connection. While we ensure that the messages are delivered in time to the subscribers Mobile Network, the delivery of these messages to the customer's mobile phone/handset is the responsibility of the customer's Mobile Network. SMS may be delayed and/or not delivered to the customer's mobile phone/handset on certain days, owing to technical reasons and Sajhaprofit.com cannot be held responsible for the same.</li>
                            <li>sajhaprofit.com hereby expressly disclaims any implied warranties imputed by the laws of any jurisdiction. We consider ourselves and intend to be subject to the jurisdiction only of the courts of Indore in India. If you don't agree with any of our disclaimers above please do not read the material on any of our pages. This site is specifically for users in the territory of India. Although the access to users outside India is not denied, Sajhaprofit.com shall have no legal liabilities whatsoever in any laws of any jurisdiction other than India. We reserve the right to make changes to our site and these disclaimers, terms, and conditions at any time.</li>
                            <li>Stock trading is inherently risky and you agree to assume complete and full responsibility for the outcomes of all trading decisions that you make. All comments and posts made by sajhaprofit.com is for information purposes only and under no circumstances should be used for actual trading. Under no circumstances should any person at this site make trading decisions based solely on the information discussed herein. We are qualified financial advisor and we provide all the market based information to keep our clients informed about the real time changes happening in financial market, complete information provided by us consists of factual information which help you to understand the movement of market through the investment advice we provide. It is completely informative in nature.</li>
                            <li>sajhaprofit.com operates a real time chat room intended to provide a private forum for users to exchange information and to discuss various investing techniques. You agree, by accessing this or any associated site, Sajhaprofit.com bears no liability for any postings on the website or actions of associate site. We reserve the right to deny service to anyone. You, and not Sajhaprofit.com, assume the entire cost and risk of any trading you choose to undertake. You are solely responsible for making your own investment decisions. If you choose to engage in such transactions with or without seeking advice from a licensed and qualified financial advisor or entity, then such decision and any consequences flowing there from are your sole responsibility. The information and commentaries are not meant to be an endorsement or offering of any stock purchase. They are meant to be a guide only, which must be tempered by the investment experience and independent decision making process of the subscriber. Sajhaprofit.com or any employees are in no way liable for the use of the information by others in investing or trading in investment vehicles utilizing the principles disclosed herein. The materials and information in, and provided by, this site are not, and should not be construed as an offer to buy or sell any of the securities named in materials, services, or on-line postings as the information shown on website may have a time gap & may be later than the original releasing time of the useful information.</li>
                            <li>We encourage all investors to use the information on the site as a resource only to further their own research on all featured companies, stocks, sectors, markets and information presented on the site.</li>
                            <li>sajhaprofit.com, its management, its associate companies and/or their employees take no responsibility for the veracity, validity and the correctness of the expert recommendations or other information or research. Although we attempt to research thoroughly on information provided herein, there are no guarantees in consistency. The information presented on the site has been gathered from various sources believed to be providing correct information. Sajhaprofit.com, group, companies, associates and/or employees are not responsible for errors, inaccuracies if any in the content provided on the site.</li>
                            <li>sajhaprofit.com does not represent or endorse the consistency or reliability of any of the information, conversation, or content contained on, distributed through, or linked, downloaded or accessed from any of the services contained on this website (hereinafter, the "Service"), nor the quality of any products, information or other materials displayed, purchased, or obtained by you as a result of any other information or offer by or in connection with the Service.</li>
                            <li>sajhaprofit.com, its management, its associate companies and/or their employees take no responsibility for the veracity, validity and the correctness of the expert recommendations or other information or research. Although we attempt to research thoroughly on information provided herein, there are no guarantees in consistency. The information presented on the site has been gathered from various sources believed to be providing correct information. Sajhaprofit.com, group, companies, associates and/or employees are not responsible for errors, inaccuracies if any in the content provided on the site.</li>
                            <li>sajhaprofit.com has license to provide investment advice. No materials in Sajhaprofit.com, either on behalf of Sajhaprofit.com or any site host, or any participant in Sajhaprofit.com or any of its associated sites should be taken as investment advice directly, indirectly, implicitly, or in any manner whatsoever, including but not limited to trading of stocks on a short term or long term basis, or trading of any financial instruments whatsoever. Past Performance Is Not an Indicator of Future Returns. All the analyst commentary provided on sajhaprofit.com is provided for information purposes only. This information is NOT a recommendation or solicitation to buy or sell any securities. Your use of this and all information contained on sajhaprofit.com is governed by these Terms and Conditions of Use. This material is based upon information that we consider reliable, but we do not represent that it is consistent or complete, and that it should be relied upon, as such. You should not rely solely on the Information in making any investment. Rather, you should use the Information only as a starting point for doing additional independent research in order to allow you to form your own opinion regarding investments. By using Sajhaprofit.com including any software and content contained therein, you agree that use of the Service is entirely at your own risk. sajhaprofit.com is a registered investment advisor.You understand and acknowledge that there is a very high degree of risk involved in trading securities. Past results of any trader published on this website are not an indicator of future returns by that trader, and are not an indicator of future returns which be realized by you. Any information, opinions, advice or offers posted by any person or entity logged in to sajhaprofit.com or any of its associated sites is to be construed as public conversation only. sajhaprofit.com makes no warranties and gives no assurances regarding the truth, timeliness, reliability, or good faith of any material posted on sajhaprofit.com. Sajhaprofit.com does not warranties that trading methods or systems presented in their services or the information herein, or obtained from advertisers or members will result in profits or losses.</li>
                            <li>Any surfing and reading of the information is the acceptance of this disclaimer. All Rights Reserved.</li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}">
    </script>

@endsection