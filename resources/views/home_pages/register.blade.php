<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sajhaprofit</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="text/css" href="{{URL::asset('assets/home/images/favicon.ico')}}" sizes="128x128"  type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/fonts/iconic/css/material-design-iconic-font.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/css/main.css') }}">
	<link rel="shortcut icon" type="text/css" href="{{URL::asset('assets/home/images/favicon.ico')}}" sizes="128x128"  type="image/x-icon" />
    <link rel="shortcut icon" type="text/css" href="{{URL::asset('assets/home/images/favicon.ico')}}" sizes="128x128"  />
	<style type="text/css" media="screen">
		.container-login100{
			background-image: url("{{URL::asset('assets/login/images/bg-01.jpg') }}")
		}
	</style>
</head>
<body style="background-color: #999999;">
	
	<div class="limiter">
		<div class="container-login100">
			<div class="login100-more" style="background-image: url('images/bg-01.jpg');"></div>

			<div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
				<form class="login100-form validate-form" method="post" action="{{ url('register-success') }}">
					{{ csrf_field() }}
					<span class="login100-form-title p-b-59">
						Sign Up
					</span>

					<div class="wrap-input100">
						<span class="label-input100">Full Name</span>
						<input class="input100" type="text" name="first_name" value="{{ old('first_name') }}" placeholder="First Name..." autocomplete="off">
					</div>
						@if($errors->has('first_name'))
							<span class="text-danger">{{ $errors->first('first_name') }}</span>
						@endif

					<div class="wrap-input100">
						<span class="label-input100">Last Name</span>
						<input class="input100" type="text" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name..." autocomplete="off">
					</div>
						@if($errors->has('last_name'))
							<span class="text-danger">{{ $errors->first('last_name') }}</span>
						@endif

					<div class="wrap-input100">
						<span class="label-input100">Email</span>
						<input class="input100" type="text" name="email" value="{{ old('email') }}" placeholder="Email addess..." autocomplete="off">
					</div>
						@if($errors->has('email'))
							<span class="text-danger">{{ $errors->first('email') }}</span>
						@endif

					<div class="wrap-input100">
						<span class="label-input100">Username</span>
						<input class="input100" type="text" name="user_name" value="{{ old('user_name') }}" placeholder="Username..." autocomplete="off">
					</div>
						@if($errors->has('user_name'))
							<span class="text-danger">{{ $errors->first('user_name') }}</span>
						@endif

					<div class="wrap-input100">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="password" placeholder="*************" autocomplete="off">
					</div>
						@if($errors->has('password'))
							<span class="text-danger">{{ $errors->first('password') }}</span>
						@endif

					<div class="wrap-input100">
						<span class="label-input100">Re-Enter Password</span>
						<input class="input100" type="password" name="re_password" placeholder="*************" autocomplete="off">
					</div>
						@if($errors->has('re_password'))
							<span class="text-danger">{{ $errors->first('re_password') }}</span>
						@endif

					<div class="flex-m w-full p-b-33">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me" checked="">
							<label class="label-checkbox100" for="ckb1">
								<span class="txt1">
									I agree to the
									<a href="#" class="hov1">
										Terms of User
									</a>
								</span>
							</label>
						</div>

						
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Sign Up
							</button>
						</div>
					</div><br>
					<div class="flex-col-c">
						<a href="{{ url('/') }}" class="txt2">Back to Home</a>
						<a href="{{ url('login') }}" class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30 txt2">
							Sign in
							<i class="fa fa-long-arrow-right m-l-5"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<script src="{{URL::asset('assets/login/js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{URL::asset('assets/login/js/bootstrap.min.js') }}"></script>
	<script src="{{URL::asset('assets/login/js/main.js') }}"></script>
	<script src="{{URL::asset('assets/login/js/popper.js') }}"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  	@include('sweet::alert')
</body>
</body>
</html>