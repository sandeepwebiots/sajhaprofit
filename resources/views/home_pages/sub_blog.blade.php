@extends('layouts.home.master') 

@section('title') Blog Details | CRM @endsection 

@section('content')

<div class="wrapper">
    <!-- Header Middle -->

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home">
        <div class="container text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <h1 class="text-uppercase">Blog Details</h1>
                        <!-- <div class="typed_text_bg">
                            <div id="typed-strings">
                                <p>Want to work with a professional & competent team?</p>
                                <p>We have the best team that is client – centric & believes in collaborative culture.</p>
                                <p>Go for it.</p>
                            </div>
                            <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                          </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="blog-section">
        <!--Blog Area Start-->
        <div class=" countries blog-area pt-100 pb-100">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-12 order-2 order-lg-1" data-aos="fade-right" data-aos-duration="2000">
                        <div class="sidebar-widget">
                            <div class="single-sidebar-widget">
                                <h4 class="title">Recent Blog Post</h4>
                                <div class="recent-content">
                                    @foreach($blogs as $blog)
                                    <div class="recent-content-item">
                                        <a href="#"> <img src="{{ URL::asset('assets\dashboard\upload\blog') }}/{{ $blog->img }}" height="76px" width="72px" alt=""></a>
                                        <div class="recent-text">
                                            <h4 class="text-uppercase"><a href="#">{{ str_limit($blog['title'], 20) }}</a></h4>
                                            <p>{{ str_limit($blog['content'], 55) }} <a href="{{ url('blog-detail') }}/{{ $blog->id }}" class="text-primary">Red more..</a></p>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 blog-post-item order-1 order-lg-2">
                        <div class="blog-wrapper blog-details">
                            <div class="blog-img img-full img-container">
                                <img src="{{ URL::asset('assets/dashboard/upload/blog') }}/{{ $blog_details->img }}" class="img-hover">
                                <div class="overlay"></div>

                            </div>
                            <div class="blog-content">
                                <div class="section-title">
                                    <h3 class="color-orange m-t-30 text-uppercase" data-aos="fade-right" data-aos-duration="2000"> <span class="text-thm2">{{ $blog_details->title }}</span></h3>
                                    <p data-aos="fade-left" data-aos-duration="2000">{{ $blog_details->content }}</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--Blog Area End-->
    </section>
</div>

@endsection @section('script')
<script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}"></script>
@endsection