@extends('layouts.home.master')

@section('title') Kyc | Sajha Profit @endsection

@section('content')
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home-explore">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <div class="center-content">
                            <h1 class="text-uppercase">FORM FOR INDIVIDUAL</h1>
                            <div class="typed_text_bg">
                                <div id="typed-strings" style="display: none;">
                                    <p>When everything seems to be going against you,</br>remember that the airplane takes off against the wind,</br>not with it.</p>
                                </div>
                                <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu">
                            <li><a href="#"> HOME </a></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                            <li> <a href="#"> KYC </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Our Feature Project -->
    <section id="project" class="our-project ulockd_bgp1">
        <div class="container-fluid">
            <div class="row">
                <!--    <div class="col-md-6 col-md-offset-3 text-center">
                        <div class="ulockd-main-title">
                            <h2>Explore</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
                        </div>
                    </div> -->
            </div>
            <div class="row">
                <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="profit-service-lable">
                        <h5 class="text-center">FORM FOR INDIVIDUAL</h5>
                    </div>
                </div>


                <div class="col-md-8 aos-init " data-aos="fade-left" data-aos-duration="2000">
                    <div class="risk-warning ">
                            <div class="panel-bodis shadoset">
                                <form class="form-horizontal" method="post" action="" name="quick_inquiry">
                                    <div class="col-md-6 col-sm-6 col-12 ">
                                            <div class="col-sm-12 form-group">
                                                <input type="email" placeholder="Email" name="email" id="email_7" class="form-control form-space">
                                                <input type="hidden" name="task" value="send">
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-12 ">
                                            <div class="col-sm-12 form-group">
                                                <input type="text" placeholder="Contact No." name="phone" id="phone_7" class="form-control" maxlength="10" onkeypress="return validate(event)">
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="col-sm-12 form-group">
                                                <select class="form-control" name="gender">
                                                    <option>Male</option>
                                                    <option>Female</option>
                                                </select>
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="col-sm-12 form-group">
                                                <select class="form-control" name="married_status">
                                                    <option>Married</option>
                                                    <option>Unmarried</option>
                                                </select>
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="col-sm-12 form-group">
                                                <input type="text" placeholder="State" name="state" id="state_7" class="form-control">
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="col-sm-12 form-group">
                                                <select class="form-control" name="resident_status">
                                                    <option>Resident Individual</option>
                                                    <option>Non Resident</option>
                                                    <option>Foreign National</option>
                                                </select>
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="col-sm-12 form-group">
                                                <input type="text" placeholder="dd/mm/yyyy" name="date" id="date_7" class="form-control date-picker">
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="col-sm-12 form-group">
                                                <input type="text" placeholder="Pincode" name="pincode" id="pincode_7" class="form-control">
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="col-sm-12 form-group">
                                                <input type="text" placeholder="Pan No." name="pan_no" id="pan_no_7" class="form-control">
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="col-sm-12 form-group">
                                                <input type="text" placeholder="UID Adhar No." name="adhar" id="adhar_7" class="form-control">
                                            </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-sm-12 form-group">
                                                <textarea placeholder="Address" name="address" id="address_7" class="form-control" rows="3"></textarea>
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 label_small_block">
                                        <div class="form-group">
                                            <label class="col-sm-12 control-label">Upload Signature: (Upload Only .JPG,.PNG ,.jpeg Format)</label>
                                            <div class="col-sm-12 form-group">
                                                <div class="form-control">
                                                    <input type="file" name="file_signature" id="file_signature_7">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 label_small_block">
                                        <div class="form-group  ">
                                            <label class="col-sm-12 control-label">Upload Photo:(Upload Only .JPG,.PNG ,.jpeg Format Only)</label>
                                            <div class="col-sm-12 form-group">
                                                <div class="form-control">
                                                    <input type="file" name="file_photo" id="file_photo_7">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 label_small_block">
                                        <div class="form-group  ">
                                            <label class="col-sm-12 control-label">Upload Document: Upload Document in .PDF,.WORD Format Only</label>
                                            <div class="col-sm-12 form-group">
                                                <div class="form-control">
                                                    <input type="file" name="file_doc" id="file_doc_7">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 label_small_block">
                                        <div class="form-group">
                                        <label class="col-sm-12 control-label">Please enter the verication code shown below.<span>*</span></label>




                                            <div id="captcha-wrap">
                                                <a href="javascript: refreshCaptcha4();"><img src="/img/refresh.jpg" alt="refresh captcha" id="refresh-captcha" class="refresh_captcha_001"></a>
                                                <img src="captcha/captcha_4.php?rand=&lt;?php echo rand();?&gt;" id="captchaimg4">
                                            </div>
                                            <br>
                                            <input class="form-control cap-set" style="width:25%;" name="captcha" type="text" placeholder="Verification Code">
                                            <br>

                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                                            <div class="col-sm-12 set-submit-btn">
                                                <button  class="btn btn-primary set-buttan">SUBMIT</button>
                                            </div>
                                        </div>
                                       </form>
                            </div>  {{--....body pannel div....--}}
                          </div>{{--....risk div close....--}}
                    </div>
                </div>
            </div>
    </section>

@endsection