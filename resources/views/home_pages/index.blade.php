@extends('layouts.home.master') @section('title') CRM @endsection @section('style') @endsection @section('content')
    <!-- start-home-section -->
    <!-- Home Design -->
    <div class="ulockd-home-slider">
        <div class="container-fluid m-0 p-0">
            <div class="row">
                <div class="col-md-12 ulockd-pmz">
                    <div class="owl-carousel owl-theme" id="banner-slider">
                        @foreach($slider as $slide)
                            <div class="item">
                                <div class="ulockd_bgi1">
                                    <img src="{{url('assets/home/images/team')}}/{{ $slide->image }}" alt="faq">
                                    <div class="container">
                                            <div class="banner-text-home">
                                             <div class="cd-full-height">
                                                {!! $slide->title !!}
                                                <h3 class="text-white m-b-50">{{ $slide->sub_title }}</h3>
                                                <div class="collection-btn">
                                                    <a href="{{ url('service') }}" class="btn_welcome button--aylen m-r-10 set-f-right">Services</a>
                                                    <a href="{{ url('about') }}" class="btn_welcome button--aylen m-r-10 set-f-right">About Us</a>
                                                </div>
                                             </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Our About -->
    <section class="ulockd-about">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 col-sm-6 text-center">
                    <div class="ulockd-main-title" data-aos="fade-right" data-aos-duration="2000">
                        <h2>WELCOME TO <span class="text-thm3">Sajha Profit</span> <br> JOIN US, FOR
                            COMPLETE FINANCIAL ADVICE.</h2>
                        <h3>Having 21 years experience
                        </h3>
                        <h4 class="justify">Sajha Profit is an organization that believes in delivering profits in a systematic &
                            professional way. We have a team of passionate professionals that provide Equity and commodity
                            services throughout the world on Indian market platforms. We cover all Indian Equity and
                            Commodity market like NSE, BSE, MCX and NCDEX. We also provide services related to IPOs and
                            Mutual Funds. Sajha Profit is presently serving in more than 35 countries with the team of talented and
                            skilled pool of professionals.<span id="dots">...</span><br>
                            <br>
                          <span id="more">Sajha Profit was created to better serve clients by breaking the traditional business model and culture
                          of the wealth management industry by applying the technical research , fundamental analysis &
                          Artificial Intelligence.At Sajha Profit, we always prefer to give high quality personalized attention &
                          services to all small investors and high net-worth investors.Our committed team always focuses
                          to provide maximum benefit to all the customers & always have the clients on the top priority.
                          </span>
                            <button onclick="myFunction()" id="myBtn" class="read-btn">Read more</button>
                        </h4>

                        <a href="{{ url('explore') }}" class="btn_welcome  button--aylen m-l-50 m-b-50 m-t-20 m-r-10">Explore<i class="fa fa-arrow-right " aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-5">
                    <div class="inquiry_form_part" data-aos="fade-left" data-aos-duration="2000" style="width: 100%">
                        <h2 class="wow bounceInRight animated">Quick Contact</h2>
                        <form action="{{ url('quickinquiry') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="name" id="name" class="form-control" placeholder="Your Name">
                                @if($errors->has('name'))<strong class="text-danger">{{ $errors->first('name') }}</strong>@endif
                            </div>
                            <div class="form-group wow fadeInUp animated">
                                <input type="number" name="mobile_no" class="form-control" placeholder="Your Mobile No." maxlength="10">
                                @if($errors->has('mobile_no'))<strong class="text-danger">{{ $errors->first('mobile_no') }}</strong>@endif
                            </div>
                            <div class="form-group wow fadeInUp animated">
                                <input type="email" name="email" class="form-control" placeholder="Your Email">
                                @if($errors->has('email'))<strong class="text-danger">{{ $errors->first('email') }}</strong>@endif
                            </div>
                            <div class="form-group wow fadeInUp animated">
                                <select name="country" id="country">
                                    <option name="country" value="">Select Country</option>
                                    <option name="country" value="1">India</option>
                                </select>
                                @if($errors->has('country'))<strong class="text-danger">{{ $errors->first('country') }}</strong>@endif
                            </div>
                            <div class="form-group wow fadeInUp animated">
                                <select name="state" id="state">
                                    <option name="state" value="">Select State</option>
                                </select>
                                @if($errors->has('state'))<strong class="text-danger">{{ $errors->first('state') }}</strong>@endif
                            </div>
                            <div class="form-group wow fadeInUp animated">
                                <select name="city" id="city">
                                    <option name="city" value="">Select City</option>
                                </select>
                                @if($errors->has('city'))<strong class="text-danger">{{ $errors->first('city') }}</strong>@endif
                            </div>

                            <div class="form-group wow fadeInUp animated">
                                <textarea class="form-control" name="details" placeholder="Inquiry Details" rows="5" id="comment"></textarea>
                                
                                @if($errors->has('details'))<strong class="text-danger">{{ $errors->first('details') }}</strong>@endif
                            </div>

                            <div class="form-group wow fadeInUp animated">
                                <label class="col-sm-12 control-label">Please enter the verication code shown below.<span>*</span></label>
                                <div class="col-sm-12">

                                    <div id="captcha-wrap" class="varify-cls">
                                        <div class="g-recaptcha" data-sitekey="6LfaCWMUAAAAAHf7Q6V58Ec5vyU-Iui_o1YYsy3Z"></div>
                                    </div>
                                    @if($errors->has('g-recaptcha-response'))<strong class="text-danger">{{ $errors->first('g-recaptcha-response') }}</strong>@endif
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary  m-t-20 text-center" style="width: 100%;">submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="about" class="about-us our-service">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="ulockd-main-title text-center">
                        <h2>ABOUT <span class="text-thm3">US</span></h2>
                    </div>
                    <div class="row">
                        <div class="col-md-3" data-aos="fade-right" data-aos-duration="2000">
                            @foreach($about_us as $about)
                                <div class="nav nav-tabs set-display tab border-right">
                                    <a class="tablinks @if($about->id == '1') active @endif" onmouseover="openCity(event, 'home{{ $about->id }}')">{{ $about->title }}</a>

                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-9" data-aos="fade-left" data-aos-duration="2000">
                            @foreach($about_us as $about)
                                <div id="home{{ $about->id }}" class="tabcontent" @if($about->id == '1') style="display: block;" @else style="display: none;" @endif>
                                    <div class="row">
                                        <div class="col-md-8 set-heading">
                                            <h2>{{ $about->title }}</h2>
                                            <p>{{ $about->content }}</p>
                                        </div>
                                        <div class="col-md-4 img-container-fluid">
                                            <img class="img-responsive img-whp img-hover" src="{{url('assets/home/images/banners/about')}}/{{ $about->image }}" alt="2.jpg">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <!-- Our Service -->
    <section id="service">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="ulockd-main-title" data-aos="fade-down" data-aos-duration="2000">
                        <h2>Our <span class="text-thm3">Services</span></h2>
                    </div>
                </div>
            </div>
            <div class="row" data-aos="zoom-out" data-aos-duration="1000">
                @foreach($service as $serve)
                    <div class="col-12  col-sm-6 col-md-4 set-images">
                        <div class="recent_box">
                            <a href="{{ url('service') }}">
                                <div class="rb_img">
                                    <img class="img-responsive" src="{{url('assets/dashboard/upload/service')}}/{{ $serve->image }}" alt="1.jpg" class="img-fluid">
                                    <div class="over-layer-inside">
                                        <h4 style="text-transform: uppercase;">{{ $serve->name }}</h4> </div>
                                    <div class="over-layer">
                                        <div>
                                            <a href="{{ route('list.service',$serve->id) }}" class="btn_welcome btn_padding button--aylen">{{ $serve->name }}</a>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <!-- Our About Info -->
    <section class="ulockd-team-two our-service our-vision">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-7">
                    <h3 class="title-bottom ulockd-mrgn120 ulockd-mrgn635" data-aos="fade-down" data-aos-duration="2000">Our
                        <span class="text-thm3">Info</span></h3>
                    <div class="row" data-aos="fade-right">
                        <div class="col-xxs-12 col-xs-6 col-sm-6" data-aos="fade-right" data-aos-duration="1500">
                            <img class="img-responsive img-whp1" src="{{url('assets/home/images/home/2.jpg')}}" alt="3.jpg">
                            <h3>Our<span class="text-thm3"> Vision</span></h3>
                            <h4 style="font-weight: bold;">
                                Focused on providing clarity, direction, and advice.</h4>
                            <h5>Our role is to lead, coach and work with clients to improve their everyday lives, we:</h5>
                            <ul>
                                <li>Make it a priority to completely understand clients’ personal & financial goals.</li>
                                <li>Comeback with a well-defined roadmap to achieve the goals.</li>
                                <li>If required, provide advice to adjust the roadmap.</li>
                            </ul>
                        </div>
                        <div class="col-xxs-12 col-xs-6 col-sm-6 our-mision" data-aos="fade-right" data-aos-duration="2000">
                            <img class="img-responsive img-whp1" src="{{url('assets/home/images/home/1.jpg')}}" alt="2.jpg">
                            <h3>Our<span class="text-thm3"> Mision</span></h3>
                            <h4 style="font-weight: bold;">To strike the optimal balance between professional values and business values.</h4>
                            <ul>
                                <li>By putting clients’ interests first, we achieve more together. We bring transparent, evidence-based advice to help our clients safeguard their financial futures and realize their dreams.</li>
                                <li>We strive for alignment of interests with our clients’ in a comprehensive, strategic approach & are committed to acting in your best interest.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5" data-aos="fade-left" data-aos-duration="2000">
                    <h3 class="title-bottom ulockd-mrgn120 ulockd-mrgn635">Have Any Questions?</h3>
                    <div class="about-box">
                        <div class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="ulockd-faq-content">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    @foreach($faqs as $faq)
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading{{ $faq->id }}">
                                                <h4 class="panel-title p-0">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $faq->id }}" aria-expanded="true" aria-controls="collapse{{ $faq->id }}"><i class="icon-Down-2 icon-1"></i><i class="icon-Right-2 icon-2"></i>{{ $faq->quotation }}</a>
                                                </h4>
                                            </div>
                                            <div id="collapse{{ $faq->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $faq->id }}">
                                                <div class="panel-body">
                                                    <p>{{ $faq->answer }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <a href="{{ url('faq') }}" class="btn_welcome button--aylen m-r-10 set-f-right">view more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Our First Divider -->
    <section class="parallax ulockd_bgi3 bgc-overlay-black5" data-stellar-background-ratio="0.3" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1500">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="subscriber-form subscribe">
                        <h2 class="ulockd-mrgn120 color-white">Proud to Say, After Providing The Best Service .</h2>
                    </div>
                </div>
            </div>
            <div class="row ulockd-mrgn1230 text-center">
                <div class="col-xxs-12 col-xs-6 col-sm-3">
                    <div class="ulockd-ffact-one hvr-pulse text-center">
                        <span class="color-white flaticon-multiple-users-silhouette"></span>
                        <p class="color-white">Global Followers</p>
                        <ul class="list-inline">
                            <li class="ulockd-pdng0">
                                <div class="timer">2200</div>
                            </li>
                            <li class="fz30 ulockd-pdng0 color-white">+</li>
                        </ul>
                    </div>
                </div>
                <div class="col-xxs-12 col-xs-6  col-sm-3">
                    <div class="ulockd-ffact-one hvr-pulse text-center">
                        <span class="color-white flaticon-trophy-for-sports"></span>
                        <p class="color-white">Cup Coffee</p>
                        <ul class="list-inline">
                            <li class="ulockd-pdng0">
                                <div class="timer">1200</div>
                            </li>
                            <li class="fz30 ulockd-pdng0 color-white">+</li>
                        </ul>
                    </div>
                </div>
                <div class="col-xxs-12 col-xs-6 col-sm-3">
                    <div class="ulockd-ffact-one hvr-pulse text-center">
                        <span class="color-white flaticon-people-5"></span>
                        <p class="color-white">Satisfied Clients</p>
                        <ul class="list-inline">
                            <li class="ulockd-pdng0">
                                <div class="timer">100</div>
                            </li>
                            <li class="fz30 ulockd-pdng0 color-white">%</li>
                        </ul>
                    </div>
                </div>
                <div class="col-xxs-12 col-xs-6 col-sm-3">
                    <div class="ulockd-ffact-one hvr-pulse text-center">
                        <span class="color-white flaticon-medal"></span>
                        <p class="color-white">Awards Won</p>
                        <ul class="list-inline">
                            <li class="ulockd-pdng0">
                                <div class="timer">21</div>
                            </li>
                            <li class="fz30 ulockd-pdng0 color-white">+</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Our Team -->
    <section class="difference">
        <div class="services_differenec_content">
            <div class="container">
                <h2 class="m-b-50 text-center">Difference between <span class="text-thm3">Sajha Profit Services</span> and <span class="text-thm3">other Services</span>
                </h2>

                <div class="service_diffrence_item_bg">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="service_diffrence_item equitics_service_tab">
                                <div class="title_service_block">
                                    <h3>Sajha Profit Services</h3>
                                </div>
                                <ul class="service_diffrence_listing">
                                    <li><i class="fa fa-caret-right"></i> <span>High Quality 90% to 95% Accurate calls</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Very limited trades to build capital</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Only 1-2 calls through Phone call</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Specific Entry and exit price on time</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Real time entry and exit price through phone call</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Personalized service: Personally Phone call entry and exit.</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Customized service: Providing services as per everyone's requirement and capital</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Best growth in capital on consistent basis.</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Monthly Assured profit in Assured profit packages</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Dedicated person available always to guide you in case of any query during live trading.</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Account handling/PMS facility available in case you can't trade due to any reason.</span> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="service_diffrence_item other_service_tab">
                                <div class="title_service_block_red">
                                    <h3>Other Services</h3>
                                </div>
                                <ul class="service_diffrence_listing">
                                    <li><i class="fa fa-caret-right"></i> <span>Practically 60% to 70% accurate call </span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Too many numbers of calls which spoil capital</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Too many SMS and Whatsapp messages</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Confusing messages like buy above and sell below with multiple targets and exit prices.</span> </li>
                                    <li><i class="fa fa-caret-right"></i> <span>Missing entry price because of delay in messages and no clear exit price.</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Common messages for all.</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>No Customization available. Same calls for 50,000 capital and 5 lac capital person.</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>Chances of capital losses and too much brokerage charges.</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>No monthly commitments. Only 30 days Messages services.</span> </li>
                                    <li><i class="fa fa-caret-right"></i> <span>Nobody provides guidance during live market and many people doesn't receive phone calls.</span></li>
                                    <li><i class="fa fa-caret-right"></i> <span>No such facilities are available.</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our-team triangle">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="ulockd-main-title">
                        <h2>Our <span class="text-thm3">Experts</span></h2>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p> -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
                    <div class="team_post" data-aos="flip-left" data-aos-duration="2000">
                        <div class="thumb-box">
                            <img src="{{url('assets/home/images/team/4.jpg')}}" alt="1.jpg">
                            <div class="thumb-box-content">
                                <h3 class="title text-thm2">Expert 1</h3>
                                <span class="post">- Our Director</span>
                                <ul class="icon">
                                    <li>
                                        <a href="#" class="fa fa-linkedin ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-facebook ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-twitter ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-link ulockd-bgthm"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-details bgc-white">
                            <h3 class="member-name">Expert 1</h3>
                            <h5 class="member-post">- Our Director</h5>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
                    <div class="team_post" data-aos="flip-right" data-aos-duration="2000">
                        <div class="thumb-box text-thm2">
                            <img src="{{url('assets/home/images/team/2.jpg')}}" alt="2.jpg">
                            <div class="thumb-box-content">
                                <h3 class="title text-thm2">Expert 2</h3>
                                <span class="post">- Our Manager</span>
                                <ul class="icon">
                                    <li>
                                        <a href="#" class="fa fa-linkedin ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-facebook ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-twitter ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-link ulockd-bgthm"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-details bgc-white">
                            <h3 class="member-name">Expert 2</h3>
                            <h5 class="member-post">- Our Manager</h5>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
                    <div class="team_post" data-aos="flip-up" data-aos-duration="2000">
                        <div class="thumb-box">
                            <img src="{{url('assets/home/images/team/3.jpg')}}" alt="3.jpg">
                            <div class="thumb-box-content">
                                <h3 class="title text-thm2">Expert 3</h3>
                                <span class="post">- Our Supervisor</span>
                                <ul class="icon">
                                    <li>
                                        <a href="#" class="fa fa-linkedin ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-facebook ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-twitter ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-link ulockd-bgthm"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-details bgc-white">
                            <h3 class="member-name">Expert 3</h3>
                            <h5 class="member-post">- Our Supervisor</h5>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3">
                    <div class="team_post" data-aos="flip-down" data-aos-duration="2000">
                        <div class="thumb-box">
                            <img src="{{url('assets/home/images/team/4.jpg')}}" alt="4.jpg">
                            <div class="thumb-box-content">
                                <h3 class="title text-thm2">Expert 4</h3>
                                <span class="post">Senior Worker</span>
                                <ul class="icon">
                                    <li>
                                        <a href="#" class="fa fa-linkedin ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-facebook ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-twitter ulockd-bgthm"></a>
                                    </li>
                                    <li>
                                        <a href="#" class="fa fa-link ulockd-bgthm"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-details bgc-white">
                            <h3 class="member-name">Expert 4</h3>
                            <h5 class="member-post">- Senior Worker</h5>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Our Testimonials -->
    <section class="ulockd-testimonial ulockd_bgp4">
        <div class="container" data-aos="fade-up" data-aos-duration="2000">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="ulockd-main-title">
                        <h2>WHAT <span class="text-thm3">CLIENTS SAY?</span> </h2>
                    </div>
                </div>
            </div>
            <section id="dg-container" class="dg-container">
                <div class="dg-wrapper">
                    <a href="#" class=""> <img class="img-responsive img-fluid" src="{{url('assets/home/images/team/111.jpg')}}" alt="2.jpg"> </a>
                    <a href="#" class=""> <img class="img-responsive img-fluid" src="{{url('assets/home/images/team/222.jpg')}}" alt="2.jpg"> </a>
                    <a href="#" class="dg-center"><img class="img-responsive img-fluid" src="{{url('assets/home/images/team/111.jpg')}}" alt="2.jpg"></a>
                    <a href="#" class=""><img class="img-responsive img-fluid" src="{{url('assets/home/images/team/222.jpg')}}" alt="2.jpg"></a>
                </div>
                <nav>
                    <span class="dg-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></span>
                    <span class="dg-next"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                </nav>
            </section>
    </section>
    <!-- Our Blog -->
    @if($blogs)
        <section class="ulockd-blog bgc-snowshade2 triangle">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <div class="ulockd-main-title">
                            <h2>Our <span class="text-thm3">Blog</span></h2>
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12" data-aos="zoom-in-up" data-aos-duration="1750">
                        <div class="two-grid-slider">
                            @foreach($blogs as $blog)
                                <div class="item">
                                    <a href="{{ url('blog') }}">
                                        <article class="blog_post2 text-left">
                                            <div class="post_review">
                                                <img class="img-responsive img-whp" src="{{ URL::asset('assets\dashboard\upload\blog') }}/{{ $blog->img }}" alt="1.jpg">
                                                <div class="post_date ulockd-bgthm">{{ $blog->created_at->format('d') }}
                                                    <small>{{ $blog->created_at->format('M') }}</small>
                                                </div>
                                                <h5 class="post_title">{{ substr($blog['title'],0,30) }}</h5>
                                                <ul class="post_comment list-inline">
                                                    <li>{{ substr($blog['sub_title'],0,30) }}
                                                    </li>
                                                </ul>
                                            </div>
                                        </article>
                                    </a>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    {{--<a href="{{ url('blog') }}" class="btn_welcome button--aylen m-r-10 set-f-right" style="margin-top: 70px;float: right;">View More</a>--}}
                </div>
            </div>
        </section>
    @endif

    <!-- news section -->
    @if($news)
        <section class="">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <div class="ulockd-main-title">
                            <h2>Latest <span class="text-thm3">News</span></h2>
                        </div>
                    </div>
                    @foreach($news as $new)
                        <div class="col-md-4" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="1750">
                            <div class="example-1 card">
                                <div class="wrapper">
                                    <a href="{{ url('news') }}">
                                        <img class="img-responsive img-whp  img-fluid" src="{{url('assets/dashboard/upload/news/')}}/{{ $new->img }}"
                                             alt="news.jpg">
                                        <div class="date">
                                            <span class="day">{{ $new->created_at->format('d') }}</span>
                                            <span class="month">{{ $new->created_at->format('M') }}</span>
                                            <span class="year">{{ $new->created_at->format('y') }}</span>
                                        </div>
                                    </a>
                                    <div class="data">
                                        <div class="content">
                                            <span class="author">{{ str_limit($new['title'],30) }}</span>
                                            <h4 class="title"><a href="#">{{ str_limit($new['sub_title'],30) }}</a></h4>
                                            <p class="text">{{ str_limit($new['content'],50) }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        <!-- <a href="{{ url('faq') }}" class="cd-btn btn btn-lg ulockd-btn-thm2 center">View More</a> -->
        </section>
    @endif
    <!--end news section-->
    <!-- <a class="scrollToHome ulockd-bgthm" href="#"><i class="fa fa-home"></i></a> -->
    <!-- end-home-section -->
@endsection @section('script')
    <script type="text/javascript" src="{{url('assets/home/js/jquery.gallery.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/home/js/modernizr.custom.53451.js')}}"></script>
    <script type="text/javascript">
        $('#banner-slider').owlCarousel({
            loop: true,
            responsiveClass: true,
            autoplay: true,
            mouseDrag:false,
            nav: true,
            autoplayTimeout:5000,
            smartSpeed:3000,
            navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 1,
                    nav: true,
                }
            }
        })
        $(function() {
            $('#dg-container').gallery({
                autoplay: true
            });
        });

        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        $('#country').on('change', function() {

            var id =jQuery(this).val();
            var state='';
            console.log(id);
            $.ajax({
                method: 'GET',
                url: '/state/'+id,
                success: function(response){
                    console.log(response);

                    if (response.length>0)
                    {
                        for(var i=0;i<response.length;i++)
                        {
                            state+='<option name="state" value="'+response[i]["state"]+'">'+response[i]["state"]+'</option>';
                            // console.log(response[i]['service_id']);
                        }
                    }else
                    {
                        state = '<option value="">No service</option>';
                    }
                    $('#state').html(state);
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    state = '<option value="">No service</option>';
                    $('#state').html(state);
                    city = '<option value="">No service</option>';
                    $('#city').html(city);
                }
            });
        });

        $('#state').on('change', function() {

            var id =jQuery(this).val();
            // console.log(id);
            var city='';
            $.ajax({
                method: 'GET',
                url: '/city/'+id,
                success: function(response){
                    // console.log(response);

                    if (response.length>0)
                    {
                        for(var i=0;i<response.length;i++)
                        {
                            city+='<option name="city" value="'+response[i]["city_name"]+'">'+response[i]["city_name"]+'</option>';
                        }
                    }else
                    {
                        city = '<option value="">No service</option>';
                    }
                    $('#city').html(city);
                },
                error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                    city = '<option value="">No service</option>';
                    $('#city').html(city);
                }
            });
        });

        function myFunction() {
            var dots = document.getElementById("dots");
            var moreText = document.getElementById("more");
            var btnText = document.getElementById("myBtn");

            if (dots.style.display === "none") {
                dots.style.display = "inline";
                btnText.innerHTML = "Read more";
                moreText.style.display = "none";
            } else {
                dots.style.display = "none";
                btnText.innerHTML = "Read less";
                moreText.style.display = "inline";
            }
        }



    </script>
@endsection