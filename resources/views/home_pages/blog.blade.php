@extends('layouts.home.master')

@section('title') Blogs | CRM @endsection

@section('content')
<div class="wrapper">
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <div class="banner-bg">
                            <img src="{{url('assets/home/images/team/13.jpg')}}" alt="blog" class="img-responsive">
                      <div class="center-content">
                        <h1 class="text-uppercase">Blog</h1>
                          <div class="typed_text_bg">
                              <div id="typed-strings" style="display: none;">
                                  <p>Conversations are happening whether you are there are not.</p>
                                  <p>Social media should improve your life, not become your life.</p>
                                  <p>You can’t be everything to everyone <br>But you can be eomething to someone.</p>
                              </div>
                              <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                          </div>
                    </div>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ulockd-inner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu">
                            <li><a href="#"> HOME </a></li>
                            <li> <i class="fa fa-chevron-right" aria-hidden="true"></i></li>
                            <li> <a href="#"> BLOG </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <section class="blog-section">
      <div class="container pt-100 pb-100">
         <div class="row">
            <div class="col-md-9 has-margin-bottom">
              @foreach($blogs as $blog)
               <div class="row has-margin-bottom">
                  <div class="col-md-4 col-sm-4"> <img src="{{ URL::asset('assets\dashboard\upload\blog') }}/{{ $blog->img }}" alt="3.jpg"> </div>
                  <div class="col-md-8 col-sm-8 bulletin">
                     <h4 class="title text-thm2 font-700">{{ $blog->title }}</h4>
                     <p>{{ $blog->created_at->format('d,M,y') }} </p>
                     <p> {{ str_limit($blog['content'],200) }}</p>
                     <a href="{{ url('blog-detail') }}/{{ $blog->id }}" class="btn_welcome button--aylen m-r-10 set-f-right">Read Article →</a>
                  </div>
               </div>
               @endforeach

              <div class="text-center center-block">
                <ul class="pagination">
                   <li class="active">{{ $blogs->links() }}</li>
                </ul>
              </div>
          </div>
      </div>

   </section>
</div>

@endsection

@section('script')
<script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}"></script>
@endsection
