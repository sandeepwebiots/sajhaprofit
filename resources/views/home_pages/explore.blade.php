@extends('layouts.home.master')

@section('title') Explore | Sajhaprofit @endsection

@section('content')
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <div class="banner-bg">

                            <img src="{{url('assets/home/images/background/city.jpg')}}" alt="explorer" class="img-responsive">
                        <div class="center-content">
                            <h1 class="text-uppercase">Explore</h1>
                            <div class="typed_text_bg">
                                <div id="typed-strings" style="display: none;">
                                    <p>Rethink what your wealth can do for you,<br>change your point of view.</p>
                                    <p>Get guidance to navigate through life’s challenges <br> and toward financial success.</p>
                                    <p>Pursuing best for you.</p>
                                </div>
                                <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu">
                            <li><a href="#"> HOME </a></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                            <li> <a href="#">  EXPLORE </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Our Feature Project -->
    <section id="project" class="our-project ulockd_bgp1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="col-md-8 set-explore" data-aos="fade-left" data-aos-duration="2000">
                    <div class="profit-service-lable">
                        <h5>Explore</h5>
                    </div>
                </div>

                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">Our Focus</a></li>
                        <li><a data-toggle="tab" href="#menu1" id="second_tab">Our Services</a></li>
                        <li><a data-toggle="tab" href="#menu2" id="third_tab">Make a Start</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">

                            <div class="profit-service-box">
                                <div class="row m-b-50">
                                    <div class="col-md-12">
                                        <div class="profit-service-details">
                                            <h4 class="color-dark">What is Sajha Profit Services?</h4>
                                            <!-- <div class="set-bottom-border"></div> -->
                                            <p style="clear: both;"> <img src="{{url('assets/home/images/banners/EXPLORE/1.jpg')}}" class="img-fluid" height="150" width="200" alt="Image" style="float: left; margin: 4px 10px 0px 0px; border: 1px solid #000000;"> We are a team of highly enthusiast, talented & highly passionate professionals who always try to provide the best services with conviction & dedication. Our platform provides best services to traders and investors of equity and commodity market.We at Sajha Profit always are concerned & committed to provide the Best services to all the clients with the help of innovative approach.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-b-50">
                                    <div class="col-md-12">
                                        <div class="profit-service-details">
                                            <h4 class="color-dark">Customer First</h4>
                                            <!-- <div class="set-bottom-border"></div> -->
                                            <p style="clear: both;"> <img src="{{url('assets/home/images/banners/EXPLORE/2.jpg')}}" class="img-fluid" height="150" width="200" alt="Image" style="float: right; margin: 4px 10px 0px 0px; border: 1px solid #000000;">By putting your interests first, we achieve more together. We bring transparent, evidence-based advice to help our clients safeguard their financial futures and realize their dreams.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-b-50">
                                    <!-- <div class="set-bottom-border"></div> -->
                                    <div class="innovate">
                                        <h4 class="color-dark">We are Innovative</h4>
                                    </div>
                                    <div class="col-md-6  ">


                                        <div class="explore_detail_item">
                                            <h4 class="color-dark"> Progressive</h4>
                                            <p>We always try to adopt new & proven technologies to take proactive action & get maximum benefit from the market in all conditions. </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6  ">
                                        <div class="explore_detail_item">
                                            <h4 class="color-dark">Visionary </h4>
                                            <p>A platform developed by Sajha Profit to educate its client base to benefit from the market & remain updated about the Indian & global markets.</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6  ">
                                        <div class="explore_detail_item">
                                            <h4 class="color-dark">Ingenious </h4>
                                            <p class="comment more">We take a holistic approach to our clients' unique requirements for financial planning and wealth management under one roof. At SP we help you to track your Finances 24X7 from anywhere in the world. We provide Unique Financial Products and Investments</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6  ">
                                        <div class="explore_detail_item">
                                            <h4 class="color-dark">Prolific</h4>
                                            <p class="comment more">A unique concept developed by Sajha Profit that removes all the obstacles that hold back the clients to learn & earn from the markets. SP ensures that its clients get the best available service with full transparency you can rely on.</p>
                                        </div>
                                    </div>

                                </div>
                                <div class="row m-b-50">
                                    <div class="col-md-12">
                                        <div class="profit-service-details">
                                            <h4 class="color-dark">WHY CHOOSE SAJHA PROFIT?</h4>
                                            <!-- <div class="set-bottom-border"></div> -->
                                            <p style="clear: both;"> <img src="{{url('assets/home/images/banners/EXPLORE/3.jpg')}}" class="img-fluid" height="150" width="200" alt="Image" style="float:left; margin: 4px 10px 0px 0px; border: 1px solid #000000;">At Sajha Profit, our focus is to provide quality & consistent services to all our valued clients.
                                                Our Team is always ready to support the client in best possible way. We believe in long & healthy business relationship with the Clients.<br /> At Sajha Profit Client is always first.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <a href="#menu1" data-toggle="tab" onclick="$('#second_tab').trigger('click')" class="btn_welcome button--aylen m-r-10 set-f-right">NEXT<i class="fa fa-arrow-right m-l-10" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <div class="profit-service-box">
                                <div class="row m-b-50">
                                    <div class="col-md-12">
                                        <div class="profit-service-details">
                                            <h4 class="color-dark">How can we help?</h4>
                                            <!-- <div class="set-bottom-border"></div> -->
                                            <p style="clear: both;"> <img src="{{url('assets/home/images/banners/EXPLORE/4.jpg')}}" class="img-fluid" height="150" width="200" alt="Image" style="float: left; margin: 4px 10px 0px 0px; border: 1px solid #000000;"> Sajha Profit was formed with a vision to always provide the best advice & services to the clients. Our Client’s feedback & belief on us assures faith & confidence on our services. We have a good Client base & this is rapidly broadening without any compromise on the service quality.<br />Sajha Profit clients receive more than a plan & service. They get something that may be even more valuable: guidance, accountability, and help at every turn.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row m-b-50">
                                    <h4 class="color-dark">Select our Services</h4>
                                    <!-- <div class="set-bottom-border"></div> -->
                                    <div class="col-md-6 p-l-0">
                                        <div class="explore_detail_item comment  comment-set">
                                            <h4 class="color-dark">NSE F&O Services</h4>
                                            <h5 class="quotes">"A goal without a plan is just a wish."You decide your fate; the MARKET doesn’t.</h5>
                                            <p class="more">These services are for equity traders and investors.In this we have a variety of services to be offered as per client’s capital investment, risk profile and returns requirements. Client can choose from low, medium and high risk plans as per their risk profile. We always focus on innovative technology for research & analysis.
                                            </p>
                                            <a href="{{ url('service') }}" class="cd-btn btn_welcome button--aylen m-r-10 set-f-right btn btn-lg ulockd-btn-thm2">view more</a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 p-l-0">
                                        <div class="explore_detail_item comment  comment-set">
                                            <h4 class="color-dark">Commodity services</h4>

                                            <h5 class="quotes"> Learn EVERYDAY,But specially from the Experiences of OTHERS.
                                                It’s Cheaper !</h5>
                                            <p class="more">These services are suitable for traditional commodity traders and investors who always target  commodity market. We have a variety of services to be offered in this segment as per client’s capital investment, risk profile and returns requirements. Round the clock our experts  track and analyze international commodity markets.</p>
                                            <a href="{{ url('service') }}" class="cd-btn btn_welcome button--aylen m-r-10 set-f-right btn btn-lg ulockd-btn-thm2">view more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-b-50">
                                    <div class="col-md-12">
                                        <div class="profit-service-details">
                                            <h4 class="color-dark">WHY CHOOSE SAJHA PROFIT SERVICES?</h4>
                                            <!-- <div class="set-bottom-border"></div> -->
                                            <p style="clear: both;"> <img src="{{url('assets/home/images/banners/EXPLORE/5.jpg')}}" class="img-fluid" height="150" width="200" alt="Image" style="float:right; margin: 4px 10px 0px 0px; border: 1px solid #000000;"> At Sajha Profit , we always focus on providing quality services and consistent results which helps in building long & healthy business relations. We ensure that client gets educated, remains updated & benefit from the research & analysis done by our experts. Our team is always ready to help the clients in best possible way to achieve their financial goals.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-b-50">
                                    <div class="col-md-12">
                                        <div class="profit-service-details">
                                            <h4 class="color-dark">WHY RELY ON US FOR BEST ADVICE?</h4>
                                            <!-- <div class="set-bottom-border"></div> -->
                                            <p style="clear: both;"> <img src="{{url('assets/home/images/banners/EXPLORE/6.jpg')}}" class="img-fluid" height="150" width="200" alt="Image" style="float: left; margin: 4px 10px 0px 0px; border: 1px solid #000000;"> Sajha Profit is a very reliable name in this industry designed for all traders and investors with a focus on quality services.<br> Our Client's feedback & renewal frequency speaks about our services & we always believe to provide the best to our clients.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center m-t-25">
                                <a href="#menu2" data-toggle="tab" onclick="$('#third_tab').trigger('click')" class="btn_welcome button--aylen m-r-10 set-f-right">NEXT<i class="fa fa-arrow-right m-l-10" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <div class="profit-service-box">
                                <div class="row m-b-50">
                                    <div class="col-md-12">
                                        <div class="profit-service-details">
                                            <h4 class="color-dark">Our Past Results</h4>
                                            <!-- <div class="set-bottom-border"></div> -->
                                            <p style="clear: both;"> <img src="{{url('assets/home/images/banners/EXPLORE/6.jpg')}}" class="img-fluid" height="150" width="200" alt="Image" style="float: left; margin: 4px 10px 0px 0px; border: 1px solid #000000;">  We believe  in “Actions Speak Louder than words”. <br>Our past performance speaks about our research, innovative approach & service towards achieving high level of standards. <br> At Sajha Profit we always strive for excellence in service delivery. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-b-50">
                                    <div class="col-md-12">
                                        <div class="profit-service-details">
                                            <h4 class="color-dark">Rendering  Our Services</h4>
                                            <!-- <div class="set-bottom-border"></div> -->
                                            <p style="clear: both;"> <img src="{{url('assets/home/images/banners/EXPLORE/7.jpg')}}" class="img-fluid" height="150" width="200" alt="Image" style="float: right; margin: 4px 10px 0px 0px; border: 1px solid #000000;"> We are the one to provide best service in this sector. <br>Anyone willing to opt for our services can do through our website by selecting particular service. Complete all registration procedures & payment through our website portal. <br>Once registration & payment is done & confirmed, client can immediately start receiving our services.
                                                <br>All the visitors to the site are advised to carefully read all terms and conditions before making the payment. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-b-50">
                                    <div class="col-md-12">
                                        <div class="profit-service-details">
                                            <h4 class="color-dark">Refer a Friend</h4>
                                            <!-- <div class="set-bottom-border"></div> -->
                                            <p style="clear: both;"> <img src="{{url('assets/home/images/banners/EXPLORE/8.jpg')}}" class="img-fluid" height="150" width="200" alt="Image" style="float: left; margin: 4px 10px 0px 0px; border: 1px solid #000000;"> Our motto is to have Delighted Client. We believe to have good client base & encourage you to refer us to your friends.
                                                As per our company policy we always benefit our clients who refer us to someone. Please be in contact with our customer care numbers to know latest referral program & benefits. You can send references on refer@sajhaprofit.in</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-b-50">
                                    <div class="col-md-12">
                                        <div class="profit-service-details">
                                            <h4 class="color-dark">Support from Sajha Profit</h4>
                                            <!-- <div class="set-bottom-border"></div> -->
                                            <p style="clear: both;"> <img src="{{url('assets/home/images/banners/EXPLORE/9.jpg')}}" class="img-fluid" height="150" width="200" alt="Image" style="float:right; margin: 4px 10px 0px 0px; border: 1px solid #000000;"> Our team is always available & ready to help the valuable clients in case of any query or difficulty.
                                                You can always reach us by Phone, Emails or WhatsApp number given on website in case of any query.
                                                Please feel free to contact us on support@sajhaprofit.in</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center m-t-25">
                                    <a href="{{ url('pricing') }}" class="btn_welcome button--aylen m-r-10 set-f-right">Get Started<i class="fa fa-arrow-right m-l-10" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}"></script>

@endsection