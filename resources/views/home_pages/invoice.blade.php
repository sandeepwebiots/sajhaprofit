@extends('layouts.home.master')

@section('title') News | Sajhaprofit @endsection

@section('style')

@endsection

@section('content')

    <div class="wrapper">
        <!-- Home Design Inner Pages -->
        <div class="ulockd-inner-invoice">
            <div class="container text-center">
                <div class="row">
                    <div class="inner-conraimer-details">
                        <div class="col-md-12">
                            <div class="center-content">
                                <h1 class="text-uppercase">news</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Home Design Inner Pages -->
        <div class="ulockd-inner-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ulockd-icd-layer">
                            <ul class="list-inline ulockd-icd-sub-menu">
                                <li><a href="#"> HOME </a></li>
                                <li><a href="#"> <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                                <li> <a href="#"> NEWS </a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="invoice_section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center" data-aos="zoom-out" data-aos-duration="2000">
                        <div class="container">
                            <div class="card">
                                <div class="card-header">
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive-md">
                                        <table class="table table-striped">
                                            <tbody class="table_body_modify">
                                            <tr>
                                                <td  colspan="12" class="center border-bottom-set heading_1">TAX INVOICE</td>
                                            </tr>
                                            <tr>
                                                <td  colspan="12" class="center heading_2">SajhaProfit Investment Advisors</td>
                                            </tr>
                                            <tr>
                                                <td  colspan="12" class="center bold_title">Add: 409, Nishan Arcade, 4th Floor, Pal, Adajan, Surat/Gujarat – 395 009</td>
                                            </tr>
                                            <tr>
                                                <td  colspan="12" class="center bold_title">Mb:- xxxxxxxxxx, Email: xxxxxxxxx@xxxx.xxxx</td>
                                            </tr>
                                            <tr>
                                                <td colspan="12" class="center heading_3">CIN: **************<br>
                                                    GST No: ****************
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="w25 td_heading_bold" >Bill to</td>
                                                <td class="td_heading_bold" colspan="4">Place of Supply</td>
                                                <td class="td_heading_bold" rowspan="2">INVOICE No</td>
                                                <td class="td_heading_bold" rowspan="2">Dated</td>
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold">Name:</td>
                                                <td colspan="4">XYZ</td>
                                            </tr>
                                            <tr>
                                                <td rowspan="">Address:</td>
                                                <td colspan="4">tded dsgygfh dggweg Add: 409, Nishan Arcade, 4th Floor, Pal, Adajan, Surat/ </td>
                                                <td ></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold">Description of Service </td>
                                                <td class="td_heading_bold" colspan="4">SAC CODE
                                                    9971</td>
                                                <td class="td_heading_bold" colspan="2">Amount</td>
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold">CONSULATNCY SERVICES</td>
                                                <td colspan="4"></td>
                                                <td colspan="2"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="12" class="td_heading_bold">TAXABLE VALUE</td>
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold"> IGST @18%</td>
                                                <td colspan="4" rowspan="3"></td >
                                                <td colspan="2" rowspan="3"></td >
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold"> CGST @9%</td>
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold">SGST @9%</td>
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold">Total</td>
                                                <td colspan="4"></td>
                                                <td></td>
                                                <td>******.00</td>
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold">Amount Chargeable (in words)</td>
                                                <td colspan="8"></td>
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold" colspan="12">For SajhaProfit Investment Advisors</td>
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold" colspan="">Company's PAN : *************</td>
                                                <td colspan="6"></td>
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold">Note-Please make cheques in favor of:</td>
                                                <td colspan="4"></td>
                                                <td class="td_heading_bold" colspan="4" rowspan="3">Authorized Signatory:<img src="{{url('assets/home/images/home/signature1.png')}}" alt="signature_img" class="img-fluid"></td>
                                            </tr>
                                            <tr>
                                                <td class="td_heading_bold" rowspan="2">"SajhaProfit Investment Advisors"</td>
                                                <td colspan="4"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection