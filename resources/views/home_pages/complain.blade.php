@extends('layouts.home.master')

@section('title')Complaint Grievance & Redressal | Sajhaprofit @endsection

@section('content')
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home-explore">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <div class="banner-bg">
                            <img src="{{url('assets/home/images/background/city.jpg')}}" alt="explorer">
                        <div class="center-content">
                            <h1 class="text-uppercase">COMPLAINT GRIEVANCE & REDRESAL</h1>
                            <div class="typed_text_bg">
                                <div id="typed-strings" style="display: none;">
                                    <p>Customer service is not a department, it’s everyone’s job.</p>
                                </div>
                                <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                            </div>
                        </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu">
                            <li><a href="#"> HOME </a></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                            <li> <a href="#"> COMPLAINT GRIEVANCE & REDRESAL </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Our Feature Project -->
    <section id="project" class="our-project ulockd_bgp1">
        <div class="container-fluid">
            <div class="row">
                <!-- 	<div class="col-md-6 col-md-offset-3 text-center">
                        <div class="ulockd-main-title">
                            <h2>Explore</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
                        </div>
                    </div> -->
            </div>
            <div class="row">
                <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="profit-service-lable">
                        <h5 class="text-center">  COMPLAINT GRIEVANCE & REDRESAL
                        </h5>
                    </div>
                </div>


                <div class="col-md-8 complin "  data-aos="fade-left" data-aos-duration="2000">
                    <p>Our team at Sajha Profit always try to deliver best services to customers & respect concerns of our clients. If any client is having any concern with our services the matter can be addressed at:</p>


                    <div class="re-heading">
                        <h4> Level-1 :</h4>
                        <p>If a client has any concern regarding any service or otherwise shall inform the respective executive by e-mail. Client is also advised to send a copy of the same mail at complaint@sajhaprofit.com. Respective executive shall revert within 3 working days.</p>
                    </div>



                    <div class="re-heading">
                        <h4> Level-2 :</h4>
                        <p>If the client still wants to escalate the matter, client can approach Management Team at management@sajhaprofit.com. Management being the highest authority at Sajha Profit can resolve the issue in the best possible manner. Management Team will reply the client within 3 working days.</p>
                    </div>


                    <div class="re-heading">
                        <h4> Level-3 :</h4>
                        <p>In case the client is still not satisfied by the response from the above authorities , issue can be escalated  to director@sajhaprofit.com.</p>
                    </div>


                    <div class="re-heading">

                        <p>Clients are advised to address the issues in the indicated hierarchy.</p>
                    </div>


                    <div class="fomtype">
                        <p >
                            Name:<br>
                            E-mail:<br>
                            Phone:<br>
                            Related Serivices:<br>
                            Complains:<br>
                        </p>
                    </div>
                    <p class="space"><i class="notes">Note:-</i>You can mail as in above format.</p>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}">
    </script>

@endsection