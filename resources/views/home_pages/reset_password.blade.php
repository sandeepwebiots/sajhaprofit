<!DOCTYPE html>
<html lang="en">
<head>
	<title>Reset Password | Sajhaprofit</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<!-- <link rel="icon" type="image/png" href="images/icons/favicon.ico"/> -->
	<link rel="shortcut icon" type="text/css" href="{{URL::asset('assets/home/images/favicon.ico')}}" sizes="128x128"  type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/fonts/iconic/css/material-design-iconic-font.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/css/main.css') }}">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
	<link rel="shortcut icon" type="text/css" href="{{URL::asset('assets/home/images/favicon.ico')}}" sizes="128x128"  type="image/x-icon" />
    <link rel="shortcut icon" type="text/css" href="{{URL::asset('assets/home/images/favicon.ico')}}" sizes="128x128"  />
	<style type="text/css" media="screen">
		.container-login100{
			background-image: url("{{URL::asset('assets/login/images/bg-01.jpg') }}")
		}
	</style>
</head>
<body>
	
	<div class="limiter">
		<!-- @if (Session::has('sweet_alert.alert'))
	    <script>
	        swal({!! Session::get('sweet_alert.alert') !!});
	    </script>
		@endif -->
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<form class="login100-form validate-form" method="post" action="{{ url('reset-password') }}/{{ $user->email }}/{{ $resetCode }}">
					<input type="hidden" name="email" value="{{ $user->email }}">
					<input type="hidden" name="resetCode" value="{{ $resetCode }}">
					{{ csrf_field() }}
					<span class="login100-form-title p-b-49">
						Forgot Password
					</span>

					<div class="wrap-input100 m-b-23">
						<span class="label-input100">New Password</span>
						<input class="input100" type="password" name="new_password" placeholder="Please Enter New Password">
					</div>
						@if($errors->has('new_password'))
							<span class="text-danger">{{ $errors->first('new_password') }}</span>
						@endif
					<div class="wrap-input100 m-b-23">
						<span class="label-input100">Confirm New Password</span>
						<input class="input100" type="password" name="confnewpassword" placeholder="Please Re-Enter New Password">
					</div>
						@if($errors->has('confnewpassword'))
							<span class="text-danger">{{ $errors->first('confnewpassword') }}</span>
						@endif
					
					<div class="text-right p-t-8 p-b-31">
						<a href="{{ url('login') }}">
							Login
						</a>
					</div>
					
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Update Password
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>

	<script src="{{URL::asset('assets/login/js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{URL::asset('assets/login/js/bootstrap.min.js') }}"></script>
	<script src="{{URL::asset('assets/login/js/main.js') }}"></script>
	<script src="{{URL::asset('assets/login/js/popper.js') }}"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  	@include('sweet::alert')
	
</body>
</body>
</html>