@extends('layouts.home.master')

@section('title') Reviews | Sajhaprofit @endsection

@section('content')
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home-explore">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <div class="banner-bg">
                            <img src="{{url('assets/home/images/background/city.jpg')}}" alt="explorer">
                        <div class="center-content">
                            <h1 class="text-uppercase"> REVIEWS</h1>
                            <div class="typed_text_bg">
                                <div id="typed-strings" style="display: none;">
                                    <p>Good Company in a journey makes the way seem shorter.</br>Change your thoughts and you change your world.</p>
                                </div>
                                <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                            </div>
                        </div>
                    </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu text-uppercase">
                            <li><a href="#"> HOME </a></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                            <li> <a href="#"> Reviews </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Our Feature Project -->
    <section id="project" class="our-project ulockd_bgp1">
        <div class="container-fluid">
            <div class="row">
                <!-- 	<div class="col-md-6 col-md-offset-3 text-center">
                        <div class="ulockd-main-title">
                            <h2>Explore</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
                        </div>
                    </div> -->
            </div>
            <div class="row">
                <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="profit-service-lable">
                        <h5 class="text-center"> REVIEWS </h5>
                    </div>
                </div>


                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">

                    <div class="d-block warning-img">
                        <img src="{{url('assets/home/images/gallery/2.jpg')}}"

                    </div>

                        <div class="re-heading">
                            <h4> Facts about reviews Facts about Complaints and reviews of Equitics Global Reserach:</h4>
                            <div class="refundpolicy-list">
                                <ul class="refund_policy_listing">
                                    <li>First of all, we are in service industry and that too in Stock market which everyone knows how volatile and unpredictable are.</li>
                                    <li>Service industry has always remain sensitive specially in stock market because we can’t satisfy 100% customers. Though we are providing best services, 5 to 10 people out of 100 will remain unsatisfied due to some reason. It’s not because our services are poor but it’s because we can’t fulfill their requirement as they want. We never make wrong commitments or fake promises but still we can’t satisfy 100% of our customers.</li>
                                    <li>People coming here are having different minds, requirements, capital, risk taking ability and many more. So it’s always difficult to make everyone happy. Some people are very conservative which can’t handle market movement and expect miracles from our side which remains unsatisfied. Those who have patience and those who can handle stock market volatility can always stay here for long term and earn good profit.</li>
                                    <li>Ultimately we are not God who runs this market but we are working on the basis of theory, experience and most important thorough research which is not 100% perfect but it works well. We are human beings and we can be wrong at any point but we are sensible and responsible who care for others money.</li>
                                 </ul>
                            </div>
                        </div>

                    <div class="re-heading">
                        <h4>  Complaints of Equitics Global Research:</h4>
                        <div class="refundpolicy-list">
                            <ul class="refund_policy_listing">
                                <li>It’s very common to see complaints and comments about any company whether it’s stock market or any other field. Internet has given people freedom to write anything anywhere without checking the facts and people easily believe negative things.</li>
                                <li>Service industry has always remain sensitive specially in stock market because we can’t satisfy 100% customers. Though we are providing best services, 5 to 10 people out of 100 will remain unsatisfied due to some reason. It’s not because our services are poor but it’s because we can’t fulfill their requirement as they want. We never make wrong commitments or fake promises but still we can’t satisfy 100% of our customers.</li>
                                <li>People coming here are having different minds, requirements, capital, risk taking ability and many more. So it’s always difficult to make everyone happy. Some people are very conservative which can’t handle market movement and expect miracles from our side which remains unsatisfied. Those who have patience and those who can handle stock market volatility can always stay here for long term and earn good profit.</li>
                                <li>Ultimately we are not God who runs this market but we are working on the basis of theory, experience and most important thorough research which is not 100% perfect but it works well. We are human beings and we can be wrong at any point but we are sensible and responsible who care for others money.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="re-heading">
                        <h4> Reviews and feedback about Equitics Global Research:</h4>
                        <div class="refundpolicy-list">
                            <ul class="refund_policy_listing">
                                <li>First of all, we are in service industry and that too in Stock market which everyone knows how volatile and unpredictable are.</li>
                                <li>Service industry has always remain sensitive specially in stock market because we can’t satisfy 100% customers. Though we are providing best services, 5 to 10 people out of 100 will remain unsatisfied due to some reason. It’s not because our services are poor but it’s because we can’t fulfill their requirement as they want. We never make wrong commitments or fake promises but still we can’t satisfy 100% of our customers.</li>
                                <li>People coming here are having different minds, requirements, capital, risk taking ability and many more. So it’s always difficult to make everyone happy. Some people are very conservative which can’t handle market movement and expect miracles from our side which remains unsatisfied. Those who have patience and those who can handle stock market volatility can always stay here for long term and earn good profit.</li>
                                <li>Ultimately we are not God who runs this market but we are working on the basis of theory, experience and most important thorough research which is not 100% perfect but it works well. We are human beings and we can be wrong at any point but we are sensible and responsible who care for others money.</li>
                            </ul>
                        </div>
                    </div>


                </div>


            </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}">
    </script>

@endsection