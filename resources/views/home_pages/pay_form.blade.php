@extends('layouts.home.master')

@section('title') Payment | CRM @endsection

@section('style')
<style type="text/css">
	.form-group {
	    padding-bottom: 40px;
	}
	.btn-payment {
		width: 100%;
    	display: inline-block;
	}
	td.text-center.bg-white {
		text-transform: capitalize;
	}
	

.inner-conraimer-details h1 {
    color: #ffffff;
    margin-top: 0;
    position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	-ms-transform: translate(-50%, -50%);
	text-align: center;
	z-index: 154;
}
</style>
@endsection

@section('content')


<!-- Home Design Inner Pages -->
<div class="ulockd-inner-home">
	<div class="container-fluid text-center">
		<div class="row">
			<div class="inner-conraimer-details">
				<div class="col-md-12">
					<div class="banner-bg">
							<img src="{{url('assets/home/images/team/payment.jpg')}}" alt="payment.jpg" class="img-responsive">
					<h1 class="text-uppercase">Payment</h1>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="ulockd-icd-layer">
					<ul class="list-inline ulockd-icd-sub-menu">
						<li><a href="{{ url('/') }}"> HOME </a></li>
						<li><i class="fa fa-chevron-right" aria-hidden="true"></i></li>
						<li> <a href="#"> Payment </a> </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Our Pricing Table -->
<!-- Our Pricing Table -->
<section class="ulockd-pricing">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 text-center">
				<div class="ulockd-main-title">
					<h2>Payment</h2>
				</div>
			</div>
		</div>

		<div class="row m-b-70">
			<div class="col-lg-6 col-12">
				<div>
					<h3 class="set-border">Fill These Details !!</h3>
				</div>
				
				<form action="{{  url('payment')  }}" method="post" >
					{{ csrf_field() }}
					<input type="hidden" name="package" value="{{  $package  }}">
					<input type="hidden" name="package_id" value="{{  $payment->id  }}">
					<div class="form-group">
						<label for="Name" class="col-sm-4">Name:</label>
						<div class="col-sm-6">
							<input type="text" placeholder="Enter Your Name" class="form-control" name="name" autocomplete="off" value="{{  old('name')  }}">
							@if($errors->has('name'))<strong class="text-danger">{{ $errors->first('name') }}</strong>@endif
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-4">Email:</label>
						<div class="col-sm-6">
							<input type="email" name="email" class="form-control" placeholder="Enter Your Email" autocomplete="off" value="{{  old('email')  }}">
							@if($errors->has('email'))<strong class="text-danger">{{ $errors->first('email') }}</strong>@endif
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-4">Password:</label>
						<div class="col-sm-6">
							<input type="password" name="password" class="form-control" placeholder="Enter Your Password" autocomplete="off" value="{{  old('password')  }}">
							@if($errors->has('password'))<strong class="text-danger">{{ $errors->first('password') }}</strong>@endif
						</div>
					</div>
					<div class="form-group">
						<label for="Telephone" class="col-sm-4">Mobile Number:</label>
						<div class="col-sm-6">
							<input type="number" name="mobile_no" class="form-control" placeholder="Enter Your Mobile Number" autocomplete="off" value="{{  old('mobile_no')  }}">
							@if($errors->has('mobile_no'))<strong class="text-danger">{{ $errors->first('mobile_no') }}</strong>@endif
						</div>
					</div>
					<div class="form-group">
						<label for="Telephone" class="col-sm-4">Unique ID:</label>
						<div class="col-sm-8">
							<input type="text" name="sponser_code" class="form-control" placeholder="You don't have unique code enter 10 digits" autocomplete="off" value="{{  old('sponser_code')  }}">
							@if($errors->has('sponser_code'))<strong class="text-danger">{{ $errors->first('sponser_code') }}</strong>@endif
						</div>
					</div>
					<div class="form-group">
						<label for="Telephone" class="col-sm-4">Address:</label>
						<div class="col-sm-6">
							<input type="text" name="address" class="form-control" placeholder="Enter Adress" autocomplete="off" value="{{  old('address')  }}">
							@if($errors->has('address'))<strong class="text-danger">{{ $errors->first('address') }}</strong>@endif
						</div>
					</div>
					<div class="form-group">
						<label for="Telephone" class="col-sm-4">State:</label>
						<div class="col-sm-6">
		                    <select name="state" id="state" class="form-control">
		                        <option name="state" value="">Select State</option>
		                        @foreach($state as $stat)
		                        	<option name="state" value="{{ $stat->state }}">{{ $stat->state }}</option>
		                        @endforeach
		                    </select>
	                	@if($errors->has('state'))<strong class="text-danger">{{ $errors->first('state') }}</strong>@endif
		                </div>
	                </div>
	                <div class="form-group">
	                	<label for="Telephone" class="col-sm-4">City:</label>
	                	<div class="col-sm-6">
		                    <select name="city" id="city" class="form-control">
		                        <option name="city" value="">Select City</option>
		                    </select>
	                		@if($errors->has('city'))<strong class="text-danger">{{ $errors->first('city') }}</strong>@endif
		                </div>
	                </div>
	                <div class="form-group">
	                	<label for="Telephone" class="col-sm-4">ZipCode</label>
	                	<div class="col-sm-6">
		                   <input type="text" name="zipcode" class="form-control" placeholder="Enter ZipCode" autocomplete="off" value="{{  old('zipcode')  }}">
	                		@if($errors->has('zipcode'))<strong class="text-danger">{{ $errors->first('zipcode') }}</strong>@endif
		                </div>
	                </div>
				  	<div class="form-group">        
				      <div class="col-sm-offset-2 col-sm-10">
				        <div class="checkbox"><input type="checkbox" name="remember">I have read and accepted the <a href="#" class="set-link">terms and conditions </a>stated. 
											(Click Check Box to proceed for payment.)
				        </div>
				      </div>
				    </div>
						<div class="text-center">
							<button type="submit" class="cd-btn btn btn-primary btn-lg ulockd-btn-thm2">Submit</button>
						</div>
				</form>
			</div>
			<div class="col-lg-6 col-12">
				<div>
					@php $amount = 'price_'.$package; @endphp
					<h3 class="set-border">Package Detail!!</h3>
					<div class="row">
						<table class="table">
							<thead class="bg-primary text-white">
								<tr>
									<th class="text-center">Amount</th>
									<th class="text-center">Package</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center bg-white">₹ {{ $payment[$amount] }}</td>
									<td class="text-center bg-white">{{ $package }}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
</section>
@endsection

@section('script')
<script>
	 $('#state').on('change', function() {

        var id =jQuery(this).val();
        // console.log(id);
        var city='';
            $.ajax({
            method: 'GET',
            url: '/city/'+id,
            success: function(response){
                // console.log(response);
                
                if (response.length>0) 
                {
                    for(var i=0;i<response.length;i++)
                    {
                        city+='<option name="city" value="'+response[i]["city_name"]+'">'+response[i]["city_name"]+'</option>';
                    }
                }else
                {
                    city = '<option value="">No service</option>';
                }
                $('#city').html(city);
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                city = '<option value="">No service</option>';
                $('#city').html(city);
            }
        });
    });
</script>

@endsection