@extends('layouts.home.master')

@section('title') Risk Profile | Sajha Profit @endsection

@section('content')
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home-explore">
        <div class="container text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <div class="center-content">
                            <h1 class="text-uppercase">RISK PROFILE</h1>
                            <div class="typed_text_bg">
                                <div id="typed-strings" style="display: none;">
                                    <p>When everything seems to be going against you,</br>remember that the airplane takes off against the wind,</br>not with it.</p>
                                </div>
                                <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu">
                            <li><a href="#"> HOME </a></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                            <li> <a href="#"> RISK PROFILE </a> </li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                            <li> <a href="#"> Risk Profile Upload Form </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Our Feature Project -->
    <section id="project" class="our-project ulockd_bgp1">
        <div class="container">
            <div class="row">
                
            </div>
            <div class="row">
                <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ strtoupper($serve->name) }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ strtoupper($serve->name) }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ strtoupper($sub_serve->name) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="profit-service-lable">
                        <h5 class="text-center">RISK PROFILE</h5>
                    </div>
                </div>
                <div class="col-md-8 aos-init" data-aos="fade-left" data-aos-duration="2000">
                    <div class="risk-warning  ">

                        <div class="form-body">
                            <form class="form-horizontal" method="post" action="{{ url('upload-risk-profile-form') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div id="success-alert_5"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Name</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="name" id="name" class="form-control wow fadeInUp animated" placeholder="Your Name" style="visibility: visible; animation-name: fadeInUp;" value="{{ old('name') }}">
                                        @if($errors->has('name'))<strong class="text-danger">{{ $errors->first('name') }}</strong>@endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Mobile No</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="mobile_no" id="phone" class="form-control wow fadeInUp animated" placeholder="Your Mobile No." maxlength="10" onkeypress="return validate(event)" style="visibility: visible; animation-name: fadeInUp;" value="{{ old('mobile_no') }}">
                                        @if($errors->has('mobile_no'))<strong class="text-danger">{{ $errors->first('mobile_no') }}</strong>@endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Email</label>
                                    <div class="col-sm-8">

                                        <input type="text" name="email" id="email_5" class="form-control wow fadeInUp animated" placeholder="Your Email" style="visibility: visible; animation-name: fadeInUp;" value="{{ old('email') }}">
                                        @if($errors->has('email'))<strong class="text-danger">{{ $errors->first('email') }}</strong>@endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">Upload Downloaded Form</label>
                                    <div class="col-sm-8">
                                        <div class="form-control wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                                            <input type="file" name="risk_form" id="upload_file_5">
                                        </div>
                                            @if($errors->has('risk_form'))<strong class="text-danger">{{ $errors->first('risk_form') }}</strong>@endif
                                    </div>
                                </div>
                                <div class="form-group captcha_group">
                                    <div class="col-sm-4">
                                        
                                    </div>
                                    <div class="col-sm-8">

                                        <div id="captcha-wrap" class="varify-cls">
                                           <div class="g-recaptcha" data-sitekey="6LfaCWMUAAAAAHf7Q6V58Ec5vyU-Iui_o1YYsy3Z"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8 text-left">
                                        <button type="submit"  class="btn_pay  button--aylen" >Submit </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection