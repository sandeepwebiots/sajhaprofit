@extends('layouts.home.master')

@section('title') terms &conditions | Sajhaprofit @endsection

@section('style')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
@endsection
@section('content')
    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-home-explore">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="inner-conraimer-details">
                    <div class="col-md-12">
                        <div class="banner-bg">
                            <img src="{{url('assets/home/images/background/city.jpg')}}" alt="explorer">
                        <div class="center-content">
                            <h1 class="text-uppercase"> TERMS & CONDITIONS</h1>
                            <div class="typed_text_bg">
                                <div id="typed-strings" style="display: none;">
                                    <p>We don’t have to be smarter than the rest,</p>
                                    <p>We have to be more disciplined than the rest.</p>
                                </div>
                                <span id="typed" class="typed_article_text" style="white-space:pre;"></span>
                            </div>
                        </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Home Design Inner Pages -->
    <div class="ulockd-inner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="ulockd-icd-layer">
                        <ul class="list-inline ulockd-icd-sub-menu">
                            <li><a href="#"> HOME </a></li>
                            <li><i class="fa fa-chevron-right" aria-hidden="true"></i> </li>
                            <li class="text-uppercase"> <a href="#"> Terms & Conditions </a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Our Feature Project -->
    <section id="project" class="our-project ulockd_bgp1">
        <div class="container-fluid">
            <div class="row">
                <!-- 	<div class="col-md-6 col-md-offset-3 text-center">
                        <div class="ulockd-main-title">
                            <h2>Explore</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem labore voluptates consequuntur velit maiores fugiat eaque.</p>
                        </div>
                    </div> -->
            </div>
            <div class="row">
                <div class="col-md-4 aos-init aos-animate" data-aos="fade-right" data-aos-duration="2000">
                    <div class="set-dropmenu">
                        @foreach($service as $serve)    
                        <div class="ulockd-explo-content">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading " role="tab" id="heading{{ $serve->id }}">
                                        <h4 class="panel-title set-back">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $serve->id }}" aria-expanded="false" aria-controls="collapse{{ $serve->id }}" class="collapsed">
                                                <i class="icon-Down-2 icon-1"></i>
                                                <i class="icon-Right-2 icon-2"></i>
                                                <div class="categories">
                                                    <p>{{ $serve->name }}</p>
                                                </div>
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="collapse{{ $serve->id }}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading{{ $serve->id }}">
                                        <div class="sub-category">
                                            <ul>
                                                @foreach($serve->sub_service as $sub_serve)
                                                    <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ $sub_serve->name }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    @foreach($service as $serve)
                        <div class="hide-explorer-div">
                            <div class="service-category service_category{{$serve->id}}">
                                <div class="category-label">
                                    <p>{{ $serve->name }}</p>
                                </div>
                                <div class="sub-category">
                                    <ul>
                                        @foreach($serve->sub_service as $sub_serve)
                                            <li><a href="{{ url('service-detial') }}/{{ $sub_serve->id }}"><i class="fa fa-arrow-right" aria-hidden="true"></i> {{ $sub_serve->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="col-md-8" data-aos="fade-left" data-aos-duration="2000">
                    <div class="profit-service-lable">
                        <h5 class="text-center"> Terms & Conditions</h5>
                    </div>
                </div>


                <div class="col-md-8" >
                    <div class="risk-warning  terms-bullet-setting" data-aos="fade-left" data-aos-duration="2000">
                        <p><i class="fas fa-angle-double-right"></i>Our packages and services are designed based on assumption and there is no guarantee of profit. Amount of profit given in our services are just approximate amount which you may earn from our services and there is no guarantee of the same. Equitics or any of its employee are not responsible for any kind of losses occurred during trading and taking our services. F&O trading is 100% risky and it's nowhere related to fixed return or guaranteed return and traders please note that profit and loss are part of market so don't trade with sentiments and take risk of your spare capital only. If you are subscribing to your packages, you are agreed and aware about risk and you have read our disclaimers, terms and conditions and other manuals carefully</p>
                        <p><i class="fas fa-angle-double-right"></i>Our Recommendations for stocks/commodities are based on the theory of technical/fundamental analysis and personal observations. This does not claim for sure profit or any fix returns. We are not responsible for any losses made by traders. It is only the outlook of the market with reference to its previous performance. You are advised to take your position with your sense and judgment.</p>
                        <p><i class="fas fa-angle-double-right"></i>We are trying to consider the fundamental validity of stocks or commodities as far as possible, but demand and supply affects it with vision variations. If any other company also giving same script and recommendation, then we are not responsible for that. We have not any position in our given scripts. Visiting our web one should by agree to our terms and condition and disclaimer also.</p>

                        <div class="d-block warning-img">
                            <img src="{{url('assets/home/images/termsandconditions/terms_conditions_thumb.jpg')}}"

                        </div>
                        <div class="re-heading">
                            <h4>(1) SERVICES ACTIVATION PROCESS</h4>
                        </div>
                        <p>We Respect Your Hard Earned wealth/asset/money etc it's very important for human being, losing it creates lots of pain Hence Always trade safely, follow guidelines and trading rules without missing single time.</p>

                        <div class="re-heading">
                            <h4>(2) SERVICES ACTIVATION PROCESS</h4>
                        </div>
                        <p>We Respect Your Hard Earned wealth/asset/money etc it's very important for human being, losing it creates lots of pain Hence Always trade safely, follow guidelines and trading rules without missing single time.</p>

                        <div class="re-heading">
                            <h4>(3) SERVICES ACTIVATION PROCESS</h4>
                        </div>
                        <p>We Respect Your Hard Earned wealth/asset/money etc it's very important for human being, losing it creates lots of pain Hence Always trade safely, follow guidelines and trading rules without missing single time.</p>

                        <div class="re-heading">
                            <h4>(4) SERVICES ACTIVATION PROCESS</h4>
                        </div>
                        <p>We Respect Your Hard Earned wealth/asset/money etc it's very important for human being, losing it creates lots of pain Hence Always trade safely, follow guidelines and trading rules without missing single time.</p>

                        <div class="re-heading">
                            <h4>(5) SERVICES ACTIVATION PROCESS</h4>
                        </div>
                        <p>We Respect Your Hard Earned wealth/asset/money etc it's very important for human being, losing it creates lots of pain Hence Always trade safely, follow guidelines and trading rules without missing single time.</p>

                    </div>
                </div>


            </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}">
    </script>

@endsection