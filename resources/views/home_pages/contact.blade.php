 @extends('layouts.home.master')
@section('title') Contact Us | Sajhaprofit @endsection
@section('content')
	<link rel="stylesheet" type="text/css" href="../../../public/assets/home/css/font-awesome.min.css">

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-home">
		<div class="container-fluid text-center">
			<div class="row">
				<div class="inner-conraimer-details">
					<div class="col-md-12">
						<div class="banner-bg">
							<img src="{{url('assets/home/images/team/contact-us.jpg')}}" alt="contact" class="img-responsive">
						<div class="center-content">
							<h1 class="text-uppercase ">CONTACT US</h1>
							<div class="typed_text_bg">
								<div id="typed-strings" style="display: none;">
									<p>We’re ready to provide you Comprehensive,coordinated <br>& disciplined financial services.<br> We’re always standing by and eager to help.</p>
								</div>
								<span id="typed" class="typed_article_text" style="white-space:pre;"></span>
							</div>
						</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-page">
		<div class="container-fluid ">
			<div class="row">
				<div class="col-md-12">
					<div class="ulockd-icd-layer">
						<ul class="list-inline ulockd-icd-sub-menu">
							<li><a href="#"> HOME </a></li>
							<li><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i> </a></li>
							<li><a href="#"> CONTACT US</a> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>


	<section class="contact-section">
		<div class="container-fluid">
			<h2 class="contact-h2">Contact<span class="text-thm3"> Info</span></h2>
			<div class="contact-info-border-set">
				<div class="row">
					<div class="col-lg-4">
						<div class="contact-media-col-set">
							<div class="media contact-media">
								<div class="contact-fa-set">
									<i class="fa fa-address-book contact-fa"></i>
								</div>
								<div class="media-body">
									<h5 class="">OUR ADDRESS</h5>
									<p>409, Nishal Arcade,Nr. J.K. Motors, Pal</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="contact-media-col-set">
							<div class="media contact-media">
								<div class="contact-fa-set">
									<i class="fa fa-address-book contact-fa"></i>
								</div>
								<div class="media-body">
									<h5 class="">WEB</h5>
									<p><a class="" href="#">www.google.com</a></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="contact-media-col-set">
							<div class="media contact-media">
								<div class="contact-fa-set">
									<i class="fa fa-envelope contact-fa"></i>
								</div>
								<div class="media-body">
									<h5 class="">EMAIL</h5>
									<p><a class="" href="#">contact@sajhaprofit.com</a></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="contact-media-col-set">
							<div class="media contact-media">
								<div class="contact-fa-set">
									<i class="fa fa-envelope contact-fa"></i>
								</div>
								<div class="media-body">
									<h5 class="">EMAIL</h5>
									<p><a class="" href="#">info@sajhprofit.com</a></p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="contact-media-col-set">
							<div class="media contact-media">
								<div class="contact-fa-set">
									<i class="fa fa-phone-square contact-fa"></i>
								</div>
								<div class="media-body">
									<h5 class="">PHONE</h5>
									<p>+91 1234569874 / +91 9874563211</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="contact-media-col-set">
							<div class="media contact-media">
								<div class="contact-fa-set">
									<i class="fa fa-clock-o contact-fa" aria-hidden="true"></i>
								</div>
								<div class="media-body">
									<h5 class="">BUSINESS HOURS</h5>
									<p>Monday-Saturday(10AM-8PM)</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="contact-section">
		<div class="container-fluid">
			<h2 class="contact-h2">Our<span class="text-thm3"> Partners</span></h2>
			<div class="contact-info-border-set">
				<div class="row text-center">
					<div class="col-sm-4 text-center">
						<div class="media contact-media text-center">
							<div class="media-body text-center">
								<figure>
									<img src="{{url('assets/home/images/testimonial/2.jpg')}}" alt="contact" width="150" height="150" class="img-circle">
								<h5 class="">Rajiv Hari Om Bhatia</h5>
								 <a href="#" data-toggle="tab"  class="btn_partner button--aylen">View More</a>
								</figure>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="media contact-media text-center">
							<div class="media-body">
								<figure>
									<img src="{{url('assets/home/images/testimonial/2.jpg')}}" alt="contact" width="150" height="150" class="img-circle">
								</figure>
								<h5 class="">Vijaylakshmi Vadlapati.</h5>
								<p><a href="#" data-toggle="tab"   class="btn_partner button--aylen">View More</a></p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="media contact-media">
							<i class="fa fa-envelope"></i>
							<div class="media-body">
								<figure>
									<img src="{{url('assets/home/images/testimonial/2.jpg')}}" alt="contact" width="150" height="150" class="img-circle">
								</figure>
								<h5 class="">Katrina Kaif</h5>
								<p><a href="#" data-toggle="tab"   class="btn_partner button--aylen  ">View More</a></p>
							</div>
						</div>
					</div>
				</div>




				<div class="row text-center">
					<div class="col-sm-2"></div>
					<div class="col-sm-4 ">
						<div class="contact-media-col-set">
							<div class="media contact-media">
								<i class="fa fa-envelope"></i>
								<div class="media-body">
									<figure>
										<img src="{{url('assets/home/images/testimonial/2.jpg')}}" alt="contact" width="150" height="150" class="img-circle">
									</figure>
									<h5 class="">Amitabh Bachchan</h5>
									<p><a href="#" data-toggle="tab"  class="btn_partner button--aylen ">View More</a></p>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="contact-media-col-set">
							<div class="media contact-media">
								<i class="fa fa-phone-square"></i>
								<div class="media-body">
									<figure>
										<img src="{{url('assets/home/images/testimonial/2.jpg')}}" alt="contact" width="150" height="150" class="img-circle">
									</figure>
									<h5 class="">Ashwini Shetty</h5>
									<p><a href="#" data-toggle="tab"  class="btn_partner button--aylen">View More</a></p>
								</div>
							</div>
						</div>
						<div class="col-sm-2"></div>
					</div>
					{{--<div class="col-lg-4">--}}
					{{--<div class="contact-media-col-set">--}}
					{{--<div class="media contact-media">--}}
					{{--<i class="fa fa-phone-square"></i>--}}
					{{--<div class="media-body">--}}
					{{--<figure>--}}
					{{--<img src="{{url('assets/home/images/testimonial/2.jpg')}}" alt="contact" width="150" height="150" class="img-circle">--}}
					{{--</figure>--}}
					{{--<h5 class="">Ashwini Shetty</h5>--}}
					{{--<p><a href="#" data-toggle="tab" id="" class="btn_partner button--aylen">View More</a></p>--}}
					{{--</div>--}}
					{{--</div>--}}
					{{--</div>--}}
					{{--</div>--}}
				</div>
			</div>




		</div>
	</section>

	<!-- Inner Pages Main Section -->
	<section class="ulockd-contact-page">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6">
					<h2 class="contact-h2">location<span class="text-thm3"> Map</span></h2>
					<div class="ulockd-google-map">
						<div class="h300" id="map-canvas">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29772.532656347415!2d73.08627562443888!3d21.12983892628176!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be0676962062319%3A0x42a65da81e176ab3!2sBardoli%2C+Gujarat+394601!5e0!3m2!1sen!2sin!4v1546343643394" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				<div class="col-lg-6 contect-seting">
					<h2 class="contact-h2">Send Us a<span class="text-thm3"> Message</span></h2>
					<div class="ulockd-contact-form">
                        <form id="contact_form" name="contact_form" class="contact-form" action="#" method="post" novalidate="novalidate">
                            <div class="messages"></div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
										<label class="">Name<span class="symbol required"></span></label>
                                        <input id="form_name" name="form_name" class="form-control ulockd-form-fg required" required="required" data-error="Name is required." type="text">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
										<label class="">Email<span class="symbol required"></span></label>
                                        <input id="form_email" name="form_email" class="form-control ulockd-form-fg required email" required="required" data-error="Email is required." type="email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
	                            <div class="col-lg-6">
	                                <div class="form-group">
										<label class="">Phone<span class="symbol required"></span></label>
	                                    <input id="form_subject" name="form_subject" class="form-control ulockd-form-fg required" required="required" data-error="Subject is required." type="text">
	                                    <div class="help-block with-errors"></div>
	                                </div>
	                            </div>
                                <div class="col-lg-12">
		                            <div class="form-group">
		                                <textarea id="form_message" name="form_message" class="form-control ulockd-form-tb required" rows="8" placeholder="Your massage" required="required" data-error="Message is required."></textarea>
		                                <div class="help-block with-errors"></div>
		                            </div>
		                            <div class="form-group ulockd-contact-btn">
		                                <input id="form_botcheck" name="form_botcheck" class="form-control" value="" type="hidden">
										<a href="#menu1" data-toggle="tab" id="" class="btn_welcome button--aylen m-r-10 set-f-right">Send</a>
									</div>
                                </div> 
                            </div>
                        </form>
                        
					</div>
				</div>
			</div>
		</div>
	</section>

	

@endsection
@section('script')
	<script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}"></script>
@endsection