@extends('layouts.home.master')
@section('title') Stock Chart @endsection


@section('style')

<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<style>
	#chartdiv {
	width	: 100%;
	height	: 500px;
	
}	
#stock_0ca7d{
	margin: 36px;
}																
</style>
@endsection


@section('content')

<div class="wrapper">
	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-chart">
		<div class="container-fluid text-center">
			<div class="row">
				<div class="inner-conraimer-details">
					<div class="col-md-12">
						<div class="banner-bg">
							<img src="{{url('assets/home/images/team/5.jpg')}}" alt="services">
						<div class="center-content">
						<h1 class="text-uppercase">CHART</h1>
							<div class="typed_text_bg">
								<div id="typed-strings" style="display: none;">
									<p>If You cannot control your emotions,<br> you cannot control your money.</p><p>We have two kinds of forecasters:<br>those who don’t know, <br>and those don’t know that they don’t know.</p>
								</div>
								<span id="typed" class="typed_article_text" style="white-space:pre;"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-page">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="ulockd-icd-layer">
						<ul class="list-inline ulockd-icd-sub-menu">
							<li><a href="#"> HOME </a></li>
							<li><a href="#"><i class="fa fa-chevron-right" aria-hidden="true"></i> </a></li>
							<li> <a href="#"> CHART </a> </li>
						
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="container-fluid">
<div id="stock_0ca7d"></div>		
</div>
</div>
@endsection

@section('script')
<!-- AM CHART JS -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<!-- CHART JS -->
<script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>

	<script type="text/javascript" src="{{url('assets/home/js/typed.min.js')}}"></script>
<script>
   new TradingView.widget(
        {
          "width": 1480,
          "height": 610,
          "symbol": "NASDAQ:AAPL",
          "interval": "D",
          "timezone": "Etc/UTC",
          "theme": "Light",
          "style": "1",
          "locale": "en",
          "toolbar_bg": "#f1f3f6",
          "enable_publishing": false,
          "allow_symbol_change": true,
          "container_id": "stock_0ca7d"
        }
    );
</script>

@endsection



