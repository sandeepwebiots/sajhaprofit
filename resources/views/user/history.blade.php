@extends('layouts.home.master')

@section('title') History | CRM @endsection

@section('style')
<style type="text/css">
	.form-group {
	    padding-bottom: 40px;
	}
	.btn-payment {
		width: 100%;
    	display: inline-block;
	}
	.badge-primary {
	  background-color: #054d9a; }

	.badge-secondary {
	  background-color: #26c6da; }

	.badge-success {
	  background-color: #00c292; }

	.badge-danger {
	  background-color: #FF5370; }

	.badge-info {
	  background-color: #4099ff; }

	.badge-light {
	  background-color: #eeeeee; }

	.badge-dark {
	  background-color: #263238; }

	.badge-warning {
	  background-color: #f3d800; }
</style>
@endsection

@section('content')


<!-- Home Design Inner Pages -->
<div class="ulockd-inner-price">
		<div class="container-fluid text-center  set-fluid">
			<div class="row">
				<div class="inner-conraimer-details">

					<div class="col-md-12 pr-left-right">
						<div class="banner-bg">
							<img src="{{url('assets/home/images/team/payment.jpg')}}" alt="payment.jpg" class="img-responsive">
						<div class="center-contents-pricing">
							<h1 class="text-uppercase">History</h1>
							<div class="typed_text_bg">
								<div id="typed-strings" style="display: none;">
									<p>No price is too low for a bear or too high for a bull</p>
									<p>No matter how brilliant your mind or strategy,<br>if you're playing a solo game, you'll always lose out to a team.</p>
									<p>With us get guidance to navigate through life’s<br>challenges and toward financial success.</p>
								</div>
								<span id="typed" class="typed_article_text" style="white-space:pre;"></span>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>>

<!-- Home Design Inner Pages -->
<div class="ulockd-inner-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="ulockd-icd-layer">
					<ul class="list-inline ulockd-icd-sub-menu">
						<li><a href="#"> HOME </a></li>
						<li><i class="fa fa-chevron-right" aria-hidden="true"></i></li>
						<li> <a href="#"> History </a> </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Our Pricing Table -->
<!-- Our Pricing Table -->
<section class="ulockd-pricing">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 text-center">
				<div class="ulockd-main-title">
					<h2>History</h2>
				</div>
			</div>
		</div>

	<div class="row m-b-70">
		<div class="col-lg-12 col-12">
			<div>
				<h3 class="set-border">Package Purchase History</h3>
				<div class="row">
					<table class="table" id="history">
						<thead class="bg-primary text-white">
							<tr>
								<th class="text-center">#</th>
								<th class="text-center">Package</th>
								<th class="text-center">Service</th>
								<th class="text-center">Amount</th>
								<th class="text-center">Status</th>
							</tr>
						</thead>
						<tbody>
							@php $i = 1; @endphp
							@foreach($purchase as $purch)
							<tr>
								<td class="text-center bg-white">{{ $i++ }}</td>
								<td class="text-center bg-white">{{ strtoupper($purch->pack) }}</td>
								<td class="text-center bg-white">{{ $purch->package->sub_service->name }}</td>
								<td class="text-center bg-white">{{ $purch->total_amount }}</td>
								<td class="text-center bg-white">@if($purch->status == 0)
									<span class="badge badge-warning">Pending</span>
									@elseif($purch->status == 1)<span class="badge badge-success">Complete</span>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('script')


<script>
$(document).ready( function () {
    $('#history').DataTable();
} );
</script>
@endsection