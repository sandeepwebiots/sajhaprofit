@extends('layouts.dashboard.master')

@section('title') Dashboard @endsection

@section('content')

<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h3>Dashboard
                    </h3>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>PAckages</h5>
                                
                            </div>
                            <div class="card-body">
                                <div class="dt-ext table-responsive">
                                    <table id="export-button" class="display">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Service</th>
                                            <th>Package</th>
                                            <th>Amount</th>
                                            <th>Details</th>
                                            <th>Action</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                         
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>

@endsection
        