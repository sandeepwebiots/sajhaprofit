    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/bootstrap.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/style.css')}}">
    <!-- <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/style.css')}}"> -->
    <!-- Responsive stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/responsive.css')}}">
     
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/bootsnav.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/slider.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/owl.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/jquery-ui.min.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/isotop.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/imagehover.css')}}"> 
    <!-- Datatable css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatable-extension.css') }}" />
    <!-- <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/dark.css')}}">  -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/icon-moon.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/hover.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/fullcalendar.min.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/font-awesome-animation.min.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/font-awesome.min.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/flipclock.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/flaticon.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/elegantIcons.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/animate.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/3d-buttons.css')}}"> 
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/magnific-popup.css')}}">
    <!-- <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/pe-icon-7-stroke.css')}}">  -->
     <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/home/css/aos.css')}}">
    
    <style type="text/css" media="screen">
    .theme-background-color{
        background-color: #054d9a !important;
    }    
    </style>