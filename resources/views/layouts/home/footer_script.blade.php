  <script type="text/javascript" src="{{url('assets/home/js/jquery.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/bootstrap.min.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/bootsnav.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/parallax.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/scrollto.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/jquery-scrolltofixed-min.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/jquery.counterup.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/gallery.js')}}"></script>
  <!-- data table -->
  <script src="{{ URL::asset('assets/dashboard/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ URL::asset('assets/dashboard/js/datatable.custom.js') }}"></script>
  
  <script type="text/javascript" src="{{url('assets/home/js/wow.min.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/slider.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/video-player.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/jquery.barfiller.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/jflickrfeed.min.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/timepicker.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/tweetie.js')}}"></script>
  <script type="text/javascript" src="{{url('assets/home/js/aos.js')}}"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  
  @include('sweet::alert')
  <script>
    AOS.init();
  </script>
  <!-- Custom script for all pages --> 
  <script type="text/javascript" src="{{url('assets/home/js/script.js')}}"></script>
 <!--  <script type="text/javascript">
      $('#form_date').datetimepicker({
        format: 'HH:mm:ss'
    });
</script> -->
<!-- <script type="text/javascript">
    var LiveHelpSettings = {};
    LiveHelpSettings.server = 'www.example.com';
    LiveHelpSettings.embedded = true;
    (function(d, $, undefined) { 
        $(window).ready(function() {
            // JavaScript
            var LiveHelp = d.createElement('script'); LiveHelp.type = 'text/javascript'; LiveHelp.async = true;
            LiveHelp.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + LiveHelpSettings.server + '/livehelp/scripts/jquery.livehelp.js';
            var s = d.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(LiveHelp, s);
        });
    })(document, jQuery);
</script>
 -->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5bffe1dcfd65052a5c930d5c/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


</script>

  <script type="text/javascript" src="{{url('assets/home/js/sweetalert.min.js')}}" ></script>

  <script type="text/javascript" charset="utf-8" async defer>
    function addSubscribe()
    {
        var email = $("#subscribe_email").val();
        var _token="{{csrf_token()}}";
        if(email!='')
        {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
            {
              console.log('hello');
                $.ajax({
                    type:'post',
                    data: { 'email': email, '_token':_token},
                    dataType: 'json',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    url:'{{url('email-subscription')}}',
                    success:function(data){
                        if(data==0)
                        {
                            $("#subscribe_email").val('');
                            swal("Warning", "You email address was already subscribed!", "warning");
                        }
                        else if(data==1)
                        {
                            $("#subscribe_email").val('');
                            swal("Success", "Your subscribption done successfully!", "success");
                        }
                        else
                        {
                            swal("Error", "Oops! Something went wrong, Please try again.", "error");
                        }
                    }
                });
            }
            else{
                swal("Warning", "Enter valid email address.", "warning");
            }
        }
        else{
            swal("Error", "Please enter email address first.", "warning");
        }
    }
  </script>

