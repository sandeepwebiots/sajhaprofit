<!-- Our Footer -->
<div class="container-fluid p-0">
    <div class="footer-marquee">
        <marquee class="marquee">
            Investment h Finetechresearch and investment advisor, client should read disclaimer, terms and conditions, refund policy of the company. Past Performance is not a Guarantee of Future Performance.Finetechresearch and investment advisor does not claim/give any assured returns, profit sharing, commitment and guaranteed plan. Please beware of fraudulent calls. We do not accept any payment in saving accounts. Free Trial client get sms or call DND number also.
        </marquee>
    </div>
</div>
<section class="ulockd-footer ulockd-pdng0" >
    <div class="container footer-padding">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-4">
                <div class="logo-widget tac-xxsd">
                    <img src="{{url('assets/home/images/logo.png')}}" alt="footer-logo.png">
                </div>
            </div>
            <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-4">
                <div class="font-icon-social text-center">
                    <ul class="list-inline footer-font-icon">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-feed"></i></a></li>
                        <li><a href="#"><i class="fa fa-google"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-4">
                <div class="payment-card tac-xsd">
                    <img class="pull-right fn-xsd" src="{{url('assets/home/images/resource/payment.png')}}" alt="payment.png">
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-4">
                <div class="ulockd-footer-newsletter">
                    <h3 class="title">News Letter</h3>
                    <div class="ulockd-mailchimp">
                        <div class="input-group">
                            <input type="text" name="email" id="subscribe_email" class="form-control input-md"
                                placeholder="Your email">
                            <span class="input-group-btn">
                                <button type="button" onclick="addSubscribe()" class="btn btn-md"><i class="icon flaticon-right-arrow"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-xxs-6 col-xs-6 col-sm-4 col-md-4">
                <div class="link-widget">
                    <ul class="list-style-square">
                        <li><a href="{{url('/')}}"> Home</a></li>
                        <li><a href="{{url('/about')}}"> About Us</a></li>
                        <li><a href="{{url('/explore')}}"> Explore</a></li>
                        <li><a href="{{url('/service')}}"> Services</a></li>
                        <li><a href="{{url('/pricing')}}"> Pricing</a></li>
                        <li><a href="{{url('/news')}}"> News</a></li>
                        
                    </ul>
                </div>
            </div>
            <div class="col-xxs-6 col-xs-6 col-sm-4 col-md-4">
                <div class="link-widget">
                    <!-- <h3>Important Link</h3> -->
                    <ul class="list-style-square">
                        <li><a href="{{url('/contact-us')}}"> Contact Us</a></li>
                        <li><a href="{{url('/blog')}}"> Blog</a></li>
                        <li><a href="{{ url('kyc') }}"> KYC</a></li>
                        <li><a href="{{url('risk-profile')}}"> Risk Profile</a></li>
                        <li><a href="{{url('/faq')}}"> FAQs</a></li>
                         <li><a href="{{ url('chart') }}"> Chart</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xxs-12 col-xs-4 col-sm-4 col-md-4">
                <div class="mail-widget text-center">
                    <span class="icon fa fa-envelope"></span>
                    <h3>Mail Us</h3>
                    <p>{{ $mail->title }}</p>
                </div>
            </div>
            <div class="col-xxs-12 col-xs-4 col-sm-4 col-md-4">
                <div class="call-widget text-center">
                    <span class="icon fa fa-phone"></span>
                    <h3>Call Us</h3>
                    <p>+91-{{ $contact->title }}</p>
                </div>
            </div>
            <div class="col-xxs-12 col-xs-4 col-sm-4 col-md-4">
                <div class="location-widget text-center">

                    <span class="icon fa fa-map-marker"></span>

                    <h3>Find Us</h3>
                    <p>{{ $address->title }}</p>
                </div>
            </div>
        </div>
        <hr class="ulockd-mrgn60">
    </div>
    <div class="panel-footer">
    <div class="container-fluid">
        <ul class="footer_quick_links_block">
            <li><a href="{{ url('complain')  }}">  Complaint Grievance & Redressal</a></li>
            <li><a href="{{ url('refund-policy')  }}">Refund Policy</a></li>
            <li><a href="{{ url('risk-discloser')  }}">Risk Disclosure </a></li>
            <li><a href="{{ url('disclaimer')  }}">Disclaimer </a></li>
            <li><a href="{{ url('terms_condition')  }}">Terms &amp; conditions</a></li>
            <li><a href="{{ url('reviews')  }}">Reviews</a></li>
        </ul>
        <p class="copy_right_block">Copyright © 2018 | SAJHA PROFIT</p>
    </div>
</div>


    <div classs="scroll-top">
        <a class="scrollToHome ulockd-bgthm" href="#"><i class="fa fa-arrow-up"></i></a>
    </div>
</section>

{{--<i class="fas fa-arrow-to-top"></i>--}}
<!-- Wrapper End -->
<!-- end-home-section -->
<link rel="stylesheet" type="text/css" href="{{url('assets/home/css/sweetalert.css')}}">
<script src="{{url('assets/home/js/vendors.min.js')}}"></script>
<script src="{{url('assets/home/js/sweetalert.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8" async defer>
    function addSubscribe() {

        var email = $("#subscribe_email").val();
        var _token = "{{csrf_token()}}";
        if (email != '') {
            // alert('Yes Email');
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
                $.ajax({
                    type: 'post',
                    data: {
                        'email': email,
                        '_token': _token
                    },
                    url: '{{url("email-subscribe")}}',
                    success: function (data) {
                        if (data == 0) {
                            $("#subscribe_email").val('');
                            //console.log('warning');
                            swal("Warning", "You email address was already subscribed!", "warning");
                        } else if (data == 1) {
                            $("#subscribe_email").val('');
                            //console.log('success');
                            swal("Success", "Your subscribption done successfully!", "success");
                        } else {
                            //console.log('oops');
                            swal("Error", "Oops! Something went wrong, Please try again.", "error");
                        }
                    }

                });

            } else {
                swal("Warning", "Enter valid email address.", "warning");
            }
        } else {
            swal("Error", "Please enter email address first.", "warning");
        }
    }

</script>
