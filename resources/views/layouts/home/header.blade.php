                                               
<div class="header-top ulockd-bgthm">
  <div class="container-fluid">
    <div class="row">
        <div class="col-md-12 welcm-ht-main">
            <div class="welcm-ht">
                <p class="ulockd-welcntxt color-white"><strong>Call:</strong> +91-9876-5432-10</p>
                <div class="list-inline-set">
                  <div>
                    <ul class="list-inline">
                      <li>
                        @if(Sentinel::check())
                        <a class="color-white btn btn-primary" href="{{ url('logout') }}"><i class="icon-power-off"></i> Logout</a>
                        @else
                        <!-- <a class="color-white" href="{{ url('registration') }}">Sign Up</a> || -->
                        <a class="color-white btn btn-primary" href="{{ url('login') }}">Sign In</a>
                        @endif
                      </li>
                    </ul>
                  </div>
                </div>
          </div>
        </div>       
    </div>
  </div>
</div>

<!-- Header Styles -->
<header class="header-nav fixedup">
    <div class="menu-fixed-center navbar-scrolltofixed">
        <div class="container-fluid m-0 ulockd-pdng0">
            <nav class="navbar navbar-default bootsnav">
                <!-- Start Top Search -->
                <div class="top-search">
                    <div class="container-fluid m-0">
                        <div class="input-group">
                            <!-- <span class="input-group-addon"><i class="fa fa-search"></i></span> -->
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                        </div>
                    </div>
                </div>
                <!-- End Top Search -->

                <div class="container-fluid m-0">
                    <!-- Start Atribute Navigation -->
                    <!-- <div class="attr-nav">
                        <ul>

                            <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>

                        </ul>
                    </div> -->
                    <!-- End Atribute Navigation -->

                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand ulockd-main-logo2" href="{{ url('/') }}">
                            <img src="{{url('assets/home/images/logo1.png')}}" class="logo logo-scrolled img-responsive" alt="header-logo1.png" style="margin-top: 0;">
                        </a>
                    </div>
                    <!-- End Header Navigation -->

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="navbar-menu">
                      <ul class="nav navbar-nav navbar-right" data-in="flipInY">
                          <li class="nav-item {{ (Request::is('/') ? 'active ' : '') }}">
                              <a href="{{url('/')}}">Home</a>
                          </li>
                          <li class="dropdown {{ Request::is('about') ? 'active' : '' }}">
                              <a href="{{url('/about')}}">About us</a>
                          </li>
                          <li class="dropdown {{ Request::is('explore') ? 'active' : '' }}">
                              <a href="{{url('/explore')}}">Explore</a>
                          </li>
                           <li class="dropdown {{ Request::is('service') ? 'active' : '' }}">
                              <a href="{{url('/service')}}">Services</a>
                          </li>
                          <li class="dropdown {{ Request::is('pricing') ? 'active' : '' }}">
                              <a href="{{url('/pricing')}}">Pricing</a>
                          </li>
                           {{--<li class="dropdown {{ Request::is('chart') ? 'active' : '' }}">--}}
                              {{--<a href="{{url('/chart')}}">Chart</a>--}}
                          {{--</li>--}}
                            <li class="dropdown {{ Request::is('faq') ? 'active' : '' }}">
                              <a href="{{url('/faq')}}">Faq</a>
                          </li>
                           <li class="dropdown {{ Request::is('blog') ? 'active' : '' }}">
                              <a href="{{url('/blog')}}">Blogs</a>
                          </li>
                          <li class="dropdown {{ Request::is('contact') ? 'active' : '' }}">
                              <a href="{{url('/contact')}}">Contact Us</a>
                          </li>
                          <!-- <li class="dropdown {{ request()->is('technical-analysis') ? 'active' : '' }}">
                              <a href="{{url('/technical-analysis')}}">Technical Analysis </a>
                          </li> -->
                            @if(Sentinel::check())
                          <li class="dropdown">
                            @php
                              $slug = Sentinel::getUser()->roles()->first()->slug;
                            @endphp
                            @if($slug == "m12") 
                              <a  href="{{ url('/history') }}">History</a>
                            @elseif($slug == 'super_admin')
                              <a href="{{ url('dashboard') }}">Dashboard</a>
                            @else
                              <a  href="{{ url('dashboard') }}">Dashboard</a>
                            @endif
                          </li>
                           @endif
                          @if(Sentinel::check())
                          <div class="set-login">
                              <a class="color-white btn btn-primary " href="{{ url('logout') }}">Logout</a>
                          </div>
                          @else
                              <div class="set-login">
                                  <a class="color-white btn btn-primary " href="{{ url('login') }}">Sign In</a>
                              </div>
                          @endif
                      </ul>


                  </div><!-- /.navbar-collapse -->
              </div> 
            </nav>
        </div>
    </div>
</header>

