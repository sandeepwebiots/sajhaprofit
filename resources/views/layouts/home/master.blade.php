<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">  
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title> @yield('title') </title>

  @include('layouts.home.head')

  @yield('style')

  <link rel="shortcut icon" type="text/css" href="{{URL::asset('assets/home/images/favicon.ico')}}" sizes="128x128"  type="image/x-icon" />
    <link rel="shortcut icon" type="text/css" href="{{URL::asset('assets/home/images/favicon.ico')}}" sizes="128x128"  />
</head>
<body>
<div class="wrapper">
  <!-- <div id="preloader" class="preloader">
    <div id="pre" class="preloader_container"><div class="preloader_disabler btn btn-default">Disable Preloader</div>
    </div>
  </div> -->
  @include('layouts.home.header')

  @yield('content')

  @include('layouts.home.footer')

  @include('layouts.home.footer_script')

  @yield('script')

</div>
</body>
</html>
