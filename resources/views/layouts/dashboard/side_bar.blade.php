@php
$slug = Sentinel::getUser()->roles()->first()->slug;
@endphp
<!-- sidebar -->

@if($slug == "m12")
<div class="page-sidebar custom-scrollbar">
    <div class="sidebar-user text-center">
        @if(Sentinel::getUser()->profile)
        <div>
            <img class="img-50 rounded-circle" src="{{ URL::asset('assets/upload/images')}}/{{Sentinel::getUser()->profile}}" alt="user">
        </div>
        @else
        <div>
            <img class="img-50 rounded-circle" src="{{ URL::asset('assets/dashboard/images/1.jpg')}}" alt="user">
        </div>
        @endif
        <h6 class="mt-3 f-12">{{ Sentinel::getUser()->user_name }}</h6>
    </div>
    <ul class="sidebar-menu">
        <li class="active">
            <div class="sidebar-title">General</div>
            <a href="dashboard" class="sidebar-header {{{ (Request::is('dashboard') ? 'active' : '') }}}">
                <i class="icon-blackboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <li class="active">
            <a href="{{url ('profile_edit_user')}}" class="sidebar-header {{{ (Request::is('profile_edit_user') ? 'active' : '') }}}">
              <i class="icon-blackboard"></i><span>Profile</span></a>
        </li>
       <!--  <li class="active">
            <a href="#" class="sidebar-header">
              <i class="icon-blackboard"></i><span>2 Fa Authenticate</span></a>
        </li> -->
    </ul>
</div>

@elseif($slug == "super_admin")

   <div class="page-sidebar custom-scrollbar">
    <div class="sidebar-user text-center">
        @if(Sentinel::getUser()->profile)
        <div>
            <img class="img-50 rounded-circle" src="{{ URL::asset('assets/upload/images')}}/{{Sentinel::getUser()->profile}}" alt="user">
        </div>
        @else
        <div>
            <img class="img-50 rounded-circle" src="{{ URL::asset('assets/dashboard/images/1.jpg')}}" alt="user">
        </div>
        @endif
        <h6 class="mt-3 f-12">{{Sentinel::getUser()->user_name}}</h6>
    </div>
    <ul class="sidebar-menu">
    @php $id = json_decode($permission->permissions);  @endphp
    @php $dd = $modules->groupBy('parent'); @endphp

    @foreach($dd as $key => $parent)
        <li>
          @if($key)
          <a href="#" class="sidebar-header">
                <i class="{{ $parent[0]->icons }}"></i> <span>{{ $key }}</span>
                <i class="fa fa-angle-right pull-right"></i>
          </a>
            <ul class="sidebar-submenu">
              @foreach($parent as $module)
                <li><a href="{{ url($module->link) }}"><i class="{{ $module->icons }}"></i> {{ $module->name }}</a></li>
                @endforeach
            </ul>
            @else
                @foreach($parent as $module)
                  <li><a class="sidebar-header" href="{{ url($module->link) }}"><i class="{{ $module->icons }}"></i> {{ $module->name }}</a></li>
                @endforeach
            @endif
        </li>
    @endforeach
    </ul>
</div>
@elseif($slug == "m11")
<div class="page-sidebar custom-scrollbar">
    <div class="sidebar-user text-center">
        @if(Sentinel::getUser()->profile)
        <div>
            <img class="img-50 rounded-circle" src="{{ URL::asset('assets/upload/images')}}/{{Sentinel::getUser()->profile}}" alt="user">
        </div>
        @else
        <div>
            <img class="img-50 rounded-circle" src="{{ URL::asset('assets/dashboard/images/1.jpg')}}" alt="user">
        </div>
        @endif
        <h6 class="mt-3 f-12">{{ Sentinel::getUser()->user_name }}</h6>
    </div>
    <ul class="sidebar-menu">
        <li class="active">
            <div class="sidebar-title">General</div>
            <a href="{{ url('dashboard') }}" class="sidebar-header {{{ (Request::is('dashboard') ? 'active' : '') }}}">
                <i class="icofont icofont-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <li class="active">
            <a href="{{ url('lead') }}" class="sidebar-header {{{ (Request::is('lead') ? 'active' : '') }}}">
              <i class="fa fa-tasks"></i><span>Today Leads</span></a>
        </li>
        <li class="active">
            <a href="{{ url('all-leads') }}" class="sidebar-header {{{ (Request::is('all-leads') ? 'active' : '') }}}">
              <i class="fa fa-tasks"></i><span>All Leads</span></a>
        </li>
        <li class="active">
            <a href="{{ url('payment-history') }}" class="sidebar-header {{{ (Request::is('payment-history') ? 'active' : '') }}}">
              <i class="fa fa-history"></i><span>Payment History</span></a>
        </li>
        @if($franch_tc)
        @else
            <li class="active">
                <a href="{{ url('assigned-areas') }}" class="sidebar-header {{{ (Request::is('assigned-areas') ? 'active' : '') }}}">
                  <i class="fa fa-map-marker"></i><span>Assigned area</span></a>
            </li>
        @endif
    </ul>
</div>
@else
<div class="page-sidebar custom-scrollbar">
    <div class="sidebar-user text-center">
        
        @if(Sentinel::getUser()->profile)
        <div>
            <img class="img-50 rounded-circle" src="{{ URL::asset('assets/upload/images')}}/{{Sentinel::getUser()->profile}}" alt="user">
        </div>
        @else
        <div>
            <img class="img-50 rounded-circle" src="{{ URL::asset('assets/dashboard/images/1.jpg')}}" alt="user">
        </div>
        @endif
        <h6 class="mt-3 f-12">{{Sentinel::getUser()->user_name}}</h6>
    </div>
    <ul class="sidebar-menu">
      @php $id = json_decode($permission->permissions); @endphp
    @foreach($modules as $module)
      @foreach($id as $i)
          @if($module->id == $i)
            <li class="active">
                <a href="{{url($module->link)}}" class="sidebar-header">
                    <i class="{{ $module->icons }}"></i> <span>{{ $module->name }}</span>
                </a>
            </li>
          @endif
      @endforeach
    @endforeach

    @if($slug == "m10")
        <li class="active">
            <a href="{{ url('quick-inquiry') }}" class="sidebar-header {{{ (Request::is('quick-inquiry') ? 'active' : '') }}}">
              <i class="fa fa-tasks"></i><span>Quick Inquiry</span></a>
        </li>
        <li class="active">
            <a href="{{ url('leads') }}" class="sidebar-header {{{ (Request::is('leads') ? 'active' : '') }}}">
              <i class="fa fa-tasks"></i><span>Leads</span></a>
        </li>
        <li class="active">
            <a href="{{ url('lead-assigned') }}" class="sidebar-header {{{ (Request::is('lead-assigned') ? 'active' : '') }}}">
              <i class="fa fa-indent"></i><span>Lead Assigned</span></a>
        </li>
        <li class="active">
            <a href="{{ url('payment-history') }}" class="sidebar-header {{{ (Request::is('payment-history') ? 'active' : '') }}}">
              <i class="fa fa-history"></i><span>Payment History</span></a>
        </li>
    @endif
    </ul>
</div>

@endif
<!-- sidebar end -->