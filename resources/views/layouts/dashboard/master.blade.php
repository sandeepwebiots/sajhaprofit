<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('title')</title>
	<link rel="icon" href="{{ URL::asset('assets/dashboard/images/favicon.ico') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ URL::asset('assets/dashboard/images/favicon.ico') }}" type="image/x-icon"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    @include('layouts.dashboard.head')
    @yield('style')
</head>
<body>
<!-- Loader starts -->
<!-- <div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </div>
</div> -->
<!-- Loader ends -->
<div class="page-wrapper">
	@include('layouts.dashboard.header')

	<div class="page-body-wrapper">
		@include('layouts.dashboard.side_bar')
		@yield('content')
	</div>
</div>
@include('layouts.dashboard.footer_script')
@yield('script')
</body>
</html>