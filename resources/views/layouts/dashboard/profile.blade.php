@extends('layouts.dashboard.master')

@section('title') Profile Dashboard @endsection
@section('style')

@endsection
@section('content')
<?php
  $slug = Sentinel::getUser()->roles()->first()->slug;
?>
<style type="text/css">
  .upload-btn-wrapper {
  position: relative;
  overflow: hidden;
  display: inline-block;
}

.btn {
  border: 2px solid gray;
  color: gray;
  background-color: white;
  padding: 8px 20px;
  border-radius: 8px;
  font-size: 20px;
  font-weight: bold;
}

.upload-btn-wrapper input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
</style>
<div class="page-body">
  <!-- Container-fluid starts -->
  <div class="container-fluid">
      <div class="page-header">
          <div class="row">
              <div class="col-lg-6" data-intro="This is the name of this site">
                  <h3>Profile
                      <small>User Dashboard</small>
                  </h3>
              </div>
              <div class="col-lg-6" data-intro="This is the name of this site">
                  <ol class="breadcrumb pull-right">
                      <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                      <li class="breadcrumb-item">Dashboard</li>
                      <li class="breadcrumb-item active">Profile</li>
                  </ol>
              </div>
          </div>
      </div>
      <!-- @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif -->

      @if(session()->has('success'))
          <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session()->get('success') }}
          </div>
      @endif
      @if(session()->has('error'))
          <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session()->get('error') }}
          </div>
      @endif
      
      
      
  </div>
  <!-- Container-fluid Ends -->
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="card" >
          <div class="card-header text-center">
            <h5><b><u>Profile Picture </u></b></h5>
          </div>
          @if(session()->has('success-picture'))
              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {{ session()->get('success-picture') }}
              </div>
          @endif
          @if(session()->has('error-picture'))
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {{ session()->get('error-picture') }}
              </div>
          @endif
          @if($slug == 'm12')
          <form  enctype="multipart/form-data" action=" {{url('profile_image')}}/{{Sentinel::getUser()->id}}" method="post"> 
          @elseif($slug == 'super_admin')  
          <form  enctype="multipart/form-data" action=" {{url('profile-image')}}/{{Sentinel::getUser()->id}}" method="post">
          @else 
          <form  enctype="multipart/form-data" action=" {{url('profile-image')}}/{{Sentinel::getUser()->id}}" method="post"> 
          @endif              
            <div class="sidebar-user text-center">
              <br>
                <div>
                  @if(Sentinel::getUser()->photo == '')
                     <img class="" src="{{ URL::asset('assets/upload/images')}}/{{Sentinel::getUser()->profile}}" alt="user" style="height: 200px; width: 200px; border-radius: 10px" >
                  @else                   
                    <img class="" src="{{ URL::asset('assets/dashboard/images')}}/3.png" alt="avatar" style="height: 200px; width: 200px; border-radius: 10px">
                  @endif
                </div>
                <h6 class="mt-3 f-12">{{ Sentinel::getUser()->user_name }}</h6>
            </div>
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="profile text-center">
              <div class="p-relative">
                <div class="upload-btn-wrapper">
                  <button class="btn">Upload a Picture</button>
                  <input type='file' name="profile" accept="jpeg,png,jpg,gif,svg|max:2048"/>
                </div>
              </div>
              <div>
                <button type="submit" class="close-icon btn"><i class="glyphicon glyphicon-remove"></i>Update</button>
              </div>
              <br>
            </div>
          </form>
          </form>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
            <div class="card-header text-center">
                <h5><b><u>Profile Details</u></b></h5>
            </div>
             @if(session()->has('success-profile'))
                <div class="alert alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session()->get('success-profile') }}
                </div>
            @endif 
            <div class="container">
              <div class="col-md-12">
                <form class="form-horizontal theme-form row" action="@if($slug=='m12') {{url('/profile_details')}}/{{Sentinel::getUser()->id}} @elseif($slug=='super_admin') {{url('/profile-details')}}/{{Sentinel::getUser()->id}} @else {{url('/profile-details')}}/{{Sentinel::getUser()->id}} @endif" method="post">
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                  <input type="hidden" name="user_id" value="{{$user->id}}">
                  <div class="form-group col-md-12 mt-32 " style="margin-top: 30px;">
                      <label for="firstname">First Name</label>
                      <input type="text" class="form-control" name="first_name" value="{{$user->first_name}}" id="fullname" autocomplete="off" required="Enter First Name" >
                      @if ($errors->has('first_name'))
                        <div class="error">{{ $errors->first('first_name') }}</div>
                      @endif
                  </div>
                  <div class="form-group col-md-12 mt-32">
                      <label for="lastname">Last Name</label>
                      <input type="text" class="form-control" name="last_name" value="{{$user->last_name}}" id="lastname" autocomplete="off" required="Enter Last Name">
                      @if ($errors->has('last_name'))
                          <div class="error">{{ $errors->first('last_name') }}</div>
                      @endif
                  </div>
                  <div class="form-group col-md-12 mt-32">
                      <label for="username">User Name</label>
                      <input type="text" class="form-control" disabled="" value="{{$user->user_name}}" id="username" autocomplete="off">
                  </div>
                  <div class="form-group col-md-12 mt-32">
                    <label for="Email1">Email address</label>
                    <input type="email" class="form-control" disabled="" value="{{$user->email}}" id="email" autocomplete="off">
                  </div>
                  <div class="form-group col-md-12 text-right mt-2">
                    <button type="submit" class="btn theme-btn mt-4">Update</button>
                  </div>
                </form>
              </div>
            </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">  
          <div class="card-header text-center">
              <h5><b>Change Password</b></h5>
          </div>     
          @if(session()->has('success-password'))
              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {{ session()->get('success-password') }}
              </div>
          @endif
          @if(session()->has('error-password'))
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  {{ session()->get('error-password') }}
              </div>
          @endif
          <div class="container">       
            <div class="col-md-12">
              <form class="form-horizontal theme-form row" action="@if($slug=='m12') {{url('/change_password')}}/{{Sentinel::getUser()->id}} @elseif($slug=='super_admin') {{url('/change-password')}}/{{Sentinel::getUser()->id}} @else {{url('/change-password')}}/{{Sentinel::getUser()->id}} @endif" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="user_id" value="{{Sentinel::getUser()->id}}">
                
                <div class="form-group col-md-12 mt-32" style="margin-top: 30px;">
                    <label for="oldpassword">Currrent Password</label>
                    <input type="password" class="form-control" name="current_password" id="oldpassword" autocomplete="off" placeholder="Enter Current Password">
                    @if ($errors->has('current_password'))
                        <strong class="text text-danger">{{ $errors->first('current_password') }}</strong>
                    @endif
                    @if(session()->has('error-password-invalid'))
                        <strong class="text text-danger">  {{ session()->get('error-password-invalid') }} </strong>
                    @endif
                    @if(session()->has('error-enter-oldpassword'))
                        <strong class="text text-danger">  {{ session()->get('error-enter-oldpassword') }} </strong>
                    @endif
                    
                </div>
                <div class="form-group col-md-12 mt-32">
                    <label for="newpassword">New Password</label>
                    <input type="password" class="form-control" name="new_password" id="newpassword" autocomplete="off" placeholder="Enter New Password">
                    @if ($errors->has('new_password'))
                        <strong class="text text-danger">{{ $errors->first('new_password') }} </strong>
                    @endif
                </div>
                <div class="form-group col-md-12 mt-32">
                    <label for="repassword">Re-Password</label>
                    <input type="password" class="form-control" name="confirm_password" id="repassword" autocomplete="off" placeholder="Enter Re-Enter Password">
                    @if ($errors->has('confirm_password'))
                        <strong class="text text-danger">{{ $errors->first('confirm_password') }}</strong>
                    @endif
                </div>
                <div class="form-group col-md-12 mt-32">
                </div>
                <div class="form-group col-md-12 text-right mt-2">
                  <button type="submit" class="btn theme-btn mt-4">Change Password</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
        