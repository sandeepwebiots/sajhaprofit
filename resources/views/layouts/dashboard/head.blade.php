<!--Google font-->
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ url('assets/dashboard/css/font-awesome.min.css') }}">

<!-- ico css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/icofont.css') }}">
<!-- chart css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/chartist.css') }}">

<!-- fontawesome css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/fontawesome.css') }}">

<!-- flagicon css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/flag-icon.css') }}">

<!-- themify css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/themify.css') }}">

<!-- Owl css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/owlcarousel.css') }}">

<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/bootstrap.css') }}">

<!-- App css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/style1.css') }}">

<!-- custom css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/custom.css') }}">

<!-- Responsive css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/responsive.css') }}">

<!-- Datatable css -->
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatable-extension.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/date-picker.css') }}" />



