
<!-- latest jquery-->
<script src="{{ URL::asset('assets/dashboard/js/jquery-3.2.1.min.js') }}" ></script>
<script src="{{ URL::asset('assets/dashboard/js/jquery.ui.min.js') }}" ></script>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<!-- Bootstrap js-->
<script src="{{ URL::asset('assets/dashboard/js/popper.min.js') }}" ></script>
<script src="{{ URL::asset('assets/dashboard/js/bootstrap.js') }}" ></script>

<!-- owlcarousel js-->
<script src="{{ URL::asset('assets/dashboard/js/owl.carousel.js') }}" ></script>
<!-- Sidebar jquery-->
<script src="{{ URL::asset('assets/dashboard/js/sidebar-menu.js') }}" ></script>
<!-- Theme js-->


<!-- chart js-->
<script src="{{ URL::asset('assets/dashboard/js/chartist.js') }}" ></script>
<!-- <script src="{{ URL::asset('assets/dashboard/js/chartist-custom.js') }}" ></script> -->
<!-- sparkline js-->
<script src="{{ URL::asset('assets/dashboard/js/sparkline.js') }}" ></script>
<!-- dashboard-business js-->
<script src="{{ URL::asset('assets/dashboard/js/dashboard-business.js') }}" ></script>
<!-- <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script> -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	@include('sweet::alert')

<!-- <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script> -->
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->

<!--Data table script-->
<script src="{{ URL::asset('assets/dashboard/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/datatable.custom.js') }}"></script>

<!-- <script src="{{ URL::asset('assets/dashboard/js/datatable-extension/jquery.dataTables.min.js') }}"></script> -->
<script src="{{ URL::asset('assets/dashboard/js/datatable-extension/dataTables.buttons.min.js') }}"></script>
<script src=" {{ URL::asset('assets/dashboard/js/datatable-extension/buttons.colVis.min.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/datatable-extension/jszip.min.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/datatable-extension/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/datatable-extension/vfs_fonts.js') }}"></script>
<!-- <script src="{{ URL::asset('assets/dashboard/js/datatable-extension/autoFill.min.js') }}"></script> -->
<script src="{{ URL::asset('assets/dashboard/js/datatable-extension/dataTables.select.min.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/datatable-extension/buttons.print.min.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/datatable-extension/buttons.html5.min.js') }}"></script>
<script src=" {{ URL::asset('assets/dashboard/js/datatable-extension/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/datatable-extension/dataTables.responsive.min.js') }}"></script>
<script src=" {{ URL::asset('assets/dashboard/js/datatable-extension/responsive.bootstrap4.min.js') }}"></script>
<script src=" {{ URL::asset('assets/dashboard/js/datatable-extension/custom.js') }}"></script>
<!-- Datatable script end -->
<script src="{{ URL::asset('assets/dashboard/js/date-picker/datepicker.custom.js') }}"></script>
<!-- counter -->
<script src="{{ URL::asset('assets/dashboard/js/counter/counter-custom.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/counter/jquery.counterup.min.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/counter/jquery.waypoints.min.js') }}"></script>
<!-- ckeditor -->
<script src="{{ URL::asset('assets/dashboard/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/ckeditor/styles.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/ckeditor/adapters/jquery.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/ckeditor/ckeditor.custom.js') }}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="{{ URL::asset('assets/dashboard/js/script.js') }}" ></script>

<script>
	// active link
for (var i = 0; i < document.links.length; i++) {
    if (document.links[i].href == document.URL) {
        document.links[i].className += ' active';
        document.links[i].parentElement.parentElement.parentElement.className += ' active';
    }
}
</script>

