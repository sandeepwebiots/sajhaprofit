@extends('layouts.dashboard.master')

@section('title') Staff | Dashboard @endsection

@section('style')    
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatable-extension.css') }}" />
@endsection

@section('content')
 <div class="page-body user-management">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>Staff Management
                            </h5>
                        </div>
                        <div class="col-lg-6">
                            <ol class="breadcrumb pull-right">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                                <li class="breadcrumb-item active">Staff Management</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Staff Management</h5>
                                <a href="{{ route('staff-manage.create') }}" class="btn btn-primary pull-right">Add Staff</a>
                            </div>
                            <div class="card-body">
                                <div class="dt-ext table-responsive">
                                    <table id="export-button" class="display">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Status</th>
                                            <th>Details</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                          @php $i=1; @endphp

                                          @foreach($staff as $staf)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $staf->user_name }}</td>
                                                <td>@if(isset($staf->designation->post))
                                                {{ $staf->designation->post }}@endif</td>
                                                <td>@if($staf->status == 0)
                                                    <span class="badge badge-primary">New Join</span> @elseif($staf->status == 1)
                                                    <span class="badge badge-success">Active</span>
                                                    @endif
                                                </td>
                                                <td><a href="{{ url('staff-detial') }}/{{ $staf->id }}" class="btn btn-primary">Details</a></td>
                                                <td><a href="{{ url('staff-edit') }}/{{ $staf->id }}" class="btn btn-info"><i class="fa fa-pencil"></i>&nbsp; Edit</a>
                                                    <!-- <a href="{{ url('staff-delete') }}/{{ $staf->id }}" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp; Delete</a> --></td>
                                            </tr>
                                          @endforeach
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

