@extends('layouts.dashboard.master')

@section('title') Staff Form | Dashboard @endsection

@section('style')    
<link rel="stylesheet" type="text/css" href="{{ url('assets/dashboard/css/jquery-ui.css') }}">
@endsection

@section('content')
@php $slug = Sentinel::getUser()->roles()->first()->slug @endphp
<div class="page-body">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>Form</h5>
                        </div>
                        <div class="col-lg-6">
                            <ol class="breadcrumb pull-right">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item active">Form</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="invoice">
                                    <div>
                                        <div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <img src="{{ URL::asset('assets/dashboard/images/logo.png')}}" style="width: 95px;" class="media-object " alt="">
                                                        </div>
                                                        <div class="media-body m-l-20">
                                                            <!-- <h4 class="media-heading">Universal</h4>
                                                            <p> hello@universal.in<br>
                                                                <span class="digits">289-335-6503</span>
                                                            </p> -->
                                                        </div>
                                                    </div>
                                                    <!--End Info-->
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="text-md-right">
                                                        <h3>Staff Form</h3>
                                                    </div><!--End Title-->
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <!--End InvoiceTop-->
                                        
                                        <!--End Invoice Mid-->
                                        <div>
                                            <br>
                                            <!-- form start -->
                                
                                <form method="post" action="{{ url('upload-details') }}" enctype="multipart/form-data">
                                
                                    {{ csrf_field() }}
                                    <div class="form-row">
                                        @if($slug == 'm10')
                                        <div class="col-sm-12">
                                            <div class="card-body megaoptions-border-space-sm">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="card">
                                                            <div class="media p-20">
                                                                <div class="radio radio-secondary mr-3">
                                                                    <input type="radio" name="staff" id="staff" value="0" onclick="errorhide();">
                                                                    <label for="staff"></label>
                                                                </div>
                                                                <div class="media-body">
                                                                    <h6 class="mt-0 mega-title-badge">Telecaller [Staff]</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="card">
                                                            <div class="media p-20">
                                                                <div class="radio radio-secondary mr-3">
                                                                    <input type="radio" name="staff" id="otherstaff" value="1">
                                                                    <label for="otherstaff"></label>
                                                                </div>
                                                                <div class="media-body">
                                                                    <h6 class="mt-0 mega-title-badge">Other</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <strong class="text-danger" id="error-otherstaff"></strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-md-4 mb-3">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Name" autocomplete="off">
                                            @if($errors->has('name'))<strong class="text-danger">{{ $errors->first('name') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="last_name">Father's Name / Husband Name</label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}" placeholder="Father's Name / Husband Name" autocomplete="off">
                                            @if($errors->has('last_name'))<strong class="text-danger">{{ $errors->first('last_name') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Email" autocomplete="off">
                                            @if($errors->has('email'))<strong class="text-danger">{{ $errors->first('email') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="birth">Date of Birth</label>
                                            <input type="text" id="date_picker-date" name="birth" value="{{ old('birth') }}" class="datepicker-here form-control digits" data-language='en' data-multiple-dates-separator=", " data-position='bottom left' placeholder="Date of Birth" autocomplete="off"/>
                                            @if($errors->has('birth'))<strong class="text-danger">{{ $errors->first('birth') }}</strong>@endif
                                            
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="mobile_no">Mobile No</label>
                                            <input type="number" class="form-control" name="mobile_no" value="{{ old('mobile_no') }}" id="mobile_no" placeholder="Mobile No" autocomplete="off">
                                            @if($errors->has('mobile_no'))<strong class="text-danger">{{ $errors->first('mobile_no') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="gender">Gender</label>
                                            <div class="row">
                                               <div class="col">
                                                  <div class="form-group m-t-15 m-checkbox-inline">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="gender" id="male"  value="male" @if(old("gender") == 'male') checked @endif>
                                                        <label for="male" class="mb-0">Male</label>
                                                    </div>  
                                                     <div class="radio radio-info">
                                                        <input type="radio" name="gender" id="female" value="female" @if(old("gender") == 'female') checked @endif>
                                                        <label for="female" class="mb-0">Female</label>
                                                    </div>

                                                  </div>
                                               </div>
                                            </div>
                                            @if($errors->has('gender'))<strong class="text-danger">{{ $errors->first('gender') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="adhaar_no">Adhaar Card</label>
                                            <input type="number" class="form-control" name="adhaar_no" value="{{ old('adhaar_no') }}" id="adhaar_no" placeholder="0000 0000 0000" autocomplete="off">
                                            @if($errors->has('adhaar_no'))<strong class="text-danger">{{ $errors->first('adhaar_no') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="pan_no">PAN Card</label>
                                            <input type="text" class="form-control" name="pan_no" value="{{ old('pan_no') }}" id="pan_no" placeholder="PAN Card" autocomplete="off">
                                            @if($errors->has('pan_no'))<strong class="text-danger">{{ $errors->first('pan_no') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="driving_li">Driving Licence</label>
                                            <input type="text" class="form-control" name="driving_li" value="{{ old('driving_li') }}" id="driving_li" placeholder="Driving Licence" autocomplete="off">
                                            @if($errors->has('driving_li'))<strong class="text-danger">{{ $errors->first('driving_li') }}</strong>@endif
                                        </div>
                                         <div class="col-md-12 mb-3">
                                            <label for="address">Address</label>
                                            <input type="text" class="form-control" name="address" value="{{ old('address') }}" id="address" placeholder="Address" autocomplete="off">
                                            @if($errors->has('address'))<strong class="text-danger">{{ $errors->first('address') }}</strong>@endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label for="state">State</label>
                                            <select name="state" id="state" class="form-control">
                                                <option name="state" value="">Select State</option>
                                                @foreach($state as $sta)
                                                <option name="state" value="{{ $sta->state }}" @if(old("state") == $sta->state) selected @endif>{{ $sta->state }}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('state'))<strong class="text-danger">{{ $errors->first('state') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="city">City</label>
                                            <select name="city" id="city" class="form-control">
                                                <option name="city" value="">Select City</option>
                                            </select>
                                            @if($errors->has('city'))<strong class="text-danger">{{ $errors->first('city') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="zip_code">Zip</label>
                                            <input type="number" class="form-control" name="zip_code" value="{{ old('zip_code') }}" id="zip_code" placeholder="Zip" autocomplete="off">
                                            @if($errors->has('zip_code'))<strong class="text-danger">{{ $errors->first('zip_code') }}</strong>@endif
                                        </div>
                                         <div class="col-md-4 mb-3">
                                            <label for="education">Education</label>
                                            <input type="text" class="form-control" name="education" value="{{ old('education') }}" id="education" placeholder="Qaulification" autocomplete="off">
                                            @if($errors->has('education'))<strong class="text-danger">{{ $errors->first('education') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="school">School / College</label>
                                            <input type="text" class="form-control" name="school" value="{{ old('school') }}" id="school" placeholder="School / College" autocomplete="off">
                                            @if($errors->has('school'))<strong class="text-danger">{{ $errors->first('school') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="ref_1">Reference person 1</label>
                                            <input type="text" class="form-control" name="ref_1" value="{{ old('ref_1') }}" id="ref_1" placeholder="Reference" autocomplete="off">
                                            @if($errors->has('ref_1'))<strong class="text-danger">{{ $errors->first('ref_1') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="ref_2">Reference person 2</label>
                                            <input type="text" class="form-control" name="ref_2" value="{{ old('ref_2') }}" id="ref_2" placeholder="Reference" autocomplete="off">
                                            @if($errors->has('ref_2'))<strong class="text-danger">{{ $errors->first('ref_2') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="experience">Experience</label>
                                            <input type="text" class="form-control" name="experience" value="{{ old('experience') }}" id="experience" placeholder="Experience" autocomplete="off">
                                            @if($errors->has('experience'))<strong class="text-danger">{{ $errors->first('experience') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="company">Company Name</label>
                                            <input type="text" class="form-control" name="company" value="{{ old('company') }}" id="company" placeholder="Company Name" autocomplete="off">
                                            @if($errors->has('company'))<strong class="text-danger">{{ $errors->first('company') }}</strong>@endif
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <div class="">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="media">
                                                            <div class="media-left">
                                                                <img src="{{ URL::asset('/assets/dashboard/images/user/pic.jpg') }}" class="media-object rounded-circle img-60" alt="" id="change">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5 mb-3">
                                            <div class="file-upload-1">
                                            <label for="profile_pic" class="file-upload__label">Upload Photograph</label>
                                            <input id="profile_pic" class="file-upload__input" type="file" name="profile_pic" onchange="uploadPic(this);">
                                            </div>
                                            @if($errors->has('profile_pic'))<strong class="text-danger">{{ $errors->first('profile_pic') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <div class="file-upload-1">
                                            <label for="resume" class="file-upload__label">Upload Resume</label>
                                            <input id="resume" class="file-upload__input" type="file" name="resume" >
                                            </div>
                                            @if($errors->has('resume'))<strong class="text-danger">{{ $errors->first('resume') }}</strong>@endif
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                    @if($slug == 'm8' || $slug == 'm9' || $slug == 'm10')
                                    @else
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label class="hedding">User Name</label>
                                            <input type="text" name="user_name" class="form-control" placeholder="User name">
                                            @if($errors->has('user_name'))<strong class="text-danger">{{ $errors->first('user_name') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label class="hedding">Position</label>
                                            <select class="form-control" name="role" id="role">
                                                    <option value="">Select</option>
                                                @foreach($roles as $role)
                                                    <option class="role" name="role" value="{{ $role->slug }}" id="{{ $role->slug }}">{{ $role->name }}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('role'))<strong class="text-danger">{{ $errors->first('role') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label class="hedding">Post</label>
                                            <select name="designation_id" id="post" class="form-control form-control-success btn-square" onchange="alredygive(this);">
                                                <option name="designation_id" value="">Select Post</option>
                                            </select>
                                            @if($errors->has('designation_id'))<strong class="text-danger">{{ $errors->first('designation_id') }}</strong>@endif
                                            <strong class="text-danger" id="alredy-staff"></strong>
                                        </div>
                                    </div>
                                    @endif
                                    <!-- @if($slug == 'm10')
                                    @else
                                    <div class="hedding">
                                        <label>Sidebar Permission</label> 
                                    </div>
                                    <div class="form-row col-md-12 mb-3">
                                       @foreach($sidebar  as $side)
                                       
                                        <?php $per = Sentinel::getUser()->permissions; 

                                            if(in_array($side->id, $per) || $slug == 'super_admin'){
                                         ?>
                                        <div class="media">
                                            <label class="col-form-label m-r-10 m-l-10">{{ $side->name }}</label>
                                            <div class="media-body text-right icon-state switch-outline">
                                                <label class="switch">
                                                    <input name="sidebarid[]" value="{{ $side->id }}" type="checkbox">
                                                    <span class="switch-state bg-info"></span>
                                                </label>
                                            </div>
                                        </div>
                                            <?php }   ?>
                                        @endforeach 
                                        @if($errors->has('sidebarid'))<strong class="text-danger">{{ $errors->first('sidebarid') }}</strong>@endif
                                    </div>
                                    @endif -->
                                    <hr />
                                    <button class="btn btn-success"  id="form_submit" type="submit">Submit form</button>
                                </form>

                                <!-- form -->
                                            
                                        </div>
                                    </div>
                                </div>
                                <!-- End Invoice Holder-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('script')
<script>
    // var dates = $("#date_picker").datepicker({
    //     dateFormat: 'dd/mm/yy',
    //     // minDate: dateToday,
    //     maxDate: '-21y',
    // });


    $( function() {
        $( "#date_picker-date" ).datepicker({
            dateFormat: 'dd-mm-yy',
            maxDate: '-21y',
        });

      } );

    function uploadPic(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#change').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<script>
    $('#role').on('change', function() {

        var slug =jQuery(this).val();
        var subservice='';
        // console.log(slug);
        if (slug != '') {

            $.ajax({
            method: 'GET',
            url: '/designation-id/'+slug,
            success: function(response){
                    // console.log(response);
                    if (response.length>0) 
                    {
                        for(var i=0;i<response.length;i++)
                        {
                            subservice+='<option name="designation_id" value="'+response[i]["id"]+'">'+response[i]["post"]+'</option>';
                        }
                    }else
                    {
                        subservice = '<option value="">No service</option>';
                    }
                    $('#post').html(subservice);
                    alredygive();
                },
                error: function(jqXHR, textStatus, errorThrown) { 
                    subservice = '<option value="">No service</option>';
                    $('#post').html(subservice);
                    $('#alredy-staff').html('');
                }
            });
        }else{
            subservice = '<option value="">No service</option>';
            $('#post').html(subservice);
            $('#alredy-staff').html('');
        }
    });

    function alredygive(val){
        var post = $('#post').val();
        var error = '';

        $.ajax({
            method:'GET',
            url: '/chek-position/'+post,
            success:function(data){
                if (data == 1) {
                    // $( ".btn-primary" ).prop( "disabled", true );
                    $('#alredy-staff').html('Alredy staff worked this department.');
                }else if (data == 0) {
                    $('#alredy-staff').html('');
                    // $( ".btn-primary" ).prop( "disabled", false );
                }
            },
            error:function(){
                $('#alredy-staff').html('');
            }
        });
    }

    $('#state').on('change', function() {

        var id =jQuery(this).val();
        var city='';
            $.ajax({
            method: 'GET',
            url: '/city/'+id,
            success: function(response){
                if (response.length>0) 
                {
                    for(var i=0;i<response.length;i++)
                    {
                        city+='<option name="city" value="'+response[i]["city_name"]+'">'+response[i]["city_name"]+'</option>';
                    }
                }else
                {
                    city = '<option value="">No service</option>';
                }
                $('#city').html(city);
            },
            error: function(jqXHR, textStatus, errorThrown) { 
                city = '<option value="">No service</option>';
                $('#city').html(city);
            }
        });
    });

    $('#otherstaff').on('click', function() {

        var id = '{{  Sentinel::getUser()->id  }}';
        var tcstaff = '';
            $.ajax({
            method: 'GET',
            url: '/franchise-tc/'+id,
            success: function(response){
                if (!$.trim(response)) 
                {
                }else
                {
                    tcstaff = 'Already added other';
                }
                $('#error-otherstaff').html(tcstaff);
            },
            error: function(jqXHR, textStatus, errorThrown) { 
                tcstaff = 'Already person added other';
                $('#error-otherstaff').html(tcstaff);
            }
        });
    });

    function errorhide()
    {
        $('#error-otherstaff').html('');
    }

   

   
</script>
@endsection