@extends('layouts.dashboard.master')

@section('title') Staff Edit | Dashboard @endsection

@section('style')    

@endsection

@section('content')
@php $slug = Sentinel::getUser()->roles()->first()->slug @endphp
<div class="page-body">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>Edit Staff Details</h5>
                        </div>
                        <div class="col-lg-6">
                            <ol class="breadcrumb pull-right">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item active">Edit Staff Details</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="invoice">
                                    <div>
                                        <div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="media">
                                                        <div class="media-left">
                                                            <img src="{{ URL::asset('assets/dashboard/images/logo.png')}}" class="media-object " style="width: 100px;" alt="">
                                                        </div>
                                                        <div class="media-body m-l-20">
                                                            <!-- <h4 class="media-heading">Universal</h4>
                                                            <p> hello@universal.in<br>
                                                                <span class="digits">289-335-6503</span>
                                                            </p> -->
                                                        </div>
                                                    </div>
                                                    <!--End Info-->
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="text-md-right">
                                                        <h3>Edit Form</h3>
                                                        <!-- <p>Issued: May<span class="digits"> 27, 2015</span><br>
                                                            Payment Due: June <span class="digits">27, 2015</span>
                                                        </p> -->
                                                    </div><!--End Title-->
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <!--End InvoiceTop-->
                                             
                                        <div>
                                            <!-- form start -->
                                <form method="post" action="{{ url('staff-update') }}/{{ $staff->id }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" id="name" name="name" value="{{ $staff->first_name }}" placeholder="Name" autocomplete="off">
                                            @if($errors->has('name'))<strong class="text-danger">{{ $errors->first('name') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="last_name">Father's Name / Husband Name</label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $staff->last_name }}" placeholder="Father's Name / Husband Name" autocomplete="off">
                                            @if($errors->has('last_name'))<strong class="text-danger">{{ $errors->first('last_name') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email" value="{{ $staff->email }}" placeholder="Email" autocomplete="off">
                                            @if($errors->has('email'))<strong class="text-danger">{{ $errors->first('email') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="birth">Date of Birth</label>
                                            <input type="date" class="form-control" name="birth" value="{{ $staff->date_birth }}" id="birth" >
                                            @if($errors->has('birth'))<strong class="text-danger">{{ $errors->first('birth') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="mobile_no">Mobile No</label>
                                            <input type="number" class="form-control" name="mobile_no" value="{{ $staff->mobile_no }}" id="mobile_no" placeholder="Mobile No" autocomplete="off">
                                            @if($errors->has('mobile_no'))<strong class="text-danger">{{ $errors->first('mobile_no') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="gender">Gender</label>
                                            <div class="row">
                                               <div class="col">
                                                  <div class="form-group m-t-15 m-checkbox-inline">
                                                    <div class="radio radio-info">
                                                        <input type="radio" name="gender" id="male" value="male" {{ $staff->gender == 'male' ? 'checked' : ''}}>
                                                        <label for="male" class="mb-0">Male</label>
                                                    </div>  
                                                     <div class="radio radio-info">
                                                        <input type="radio" name="gender" id="female" value="female" {{ $staff->gender == 'female' ? 'checked' : ''}}>
                                                        <label for="female" class="mb-0">Female</label>
                                                    </div>

                                                  </div>
                                               </div>
                                            </div>
                                            @if($errors->has('gender'))<strong class="text-danger">{{ $errors->first('gender') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="adhaar_no">Adhaar Card</label>
                                            <input type="number" class="form-control" name="adhaar_no" value="{{ $staff->adhaar_no }}" id="adhaar_no" placeholder="0000 0000 0000" autocomplete="off">
                                            @if($errors->has('adhaar_no'))<strong class="text-danger">{{ $errors->first('adhaar_no') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="pan_no">PAN Card</label>
                                            <input type="text" class="form-control" name="pan_no" value="{{ $staff->pan_no }}" id="pan_no" placeholder="PAN Card" autocomplete="off">
                                            @if($errors->has('pan_no'))<strong class="text-danger">{{ $errors->first('pan_no') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="driving_li">Driving Licence</label>
                                            <input type="text" class="form-control" name="driving_li" value="{{ $staff->driving_li }}" id="driving_li" placeholder="Driving Licence" autocomplete="off">
                                            @if($errors->has('driving_li'))<strong class="text-danger">{{ $errors->first('driving_li') }}</strong>@endif
                                        </div>
                                         <div class="col-md-12 mb-3">
                                            <label for="address">Address</label>
                                            <input type="text" class="form-control" name="address" value="{{ $staff->address }}" id="address" placeholder="Address" autocomplete="off">
                                            @if($errors->has('address'))<strong class="text-danger">{{ $errors->first('address') }}</strong>@endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label for="city">City</label>
                                            <input type="text" class="form-control" name="city" value="{{ $staff->city }}" id="city" placeholder="City" autocomplete="off">
                                            @if($errors->has('city'))<strong class="text-danger">{{ $errors->first('city') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="state">State</label>
                                            <input type="text" class="form-control" name="state" value="{{ $staff->state }}" id="state" placeholder="State" autocomplete="off">
                                            @if($errors->has('state'))<strong class="text-danger">{{ $errors->first('state') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="zip_code">Zip</label>
                                            <input type="number" class="form-control" name="zip_code" value="{{ $staff->zip_code }}" id="zip_code" placeholder="Zip" autocomplete="off">
                                            @if($errors->has('zip_code'))<strong class="text-danger">{{ $errors->first('zip_code') }}</strong>@endif
                                        </div>
                                       <!--  <div class="col-md-4 mb-3">
                                            <label for="exampleFormControlSelect1">Level</label>
                                            <select class="form-control" id="exampleFormControlSelect1">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            </select>
                                        </div> -->
                                         <div class="col-md-4 mb-3">
                                            <label for="education">Education</label>
                                            <input type="text" class="form-control" name="education" value="{{ $staff->education }}" id="education" placeholder="Qaulification" autocomplete="off">
                                            @if($errors->has('education'))<strong class="text-danger">{{ $errors->first('education') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="school">School / College</label>
                                            <input type="text" class="form-control" name="school" value="{{ $staff->school }}" id="school" placeholder="School / College" autocomplete="off">
                                            @if($errors->has('school'))<strong class="text-danger">{{ $errors->first('school') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="ref_1">Reference person 1</label>
                                            <input type="text" class="form-control" name="ref_1" value="{{ $staff->ref_1 }}" id="ref_1" placeholder="Reference" autocomplete="off">
                                            @if($errors->has('ref_1'))<strong class="text-danger">{{ $errors->first('ref_1') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="ref_2">Reference person 2</label>
                                            <input type="text" class="form-control" name="ref_2" value="{{ $staff->ref_2 }}" id="ref_2" placeholder="Reference" autocomplete="off">
                                            @if($errors->has('ref_2'))<strong class="text-danger">{{ $errors->first('ref_2') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="experience">Experience</label>
                                            <input type="text" class="form-control" name="experience" value="{{ $staff->experience }}" id="experience" placeholder="Experience" autocomplete="off">
                                            @if($errors->has('experience'))<strong class="text-danger">{{ $errors->first('experience') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="company">Company Name</label>
                                            <input type="text" class="form-control" name="company" value="{{ $staff->company }}" id="company" placeholder="Company Name" autocomplete="off">
                                            @if($errors->has('company'))<strong class="text-danger">{{ $errors->first('company') }}</strong>@endif
                                        </div>
                                        <div class="media">
                                           @if($staff->profile)
                                            <div class="media-left">
                                                <img src="{{ URL::asset('/assets/upload/images') }}/{{ $staff->profile }}" class="media-object rounded-circle img-60" alt="" id="change">
                                            </div>
                                            @else
                                            <div class="media-left">
                                                <img src="{{ URL::asset('/assets/dashboard/images/user/1.jpg') }}" class="media-object rounded-circle img-60" alt="" id="change">
                                            </div>
                                            @endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <div class="file-upload-1">
                                            <label for="profile_pic" class="file-upload__label">Upload Photograph</label>
                                            <input id="profile_pic" class="file-upload__input" type="file" name="profile_pic" onchange="uploadPic(this);">
                                            </div>
                                            @if($errors->has('profile_pic'))<strong class="text-danger">{{ $errors->first('profile_pic') }}</strong>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <div class="file-upload-1">
                                            <label for="resume" class="file-upload__label">Upload Resume</label>
                                            <input id="resume" class="file-upload__input" type="file" name="resume" >
                                            </div>
                                            @if($errors->has('resume'))<strong class="text-danger">{{ $errors->first('resume') }}</strong>@endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-check">
                                            <div class="checkbox p-0">
                                                <input id="invalidCheck" type="checkbox" class="form-check-input" checked="">
                                                <label class="form-check-label" for="invalidCheck">Agree to terms and conditions</label>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-success" id="form_submit" type="submit">Submit form</button>
                                </form>

                                <!-- form -->
                                            <div class="row">
                                                <div class="col-md-8">
                                            <div>
                                               
                                            </div>
                                                </div>
                                
                                            </div>
                                        </div>
                                        <!--End InvoiceBot-->

                                    </div>

                                  
                                    <!--End Invoice-->

                                </div>
                                <!-- End Invoice Holder-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('script')
<script>
     function uploadPic(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#change').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<script>
    $('#role').on('change', function() {

        var slug =jQuery(this).val();
        // console.log(slug);
        var subservice='';

            $.ajax({
            method: 'GET',
            url: '/designation-id/'+slug,
            success: function(response){
                // console.log(response);
                
                if (response.length>0) 
                {
                    for(var i=0;i<response.length;i++)
                    {
                        subservice+='<option name="designation_id" value="'+response[i]["id"]+'">'+response[i]["post"]+'</option>';
                        // console.log(response[i]['service_id']);
                    }
                }else
                {
                    subservice = '<option value="">No service</option>';
                }
                $('#post').html(subservice);
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                // console.log("error");
            }
        });
    });       
</script>
@endsection