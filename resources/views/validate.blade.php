<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sajhaprofit</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/fonts/iconic/css/material-design-iconic-font.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('assets/login/css/main.css') }}">
	<style type="text/css" media="screen">
		.container-login100{
			background-image: url("{{URL::asset('assets/login/images/bg-01.jpg') }}")
		}
	</style>
</head>
<body style="background-color: #999999;">
	
	<div class="limiter">
		<div class="container-login100">
			<div class="login100-more" style="background-image: url('images/bg-01.jpg');"></div>

			<div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
				<form class="login100-form validate-form" method="post" action="{{ url('validate') }}">
					{{ csrf_field() }}
					<span class="login100-form-title p-b-59">
						2FA Validate
					</span>

					<div class="wrap-input100">
						<span class="label-input100">2FA Code</span>
						<input class="input100" type="text" name="code" placeholder="Enter 2FA code" autocomplete="off">
					</div>
						@if($errors->has('code'))
							<span class="text-danger">{{ $errors->first('code') }}</span>
						@endif

					
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Validate
							</button>
						</div>

						<a href="{{ url('login') }}" class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30">
							Back
							<i class="fa fa-long-arrow-right m-l-5"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<script src="{{URL::asset('assets/login/js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{URL::asset('assets/login/js/bootstrap.min.js') }}"></script>
	<script src="{{URL::asset('assets/login/js/main.js') }}"></script>
	<script src="{{URL::asset('assets/login/js/popper.js') }}"></script>
	<script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
	@include('sweet::alert')
</body>
</body>
</html>