@extends('layouts.dashboard.master')

@section('title') Staff Assign Area | Dashboard @endsection

@section('style')

@endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Assigned Areas</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Assigned Areas</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        @foreach($assign->groupBy('state') as $key => $cities)
          <div class="col-sm-4">
              <div class="card">
                  <div class="card-header">
                      <h5>{{$key}}</h5>
                  </div>
                  <div class="card-body">
                      <ul class="list-group">
                        @foreach($cities as $city)
                          <li class="list-group-item">{{$city->city}}</li>
                          @endforeach
                      </ul>
                  </div>
              </div>
          </div>
          @endforeach
      </div>
  </div>
   
</div>
@endsection

@section('script')


@endsection