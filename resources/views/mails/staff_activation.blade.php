<!DOCTYPE html>
<html lang="en" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
<title>Activation Email</title>
<style type="text/css">
body{
  font-family: 'Ubuntu', sans-serif;
}
  .email {
    padding-top: 50px;
    display: inline-block;
    text-align: center;
    margin: 0 auto;
    background-color: #ffffff;
    border-top: 3px solid #2B999F;
    border-bottom: 3px solid #2B999F;
    width: 600px;
    height: 600px;
    margin: 0 auto;
    display: block;
}

.logo {
  margin: 0 auto;
  text-align: center;
  display: block;
  margin-top: 30px;
  margin-bottom: 30px;
}

.white-container {
  background: #ffffff;
  width: 560px;
  text-align: center;
  margin: 0 auto;
  padding: 30px 0;
  margin-top: -10px; height: 225px;

}

h3 {
  font-weight: light;
}

.button {
  padding: 18px 25px;
  background-color: #2B999F;
  border-radius: 30px;
  text-decoration: none;
  color: white;
  text-transform: uppercase;
  font-size: 13px;
  letter-spacing: 1px;
}

.sm {
  max-width: 35px;
  display: inline-block;
  margin: 30px 5px;
}

h3 {
  width: 400px;
  margin: 0 auto;
  margin-bottom: 50px;
}
@media screen and (max-width: 768px){
.email {
  width: 100%;
}
.white-container{
  width: 100%;
}
h3{
  width: 100%;
}

}
</style>
</head>
<body style="background: #F4F7FA;">
	<div style="font-family: 'Roboto Condensed', sans-serif; margin: 20px;">
	<div class="email">
<!-- <img class="logo" src="<%= path; %>/public/assets/images/logo.png" width="200"> -->
	<img src="{{ URL::asset('assets/dashboard/images/logo.png')}}">
  	<div class="white-container">
	    <h2>Confirm Your Account</h2>
	    <h4>Login Details : </h4>
	    <p><strong>Email : {{ $staff->email }} </strong></p>
	    <p><strong>Password : {{ $password }} </strong></p>

	    <h4>You’re almost there! Click the button below to verify your email address and access your account.</h4>

	    <a href="{{ url('/login')}}" class="btn btn-info">Login Your Account</a>
	    
	</div>

	  <div class="enquiry">
	    For enquiries, please email us at <strong>support@sajhaprofit.com</strong>
	  </div>  
 	</div>
 </div>
</body>

</html>
