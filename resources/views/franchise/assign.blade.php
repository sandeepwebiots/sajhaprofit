@extends('layouts.dashboard.master')

@section('title') Staff Assign Area | Dashboard @endsection

@section('style')
<style type="text/css" media="screen">


.dropdown {
  position: relative;
  /*top:50%;
  transform: translateY(-50%);*/
}

.cl-drop {
    border: 1px solid #ced4da;
    padding: 0.375rem 0.75rem;
    border-radius: 0.25rem;
}

a {
  color: #fff;
}

.dropdown dd,
.dropdown dt {
  margin: 0px;
  padding: 0px;
}

.dropdown ul {
  margin: -1px 0 0 0;
}

.dropdown dd {
  position: relative;
}

.dropdown a,
.dropdown a:visited {
  color: #fff;
  text-decoration: none;
  outline: none;
  font-size: 12px;
}

.dropdown dt a {
  background-color: #4F6877;
  display: block;
  padding: 8px 20px 5px 10px;
  min-height: 25px;
  line-height: 24px;
  overflow: hidden;
  border: 0;
  width: 272px;
}

.dropdown dt a span,
.multiSel span {
  cursor: pointer;
  display: inline-block;
  padding: 0 3px 2px 0;
}

.dropdown dd ul {
    background-color: #fff;
    border: 1px solid #ced4da;
    color: #000;
    display: none;
    left: 0px;
    padding: 10px 15px 10px 10px;
    position: absolute;
    top: 2px;
    width: 100%;
    list-style: none;
    height: 100px;
    overflow: auto;
}

.dropdown span.value {
  display: none;
}

.dropdown dd ul li a {
  padding: 5px;
  display: block;
}

.dropdown dd ul li a:hover {
  background-color: #fff;
}

/*button {
  background-color: #6BBE92;
  width: 302px;
  border: 0;
  padding: 10px 0;
  margin: 5px 0;
  text-align: center;
  color: #fff;
  font-weight: bold;
}*/
</style>
@endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Staff Assign Area</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Staff Assign </li>
                        <li class="breadcrumb-item active">Area</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="invoice">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-md">
                                        <h3>Staff Assign Area</h3> 
                                        {{-- <a class="btn btn-primary pull-right" id="add-new">Add Staff</a> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="b-b-info"/>
                        @if(session()->has('error'))
                            <!-- <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    
                            </div> -->
                            <div class="alert alert-danger outline alert-dismissible fade show" role="alert">
                                <i class="icon-thumb-down"></i>  {{ session()->get('error') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        @endif       
                        <div>
                            <form method="post" action="{{ url('assign-area') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="staff_id" value="{{ $id }}">
                                <div class="">
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label class="hedding">Select State</label>
                                            <select class="form-control" name="state" id="state">
                                                    <option value="">Select</option>
                                                    @foreach($state as $st)
                                                    <option name="state" value="{{ $st->state }}">{{ $st->state }}</option>
                                                    @endforeach
                                            </select>
                                            @if($errors->has('role'))<span class="text-danger">{{ $errors->first('role') }}</span>@endif
                                        </div>
                                        <div class="col-md-6 mb-3">
                                                     <label class="hedding">Select City</label>
                                            <dl class="dropdown"> 
                                                <dt>
                                                    <div class="cl-drop">
                                                      <span class="hide">Select</span>    
                                                      <p class="multiSel"></p>  
                                                    </div>
                                                </dt>
                                                <dd>
                                                    <div class="mutliSelect">
                                                        <ul id="city">
                                                            
                                                        </ul>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>   
                                </div>                             
                                <button class="btn btn-primary pull-right" type="submit">Submit form</button>
                            </form>    
                        </div>
                        <!-- End Invoice Holder-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">

$(".dropdown dt .cl-drop").on('click', function() {
  $(".dropdown dd ul").slideToggle('fast');
});

$(".dropdown dd ul li a").on('click', function() {
  $(".dropdown dd ul").hide();
});


$(document).bind('click', function(e) {
  var $clicked = $(e.target);
  if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
});

$('.mutliSelect input[type="checkbox"]').on('click', function() {

  var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
    title = $(this).val() + ",";

  if ($(this).is(':checked')) {
    var html = '<span title="' + title + '">' + title + '</span>';
    $('.multiSel').append(html);
    $(".hide").hide();
  } else {
    $('span[title="' + title + '"]').remove();
    var ret = $(".hide");
    $('.dropdown dt .cl-drop').append(ret);

  }
});
</script>
<script>
$(document).ready(function(){
  $("#add-new").click(function(){
    $("#state").append(" <b>Appended text</b>.");
  });
});

 $('#state').on('change', function() {

        var id =jQuery(this).val();
        // console.log(id);
        var city='';
            $.ajax({
            method: 'GET',
            url: '/city/'+id,
            success: function(response){
                // console.log(response);
                
                if (response.length>0) 
                {
                    city+='<li><input type="checkbox" name="city" value="All" /> <label for="all">All</label></li>';
                    for(var i=0;i<response.length;i++)
                    {
                       
                        city+='<li><input type="checkbox" name="city[]" value="'+response[i]["city_name"]+'"  id="'+response[i]["city_name"]+'"/> <label for="'+response[i]["city_name"]+'">'+response[i]["city_name"]+'</label></li>';
                    }
                }else
                {
                    city = '<option value="">No service</option>';
                }
                $('#city').html(city);
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                city = '<option value="">No service</option>';
                $('#city').html(city);
            }
        });
    });
</script>

@endsection