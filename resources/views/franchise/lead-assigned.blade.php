@extends('layouts.dashboard.master')

@section('title') Lead Assigned | Dashboard @endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Lead Assigned</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Lead Assigned</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="invoice">
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="text-md">
                                        <h3>Lead Assigned</h3> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="b-b-info"/>
                                
                        <div>
                            <form method="post" action="{{ url('assigned-lead-staff') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-row">
                                            <div class="col-md-12 mb-3">
                                                <label class="hedding">Absent staff</label>
                                                <select class="form-control" name="absent" id="absent">
                                                        <option value="">Select</option>
                                                    @foreach($stafflist as $staff)
                                                        <option class="" name="absent" value="{{ $staff->designation_id }}">{{ $staff->user_name }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('absent'))<span class="text-danger">{{ $errors->first('absent') }}</span>@endif
                                            </div>
                                        </div>
                                        
                                        <div class="form-row">
                                            <div class="col-md-12 mb-3">
                                                <label class="hedding">Assigned by</label>
                                                <select class="form-control" name="assigned" id="assigned">
                                                        <option name="assigned" value="">Select</option>
                                                </select>
                                                @if($errors->has('assigned'))<span class="text-danger">{{ $errors->first('assigned') }}</span>@endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group m-t-15" id="tc_leads">
                                        </div>
                                        @if($errors->has('leads'))<span class="text-danger">{{ $errors->first('leads') }}</span>@endif
                                    </div>
                                </div>
                                <button class="btn btn-primary pull-right" type="submit">Assgined Lead</button>
                            </form>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('#absent').on('change', function() {

        var id =jQuery(this).val();
        // console.log(id);
        var assignedstaff='';
        var leads='';

            $.ajax({
            method: 'GET',
            url: '/assigned-lead-staff/'+id,
            success: function(response){
                 
                    var users = response['users'];
                    var lead = response['lead'];
                    // console.log(lead);
                    if (users.length>0) {

                        for(var i=0;i<users.length;i++)
                        {
                            assignedstaff+='<option name="assigned" value="'+users[i]["designation_id"]+'">'+users[i]["user_name"]+'</option>';
                        }
                    }else{
                        assignedstaff = '<option value="">No data</option>';    
                    }
                    if (lead.length>0) {
                        for(var j=0;j<lead.length;j++)
                        {
                            leads+='<div class="checkbox"><input id="lead'+lead[j]["id"]+'" type="checkbox" name="leads[]" value="'+lead[j]["id"]+'"><label for="lead'+lead[j]["id"]+'">'+lead[j]["name"]+'</label></div>';
                        }
                    }
                    else{
                        leads+='<div class="no_leads"><span>No leads</span></div>';
                    }

                $('#assigned').html(assignedstaff);
                $('#tc_leads').html(leads);
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                assignedstaff = '<option value="">Select</option>';
            }
        });
    });       
</script>
@endsection