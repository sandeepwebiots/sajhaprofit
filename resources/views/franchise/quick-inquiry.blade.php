@extends('layouts.dashboard.master')

@section('title') Staff List | Dashboard @endsection

@section('style')    
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatable-extension.css') }}" />
@endsection

@section('content')
@php $slug = Sentinel::getUser()->roles()->first()->slug; @endphp
 <div class="page-body user-management">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>@if($slug == 'm10') Franchise Staff @else Staff Management @endif</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">@if($slug == 'm10') Franchise Staff @else Staff Management @endif</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Quick Contact</h5>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-12 col-sm-12">
                            <div class="inquiry_form_part" data-aos="fade-left" data-aos-duration="2000" style="width: 100%">
                                
                                <form action="{{ url('quickinquiry') }}" method="post">
                                    <input type="hidden" name="manual" value="1">
                                    <input type="hidden" name="franchies_id" value="{{ Sentinel::getUser()->id  }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Your Name">
                                    @if($errors->has('name'))<strong class="text-danger">{{ $errors->first('name') }}</strong>@endif
                                    </div>
                                    <div class="form-group wow fadeInUp animated">
                                        <input type="text" name="mobile_no" class="form-control" placeholder="Your Mobile No." maxlength="10">
                                    @if($errors->has('mobile_no'))<strong class="text-danger">{{ $errors->first('mobile_no') }}</strong>@endif
                                    </div>
                                    <div class="form-group wow fadeInUp animated">
                                        <input type="email" name="email" class="form-control" placeholder="Your Email">
                                    @if($errors->has('email'))<strong class="text-danger">{{ $errors->first('email') }}</strong>@endif
                                    </div>
                                    <div class="form-group wow fadeInUp animated">
                                        <select name="country" id="country" class="form-control">
                                            <option name="country" value="">Select Country</option>
                                            <option name="country" value="1">India</option>
                                        </select>
                                    @if($errors->has('country'))<strong class="text-danger">{{ $errors->first('country') }}</strong>@endif
                                    </div>
                                    <div class="form-group wow fadeInUp animated">
                                        <select name="state" id="state" class="form-control">
                                            <option name="state" value="">Select State</option>
                                        </select>
                                    @if($errors->has('state'))<strong class="text-danger">{{ $errors->first('state') }}</strong>@endif
                                    </div>
                                    <div class="form-group wow fadeInUp animated">
                                        <select name="city" id="city" class="form-control">
                                            <option name="city" value="">Select City</option>
                                        </select>
                                    @if($errors->has('city'))<strong class="text-danger">{{ $errors->first('city') }}</strong>@endif
                                    </div>
                                    
                                    <div class="form-group wow fadeInUp animated">
                                        <textarea class="form-control" name="details" placeholder="Inquiry Details" rows="5" id="comment"></textarea>
                                        {{--<textarea rows="4" cols="40" name="details" placeholder="Inquiry Details"></textarea>--}}
                                    @if($errors->has('details'))<strong class="text-danger">{{ $errors->first('details') }}</strong>@endif
                                    </div>

                                    <div class="form-group wow fadeInUp animated">
                                        <label class="col-sm-12 control-label">Please enter the verication code shown below.<span>*</span></label>
                                        <div class="col-sm-12">

                                            <div id="captcha-wrap" class="varify-cls">
                                               <div class="g-recaptcha" data-sitekey="6LfaCWMUAAAAAHf7Q6V58Ec5vyU-Iui_o1YYsy3Z"></div>
                                            </div>
                                            </div>
                                            @if($errors->has('g-recaptcha-response'))<strong class="text-danger">{{ $errors->first('g-recaptcha-response') }}</strong>@endif
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary  m-t-20 text-center" style="width: 100%;">submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
   $('#country').on('change', function() {

        var id =jQuery(this).val();
        var state='';
        // console.log(id);
            $.ajax({
            method: 'GET',
            url: '/state/'+id,
            success: function(response){
                // console.log(response);
                
                if (response.length>0) 
                {
                    for(var i=0;i<response.length;i++)
                    {
                        state+='<option name="state" value="'+response[i]["state"]+'">'+response[i]["state"]+'</option>';
                        // console.log(response[i]['service_id']);
                    }
                }else
                {
                    state = '<option value="">No service</option>';
                }
                $('#state').html(state);
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                state = '<option value="">No service</option>';
                $('#state').html(state);
                city = '<option value="">No service</option>';
                $('#city').html(city);
            }
        });
    });    

    $('#state').on('change', function() {

        var id =jQuery(this).val();
        // console.log(id);
        var city='';
            $.ajax({
            method: 'GET',
            url: '/city/'+id,
            success: function(response){
                // console.log(response);
                
                if (response.length>0) 
                {
                    for(var i=0;i<response.length;i++)
                    {
                        city+='<option name="city" value="'+response[i]["city_name"]+'">'+response[i]["city_name"]+'</option>';
                    }
                }else
                {
                    city = '<option value="">No service</option>';
                }
                $('#city').html(city);
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                city = '<option value="">No service</option>';
                $('#city').html(city);
            }
        });
    });
</script>
@endsection

