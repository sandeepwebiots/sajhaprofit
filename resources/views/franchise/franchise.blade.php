@extends('layouts.dashboard.master')

@section('title') Franchise Staff | Dashboard @endsection

@section('style')    
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatable-extension.css') }}" />
@endsection

@section('content')
 <div class="page-body user-management">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Franchise Staff
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Franchise Staff</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Franchise Staff</h5>
                        <a href="{{ route('staff-manage.create') }}" class="btn btn-primary pull-right">Add Staff</a>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="list-franchise" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Status</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                   @php $i=1; @endphp

                                   @foreach($staff_list as $staff)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $staff->user_name }}</td>    
                                            <td>@if(isset($staff->designation->post))
                                                {{ $staff->designation->post }}@endif</td> 
                                            <td>
                                                @if($staff->status == 0)<span class="badge badge-info">New Join</span> @elseif($staff->status == 1)<span class="badge badge-success">Active</span>
                                                @elseif($staff->status == 2)<span class="badge badge-danger">Blcok</span>
                                                @endif
                                            </td>
                                            <td><a href="{{ url('staff-detial') }}/{{$staff->id}}" class="btn btn-primary"> Details </a></td> 
                                            <td>
                                                <a href="{{ url('staff-edit') }}/{{ $staff->id }}" class="btn btn-info"><i class="fa fa-pencil"></i>&nbsp; Edit </a>

                                            </td>  
                                            <td>@if($staff->status == 1)<a href="{{ url('assign-staff') }}/{{ $staff->id }}" class="btn btn-primary">Assign State </a> @endif</td>
                                        </tr>
                                   @endforeach
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#list-franchise').DataTable();
    } );
</script>
@endsection

