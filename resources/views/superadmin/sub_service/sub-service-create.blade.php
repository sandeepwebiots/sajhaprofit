@extends('layouts.dashboard.master')

@section('title') Sub Service | Dashboard @endsection

@section('style')    
<style type="text/css">
    #sub_service_form label{
    font-size: 18px;
}
</style>
@endsection

@section('content')
<div class="page-body user-management">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Create Sub Service</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Sub Service</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="card">
            <div class="edit-form">
                <div class="col-sm-12 ">
                    <div class="card">
                        <div class="card-header b-b-info">
                            <h5>Create Sub Service</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('sub-service.store') }}" method="post" id="sub_service_form" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-md-4 mb-3">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="{{ old('title') }}">
                                    @if($errors->has('title'))<strong class="text-danger">{{$errors->first('title')}}</strong>@endif
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label>Services</label>
                                    <select name="services" class="form-control btn-square">
                                        <option name="services" value="">Select Service</option>
                                        @foreach($service as $serv)
                                            <option name="services" value="{{ $serv->id }}">{{ $serv->name }}</option>
                                        @endforeach
                                    </select>
                                     @if($errors->has('services'))<strong class="text-danger">{{$errors->first('services')}}</strong>@endif
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label>Capital Amount </label>
                                    <input type="text" class="form-control" name="capital" id="capital" placeholder="Capital Amount" value="{{ old('capital') }}">
                                     @if($errors->has('capital'))<strong class="text-danger">{{$errors->first('capital')}}</strong>@endif
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label>Profit Amount</label>
                                    <input type="text" class="form-control" name="profit" id="profit" placeholder="Profit Amount" value="{{ old('profit') }}">
                                     @if($errors->has('profit'))<strong class="text-danger">{{$errors->first('profit')}}</strong>@endif
                                </div>
                                <div class="col-md-10 mb-3">
                                    <label>Duration</label>
                                    <div class="row  m-b-15">
                                        <div class="col-sm-3 pl-4">
                                        <input type="text" class="form-control" name="duration_from" id="duration_from" placeholder="Form" value="{{ old('duration_from') }}"> 
                                         @if($errors->has('duration_from'))<strong class="text-danger">{{$errors->first('duration_from')}}<br></strong>@endif 
                                         </div>
                                        <div class="col-sm-3 pl-4"> 
                                        <input type="text" class="form-control" name="duration_to" id="duration_to" placeholder="To" value="{{ old('duration_to') }}"><label class="form-label"></label>
                                         @if($errors->has('duration_to'))<strong class="text-danger">{{$errors->first('duration_to')}}</strong>@endif
                                         </div>
                                         <div class="col-sm-2">
                                            <label>Days</label>
                                         </div>
                                     </div>
                                 </div>
                                <div class="form-group">
                                    <label>Service Details</label>
                                    <textarea name="service_details" class="form-control" id="editor1" cols="20" rows="15" value="{{ old('service_details') }}">{{ old('service_details') }}</textarea>
                                     @if($errors->has('service_details'))<strong class="text-danger">{{$errors->first('service_details')}}</strong>@endif
                                </div>
                               
                                <div class="form-group">
                                    <label>Sample Call</label>
                                    <textarea name="sample_call" class="form-control" id="editor3" cols="20" rows="15" value="{{ old('sample_call') }}">{{ old('sample_call') }}</textarea>
                                     @if($errors->has('sample_call'))<strong class="text-danger">{{$errors->first('sample_call')}}</strong>@endif
                                </div>
                                <div class="form-group">
                                    <label>Updates</label>
                                    <textarea name="updates" class="form-control" id="editor4" cols="20" rows="15" value="{{ old('updates') }}">{{ old('updates') }}</textarea>
                                    @if($errors->has('updates'))<strong class="text-danger">{{$errors->first('updates')}}</strong>@endif
                                </div>
                                <div class="form-group">
                                    <input type="file" name="img" value="{{ old('img') }}">
                                    @if($errors->has('img'))<strong class="text-danger">{{$errors->first('img')}}</strong>@endif
                                </div>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')


@endsection