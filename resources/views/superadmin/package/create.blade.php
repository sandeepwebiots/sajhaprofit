@extends('layouts.dashboard.master')

@section('title') Package | Dashboard @endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Package Create
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Package cretae</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header b-b-info">
                        <h5>Create Package</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <form class="form theme-form" action="{{ route('packages.store') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}


                                    <div class="form-group row">
                                        <div class="col-sm-3 col-form-label">Services</div>
                                        <div class="col-sm-6">
                                            <select name="service" id="select" class="form-control form-control-success btn-square">
                                                <option value="">Select Services</option>
                                                @foreach($service as $serve)
                                                <option value="{{ $serve->id}}" id="{{ $serve->id}}">{{ $serve->name}}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('service'))
                                            <strong class="text-danger">{{ $errors->first('service') }}</strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-3 col-form-label">Package Services</div>
                                        <div class="col-sm-6">
                                            <select name="subserviceid" id="sub_service" class="form-control form-control-success btn-square">
                                                <option name="subserviceid" value="">Select service</option>
                                            </select>
                                        @if($errors->has('subserviceid'))
                                        <strong class="text-danger">{{ $errors->first('subserviceid') }}</strong>
                                        @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Packages </label>
                                        <div class="col-sm-9">
                                        <div class="row  m-b-15">
                                            <div class="col-sm-2 pl-4">
                                                <input type="checkbox" name="monthly" value="1" id="monthly" onclick="myFunction(this)"> Monthly                                                
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="number" step="any" class="form-control" id="price_monthly" name="price_monthly" onkeyup="checkPackgePrice()">&nbsp;<span id="errmsg"></span>
                                            </div>
                                        </div>
                                        <div class="row m-b-15">
                                            <div class="col-sm-2 pl-4">
                                                <input type="checkbox" name="quaterly" value="1" id="quaterly" data-id="price2" class="" onclick="myFunction(this)"> Quaterly
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="number" step="any" class="form-control" id="price_quaterly" name="price_quaterly" onkeyup="checkPackgePrice()">
                                            </div>
                                        </div>
                                        <div class="row m-b-15">
                                            <div class="col-sm-2 pl-4">
                                                <input type="checkbox" name="half_yearly" value="1" id="half_yearly" data-id="price3" class="" onclick="myFunction(this)"> Half Yearly
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="number" step="any" class="form-control" id="price_half_yearly" name="price_half_yearly" onkeyup="checkPackgePrice()">
                                            </div>                                             
                                        </div>
                                        <div class="row m-b-15">
                                            <div class="col-sm-2 pl-4">
                                             <input type="checkbox" name="yearly" value="1" id="yearly" data-id="price4" class="" onclick="myFunction(this)"> Yearly
                                         </div>
                                         <div class="col-sm-3">
                                            <input type="number" step="any" class="form-control" id="price_yearly" name="price_yearly" onkeyup="checkPackgePrice()">
                                        </div>   
                                        </div>
                                    @if($errors->has('terms'))
                                        <strong class="text-danger">{{ $errors->first('terms') }}</strong>
                                    @endif
                                    </div>
                                </div>


                                <div class="card-footer">
                                    <div class="col-sm-9 offset-sm-3">
                                        <button type="submit" class="btn btn-primary" id="btnSubmit">Submit</button>
                                        <a href="{{ route('packages.index') }}" class="btn btn-light">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('script')
<script>
    $('#select').on('change', function() {

        var id =jQuery(this).val();
        var subservice='';

        $.ajax({
            method: 'GET', // Type of response and matches what we said in the route
            url: '/sub-service-list/'+id, // This is the url we gave in the route

            success: function(response){ // What to do if we succeed
                // $.each(response, function (index, value) {

                // })
                if (response.length>0) 
                {
                    for(var i=0;i<response.length;i++)
                    {
                        subservice+='<option name="subserviceid" value="'+response[i]["id"]+'">'+response[i]["name"]+'</option>';
                        // console.log(response[i]['service_id']);
                    }
                }else{
                   subservice = '<option value="">No service</option>';
               }
               $('#sub_service').html(subservice);

           },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                console.log("error");
            }
        });
    }); 


    // function getfocus() {
    //     document.getElementById("price").focus();
    // }  
// $("#price1").attr("disabled", "disabled");
  function checkPackgePrice(){
             if(($("#quaterly").is(":checked") && $("#price_quaterly").val()=='')||($("#monthly").is(":checked") && $("#price_monthly").val()=='')||($("#half_yearly").is(":checked") && $("#price_half_yearly").val()=='')||($("#yearly").is(":checked") && $("#price_yearly").val()=='')){

            $('#btnSubmit').prop('disabled', true);
         }
         else{
            
             $('#btnSubmit').prop('disabled', false);
         }
 }


    function myFunction(e) {

        checkPackgePrice();




        // console.log(e);
        // var id = e.id;
        // var priceid = "#price_"+id;
        // console.log(priceid);
        // if (e.checked == true && $(priceid).val()==''){
        //     $( "#price_" + id ).focus();
        //     $('#btnSubmit').prop('disabled', true);

        // }
        // else{
        //     $('#btnSubmit').prop('disabled', false);
        // }
        // var checkBox = document.getElementById("terms");
        // // var text = document.getElementById("price1");

        // if (checkBox.checked == true){
        //     // text.style.display = "block";
        //     $("#price1").removeAttr("disabled");
        //     document.getElementById("price1").focus();
        //     $("#errmsg").html("This filed is required.").show();
        //     $('#btnSubmit').prop('disabled', true);
        //     $("#price1").keypress(function (e) {
        //         $('#btnSubmit').prop('disabled', false);
        //         $("#errmsg").html("This filed is required.").show();
        //      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //         $('#btnSubmit').prop('disabled', true);
        //         $("#errmsg").hide();
        //                return false;
        //     }
        //    });

        // } else {
        //     $("#price1").attr("disabled", "disabled");
        //     $("#errmsg").html("This filed is required.").hide();
        //     $('#btnSubmit').prop('disabled', false);

        // }
    }  

    

</script>
@endsection