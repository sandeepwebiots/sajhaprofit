@extends('layouts.dashboard.master')

@section('title') Package | Dashboard @endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Packages List</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Packages List</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Packages List</h5>
                        <a href="{{ route('packages.create') }}" class="btn btn-primary pull-right">Add New</a>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="export-button" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Service</th>
                                    <th>Package</th>
                                    <!-- <th>Amount</th> -->
                                    <th>Duration</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php $i=1; @endphp
                                    @foreach($packge as $pack)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $pack->sub_service->service->name }}</td>
                                        <td>{{ $pack->sub_service->name }}</td>
                                        <!-- <td>{{ $pack->amount }}</td> -->
                                        <td>{{ $pack->sub_service->duration_from }} To {{ $pack->sub_service->duration_to }} Days</td>
                                        <td>{{ $pack->created_at->format('d M y') }}</td>
                                        <td>
                                            <a href="{{ route('packages.edit',$pack->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                                            <form action="{{ route('packages.update',$pack->id) }}" method="post" accept-charset="utf-8">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="delete" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

