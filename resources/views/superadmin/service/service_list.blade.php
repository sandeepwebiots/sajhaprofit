@extends('layouts.dashboard.master')

@section('title') Services List|Dashboard @endsection

@section('style')
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}   
</style>
@endsection

@section('content')
<div class="page-body user-management">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Services List</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Services List</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>All Services</h5>
                        <!-- <a href="{{ route('super-admin-news-section.create') }}" class="btn btn-primary pull-right">Add New</a> -->
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="export-button" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                	@php $i=1; @endphp
                                	@foreach($services as $serve)
                                		<tr>
                                			<td>{{ $i++ }}</td>
                                			<td>{{ $serve->name }}</td>
                                            <td><img src="{{ URL::asset('assets/dashboard/upload/service') }}/{{ $serve->image }}" alt="" height="100px" width="100px"> </td>
                                			<td><a href="{{ url('sub-services-list') }}/{{ $serve->id }}" class="btn btn-primary" title="">Details</a>
                                                <a href="#" class="btn btn-info" onclick="modelpopop({{$serve->id}})" data-toggle="modal" data-target="#serviceModel">Edit</a></td>
                                		</tr>
                                	@endforeach                                          
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="serviceModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Service</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="serviceModel" action="" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label" >Service:</label>
                                <input type="text" class="form-control" id="service" name="service">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Image:</label>
                                <input type="file" id="image" name="image" class="form-control">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">

$(document).ready(function () {

    $('.serviceModel').validate({ // initialize the plugin
        rules: {
            service: {
                required: true,
            }
        }
    });
});
  function modelpopop(id)
{
    // console.log(id);
        $.ajax({
            method: 'GET',
            url: '/service-edit/'+id, 

            success: function(response){ 
                console.log(response);
                $('.serviceModel').attr('action', "{{url('/service-update')}}/"+id);
                $('#service').val(response.name);
                // $('#image').val(response.image);
                
           },
            error: function(jqXHR, textStatus, errorThrown) { 
                console.log("error");
            }
        });
}
</script>

@endsection