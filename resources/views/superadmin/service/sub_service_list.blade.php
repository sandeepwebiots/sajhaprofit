@extends('layouts.dashboard.master')

@section('title') Sub Services List|Dashboard @endsection

@section('content')
<div class="page-body user-management">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>{{ $service->name }} LIST
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Service</li>
                        <li class="breadcrumb-item active">{{ strtolower($service->name) }} List</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{ $service->name }} Service</h5>
                        <a href="{{ route('sub-service.index') }}" class="btn btn-primary pull-right">Create Service</a>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="export-button" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Capital Amount</th>
                                    <th>Profit Amount</th>
                                    <!-- <th>Service Details</th> -->
                                    <th>Duration</th>
                                    <!-- <th>Sample Call</th> -->
                                    <!-- <th>Updates</th> -->
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                	@php $i=1; @endphp
                                	@foreach($sub_service as $service)
                                		<tr>
                                			<td>{{ $i++ }}</td>
                                			<td>{{ $service->name }}</td>
                                            <td>{{ $service->capital }}</td>
                                            <td>{{ $service->profit }}</td>
                                            <!-- <td>@php echo substr($service['service_details'],0) @endphp</td> -->
                                            <td>{{ $service->duration_from }} To {{ $service->duration_to }} Days</td>
                                            <!-- <td>@php echo $service->sample_call @endphp</td> -->
                                            <!-- <td>@php echo $service->updates @endphp</td> -->
                                            <td><a href="{{ route('sub-service.edit',$service->id) }}"  class="btn btn-primary">Edit</a>
                                                <form action="{{ route('sub-service.destroy',$service->id) }}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button class="btn btn-danger delete">Delete</button>
                                                </form>
                                            </td>
                                		</tr>
                                	@endforeach                                          
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection