@extends('layouts.dashboard.master')

@section('title') Blog | Dashboard @endsection

@section('style')    
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatable-extension.css') }}" />
@endsection

@section('content')
 <div class="page-body user-management">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>Blog Management</h5>
                        </div>
                        <div class="col-lg-6">
                            <ol class="breadcrumb pull-right">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                                <li class="breadcrumb-item active">Blog Management</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Blog List</h5>
                                <a href="{{ route('super-admin-blog-section.create') }}" class="btn btn-primary pull-right">Add New</a>
                            </div>
                            <div class="card-body">
                                <div class="dt-ext table-responsive">
                                    <table id="export-button" class="display">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>SubTitle</th>
                                            <th>Content</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        	@php $i=1; @endphp
                                        	@foreach($blogs_list as $blog)
                                        	<tr>
                                        		<td>{{ $i++ }}</td>
                                        		<td>{{ str_limit($blog['title'],100) }}</td>
                                        		<td>{{ str_limit($blog['sub_title'],150) }}</td>
                                        		<td>{{ str_limit($blog['content'],200) }}</td>
                                        		<td><img src="{{ URL::asset('assets\dashboard\upload\blog') }}\{{ $blog->img }}" alt="" height="100px" width="100px"></td>
                                        		<td>
                                        			<a href="{{ route('super-admin-blog-section.edit',$blog->id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                                        			<form action="{{ route('super-admin-blog-section.destroy',$blog->id) }}" method="post" accept-charset="utf-8">
                                        				{{ csrf_field() }}
                                        				{{ method_field('DELETE') }}
                                        				<button type="delete" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                        			</form>
                                        		</td>
                                        	</tr>
                                        	@endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('script')
<!--Datatable js-->
    <script src="{{ URL::asset('assets/dashboard/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/dashboard/js/datatable.custom.js') }}"></script>

<script>
$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
@endsection