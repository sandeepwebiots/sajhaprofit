@extends('layouts.dashboard.master')

@section('title') Blog-Create | Dashboard @endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Blog Create
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Blog cretae</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Create New Blog</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <form class="form theme-form" action="{{ route('super-admin-blog-section.store') }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Blog Title</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="title" class="form-control" placeholder="Type your title">
                                            @if($errors->has('title'))
                                                <strong class="text-danger">{{ $errors->first('title') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Blog Sub Title</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="sub_title" class="form-control" placeholder="Type your sub title">
                                            @if($errors->has('sub_title'))
                                                <strong class="text-danger">{{ $errors->first('sub_title') }}</strong>
                                            @endif
                                        </div>
                                    </div>                                    
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Description</label>
                                        <div class="col-sm-9">
                                            <textarea rows="7" cols="5" name="content" class="form-control" placeholder="Type your Description"></textarea>
                                            @if($errors->has('content'))
                                                <strong class="text-danger">{{ $errors->first('content') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Upload File</label>
                                        <div class="col-sm-9">
                                            <input type="file" name="img" class="form-control">
                                            @if($errors->has('img'))
                                                <strong class="text-danger">{{ $errors->first('img') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="{{ url('super-admin-blog-section') }}" class="btn btn-light">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection