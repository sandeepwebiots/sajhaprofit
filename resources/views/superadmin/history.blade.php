@extends('layouts.dashboard.master')

@section('title') Payment History @endsection

@section('style')
<style type="text/css">
    button {
        margin-top: 0px !important;
    }
</style>
@endsection

@section('content')
@php $slug = Sentinel::getUser()->roles()->first()->slug; @endphp
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h5>
                        Payment History
                    </h5>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">History</li>
                        <li class="breadcrumb-item"><a href="{{ url('payment-history') }}"> Payment History</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
            
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>History</h5>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <!-- <input type="date" name="" value="" placeholder=""> To <input type="date" name="" value="" placeholder=""> -->
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
                            <table id="history" class="display">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="date-picker">
                                                <form method="post" action="{{ url('filter-payment-history') }}">
                                                    {{ csrf_field() }}
                                                    <div class="form-group row">
                                                        <div class="col-sm-4">
                                                            <input type="text" id="fromdate" name="from" class="datepicker-here form-control digits" data-language='en' data-multiple-dates-separator=", " data-position='bottom left' placeholder="From" autocomplete="off"/>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <input type="text" name="to" class="datepicker-here form-control digits" data-language='en' data-multiple-dates-separator=", " data-position='bottom left' placeholder="To" autocomplete="off" id="todate" />
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <button type="submit" class="btn btn-primary">Filter</button>
                                                        </div>
                                                    </div>
                                                        
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            @if($slug == 'super_admin')
                                            <div class="row pull-right m-b-5">
                                                <a href="{{ url('export-excel') }}" class="btn btn-primary m-r-5">Export</a>
                                                <a href="{{ url('all-invoice') }}" class="btn btn-primary">Download</a>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Mobile No</th>
                                        <th>Package</th>
                                        <th>Package Amount</th>
                                        <th>GST Amount</th>
                                        <th>Total Amount</th>
                                        <th>Service</th>
                                        <th>Status</th>
                                        @if($slug == 'super_admin')
                                        <th>Action</th>
                                        @endif
                                    </tr>
                                </thead>
                                   
                                <tbody>
                                  @php $i=1; @endphp
                                  @foreach($history as $hist)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $hist->name }}</td>
                                        <td>{{ $hist->mobile_no }}</td>
                                        <td>@if($hist->pack == 'half_yearly') HALF YEARLY @else {{ strtoupper($hist->pack) }} @endif</td>
                                        <td>{{ $hist->pack_amount }}</td>
                                        <td>{{ $hist->gst_amount }}</td>
                                        <td>{{ $hist->total_amount }}</td>
                                        <td>{{ $hist->package->sub_service->name }}</td>
                                        <td>@if($hist->status == 0)<span class="badge badge-warning">Pending</span>  @elseif($hist->status == 1)<span class="badge badge-success">Complete</span>@endif</td>
                                        @if($slug == 'super_admin')
                                        <td><a href="{{ URL::asset('assets/invoice') }}/{{ $hist->invoice_no }}.pdf" download><i class="fa fa-download" aria-hidden="true"></i></a></td>
                                        @endif
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
       
</div>

@endsection

@section('script')
<script src="{{ URL::asset('assets/dashboard/js/date-picker/datepicker.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/date-picker/datepicker.en.js') }}"></script> 
<script>
    $(document).ready(function() {
        $('#history').DataTable();
    } );

    var dates = $("#fromdate, #todate").datepicker({
        language: 'en',
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        maxDate: new Date(),
    });
</script>
@endsection
        