@extends('layouts.dashboard.master')

@section('title') News-Edit | Dashboard @endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>News Edit
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">News Edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>News Edit</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <form class="form theme-form" action="{{ route('super-admin-news-section.update',$news->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">News Title</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="title" class="form-control" value="{{ $news->title }}" placeholder="Type your title">
                                            @if($errors->has('title'))
                                                <strong class="text-danger">{{ $errors->first('title') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">News Sub Title</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="sub_title" class="form-control" value="{{ $news->sub_title }}" placeholder="Type your sub title">
                                            @if($errors->has('sub_title'))
                                                <strong class="text-danger">{{ $errors->first('sub_title') }}</strong>
                                            @endif
                                        </div>
                                    </div>                                    
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Description</label>
                                        <div class="col-sm-9">
                                            <textarea rows="7" cols="5" name="content" class="form-control" placeholder="Type your Description">{{ $news->content }}</textarea>
                                            @if($errors->has('content'))
                                                <strong class="text-danger">{{ $errors->first('content') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Upload File</label>
                                        <div class="col-sm-4">
                                            <input type="file" name="img" class="form-control" value="{{ $news->img }}">
                                            @if($errors->has('img'))
                                                <strong class="text-danger">{{ $errors->first('img') }}</strong>
                                            @endif
                                        </div>
                                        <div class="col-sm-4">
                                            <img src="{{ URL::asset('assets\dashboard\upload\news') }}\{{ $news->img }}" height="150px" width="150px" alt="">
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                            <a href="{{ url('super-admin-news-section') }}" class="btn btn-light">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection