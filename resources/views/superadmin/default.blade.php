@extends('layouts.dashboard.master')

@section('title') Super-Admin|Dashboard @endsection

@section('content')
  <!-- Sidebar End -->
        <div class="page-body">
            <!-- Container-fluid starts -->
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h3>Business <small>Universal Admin panel</small></h3>
                        </div>
                        <div class="col-lg-6">
                            <ol class="breadcrumb pull-right">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="breadcrumb-item">Dashboard </li>
                                <li class="breadcrumb-item active">Business</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid Ends -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-xl-6 width100">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="business-top-widget">
                                        <div class="media d-inline-flex">
                                            <div class="media-body mr-4">
                                                <p class="mb-2">Company Growth</p>
                                                <h2 class="total-value m-0">89.3%</h2>
                                            </div>
                                            <i class="icofont icofont-growth font-primary align-self-center"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="business-top-widget">
                                        <div class="media d-inline-flex">
                                            <div class="media-body mr-4">
                                                <p class="mb-2">Total Income</p>
                                                <h2 class="total-value m-0">78.2%</h2>
                                            </div>
                                            <i class="icofont icofont-money font-secondary align-self-center"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="business-top-widget">
                                        <div class="media d-inline-flex">
                                            <div class="media-body mr-4">
                                                <p class="mb-2">Business Project</p>
                                                <h2 class="total-value m-0">654</h2>
                                            </div>
                                            <i class="icofont icofont-presentation-alt font-success align-self-center"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="business-top-widget">
                                        <div class="media d-inline-flex">
                                            <div class="media-body mr-4">
                                                <p class="mb-2">Employee</p>
                                                <h2 class="total-value m-0">8963</h2>
                                            </div>
                                            <i class="icofont icofont-business-man font-info align-self-center"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-xl-6 width100">
                        <div class="card">
                            <div class="business-card">
                                <div class="row">
                                    <div class="col-md-4 align-self-center pr-md-0">
                                        <div class="text-center">
                                            <img src=" {{ URL::asset('assets/dashboard/images/logo-login.png') }}" class="logo mr-md-5" alt="u-logo">
                                           
                                        </div>
                                    </div>
                                    <div class="col-md-8 b-l-light pt-3 pt-md-0">
                                        <table>
                                            <tr>
                                                <th>Name&nbsp;&nbsp;&nbsp;&nbsp;:</th>
                                                <td>pixelstrap Web Service PVT LTD.</td>
                                            </tr>
                                            <tr>
                                                <th>Founded&nbsp;&nbsp;&nbsp;&nbsp;:</th>
                                                <td>01 January 2012</td>
                                            </tr>
                                            <tr>
                                                <th>CEO&nbsp;&nbsp;&nbsp;&nbsp;:</th>
                                                <td>Mark Jeckno</td>
                                            </tr>
                                            <tr>
                                                <th>Email&nbsp;&nbsp;&nbsp;&nbsp;:</th>
                                                <td>support@pixelstrap.com</td>
                                            </tr>
                                            <tr>
                                                <th>Phone&nbsp;&nbsp;&nbsp;&nbsp;:</th>
                                                <td class="digits">+91 234-456-7904</td>
                                            </tr>
                                            <tr>
                                                <th>Turnover&nbsp;&nbsp;&nbsp;&nbsp;:</th>
                                                <td class="digits">$78B</td>
                                            </tr>
                                            <tr>
                                                <th>About&nbsp;&nbsp;&nbsp;&nbsp;:</th>
                                                <td>Simply dummy text of the printing
                                                    and typesetting industry. Lorem
                                                    Ipsum has been the industry's
                                                    standard dummy text ever since
                                                    the 1500s,</td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-xl-3 width50">
                        <div class="card">
                            <div class="business-chart-widget bg-info">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>Products</p>
                                        <h3 class="total-num">45987</h3>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="icon text-md-right">
                                            <i class="icon-package"></i>
                                            <i class="icon-package icon-bg"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="flot-chart-container">
                                    <div id="bar-chart-sparkline-first" class="flot-chart-placeholder"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-3 width50">
                        <div class="card">
                            <div class="business-chart-widget bg-primary">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>Project</p>
                                        <h3 class="total-num">98314</h3>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="icon text-md-right">
                                            <i class="icofont icofont-presentation-alt"></i>
                                            <i class="icofont icofont-presentation-alt icon-bg"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="flot-chart-container">
                                    <div id="bar-chart-sparkline-second" class="flot-chart-placeholder"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-3 width50">
                        <div class="card">
                            <div class="business-chart-widget bg-secondary">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>Stock Price</p>
                                        <h3 class="total-num">69876</h3>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="icon text-md-right">
                                            <i class="icofont icofont-growth"></i>
                                            <i class="icofont icofont-growth icon-bg"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="flot-chart-container">
                                    <div id="bar-chart-sparkline-third" class="flot-chart-placeholder"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-3 width50">
                        <div class="card">
                            <div class="business-chart-widget bg-danger">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>Sales</p>
                                        <h3 class="total-num">45987</h3>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="icon text-md-right">
                                            <i class="icofont icofont-sale-discount"></i>
                                            <i class="icofont icofont-sale-discount icon-bg"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="flot-chart-container">
                                    <div id="bar-chart-sparkline-forth" class="flot-chart-placeholder"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-8 col-xl-9">
                        <div class="card">
                            <div class="card-header">
                                <h4>Scatter chart for sale</h4>
                                <span>Company Product Sales Chart</span>
                            </div>
                            <div class="p-2 p-md-5">
                                <div class="scatter-chart flot-chart-container p-0 m-0"></div>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4 col-xl-3">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="social-widget">
                                        <div class="media">
                                            <div  class="radial-bar radial-bar-85 radial-bar-md radial-bar-info mr-3">
                                                <div class="social-icons"><i class="icofont icofont-social-facebook font-info"></i></div>
                                            </div>
                                            <div class="media-body align-self-center">
                                                <p class="m-0 social-sub-title">Facebook</p>
                                                <hr class="m-1">
                                                <h3 class="counter m-0 total-value">2659</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="social-widget">
                                        <div class="media">
                                            <div  class="radial-bar radial-bar-60 radial-bar-md radial-bar-secondary mr-3">
                                                <div class="social-icons"><i class="icofont icofont-social-twitter font-secondary"></i></div>
                                            </div>
                                            <div class="media-body align-self-center">
                                                <p class="m-0 social-sub-title">Twitter</p>
                                                <hr class="m-1">
                                                <h3 class="counter m-0 total-value">8956</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="social-widget">
                                        <div class="media">
                                            <div  class="radial-bar radial-bar-80 radial-bar-md radial-bar-danger mr-3">
                                                <div class="social-icons"><i class="icofont icofont-social-pinterest font-danger"></i></div>
                                            </div>
                                            <div class="media-body align-self-center">
                                                <p class="m-0 social-sub-title">Pinterest</p>
                                                <hr class="m-1">
                                                <h3 class="counter m-0 total-value">7893</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="social-widget">
                                        <div class="media">
                                            <div  class="radial-bar radial-bar-50 radial-bar-md radial-bar-primary mr-3">
                                                <div class="social-icons"><i class="icofont icofont-brand-linkedin font-primary"></i></div>
                                            </div>
                                            <div class="media-body align-self-center">
                                                <p class="m-0 social-sub-title">Linkedin</p>
                                                <hr class="m-1">
                                                <h3 class="counter m-0 total-value">2659</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-xl-6 width100">
                        <div class="card">
                            <div class="card-header">
                                <h5>Product Chart</h5>
                            </div>
                            <div class="card-body">
                                <div class="user-status table-responsive">
                                    <table class="table table-bordernone">
                                        <thead>
                                        <tr>
                                            <th scope="col">Details</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Price</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Simply dummy text of the printing</td>
                                            <td class="digits">1</td>
                                            <td class="font-secondary">Pending</td>
                                            <td class="digits">$6523</td>
                                        </tr>
                                        <tr>
                                            <td>Long established</td>
                                            <td class="digits">5</td>
                                            <td class="font-danger">Cancle</td>
                                            <td class="digits">$6523</td>
                                        </tr>
                                        <tr>
                                            <td>sometimes by accident</td>
                                            <td class="digits">10</td>
                                            <td class="font-danger">Cancle</td>
                                            <td class="digits">$6523</td>
                                        </tr>
                                        <tr>
                                            <td>Classical Latin literature</td>
                                            <td class="digits">9</td>
                                            <td class="font-info">Return</td>
                                            <td class="digits">$6523</td>
                                        </tr>
                                        <tr>
                                            <td>keep the site on the Internet</td>
                                            <td class="digits">8</td>
                                            <td class="font-secondary">Pending</td>
                                            <td class="digits">$6523</td>
                                        </tr>
                                        <tr>
                                            <td>Molestiae consequatur</td>
                                            <td class="digits">3</td>
                                            <td class="font-danger">Cancle</td>
                                            <td class="digits">$6523</td>
                                        </tr>
                                        <tr>
                                            <td>Pain can procure</td>
                                            <td class="digits">8</td>
                                            <td class="font-info">Return</td>
                                            <td class="digits">$6523</td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-xl-6 width100">
                        <div class="card">
                            <div class="card-header">
                                <h5>Employee Status</h5>
                            </div>
                            <div class="card-body">
                                <div class="user-status table-responsive">
                                    <table class="table table-bordernone">
                                        <thead>
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">Designation</th>
                                            <th scope="col">Level</th>
                                            <th scope="col">Experience</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="bd-t-none">
                                                <div class="d-inline-block align-middle">
                                                    <img src="../assets/images/user/4.jpg" alt="user" class="img-radius img-40 align-top m-r-15 rounded-circle">
                                                    <div class="d-inline-block">
                                                        <h6>John Deo <span class="text-muted digits">(14+ Online)</span></h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Designer</td>
                                            <td>
                                                <div class="progress-showcase">
                                                    <div class="progress" style="height: 13px;">
                                                        <div class="progress-bar bg-primary" role="progressbar" style="width: 30%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="digits">2 Year</td>
                                        </tr>
                                        <tr>
                                            <td class="bd-t-none">
                                                <div class="d-inline-block align-middle">
                                                    <img src="../assets/images/user/1.jpg" alt="user" class="img-radius img-40 align-top m-r-15 rounded-circle">
                                                    <div class="d-inline-block">
                                                        <h6>AB Mako <span class="text-muted digits">(20+ Online)</span></h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Developer</td>
                                            <td>
                                                <div class="progress-showcase">
                                                    <div class="progress" style="height: 13px;">
                                                        <div class="progress-bar bg-danger" role="progressbar" style="width: 70%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="digits">3 Year</td>
                                        </tr>
                                        <tr>
                                            <td class="bd-t-none">
                                                <div class="d-inline-block align-middle">
                                                    <img src="../assets/images/user/5.jpg" alt="" class="img-radius img-40 align-top m-r-15 rounded-circle">
                                                    <div class="d-inline-block">
                                                        <h6>Mohi lara<span class="text-muted digits">(99+ Online)</span></h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Tester</td>
                                            <td>
                                                <div class="progress-showcase">
                                                    <div class="progress" style="height: 13px;">
                                                        <div class="progress-bar bg-info" role="progressbar" style="width: 60%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="digits">5 Month</td>
                                        </tr>
                                        <tr>
                                            <td class="bd-t-none">
                                                <div class="d-inline-block align-middle">
                                                    <img src="../assets/images/user/6.jpg" alt="" class="img-radius img-40 align-top m-r-15 rounded-circle">
                                                    <div class="d-inline-block">
                                                        <h6>Hileri Soli <span class="text-muted digits">(23+ Online)</span></h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Designer</td>
                                            <td> <div class="progress-showcase">
                                                <div class="progress" style="height: 13px;">
                                                    <div class="progress-bar bg-secondary" role="progressbar" style="width: 30%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                            </td>
                                            <td class="digits">3 Month</td>
                                        </tr>
                                        <tr>
                                            <td class="bd-t-none">
                                                <div class="d-inline-block align-middle">
                                                    <img src="../assets/images/user/7.jpg" alt="" class="img-radius img-40 align-top m-r-15 rounded-circle">
                                                    <div class="d-inline-block">
                                                        <h6>Pusiz bia <span class="text-muted digits">(14+ Online)</span></h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Designer</td>
                                            <td><div class="progress-showcase">
                                                <div class="progress" style="height: 13px;">
                                                    <div class="progress-bar bg-info" role="progressbar" style="width: 90%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div></div></td>
                                            <td class="digits">5 Year</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-xl-6 width100">
                        <div class="card">
                            <div class="card-header">
                                <h5>Customer Review</h5>
                            </div>
                            <div class="card-body">
                                <div id="customer-review" class="owl-carousel owl-theme">
                                    <div class="slide--item">
                                        <div class="review-box">
                                            <div class="media">
                                                <img class="rounded-circle img-thumbnail mr-5 img-100" src="../assets/images/user/12.png" alt="">
                                                <div class="align-self-center media-body">
                                                    <h4 class="mt-0 customer-name">Mark Jenco</h4>
                                                    <p class="m-0 text-muted">CEO & Founder At Company</p>
                                                </div>
                                            </div>
                                            <div class="testimonial mt-5">
                                                <div class="content">
                                                    <p class="description text-muted">
                                                        I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth,
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide--item">
                                        <div class="review-box">
                                            <div class="media">
                                                <img class="rounded-circle img-thumbnail mr-5 img-100" src="../assets/images/user/12.png" alt="">
                                                <div class="align-self-center media-body">
                                                    <h4 class="mt-0 customer-name">Mark Jenco</h4>
                                                    <p class="m-0 text-muted">CEO & Founder At Company</p>
                                                </div>
                                            </div>
                                            <div class="testimonial mt-5">
                                                <div class="content">
                                                    <p class="description text-muted">
                                                        I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth,
                                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slide--item">
                                        <div class="review-box">
                                            <div class="media">
                                                <img class="rounded-circle img-thumbnail mr-5 img-100" src="../assets/images/user/12.png" alt="">
                                                <div class="align-self-center media-body">
                                                    <h4 class="mt-0 customer-name">Mark Jenco</h4>
                                                    <p class="m-0 text-muted">CEO & Founder At Company</p>
                                                </div>
                                            </div>
                                            <div class="testimonial mt-5">
                                                <div class="content">
                                                    <p class="description text-muted">
                                                        I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth,
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-12 col-sm-12 width100">
                        <div class="card">
                            <div class="card-header">
                                <h5>Recent Blog</h5>
                                <span>Contrary to popular belief, Lorem Ipsum is not simply random text.</span>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="blog-box blog-grid">
                                            <img src="../assets/images/blog/blog-5.png" class="img-fluid" alt="blog">
                                            <div class="blog-details">
                                                <ul class="blog-social">
                                                    <li>Mark Jecno</li>
                                                    <li class="digits">02 Hits</li>
                                                    <li class="digits">598 Comments</li>
                                                </ul>
                                                <hr>
                                                <h6>Perspiciatis unde omnis iste natus error sit.Dummy text</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="blog-box blog-grid">
                                            <img src="../assets/images/blog/blog-6.png" class="img-fluid" alt="blog">
                                            <div class="blog-details">
                                                <ul class="blog-social">
                                                    <li>Mark Jecno</li>
                                                    <li class="digits">02 Hits</li>
                                                    <li class="digits">598 Comments</li>
                                                </ul>
                                                <hr>
                                                <h6>Perspiciatis unde omnis iste natus error sit.Dummy text</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-4 width50">
                        <div class="card">
                            <div class="calender-widget">
                                <div class="cal-img"></div>
                                <div class="cal-date">
                                    <h4>25<br>APRIL</h4>
                                </div>
                                <div class="cal-desc text-center">
                                    <h6 class="f-w-600">I must explain to you how all this mistaken idea truth</h6>
                                    <p class="text-muted f-14 mt-4 mb-0">Denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth,Letraset sheets containing Lorem Ipsum passages, and more recently with desktop </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-xl-8 width50">
                        <div class="card">
                            <div class="contact-form">
                                <form class="theme-form">
                                    <div class="form-icon">
                                        <i class="icofont icofont-envelope-open"></i>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputName">Your Name</label>
                                        <input type="text" class="form-control" id="exampleInputName" placeholder="John Dio">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="col-form-label">Email</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Demo@gmail.com">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" class="col-form-label">Message</label>
                                        <textarea rows="3" cols="50" class="form-control textarea" placeholder="Your Message"></textarea>

                                    </div>
                                    <div class="text-sm-right">
                                        <button class="btn btn-secondary-gradien">SEND IT</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection