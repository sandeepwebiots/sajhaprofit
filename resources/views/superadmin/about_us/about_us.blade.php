@extends('layouts.dashboard.master')

@section('title') About us | Dashboard @endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>About us
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Home Section</li>
                        <li class="breadcrumb-item active">About us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>About us content Change</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <form class="form theme-form" action="{{ route('about-us.update',$about_us->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Title</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="title" class="form-control" placeholder="Type your title" value="{{ $about_us->title  }}">
                                            @if($errors->has('title'))
                                                <strong class="text-danger">{{ $errors->first('title') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Sub Title</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="sub_title" class="form-control" placeholder="Type your sub title" value="{{  $about_us->sub_title  }}">
                                            @if($errors->has('sub_title'))
                                                <strong class="text-danger">{{ $errors->first('sub_title') }}</strong>
                                            @endif
                                        </div>
                                    </div>                                    
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Content</label>
                                        <div class="col-sm-9">
                                            <textarea rows="7" cols="5" name="content" id="editor1" class="form-control" placeholder="Type your Description">{{  $about_us->content }}</textarea>
                                            @if($errors->has('content'))
                                                <strong class="text-danger">{{ $errors->first('content') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="{{ url('super-admin-blog-section') }}" class="btn btn-light">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection