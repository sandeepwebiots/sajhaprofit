@extends('layouts.dashboard.master')

@section('title') Dashboard @endsection

@section('style')    
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
@endsection

@section('content')


 <div class="page-body">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>User Profile
                            </h5>
                        </div>
                        <div class="col-lg-6">
                            <ol class="breadcrumb pull-right">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="breadcrumb-item">Users</li>
                                <li class="breadcrumb-item active">User Profile</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="user-profile">
                    <div class="row">
                        <!--user profile first-style start-->
                        <div class="col-sm-12">
                            <div class="card hovercard text-center">
                                <div class="cardheader"></div>
                                <div class="user-image">
                                      <div class="avatar">
                                        <img alt="profile-pic" src=" {{ URL::asset('assets/dashboard/images/user/11.png') }}">
                                    </div>
                                    <div class="icon-wrapper"><i class="icofont icofont-pencil-alt-5"></i></div>
                                </div>
                                <div class="info">
                                    <div class="row">
                                        <div class="col-sm-6 col-lg-4 order-sm-1 order-xl-0">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="ttl-info text-left">
                                                        <h6><i class="fa fa-envelope"></i>&nbsp;&nbsp;&nbsp;Email</h6>
                                                        <span>Marekjecno@yahoo.com</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="ttl-info text-left">
                                                        <h6><i class="fa fa-calendar"></i>&nbsp;&nbsp;&nbsp;BOD</h6>
                                                        <span>02 January 1988</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-lg-4 order-sm-0 order-xl-1">
                                            <div class="user-designation">
                                                <div class="title">
                                                    <a target="_blank" href="">Mark jecno</a>
                                                </div>
                                                <div class="desc mt-2">designer</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-lg-4 order-sm-2 order-xl-2">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="ttl-info text-left">
                                                        <h6><i class="fa fa-phone"></i>&nbsp;&nbsp;&nbsp;Contact Us</h6>
                                                        <span>India +91 123-456-7890</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="ttl-info text-left">
                                                        <h6><i class="fa fa-location-arrow"></i>&nbsp;&nbsp;&nbsp;Location</h6>
                                                        <span>B69 Near Schoool Demo Home</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="social-media">
                                        <ul class="list-inline">
                                            <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li class="list-inline-item"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                            <li class="list-inline-item"><a href="#"><i class="fa fa-rss"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="follow">
                                        <div class="row">
                                            <div class="col-6 text-md-right border-right">
                                                <div class="follow-num counter">25869</div>
                                                <span>Follower</span>
                                            </div>
                                            <div class="col-6 text-md-left">
                                                <div class="follow-num counter">659887</div>
                                                <span>Following</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--user profile first-style end-->

                        <!--user profile second-style start-->
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="edit-form">
                                    <div class="col-sm-12 ">
                        <div class="card">
                            <div class="card-header">
                                <h5>Edit Profile</h5>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate>
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom01">First name</label>
                                            <input type="text" class="form-control" id="validationCustom01" placeholder="First name" value="Mark" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom02">Last name</label>
                                            <input type="text" class="form-control" id="validationCustom02" placeholder="Last name" value="Otto" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustomUsername">Username</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="inputGroupPrepend">@</span>
                                                </div>
                                                <input type="text" class="form-control" id="validationCustomUsername" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                                <div class="invalid-feedback">
                                                    Please choose a username.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom03">City</label>
                                            <input type="text" class="form-control" id="validationCustom03" placeholder="City" required>
                                            <div class="invalid-feedback">
                                                Please provide a valid city.
                                            </div>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <label for="validationCustom04">State</label>
                                            <input type="text" class="form-control" id="validationCustom04" placeholder="State" required>
                                            <div class="invalid-feedback">
                                                Please provide a valid state.
                                            </div>
                                        </div>
                                        <div class="col-md-3 mb-3">
                                            <label for="validationCustom05">Zip</label>
                                            <input type="text" class="form-control" id="validationCustom05" placeholder="Zip" required>
                                            <div class="invalid-feedback">
                                                Please provide a valid zip.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-check">
                                            <div class="checkbox p-0">
                                                <input id="invalidCheck" type="checkbox" class="form-check-input" required>
                                                <label class="form-check-label" for="invalidCheck">Agree to terms and conditions</label>
                                            </div>
                                            <div class="invalid-feedback">
                                                You must agree before submitting.
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary" type="submit">Save Changes</button>
                                </form>
                            </div>
                        </div>

                        
                    </div>
                                </div>
                            </div>
                        </div>
                        <!--user profile second-style end-->
                        <!-- 2fa google -->
                                                <div class="col-sm-12">
                            <div class="card">
                                <div class="edit-form">
                                    <div class="col-sm-12 ">
                        <div class="card">
                            <div class="card-header">
                                <h5>2FA authentication</h5>
                            </div>
                            <div class="card-body">
                                    <div class="2fa-box">
                                        1212
                                    </div>
                            </div>
                        </div>

                        
                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- google -->
                    </div>
                </div>
            </div>
        </div>
@section('script')

@endsection

@endsection