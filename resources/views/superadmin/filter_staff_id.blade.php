@extends('layouts.dashboard.master')

@section('title') Staff List | Dashboard @endsection

@section('style')    
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatable-extension.css') }}" />
@endsection

@section('content')
@php $slug = Sentinel::getUser()->roles()->first()->slug; @endphp
 <div class="page-body user-management">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>@if($slug == 'm10') Franchise Staff @else Staff Management @endif</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active"><a href="{{ url('staff-manage') }}">@if($slug == 'm10') Franchise Staff @else Staff Management @endif</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>@if($slug == 'm10') Franchise Staff @else Staff List @endif</h5>
                        <a href="{{ route('staff-manage.create') }}" class="btn btn-primary pull-right">Add Staff</a>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="list-staff" class="display">
                                <div class="col-sm-8">
                                    <div class="date-picker">
                                        <form method="post" action="{{ url('filter-id-proof') }}">
                                            {{ csrf_field() }}
                                            <div class="form-group row">
                                                <div class="col-sm-4">
                                                    <select name="id_proof" class="form-control" id="id_proof">
                                                        <option value="">Select Proof</option>
                                                        <option value="adhaar_no" {{ old('id_proof') == 'adhaar_no' ? 'selected' : '' }}>Aadhar Card</option>
                                                        <option value="pan_no" {{ old('id_proof') == 'pan_no' ? 'selected' : '' }}>Pan Card</option>
                                                    </select>
                                                    @if($errors->has('id_proof'))<span class="text-danger">{{ $errors->first('id_proof') }}</span>@endif
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" name="id_no" class="form-control" value="{{ old('id_no') }}" />
                                                    @if($errors->has('id_no'))<span class="text-danger">{{ $errors->first('id_no') }}</span>@endif
                                                </div>
                                                <div class="col-sm-4">
                                                    <button type="submit" class="btn btn-primary">Filter</button>
                                                </div>
                                            </div>   
                                        </form>
                                    </div>
                                </div>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Status</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                    @if($slug == 'm10')
                                    @else
                                    <th>Child</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                   @php $i=1; @endphp

                                   @foreach($stafflist as $staff)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $staff->user_name }}</td>    
                                            <td>@if(isset($staff->designation->post))
                                                {{ $staff->designation->post }}@endif</td> 
                                            <td>
                                                @if($staff->is_delete == 0)
                                                    @if($staff->status == 0)<span class="badge badge-info">New Join</span> @elseif($staff->status == 1)<span class="badge badge-success">Active</span>
                                                    @elseif($staff->status == 2)<span class="badge badge-danger">Blcok</span>
                                                     @elseif($staff->status == 4)
                                                    <span class="badge badge-info">Wait Admin Confirmation</span>
                                                    @endif
                                                @else
                                                    @if($staff->is_delete == 1)
                                                    <span class="badge badge-secondary">Wait Reviewer Confirmation</span>
                                                    @elseif($staff->is_delete == 2)
                                                    <span class="badge badge-secondary">Wait Admin Confirmation</span>
                                                    @endif
                                                @endif
                                            </td>
                                            <td><a href="{{ url('staff-detial') }}/{{$staff->id}}" class="btn btn-primary"> Details </a></td> 
                                            <td>
                                                
                                                @if($staff->is_delete == 1)
                                                     @if($staff->is_delete == 1)
                                                    <span class="badge badge-danger">Deleted Staff</span>
                                                    @elseif($staff->is_delete == 2)
                                                    <span class="badge badge-danger">Deleted Staff</span>
                                                    @endif
                                                @else
                                                <a href="{{ url('staff-edit') }}/{{ $staff->id }}" class="btn btn-info"><i class="fa fa-pencil"></i>&nbsp; Edit </a>
                                                <a href="{{ url('delete-staff') }}/{{ $staff->id }}" onclick="return confirm('Are you sure you want to delete?');" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp; Delete </a>
                                                @endif

                                                @if($staff->status == 1 && $slug == 'super_admin')
                                                    <a href="{{ url('deactivate-staff') }}/{{ $staff->id }}" onclick="return confirm('Are you sure you want to deactivate?');" class="btn btn-secondary"><i class="fa fa-ban"></i>&nbsp; Deactivate </a>
                                                @endif

                                                @if($slug == 'm10') @if($staff->status == 1)<a href="{{ url('assign-staff') }}/{{ $staff->designation_id }}" class="btn btn-primary">Assign State </a>
                                                <a href="{{ url('lead') }}/{{ $staff->designation_id }}" class="btn btn-primary"> Lead </a>
                                                 @endif
                                                @else @endif
                                                <!-- {{-- @if($staff->status == 0)
                                                <a href="{{ url('staff-active') }}/{{$staff->id}}" onclick="return confirm('Are you sure you want to active?');" class="btn btn-primary">Active</a>
                                                <a href="{{ url('staff-block') }}/{{$staff->id}}" onclick="return confirm('Are you sure you want to block?');" class="btn btn-danger">Cancel</a>
                                                @elseif($staff->status == 1)<span class="text-success">Active</span>
                                                @elseif($staff->status == 2)<span class="text-danger">Cancel</span>
                                                @endif --}} -->

                                            </td>
                                            @if($slug == 'm10')
                                            @else  
                                            <td>
                                                @if($staff->status == 1)<a href="{{ route('superadmin-childstaff',[$staff->designation->id]) }}" class="btn btn-primary">Child</a> @endif
                                            </td>
                                            @endif
                                        </tr>
                                   @endforeach
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#list-staff').DataTable();
    } );
    $('#id_proof').on('click',function(){
        console.log('sjdvgh');
    });
</script>
@endsection

