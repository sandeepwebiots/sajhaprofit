@extends('layouts.dashboard.master')

@section('title') Faq |  @endsection

@section('style')
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}   
</style>
@endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>FAQ</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">FAQ</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#faqModel">Add New</button>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="basic-key-table" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Quotations</th>
                                    <th>Answer</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                 @php $i = 1; @endphp
                                 @foreach($faqs as $faq)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ str_limit($faq['quotation'],60) }}</td>
                                        <td>{{ str_limit($faq['answer'],25) }}</td>
                                        <td>@if($faq->status == 0)<span class="badge badge-danger">Disable</span>@elseif($faq->status == 1)<span class="badge badge-success">Enable</span>@endif</td>
                                        <td><a href="#" class="text-primary" onclick="modelpopop({{$faq->id}})" data-toggle="modal" data-target="#faqEditModel" ><i class="fa fa-pencil"></i> Edit</a> |
                                            <a href="{{ url('faq-delete') }}/{{ $faq->id }}" class="text-danger"><i class="fa fa-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                   @endforeach 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="faqModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add New</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="faqModel" action="{{ route('faq-list.store') }}" method="post">
                            {{ csrf_field() }} 
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label" >Quotations:</label>
                                <input type="text" class="form-control" id="quotations" name="quotations">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Answer:</label>
                                <textarea class="form-control" id="answer" name="answer"></textarea>
                            </div>
                            <div class="col">
                                <div class="form-group m-t-15 m-checkbox-inline mb-0 custom-radio-ml">
                                    <label for="message-text" class="col-form-label">Status:</label>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="status" id="enable" value="1">
                                        <label for="enable" class="mb-0">Enable</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="status" id="disable" value="0">
                                        <label for="disable" class="mb-0">Disable</label>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="faqEditModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Faq</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="faqEditModel" action="" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label" >Quotations:</label>
                                <input type="text" class="form-control" id="edit_quotations" name="edit_quotations">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Answer:</label>
                                <textarea class="form-control" id="edit_answer" name="edit_answer"></textarea>
                            </div>
                            <div class="col">
                                <div class="form-group m-t-15 m-checkbox-inline mb-0 custom-radio-ml">
                                    <label for="message-text" class="col-form-label">Status:</label>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="edit_status" class="edit_status" id="edit_enable" value="1">
                                        <label for="edit_enable" class="mb-0">Enable</label>
                                    </div>
                                    <div class="radio radio-primary">
                                        <input type="radio" name="edit_status" class="edit_status" id="edit_disable" value="0">
                                        <label for="edit_disable" class="mb-0">Disable</label>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {

    $('.faqModel').validate({ // initialize the plugin
        rules: {
            quotations: {
                required: true,
            },
            answer: {
                required: true,
            }
        }
    });

    $('.faqEditModel').validate({ // initialize the plugin
        rules: {
            edit_quotations: {
                required: true,
            },
            edit_answer: {
                required: true,
            }
        }
    });

});

function modelpopop(id)
{
    // console.log(id);

        $.ajax({
            method: 'GET',
            url: '/faq/'+id, 

            success: function(response){ 
                $('.faqEditModel').attr('action', "{{url('/faq-update')}}/"+id);
                $('#edit_quotations').val(response.quotation);
                $('#edit_answer').val(response.answer);
                if(response.status == 1)
                {
                    $('#edit_enable').prop('checked', true);
                }else{
                    $('#edit_disable').prop('checked', true);
                }
           },
            error: function(jqXHR, textStatus, errorThrown) { 
                console.log("error");
            }
        });
}
</script>

@endsection