@extends('layouts.dashboard.master')

@section('title') Revenue | Dashboard @endsection

@section('content')

@php $slug = Sentinel::getUser()->roles()->first()->slug; @endphp
<div class="page-body dashboard-set">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Revenue & Commission</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Revenue & Commission</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Revenue & Commission</h5>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
                            <table id="staff-level" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Department</th>
                                    <th>Total Commission</th>
                                    <th>Total Revenue</th>
                                    <th>Month</th>
                                </tr>
                                </thead>

                                <tbody>
                                @php $i=1; @endphp
                                @foreach($revenues as $key => $revenue)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $revenue['designation']['post'] }}</td>
                                    <td>{{ $revenue->commission }}</td>
                                    <td>{{ $revenue->revenue }}</td>
                                    <td>{{ $revenue->month }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>

@endsection

@section('script')
    <script src="{{ URL::asset('assets/dashboard/js/radial-chart-tooltip.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#staff-level').DataTable();
        });
    </script>
@endsection


