@extends('layouts.dashboard.master')

@section('title') Promotion Staff | Dashboard @endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Promotion Staff</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Promotion Staff</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="invoice">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="text-md">
                                        <h3>Promotion Staff</h3> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="b-b-info"/>
                                
                        <div>
                            <!-- form start -->
                            <form method="post" action="{{ url('give-promotion-staff') }}">
                                {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="col-md-4 mb-3">
                                        <label class="hedding">Level</label>
                                        <select class="form-control" name="role" id="role">
                                            <option value="">Select</option>
                                            @foreach($roles as $role)
                                                <option class="role" name="role" value="{{ $role->slug }}">{{ $role->name }}</option>
                                            @endforeach
                                            
                                        </select>
                                        @if($errors->has('role'))<span class="text-danger">{{ $errors->first('role') }}</span>@endif
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label class="hedding">Promoted Staff Name</label>
                                        <select class="form-control" name="staff" id="head">
                                                <option value="">Select</option>
                                          
                                        </select>
                                        @if($errors->has('staff'))<span class="text-danger">{{ $errors->first('staff') }}</span>@endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4 mb-3">
                                        <label class="hedding">Level</label>
                                        <select class="form-control" name="level" id="level" onchange="designation(this);">
                                            <option value="">Select</option>
                                            @foreach($roles as $role)
                                                <option class="role" name="level" value="{{ $role->slug }}">{{ $role->name }}</option>
                                            @endforeach
                                            
                                        </select>
                                        @if($errors->has('level'))<span class="text-danger">{{ $errors->first('level') }}</span>@endif
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label class="hedding">Promoted By Department</label>
                                        <select class="form-control" name="department" id="department" onchange="alredygive(this);">
                                                <option value="">Select</option>
                                        </select>
                                        @if($errors->has('department'))<span class="text-danger">{{ $errors->first('department') }}</span>@endif
                                        <span class="text-danger" id="alredy-staff"></span>
                                    </div>
                                </div>
                                <button class="btn btn-primary pull-right" type="submit">Promoted</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('#role').on('change', function() {

        var slug =jQuery(this).val();
        // console.log(slug);
        var head='';

            $.ajax({
            method: 'GET',
            url: '/promoted-staff-name/'+slug,
            success: function(response){
                if (response) 
                { 
                    for(var i=0;i<response.length;i++)
                    { 
                        if(response[i] == null){ continue; }
                        head+='<option name="head" value="'+response[i]["id"]+'">'+response[i]["user_name"]+'</option>';
                    }
                }else
                {
                    head = '<option value="">No service</option>';
                }
                $('#head').html(head);
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                // console.log("error");
            }
        });
    }); 

    function designation(val){
        var slug =jQuery(val).val();
        var designation = '';

        $.ajax({
            method:'GET',
            url: '/designation-role/'+slug,
            success: function(data){
                // console.log(data);
                if (data.length>0) {

                    for(var i=0; i<data.length; i++)
                    {
                        designation += '<option name="department" value="'+data[i]["id"]+'">'+data[i]["post"]+'</option>';
                    }
                }else{
                    designation += '<option value="">No service</option>';
                }
                $('#department').html(designation);
                alredygive();
            },
            error:function(){

            }
        });
    }

    function alredygive(val){
        var post = $('#department').val();
        var error = '';

        $.ajax({
            method:'GET',
            url: 'chek-position/'+post,
            success:function(data){
                if (data == 1) {
                    $( ".btn-primary" ).prop( "disabled", true );
                    $('#alredy-staff').html('Alredy staff worked this department.');
                }else if (data == 0) {
                    $('#alredy-staff').html('');
                    $( ".btn-primary" ).prop( "disabled", false );
                }
            },
            error:function(){

            }
        });
    }
</script>
@endsection