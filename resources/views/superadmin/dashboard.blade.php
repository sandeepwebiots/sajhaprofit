@extends('layouts.dashboard.master')

@section('title') Dashboard @endsection

@section('content')

@php $slug = Sentinel::getUser()->roles()->first()->slug; @endphp
<div class="page-body dashboard-set">
    <div class="container-fluid pt-30">
        <div class="row div-set-dashboard">
            <div class="col-lg-12">
                <div class="card-background-color-1">
                    <div class="-dashboard-h5"><h5>Employees</h5></div>
                    <div class="row">
                        @if($slug == 'super_admin' || $slug == 'm2' || $slug == 'm3')
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="card-body" data-intro="This is the name of this site">
                                    <div class="stat-widget-dashboard">
                                        <div class="media">
                                            <img class="mr-3" src="{{ URL::asset('assets/dashboard/images/operator.png') }}" alt="Generic placeholder image">
                                            <div class="media-body text-right">
                                                <div class="progress-bar1" data-percent="20" data-duration="1000" data-color="#ccc,#E74C3C"></div>
                                                <h3 class="mt-0 counter font-danger">{{ $m2_m4 }}</h3>
                                                
                                                <a href="{{ url('staff-level') }}/Top-Management"><h6>Top Management @if($slug == 'super_admin') : M2 – M4 @endif</h6></a>

                                            </div>
                                        </div>
                                        <div class="dashboard-chart-container">
                                            <div id="line-chart-sparkline-dashboard2" class="flot-chart-placeholder line-chart-sparkline"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($slug == 'super_admin' || $slug == 'm2' || $slug == 'm3' || $slug == 'm4' || $slug == 'm5' || $slug == 'm6')
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="card-body" data-intro="This is the name of this site">
                                    <div class="stat-widget-dashboard">
                                        <div class="media">
                                            <img class="mr-3" src="{{ URL::asset('assets/dashboard/images/operator.png') }}" alt="Generic placeholder image">
                                            <div class="media-body text-right">
                                                <div class="progress-bar1" data-percent="20" data-duration="1000" data-color="#ccc,#E74C3C"></div>
                                                <h3 class="mt-0 counter font-danger">{{ $m5_m7 }}</h3>
                                                
                                                <a href="{{ url('staff-level') }}/Middle-Management"><h6>Middle Management @if($slug == 'super_admin'): M5 – M7 @endif</h6></a>
                                            </div>
                                        </div>
                                        <div class="dashboard-chart-container">
                                            <div id="line-chart-sparkline-dashboard2" class="flot-chart-placeholder line-chart-sparkline"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($slug == 'super_admin' || $slug == 'm2' || $slug == 'm3' || $slug == 'm4' || $slug == 'm5' || $slug == 'm6' || $slug == 'm7' || $slug == 'm8' || $slug == 'm9' || $slug == 'm10')
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="card-body" data-intro="This is the name of this site">
                                    <div class="stat-widget-dashboard">
                                        <div class="media">
                                            <img class="mr-3" src="{{ URL::asset('assets/dashboard/images/operator.png') }}" alt="Generic placeholder image">
                                            <div class="media-body text-right">
                                                <div class="progress-bar1" data-percent="20" data-duration="1000" data-color="#ccc,#E74C3C"></div>
                                                <h3 class="mt-0 counter font-danger">{{ $m8_m11 }}</h3>
                                                
                                                <a href="{{ url('staff-level') }}/Junior-Management"><h6>Junior Management @if($slug == 'super_admin'): M8 – M11 @endif</h6></a>
                                            </div>
                                        </div>
                                        <div class="dashboard-chart-container">
                                            <div id="line-chart-sparkline-dashboard2" class="flot-chart-placeholder line-chart-sparkline"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-lg-3">
                            <div class="card card-set">
                                <h3 class="mt-5 counter font-danger">{{ $total_staff }}</h3>
                                <h6>Total Employee</h6>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="card card-set">
                                <h3 class="mt-5 counter font-danger">{{ $total_active_clients }}</h3>
                                <h6>Total Active Clients</h6>
                            </div>
                        </div>
                        @if($slug == 'm4' || $slug == 'm5' || $slug == 'm6')
                        <div class="col-lg-4">
                        @else
                        <div class="col-lg-3">
                        @endif
                            <div class="card card-set">
                                <h3 class="mt-5 counter font-danger">{{ $direct_clients }}</h3>
                                <h6>Direct Clients</h6>
                            </div>
                        </div>
                        @if($slug == 'm4' || $slug == 'm5' || $slug == 'm6')
                        <div class="col-lg-4">
                        @else
                        <div class="col-lg-3">
                        @endif
                            <div class="card card-set">
                                <h3 class="mt-5 counter font-danger">{{ $open_leads }}</h3>
                                <h6>Open Leads</h6>
                            </div>
                        </div>
                        @if($slug == 'm4' || $slug == 'm5' || $slug == 'm6')
                        <div class="col-lg-4">
                        @else
                        <div class="col-lg-3">
                        @endif
                            <div class="card card-set">
                                <h3 class="mt-5 counter font-danger">{{ $total_lead }}</h3>
                                <h6>Total Clients</h6>
                            </div>
                        </div>
                        @if($slug == 'm11')
                            <div class="col-lg-3">
                                <div class="card card-set">
                                    <h3 class="mt-5  font-danger">{{ Sentinel::getUser()->code }}</h3>
                                    <h6>Code</h6>
                                </div>
                            </div>
                        @endif
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card-background-color-3">
                    @if($slug == 'super_admin' || $slug == 'm10' || $slug == 'm11')
                    <a class="pull-right m-t-15" href="{{ url('revenue-list') }}">Previous Months</a>
                    @endif
                    <div class="-dashboard-h5"><h5>Revenue & Commission</h5> </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="card card-set">
                                <h3 class="mt-5 counter font-danger">{{ $revenueLastMonth }}</h3>
                                <h6>Total Revenue Last Months</h6>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card card-set">
                                <h3 class="mt-5 counter font-danger">{{ $revenueThisMonth }}</h3>
                                <h6>Total Revenue This Months</h6>
                            </div>
                        </div>
                        @if($slug == 'm11' || $slug == 'm10')
                        @else
                        <div class="col-lg-4">
                            <div class="card card-set">
                                <h3 class="mt-5 counter font-danger">{{ $m10 }}</h3>
                                <h6>Total Franchises</h6>
                            </div>
                        </div>
                        @endif
                        <div class="col-lg-4">
                            <div class="card card-set">
                                <h3 class="mt-5 counter font-danger">{{ $commissionLastMonth }}</h3>
                                <h6>Total Commission Last Months</h6>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card card-set">
                                <h3 class="mt-5 counter font-danger">{{ $commissionThisMonth }}</h3>
                                <h6>Total Commission This Months</h6>
                            </div>
                        </div>
                        @if($slug == 'm11')
                        @else
                        <div class="col-lg-4">
                            <div class="card card-set">
                                <h3 class="mt-5 counter font-danger">{{ $m11 }}</h3>
                                <h6>Total Branches</h6>
                            </div>
                        </div> 
                        @endif
                    </div>
                </div>
            </div>
            
        </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>New Staff Activate</h5>
                        </div>
                        <div class="card-body">
                            <div class="dt-ext table-responsive">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
                                <table id="list-new-staff" class="display">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Position</th>
                                        <th>Status</th>
                                        <th>Details</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @php $i=1; @endphp

                                    @foreach($staff_list as $staf)
                                        @if($staf->status == 0 || $staf->status == 4)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $staf->user_name }}</td>
                                                <td>@if(isset($staf->designation->post))
                                                        {{ $staf->designation->post }}@endif</td>
                                                <td>@if($staf->status == 0)
                                                        <span class="badge badge-primary">New Join</span> @elseif($staf->status == 1)
                                                        <span class="badge badge-success">Active</span>
                                                    @elseif($staf->status == 4)
                                                        <span class="badge badge-info">Wait Admin Confirmation</span>
                                                    @endif
                                                </td>
                                                <td><a href="{{ url('staff-detial') }}/{{ $staf->id }}" class="btn btn-primary">Details</a></td>
                                                <td>@if($staf->status == 0 || $staf->status == 4)
                                                        @if($slug == 'super_admin')
                                                            <a href="{{ url('staff-active') }}/{{$staf->id}}" onclick="return confirm('Are you sure you want to active?');" class="btn btn-primary">Active</a>
                                                        @else
                                                            <a href="{{ url('approved') }}/{{$staf->id}}" onclick="return confirm('Are you sure you want to active?');" class="btn btn-primary">Active</a>
                                                        @endif
                                                        <a href="{{ url('staff-block') }}/{{$staf->id}}" onclick="return confirm('Are you sure you want to cancel?');" class="btn btn-danger">Cancel</a>
                                                    @elseif($staf->status == 1)<span class="text-success">Active</span>
                                                    @elseif($staf->status == 2)<span class="text-danger">Cancel</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Staff Delete</h5>
                        </div>
                        <div class="card-body">
                            <div class="dt-ext table-responsive">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
                                <table id="list-delete-staff" class="display">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Position</th>
                                        <th>Status</th>
                                        <th>Details</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @php $i=1; @endphp

                                    @foreach($staff_delete as $staf)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $staf->user_name }}</td>
                                            <td>@if(isset($staf->designation->post))
                                                    {{ $staf->designation->post }}@endif</td>
                                            <td>@if($staf->is_delete == 1)
                                                    <span class="badge badge-primary">Delete by Manager</span> @elseif($staf->is_delete == 2)
                                                    <span class="badge badge-success">Delete by Reviwere</span>
                                                @endif
                                            </td>
                                            <td><a href="{{ url('staff-detial') }}/{{ $staf->id }}" class="btn btn-primary">Details</a></td>
                                            <td>@if($staf->is_delete == 1 || $staf->is_delete == 2)
                                                    <a href="{{ url('delete-approve') }}/{{$staf->id}}" onclick="return confirm('Are you sure you want to delete ?');" class="btn btn-danger">Delete</a>

                                                    <a href="{{ url('delete-cancel') }}/{{$staf->id}}" onclick="return confirm('Are you sure you want to cancel?');" class="btn btn-info">Cancel</a>
                                                @elseif($staf->status == 1)<span class="text-success">Active</span>
                                                @elseif($staf->status == 2)<span class="text-danger">Cancel</span>
                                                @endif</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#list-new-staff').DataTable();
        } );
        $(document).ready(function() {
            $('#list-delete-staff').DataTable();
        });

    </script>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36251023-1']);
        _gaq.push(['_setDomainName', 'jqueryscript.net']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>

@endsection

@section('script')
    <script src="{{ URL::asset('assets/dashboard/js/radial-chart-tooltip.js') }}"></script>
@endsection


