@extends('layouts.dashboard.master')

@section('title') Staff | Dashboard @endsection

@section('style')    

@endsection

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Staff
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Staff</li>
                        <li class="breadcrumb-item active">Add New Staff</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="invoice">
                            <div>
                                <div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="media">
                                                <div class="media-left">
                                                    <img src="{{ URL::asset('assets/dashboard/images/logo.png')}}" class="media-object " alt="">
                                                </div>
                                            </div>
                                            <!--End Info-->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-md-right">
                                                <h3>New <span class="digits counter">Staff</span></h3>
                                            </div><!--End Title-->
                                        </div>
                                    </div>
                                </div>
                                <hr class="b-b-info" />
                                
                                <div>
                                    <!-- form start -->
                                <form method="post" action="{{ url('upload-details') }} ">
                                    {{ csrf_field() }}
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label class="hedding">User Name</label>
                                            <input type="text" name="user_name" class="form-control" placeholder="User name">
                                            @if($errors->has('user_name'))<span class="text-danger">{{ $errors->first('user_name') }}</span>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label class="hedding">Email</label>
                                            <input type="email" name="email" class="form-control" placeholder="Email" autocomplete="">
                                            @if($errors->has('email'))<span class="text-danger">{{ $errors->first('email') }}</span>@endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label class="hedding">Position</label>
                                            <select class="form-control" name="role" id="role">
                                                    <option value="">Select</option>
                                                @foreach($roles as $role)
                                                    <option class="role" name="role" value="{{ $role->slug }}" id="{{ $role->slug }}">{{ $role->name }}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('role'))<span class="text-danger">{{ $errors->first('role') }}</span>@endif
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label class="hedding">Post</label>
                                            <select name="designation_id" id="post" class="form-control form-control-success btn-square">
                                                <option name="designation_id" value="">Select Post</option>
                                            </select>
                                            @if($errors->has('designation_id'))<span class="text-danger">{{ $errors->first('designation_id') }}</span>@endif
                                        </div>
                                    </div>
                                    <div class="hedding">
                                        <label>Sidebar Permission</label> 
                                    </div>
                                    <div class="form-row col-md-8 mb-3">
                                       @foreach($sidebar  as $side) 
                                        <div class="media">
                                            <label class="col-form-label m-r-10 m-l-10">{{ $side->name }}</label>
                                            <div class="media-body text-right icon-state switch-outline">
                                                <label class="switch">
                                                    <input name="sidebarid[]" value="{{ $side->id }}" type="checkbox">
                                                    <span class="switch-state bg-info"></span>
                                                </label>
                                            </div>
                                        </div>
                                        @endforeach 
                                    </div>
                                    @if($errors->has('sidebarid'))<span class="text-danger">{{ $errors->first('sidebarid') }}</span>@endif
                                    <button class="btn btn-primary pull-right" type="submit">Submit form</button>
                                </form>
                                </div>
                            </div>
                        </div>
                        <!-- End Invoice Holder-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('#role').on('change', function() {

        var slug =jQuery(this).val();
        // console.log(slug);
        var subservice='';

            $.ajax({
            method: 'GET',
            url: '/designation-id/'+slug,
            success: function(response){
                console.log(response);
                
                if (response.length>0) 
                {
                    for(var i=0;i<response.length;i++)
                    {
                        subservice+='<option name="designation_id" value="'+response[i]["id"]+'">'+response[i]["post"]+'</option>';
                        // console.log(response[i]['service_id']);
                    }
                }else
                {
                    subservice = '<option value="">No service</option>';
                }
                $('#post').html(subservice);
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                // console.log("error");
            }
        });
    });       
</script>
@endsection