@extends('layouts.dashboard.master')

@section('title') Staff Detail| Dashboard @endsection

@section('style')
<style>
.card-body span strong{
     text-transform: capitalize;
}
</style>
@endsection

@section('content')
<div class="page-body">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>Staff Management
                            </h5>
                        </div>
                        <div class="col-lg-6">
                            <ol class="breadcrumb pull-right">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                                <li class="breadcrumb-item active">Staff Management</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="invoice">
                                    <div class="invoice-form">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="media">
                                                    @if($staff->profile)
                                                    <div class="media-left">
                                                        <img src="{{ URL::asset('assets/upload/images') }}/{{ $staff->profile }}" class="media-object rounded-circle img-60" alt="" id="change">
                                                    </div>
                                                    @else
                                                    <div class="media-left">
                                                        <img src="{{ URL::asset('assets/dashboard/images/user/1.jpg') }}" class="media-object rounded-circle img-60" alt="" id="change">
                                                    </div>
                                                    @endif
                                                    <div class="media-body m-l-20">
                                                        <h4 class="media-heading">{{ $staff->user_name }}</h4>
                                                        <p>{{ $staff->email }}<br>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ URL::asset('assets/resume') }}/{{ $staff->resume }}" target="_blank" class="btn btn-primary pull-right">Resume</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xl-12">
                        <div class="card ">
                            <div class="card-header b-t-info">
                                <h5>staff Designation</h5>
                            </div>
                            <div class="card-body">
                                <span><h5>Designation&nbsp;&nbsp; : <strong >&nbsp;&nbsp;{{ $staff->designation->post }}</strong></h5></span>

                                <span><h5>Head&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong >&nbsp;&nbsp;{{ $head_desig->post }}</strong></h5></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xl-4">
                        <div class="card ">
                            <div class="card-header b-t-info">
                                <h5>Personal Details</h5>
                            </div>
                            <div class="card-body">
                                <span><h5>First Name&nbsp;&nbsp;&nbsp;&nbsp; : <strong >&nbsp;&nbsp;{{ $staff->first_name }}</strong></h5></span>
                                <span><h5>Last Name&nbsp;&nbsp;&nbsp;&nbsp;  : <strong >&nbsp;&nbsp;{{ $staff->last_name }}</strong></h5></span>
                                <span><h5>Gender&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <strong >&nbsp;&nbsp;{{ $staff->gender }}</strong></h5></span>
                                <span><h5>Date Of Birth : <strong >&nbsp;&nbsp;{{ $staff->date_birth }}</strong></h5></span>
                                <span><h5>Mobile No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <strong >&nbsp;&nbsp;{{ $staff->mobile_no }}</strong></h5></span>
                                <span><h5>Address&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <strong >&nbsp;&nbsp;{{ $staff->address }}</strong></h5></span>
                                <span><h5>City&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <strong >&nbsp;&nbsp;{{ $staff->city }}</strong></h5></span>
                                <span><h5>State&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong >&nbsp;&nbsp;{{ $staff->state }}</strong></h5></span>
                                <span><h5>Pin Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <strong >&nbsp;&nbsp;{{ $staff->zip_code }}</strong></h5></span>
                            
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xl-4">
                        <div class="card ">
                            <div class="card-header b-t-info">
                                <h5>Education Qualification</h5>
                            </div>
                            <div class="card-body">
                                <span><h5>Education&nbsp;&nbsp; : <strong >&nbsp;&nbsp;{{ $staff->education }}</strong></h5></span>
                                <span><h5>College / School : <strong >&nbsp;&nbsp;{{ $staff->school }}</strong></h5></span>
                                @if($staff->experience)
                                <span><h5>Experience : <strong >&nbsp;&nbsp;{{ $staff->experience }}</strong></h5></span>
                                @endif
                                @if($staff->company)
                                <span><h5>Company Name : <strong >&nbsp;&nbsp;{{ $staff->company }}</strong></h5></span>
                                @endif 
                                @if($staff->ref_1)
                                <span><h5>Reference&nbsp;&nbsp; : <strong >&nbsp;&nbsp;{{ $staff->ref_1 }}</strong></h5></span>
                                @endif
                                @if($staff->ref_2)
                                <span><h5>Reference&nbsp;&nbsp; : <strong >&nbsp;&nbsp;{{ $staff->ref_2 }}</strong></h5></span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xl-4">
                        <div class="card ">
                            <div class="card-header b-t-info">
                                <h5>ID Proof Details</h5>
                            </div>
                            <div class="card-body">
                                <span><h5>Adhar card no&nbsp;&nbsp; : <strong >&nbsp;&nbsp;{{ $staff->adhaar_no }}</strong></h5></span>
                                <span><h5>Pan card no&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <strong >&nbsp;&nbsp;{{ $staff->pan_no }}</strong></h5></span>
                                <span><h5>Driving Licence : <strong >&nbsp;&nbsp;{{ $staff->driving_li }}</strong></h5></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('script')

@endsection