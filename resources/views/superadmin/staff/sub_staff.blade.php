@extends('layouts.dashboard.master')

@section('title') Child Staff List | Dashboard @endsection

@section('style')    
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatable-extension.css') }}" />
@endsection

@section('content')
@php $slug = Sentinel::getUser()->roles()->first()->slug; @endphp
<div class="page-body user-management">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Child Staff 
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ url('staff-manage') }}">Staff Management</a></li>                                   
                        <li class="breadcrumb-item active">Child Staff</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Child Staff List</h5>
                        <a href="{{ route('staff-manage.create') }}" class="btn btn-primary pull-right">Add Staff</a>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="list-child-staff" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Status</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                   @php $i=1; @endphp

                                   @foreach($stafflist as $staff)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $staff->user_name }}</td>    
                                            <td>{{ $staff->designation->post }}</td> 
                                            <td>
                                                @if($staff->status == 0)<span class="badge badge-info">New Join</span> @elseif($staff->status == 1)<span class="badge badge-success">Active</span>
                                                @elseif($staff->status == 2)<span class="badge badge-danger">Blcok</span>
                                                @endif</td>
                                            <td><a href="{{ url('staff-detial') }}/{{$staff->id}}" class="btn btn-primary"> Details </a></td> 
                                            <td>
                                                <a href="{{ url('staff-edit') }}/{{ $staff->id }}" class="btn btn-info"><i class="fa fa-pencil"></i>&nbsp; Edit </a>
                                                <!-- @if($staff->status == 0)<a href="{{ url('staff-active') }}/{{$staff->id}}" onclick="return confirm('Are you sure you want to active?');" class="btn btn-primary">Active</a>
                                                <a href="{{ url('staff-block') }}/{{$staff->id}}" onclick="return confirm('Are you sure you want to block?');" class="btn btn-danger">Cancel</a>
                                                @elseif($staff->status == 1)<span class="text-success">Active</span>
                                                @elseif($staff->status == 2)<span class="text-danger">Cancel</span>
                                                @endif -->
                                                @if($staff->status == 1 && $slug == 'super_admin')
                                                    <a href="{{ url('deactivate-staff') }}/{{ $staff->id }}" class="btn btn-secondary"><i class="fa fa-ban"></i>&nbsp; Deactivate </a>
                                                @endif

                                            </td>  
                                            <td>@if($staff->status == 1)
                                                    @if($staff->designation->post == 'staff')
                                                    @else
                                                    <a href="{{ route('superadmin-childstaff',[$staff->designation->id]) }}" class="btn btn-primary">Child</a> 
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                   @endforeach
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#list-child-staff').DataTable();
    } );
</script>
@endsection