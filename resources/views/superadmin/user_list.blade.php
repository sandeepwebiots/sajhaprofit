@extends('layouts.dashboard.master')

@section('title') User Manage | Dashboard @endsection

@section('style')  
<style type="text/css" media="screen">
        .red{
    color:red;
    font-weight: bold;
}
        .green{
    color:green;
    font-weight: bold;
}
</style>  
<link rel="alternate" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="alternate" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
@endsection

@section('content')
 <div class="page-body user-management">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>User Management
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>      
                        <li class="breadcrumb-item active">User Management</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
        	<div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>User Manage</h5>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                        	<input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
                            <table id="mytablec" class="display nowrap"  style="width:100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Sr No</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    
                                    <th>Status</th>
                                    <th>Act / Dct</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                @php $i = 1; @endphp
                                      <tbody>
                                     @foreach($users as $user )

                                        @foreach($user as $values)
                                            @if($loop->index == 0)
                                             <tr id="{{ $values->user->id }}">
                                                <td></td>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $values->user->user_name }}</td>
                                                <td>{{ $values->user->email }}</td>
                                                <td>
                                                    @if($values->user->status == 0 )<span  class="badge badge-success"  id='badge{{ $values->user->id }}'>Deactive</span>
                                                    @elseif($values->user->status == 1)<span class="badge badge-secondary" id='badge{{ $values->user->id }}' >Active </span>
                                                    @elseif($values->user->status == 2)<span class="badge badge-dark" id='badge{{ $values->user->id }}'>Block</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($values->user->status == 0 )<input type="button" name="act" id='act{{ $values->user->id }}' value="Active" onclick="act('{{ $values->user->id }}')" class="btn btn-secondary">
                                                    @elseif($values->user->status == 1)<input type="button" name="act" id='act{{ $values->user->id  }}' value="Deactive" onclick="act('{{ $values->user->id }}')"  class="btn btn-danger">
                                                    @elseif($values->user->status == 2)<input type="button" name="act" id='act{{ $values->user->id  }}' value="Active" onclick="act('{{ $values->user->id }}')" class="btn btn-secondary">
                                                    @endif
                                                </td>
                                                <td>
                                                    <input type="hidden" name="id" class="userid" value="{{$values->user->id}}">
                                                    @if($values->user->status == 1)
                                                    <a href="{{ url('status-change') }}/{{ $values->user->id }}" class="btn btn-dark" title="Block">Block</a>
                                                    @elseif($values->user->status == 2)
                                                    <a href="{{ url('status-change') }}/{{ $values->user->id }}" title="Active" class="btn btn-secondary">Active</a>
                                                    @endif
                                                    <a href="javascript:void(0)" title="Delete" data-id="" value="{{$values->user->id}}" class="btn btn-danger delete">Delete</a>
                                                </td>
                                             </tr>
                                            @endif
                                            <tr class=" row_{{ $values->user->id }} table_tr bg-secondary">
                                                <td></td>
                                                <td></td>
                                                <td>@if($values->pack == 'half_yearly') HALF YEARLY @else{{ strtoupper($values->pack) }}@endif</td>
                                                <td>{{ $values->total_amount }}</td>
                                                <td>{{ date('d-m-Y', strtotime($values->start_date)) }}</td>
                                                <td class="<?php if(date('Y-m-d') <= @$values->end_date) {echo 'green';}else{echo 'red';} ?>">{{ date('d-m-Y', strtotime($values->end_date)) }}</td>
                                                <td colspan="2">@if(date('Y-m-d') <= @$values->end_date)<b class="<?php {echo 'green';} ?>">Active</b> @else<b class="<?php {echo 'red';} ?>">Expired</b></span>   @endif</td>
                                                
                                            </tr>
                                        @endforeach        
                                            
                                     @endforeach       
                               </tbody> 
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<!--Datatable js-->
<script>
// $(document).ready( function () {
//     $('#mytable').DataTable();
// } );
$('.table_tr').fadeOut();
$(document).ready(function() {
    $('#mytablec').DataTable();
} );

var _token = $('#_token').val();
$(document).on('click', '.delete', function (e) {
    var id = $('.userid').val();
    var url = "{{url('delete-user')}}/"+id;
     swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: url,
                data: {'_token' : _token,'id':id},
                success: function (data) {
                	location.reload(); 
                    swal({
		                title: "Success!",
		                icon: "success",
		                buttons: true,
		            });
                }         
            });  
        }
    });
});

function act(id)
{   var status= $('#act'+id).val();
    // console.log(id);
    // console.log(status);
   
    $.ajax({
        url:'{{ url('pack_act_de_act') }}',
        data:{'_token' : _token,'id':id,'status':status},
        success:function(result)
        {    if (result=='Active')
                {   
                    $('#act'+id).prop('value', 'Deactive');
                    $('#act'+id).removeClass('btn btn-secondary').addClass('btn btn-danger');
                    $('#badge'+id).text('Active');
                    $('#badge'+id).removeClass('badge badge-success').addClass('badge badge-secondary ');
                }else{
                    $('#act'+id).prop('value', 'Active');
                    $('#act'+id).removeClass('btn btn-danger').addClass('btn btn-secondary');
                    $('#badge'+id).text('Deactive');
                    $('#badge'+id).removeClass('badge badge-secondary ').addClass('badge badge-success');
                }
            // console.log(result);
        }
    });
}



$("#mytablec tr").click(function(){
  var id= $(this).attr('id');
  $('.table_tr').fadeOut();
  $('.row_'+id).fadeIn();
});

</script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

@endsection