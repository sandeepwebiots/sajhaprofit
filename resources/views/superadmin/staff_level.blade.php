@extends('layouts.dashboard.master')

@section('title') {{ $level }} | Dashboard @endsection

@section('content')

@php $slug = Sentinel::getUser()->roles()->first()->slug; @endphp
<div class="page-body dashboard-set">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>{{ $level }}</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">{{ $level }} Staff</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{ $level }}</h5>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
                            <table id="staff-level" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Position</th>
                                    <th>Status</th>
                                </tr>
                                </thead>

                                <tbody>
                                @php $i=1; @endphp
                                @foreach($staff as $staf)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $staf['downlinestaff']['user_name'] }}</td>
                                    <td>{{ $staf['post'] }}</td>
                                    <td>@if($staf['downlinestaff']['status'] == 0)
                                            <span class="badge badge-primary">New Join</span> @elseif($staf['downlinestaff']['status'] == 1)
                                            <span class="badge badge-success">Active</span>
                                        @elseif($staf['downlinestaff']['status'] == 4)
                                            <span class="badge badge-info">Wait Admin Confirmation</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>

@endsection

@section('script')
    <script src="{{ URL::asset('assets/dashboard/js/radial-chart-tooltip.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#staff-level').DataTable();
        });
    </script>
@endsection


