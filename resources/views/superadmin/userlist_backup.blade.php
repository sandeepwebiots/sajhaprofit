@extends('layouts.dashboard.master')

@section('title') Dashboard @endsection

@section('style')    
	
@endsection

@section('content')
 <div class="page-body user-management">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>User Management
                            </h5>
                        </div>
                        <div class="col-lg-6">
                            <ol class="breadcrumb pull-right">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                                <li class="breadcrumb-item active">User Management</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

    <div class="container-fluid">
        <div class="row">
        	<div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>User Manage</h5>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                        	<input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
                            <table id="mytable" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Pack</th>
                                    <th>Activation Date</th>
                                    <th>Expiration Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead><tbody>
                                	@php $i = 1; @endphp
                                	@foreach($users as $user)
                                		<tr>
                                              @foreach($user->payment as $payment)                              
                                			<td>{{ $i++ }}</td>
                                			<td>{{ $user->user_name }}</td>
                                			<td>{{ $user->email }}</td>  
                                            <td>{{ $payment->pack }}
                                                <input type="hidden" id="pack" value="{{ $payment->pack }}"></td>
                                            <td><input type="hidden" id="time" value="{{ $time=$payment->created_at }}">{{ $time=$payment->created_at }}</td>
                                            <td>
                                            @if($payment->pack =='quarterly')
                                             {{  $time->addMonth(3)}}
                                            @elseif($payment->pack =='monthly')
                                             {{  $time->addMonth()}}
                                            @elseif($payment->pack =='half_yearly')
                                            {{  $time->addMonth(6)}}
                                            @elseif($payment->pack =='yearly')
                                            {{  $time->addMonth(12) }}
                                            @endif 

                                            </td> 
                                			<td>
	                                			@if($user->status == 0 )<span class="badge badge-success">Deactive</span><input class="btn btn-default active toggle-off" type="checkbox"  checked data-toggle="toggle">
	                                			@elseif($user->status == 1)<span class="badge badge-secondary">Active </span><input type="checkbox" checked data-toggle="toggle">
	                                			@elseif($user->status == 2)<span class="badge badge-dark">Block</span><input type="checkbox "  class="btn btn-default active toggle-off" checked data-toggle="toggle">
	                                			@endif
                                			</td>
                                			<td>
                                				<input type="hidden" name="id" class="userid" value="{{$user->id}}">
                                                @if($user->status == 1)
                                                <a href="{{ url('status-change') }}/{{ $user->id }}" class="btn btn-dark" title="Block">Block</a>
                                                @elseif($user->status == 2)
                                                <a href="{{ url('status-change') }}/{{ $user->id }}" title="Active" class="btn btn-secondary">Active</a>
                                                @endif
                                				<a href="javascript:void(0)" title="Delete" data-id="" value="{{$user->id}}" class="btn btn-danger delete">Delete</a>
                                			</td>

                                		</tr>
                                          @endforeach
                                    @endforeach
                               </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<!--Datatable js-->
   
<script>
$(document).ready( function () {
    $('#mytable').DataTable();
} );

var _token = $('#_token').val();

$(document).on('click', '.delete', function (e) {
    var id = $('.userid').val();
    var url = "{{url('delete-user')}}/"+id;
     swal({
        title: "Are you sure?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then(function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: url,
                data: {'_token' : _token,'id':id},
                success: function (data) {
                	location.reload(); 
                    swal({
		                title: "Success!",
		                icon: "success",
		                buttons: true,
		            });
                }         
            });  
        }
    });
});

</script>
<!-- <script type="text/javascript">
function add_months(dt, n) 
 {

   return new Date(dt.setMonth(dt.getMonth() + n));      
 }
var time=$('#time').val();
dt = new Date(time);
console.log(add_months(dt,3).toString());  

dt = new Date(time);
console.log(add_months(dt, 1).toString());
</script> -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!-- <script>
    var satus={{ $user->status}};
    console.log(satus);
    $('#satus').click(function(){
    if(satus==0||satus==2)
    {    console.log(deactive);
        $('#status').text('active');
    }elseif(satus==1)
    {   console.log(active);
        $('#status').text(deactive);
    }
    });
</script> -->
@endsection