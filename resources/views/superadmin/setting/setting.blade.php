@extends('layouts.dashboard.master')

@section('title') Setting @endsection

@section('content')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h5>
                        Setting
                    </h5>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Setting</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container-fluid">
        <div class="row">
			<div class="col-md-6 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h5>Setting</h5>
					</div>
					<div class="card-body">
						<div class="user-status table-responsive product-chart">
							<table class="table table-bordernone">
								
								<tbody>
									<tr>
										<td>Lead and GST setting</td>
										<td class="font-secondary"><a href="{{ url('edit-limit') }}" class="btn btn-secondary"><i class="fa fa-pencil"></i> Edit</a></td>
									</tr>
									<tr>
										<td>Website email & mobile no setting</td>
										<td class="font-secondary"><a href="{{ url('website-details') }}" class="btn btn-secondary"><i class="fa fa-pencil"></i> Edit</a></td>
									</tr>
									<tr>
										<td>Level status setting</td>
										<td class="font-secondary"><a href="{{ url('level-active') }}" class="btn btn-secondary"><i class="fa fa-pencil"></i> Edit</a></td>
									</tr>
									<tr>
										<td>Staff commission setting</td>
										<td class="font-secondary"><a href="{{ url('staff-commission') }}" class="btn btn-secondary"><i class="fa fa-pencil"></i> Edit</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection