@extends('layouts.dashboard.master')

@section('title') Limit | Setting @endsection

@section('content')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h5>
                        Setting
                    </h5>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Setting</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container">
        <div class="row">
			<div class="col-md-8 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h5>Limit Setting</h5>
					</div>
					<div class="card-body">
					 	<form action="{{ url('update-limit') }}" method="post" >
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <label class="hedding">Lead limit for telecaller</label>
                                    <input type="text" name="telecaller" class="form-control" value="{{ $limit_tc }}">
                                    @if($errors->has('telecaller'))<span class="text-danger">{{ $errors->first('telecaller') }}</span>@endif
                                </div>
                            </div>
                             <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <label class="hedding">Lead limit for other</label>
                                    <input type="text" name="other" class="form-control" value="{{ $limit_other }}">
                                    @if($errors->has('other'))<span class="text-danger">{{ $errors->first('other') }}</span>@endif
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <label class="hedding">GST %</label>
                                    <input type="text" name="gst" class="form-control" value="{{ $gst }}">
                                    @if($errors->has('gst'))<span class="text-danger">{{ $errors->first('gst') }}</span>@endif
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <a href="{{ url('setting') }}" class="btn btn-secondary pull-right">Cancel</a>
                                    <button class="btn btn-primary pull-right" type="submit">Update</button>
                                </div>
                            </div>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection