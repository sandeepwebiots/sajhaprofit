@extends('layouts.dashboard.master')

@section('title') Limit | Setting @endsection

@section('style')
<style type="text/css" media="screen">
.text-uppercase{
    text-transform: capitalize;
}    
</style>
@endsection

@section('content')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h5>
                        Setting
                    </h5>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ url('setting') }}">Setting</a></li>
                        <li class="breadcrumb-item active">Level Active</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Level Active</h5>
                    </div>
                    <div class="card-body">
                        <div class="user-status table-responsive product-chart">
                            <table class="table table-bordernone">
                                <tbody>
                                    @foreach($roles as $role)
                                    <tr>
                                        <td>Level &nbsp;&nbsp;<span class="text-uppercase" >{{  $role->name }}</span></td>
                                        <td class="font-secondary">
                                            @if($role->status == 0)
                                                <a href="{{ url('update-level-active') }}/{{ $role->id }}" class="btn btn-secondary">Active</a>
                                            @else
                                                <strong class="text-success">Actived</strong>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
			
		</div>
	</div>
</div>
@endsection