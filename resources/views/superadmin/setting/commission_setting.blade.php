@extends('layouts.dashboard.master')

@section('title') Commission | Setting @endsection

@section('content')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h5>
                        Setting
                    </h5>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Setting</li>
                        <li class="breadcrumb-item">Commission</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container">
        <div class="row">
			<div class="col-md-8 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h5>Website Details Setting</h5>
					</div>
					<div class="card-body">
					 	<form action="{{ url('update-commission') }}" method="post" >
                            {{ csrf_field() }}
                            <div class="form-row">
                                @php $i=1; $j=1; @endphp
                                @foreach($commission_setting as $comminssion)
                                <div class="col-md-6 mb-3">
                                    <label class="hedding">Commission {{ $i++ }}</label>
                                    <input type="text" name="key{{ $comminssion->id }}" class="form-control" value="{{ $comminssion->key }}">
                                    @if($comminssion->id == '9')<p><b>*</b> Above commission</p>@endif
                                    @if($errors->has('mail_contact'))<span class="text-danger">{{ $errors->first('mail_contact') }}</span>@endif
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="hedding">Percentage {{ $j++ }}</label>
                                    <input type="text" name="value{{ $comminssion->id }}" class="form-control" value="{{ $comminssion->value }}">
                                    @if($errors->has('mail_contact'))<span class="text-danger">{{ $errors->first('mail_contact') }}</span>@endif
                                </div>
                                @endforeach
                            </div>
                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <a href="{{ url('setting') }}" class="btn btn-secondary pull-right">Cancel</a>
                                    <button class="btn btn-primary pull-right" type="submit">Update</button>
                                </div>
                            </div>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection