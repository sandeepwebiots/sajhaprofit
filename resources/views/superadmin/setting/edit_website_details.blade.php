@extends('layouts.dashboard.master')

@section('title') Limit | Setting @endsection

@section('content')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <h5>
                        Setting
                    </h5>
                </div>
                <div class="col-lg-6" data-intro="This is the name of this site">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Setting</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <div class="container">
        <div class="row">
			<div class="col-md-8 col-sm-12">
				<div class="card">
					<div class="card-header">
						<h5>Website Details Setting</h5>
					</div>
					<div class="card-body">
					 	<form action="{{ url('update-details') }}" method="post" >
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <label class="hedding">Email Contact</label>
                                    <input type="text" name="mail_contact" class="form-control" value="{{ $company_details->mail_contact }}">
                                    @if($errors->has('mail_contact'))<span class="text-danger">{{ $errors->first('mail_contact') }}</span>@endif
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <label class="hedding">Email Info</label>
                                    <input type="text" name="mail_info" class="form-control" value="{{ $company_details->mail_info }}">
                                    @if($errors->has('mail_info'))<span class="text-danger">{{ $errors->first('mail_info') }}</span>@endif
                                </div>
                            </div>
                             <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <label class="hedding">Mobile no</label>
                                    <input type="text" name="phone_no" class="form-control" value="{{ $company_details->phone_no }}">
                                    @if($errors->has('phone_no'))<span class="text-danger">{{ $errors->first('phone_no') }}</span>@endif
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <label class="hedding">Address</label>
                                    <input type="text" name="address" class="form-control" value="{{ $company_details->address }}">
                                    @if($errors->has('address'))<span class="text-danger">{{ $errors->first('address') }}</span>@endif
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <label class="hedding">CIN</label>
                                    <input type="text" name="cin" class="form-control" value="{{ $company_details->cin }}">
                                    @if($errors->has('cin'))<span class="text-danger">{{ $errors->first('cin') }}</span>@endif
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <label class="hedding">GST No</label>
                                    <input type="text" name="gst_no" class="form-control" value="{{ $company_details->gst_no }}">
                                    @if($errors->has('gst_no'))<span class="text-danger">{{ $errors->first('gst_no') }}</span>@endif
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <label class="hedding">Pan No</label>
                                    <input type="text" name="pan_no" class="form-control" value="{{ $company_details->pan_no }}">
                                    @if($errors->has('pan_no'))<span class="text-danger">{{ $errors->first('pan_no') }}</span>@endif
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-8 mb-3">
                                    <a href="{{ url('setting') }}" class="btn btn-secondary pull-right">Cancel</a>
                                    <button class="btn btn-primary pull-right" type="submit">Update</button>
                                </div>
                            </div>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection