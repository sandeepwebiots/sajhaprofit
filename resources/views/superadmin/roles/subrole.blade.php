@extends('layouts.dashboard.master')

@section('title') Roles | Dashboard @endsection

@section('content')
<style type="text/css">
    .float-right{
        float:right;
    }
</style>
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Roles
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Roles</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

     <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Roles Manage</h5>
                        <a class = "btn btn-primary float-right" href="{{url('/roles/create')}}">Add New</a>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="export-button" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Creation Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1; @endphp
                                    @foreach($subrole as $data)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $data->name }}</td>
                                            <td>{{ $data->created_at }} </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<!--Datatable js-->
   
<script>
$(document).ready( function () {
    $('#myTable').DataTable();
} );

@endsection