@extends('layouts.dashboard.master')

@section('title') Edit Slider @endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/dropzone.css') }}">
<style>
    #slider-img{
        width: 23%;
        margin: 10px;
    }
</style>
@endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h4>Edit Slider</h4>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Edit Slider</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">        
                <div class="card">
                    <div class="card-header">
                        <h5>Edit Slider</h5>
                    </div>
                    <form class="form theme-form" action="{{ route('update-slider',[$slider->id]) }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="title" class="col-sm-3">Title</label>
                                        <div class="col-sm-9">
                                            <!-- <input type="text" name="title" class="form-control form-control-lg input-air-primary" id="title" value="{{ $slider->title }}"> -->
                                            <textarea rows="4" cols="5" name="title" id="editor1" class="form-control" placeholder="Type your Title">{{ $slider->title }}</textarea>
                                            @if($errors->has('title'))<strong class="text-danger">{{ $errors->first('title') }}</strong>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="sub_title" class="col-sm-3">Sub Title</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="sub_title" class="form-control input-air-primary" id="sub_title" value="{{ $slider->sub_title }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Upload Image</label>
                                        <div class="col-sm-9">
                                            <input type="file" name="image" class="form-control input-air-primary" onchange="sliderchange(this);" value="{{ $slider->image }}">
                                            @if($errors->has('image'))<strong class="text-danger">{{ $errors->first('image') }}</strong>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                            <div class="text-right">
                                <img src="{{ URL::asset('assets/home/images/team') }}/{{ $slider->image }}" id="slider-img" alt="">
                             </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Status</label>
                                        <div class="col-sm-9">
                                            <select name="status" class="form-control btn-square" value="{{ old('status') }}">
                                                <option name="status" value="">Select option</option>
                                                <option name="status" value="1" @if($slider->status === 1) selected='selected' @endif>Enable</option>
                                                <option name="status" value="0" @if($slider->status === 0) selected='selected' @endif>Disable</option>
                                            </select>
                                            @if($errors->has('status'))<strong class="text-danger">{{ $errors->first('status') }}</strong>@endif
                                        </div>
                                     </div>
                                 </div>
                             </div>
                             <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Position</label>
                                        <div class="col-sm-9">
                                            <select name="position" class="form-control btn-square" value="{{ old('position') }}">
                                                <option name="position" value="">Select option</option>
                                                <option name="position" value="1" @if($slider->position === 1) selected='selected' @endif>1</option>
                                                <option name="position" value="2" @if($slider->position === 2) selected='selected' @endif>2</option>
                                                <option name="position" value="3" @if($slider->position === 3) selected='selected' @endif>3</option>
                                                <option name="position" value="4" @if($slider->position === 4) selected='selected' @endif>4</option>
                                                <option name="position" value="5" @if($slider->position === 5) selected='selected' @endif>5</option>
                                            </select>
                                            @if($errors->has('position'))<strong class="text-danger">{{ $errors->first('position') }}</strong>@endif
                                        </div>
                                     </div>
                                 </div>
                             </div>
                             
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
     function sliderchange(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#slider-img').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection