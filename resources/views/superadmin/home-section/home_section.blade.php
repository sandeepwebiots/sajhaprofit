@extends('layouts.dashboard.master')

@section('title') Manage | Home-Section @endsection

@section('style')    
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatable-extension.css') }}" />
@endsection

@section('content')
 <div class="page-body user-management">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Home Section Management</h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Home Section Management</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Home Section</h5>
                        <!-- <a href="{{ route('super-admin-news-section.create') }}" class="btn btn-primary pull-right">Add New</a> -->
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="export-button" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Section</th>
                                    <th>Details</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php $i=1; @endphp
                                    @foreach($home_section as $home)
                                    
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $home->section }}</td>
                                            <td>{{ $home->title }}</td>
                                            <td><a href="{{ route('manage-home-section.edit',$home->id) }}" class="btn btn-primary">Edit</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

