@extends('layouts.dashboard.master')

@section('title') Home Slider @endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/dropzone.css') }}">
@endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h4>Home Slider</h4>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Home Slider</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('add-new-slider') }}" class="btn btn-primary pull-right">Add New</a>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="basic-key-table" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Sub Title</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                   @php $i = 1; @endphp
                                   @foreach($slider as $slide)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{!! str_limit($slide['title'],60) !!}</td>
                                        <td>{{ str_limit($slide['sub_title'],25) }}</td>
                                        <td><img src="{{ URL::asset('assets/home/images/team') }}/{{ $slide->image }}" alt="" height="100px" width="100px"></td>
                                        <td>@if($slide->status == 0)<span class="badge badge-danger">Disable</span>@elseif($slide->status == 1)<span class="badge badge-success">Enable</span>@endif</td>
                                        <td width="10%"><a href="{{ route('edit-slider',[$slide->id]) }}" class="text-primary"><i class="fa fa-pencil"></i> Edit</a> |
                                            <a href="{{ url('delete-slider') }}/{{ $slide->id }}" class="text-danger"><i class="fa fa-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-12">        
                <div class="card">
                    <div class="card-header">
                        <h5>Slider <strong>#1</strong></h5>
                    </div>
                    <form class="form theme-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="colFormLabelSm26" class="col-form-label-sm">Title</label>
                                        <input type="text" class="form-control form-control-lg" id="colFormLabelSm26">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword27">Sub Title</label>
                                        <input type="text" class="form-control" id="exampleInputPassword27">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Upload File</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control input-air-primary">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col">
                            <form class="dropzone" id="singleFileUpload" action="/upload.php">
                                <div class="dz-message needsclick">
                                    <i class="icon-cloud-up"></i>
                                    <h6>Drop files here or click to upload.</h6>
                                    <span class="note needsclick">
                                        (This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)
                                        </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ URL::asset('assets/dashboard/js/dropzone/dropzone.js') }}" ></script>
<script src="{{URL::asset('assets/dashboard/js/dropzone/dropzone-script.js')}}" ></script>
@endsection