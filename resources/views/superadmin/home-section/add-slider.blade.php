@extends('layouts.dashboard.master')

@section('title') Add Slider @endsection

@section('style')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/dropzone.css') }}">
<style>
    #slider-img{
        width: 23%;
        margin: 10px;
    }
</style>
@endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h4>Add Slider</h4>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Add Slider</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">        
                <div class="card">
                    <div class="card-header">
                        <h5>Add New Slider</h5>
                    </div>
                    <form class="form theme-form" action="{{ route('added-slider') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="title" class="col-sm-3">Title</label>
                                        <div class="col-sm-9">
                                           <!--  <input type="text" name="title" class="form-control form-control-lg input-air-primary" id="title" value="{{ old('title') }}"> -->
                                           <textarea rows="4" cols="5" name="title" id="editor1" class="form-control" placeholder="Type your Title">{{ old('title') }}</textarea>
                                            @if($errors->has('title'))<strong class="text-danger">{{ $errors->first('title') }}</strong>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label for="sub_title" class="col-sm-3">Sub Title</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="sub_title" class="form-control input-air-primary" id="sub_title" value="{{ old('sub_title') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Upload Image</label>
                                        <div class="col-sm-9">
                                            <input type="file" name="image" class="form-control input-air-primary" onchange="sliderchange(this);">
                                            @if($errors->has('image'))<strong class="text-danger">{{ $errors->first('image') }}</strong>@endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                            <div class="text-right">
                                <img src="{{ URL::asset('assets/home/images/slider.png') }}" id="slider-img" alt="">
                             </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Status</label>
                                        <div class="col-sm-9">
                                            <select name="status" class="form-control btn-square" value="{{ old('status') }}">
                                                <option name="status" value="">Select option</option>
                                                <option name="status" value="1">Enable</option>
                                                <option name="status" value="0">Disable</option>
                                            </select>
                                            @if($errors->has('status'))<strong class="text-danger">{{ $errors->first('status') }}</strong>@endif
                                        </div>
                                     </div>
                                 </div>
                             </div>
                             <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Position</label>
                                        <div class="col-sm-9">
                                            <select name="position" class="form-control btn-square" value="{{ old('position') }}">
                                                <option name="position" value="">Select option</option>
                                                <option name="position" value="1">1</option>
                                                <option name="position" value="2">2</option>
                                                <option name="position" value="3">3</option>
                                                <option name="position" value="4">4</option>
                                                <option name="position" value="5">5</option>
                                            </select>
                                            @if($errors->has('position'))<strong class="text-danger">{{ $errors->first('position') }}</strong>@endif
                                        </div>
                                     </div>
                                 </div>
                             </div>
                             
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
     function sliderchange(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#slider-img').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection