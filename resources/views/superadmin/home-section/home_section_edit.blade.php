@extends('layouts.dashboard.master')

@section('title') Manage | Home-Section @endsection

@section('content')

<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Blog Edit
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Blog Edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Blog Edit</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <form class="form theme-form" action="{{ route('manage-home-section.update',$section->id) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}
                                    @if($section->slug == 'mail')
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Email</label>
                                        <div class="col-sm-4">
                                            <input type="email" name="email" class="form-control" value="{{ $section->title }}" placeholder="Type your email" required="">
                                            @if($errors->has('email'))
                                                <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    @elseif($section->slug == 'contact')
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">	Contact No</label>
                                        <div class="col-sm-4">
                                            <input type="number" name="contact_no" class="form-control" value="{{ $section->title }}" placeholder="Type your address" required="">
                                            @if($errors->has('contact_no'))
                                                <strong class="text-danger">{{ $errors->first('contact_no') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    @elseif($section->slug == 'address')
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Address</label>
                                        <div class="col-sm-4">
                                            <input type="text" name="address" class="form-control" value="{{ $section->title }}" placeholder="Type your address" required="">
                                            @if($errors->has('address'))
                                                <strong class="text-danger">{{ $errors->first('address') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">City</label>
                                        <div class="col-sm-4">
                                            <input type="text" name="city" class="form-control" value="{{ $section->sub_title }}" placeholder="Type your city">
                                            @if($errors->has('city'))
                                                <strong class="text-danger">{{ $errors->first('city') }}</strong>
                                            @endif
                                        </div>
                                    </div>                                    
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">state</label>
                                        <div class="col-sm-4">
                                            <input type="text" name="state" class="form-control" value="{{ $section->content }}" placeholder="Type your state">
                                            @if($errors->has('state'))
                                                <strong class="text-danger">{{ $errors->first('state') }}</strong>
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                    <div class="card-footer">
                                        <div class="col-sm-9 offset-sm-3">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                            <a href="{{ url('manage-home-section') }}" class="btn btn-light">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection