@extends('layouts.dashboard.master')

@section('title') Designation | Manage @endsection

@section('style')    
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatable-extension.css') }}" />
@endsection

@section('content')
 <div class="page-body user-management">
            <div class="container-fluid">
                <div class="page-header">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>Designation Management
                            </h5>
                        </div>
                        <div class="col-lg-6">
                            <ol class="breadcrumb pull-right">
                                <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                                <li class="breadcrumb-item active">Designation Management</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                <h5>Designation List</h5>
                                <a href="{{ route('designation.create') }}" class="btn btn-primary pull-right">Add New</a>
                            </div>
                            <div class="card-body">
                                <div class="dt-ext table-responsive">
                                    <table id="list-designation" class="display">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Role</th>
                                            <th>Post</th>
                                            <th>Head</th>
                                            <!-- <th>Action</th> -->
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @php $i=1; @endphp
                                            @foreach($designation as $desig)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $desig->role_id }}</td>
                                                <td>{{ $desig->post }}</td>
                                                <td>{{ $desig->designation->post}}</td>
                                                <!-- <td> -->
                                                    <!-- <a href="{{ route('designation.edit',$desig->id) }}" class="btn btn-primary">Edit</a> -->
                                                    <!-- <form action="{{ route('designation.destroy',$desig->id) }}" method="post" accept-charset="utf-8">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <button type="delete" class="btn btn-delete">Delete</button>
                                                    </form> -->
                                                <!-- </td> -->
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#list-designation').DataTable();
    } );
</script>
@endsection