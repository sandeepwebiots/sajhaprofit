@extends('layouts.dashboard.master')

@section('title') Designation-Edit | Dashboard @endsection

@section('content')
@php $slug = Sentinel::getUser()->roles()->first()->slug @endphp
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Designation</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Designation</li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="invoice">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="{{ URL::asset('assets/dashboard/images/logo.png')}}" class="media-object " style="width: 100px;">
                                        </div>
                                    </div>
                                    <!--End Info-->
                                </div>
                                <div class="col-md-6">
                                    <div class="text-md-right">
                                        <h3>Designation</h3> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="b-b-info"/>
                                
                        <div>
                            <!-- form start -->
                            <form method="post" action="{{ route('designation.update',$designation->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="hedding">Position</label>
                                        <select class="form-control" name="role" id="role">
                                            <option value="">Select</option>
                                            @foreach($roles as $role)
                                                <option class="role" name="role" value="{{ $role->slug }}" {{ $role->slug == $designation->role_id ? 'selected':'' }}>{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('role'))<span class="text-danger">{{ $errors->first('role') }}</span>@endif
                                    </div>
                                </div>
                                 <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="hedding">Post</label>
                                        <input type="text" name="post" class="form-control" placeholder="Post" value="{{ $designation->post }}">
                                        @if($errors->has('post'))<span class="text-danger">{{ $errors->first('post') }}</span>@endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="hedding">Head</label>
                                        <select class="form-control" name="head" id="head">
                                                <option value="">Select</option>
                                            
                                                <option class="role" name="head" value="{{ $head_desig->id }}" {{ $head_desig->id  == $designation->desig_id ? 'selected':'' }}>{{ $head_desig->post }}</option>
                                            
                                        </select>
                                        @if($errors->has('head'))<span class="text-danger">{{ $errors->first('head') }}</span>@endif
                                    </div>
                                </div>
                                @if($designation->role_id == 'm11')
                                @else
                                <div class="hedding">
                                    <label>Sidebar Permission</label> 
                                </div>
                                <div class="form-row col-md-12 mb-3">
                                   @foreach($sidebar  as $side)
                                   
                                    <?php $per = Sentinel::getUser()->permissions; 

                                        if(in_array($side->id, $per) || $slug == 'super_admin'){
                                            $permissions = json_decode($designation->permissions);
                                     ?>
                                    <div class="media">
                                        <label class="col-form-label m-r-10 m-l-10">{{ $side->name }}</label>
                                        <div class="media-body text-right icon-state switch-outline">
                                            <label class="switch">
                                                    @if(in_array($side->id, $permissions)) 
                                                    <input name="sidebarid[]" value="{{ $side->id }}" type="checkbox" checked > 
                                                        <span class="switch-state bg-info"></span>
                                                    @else
                                                    <input name="sidebarid[]" value="{{ $side->id }}" type="checkbox" >
                                                        <span class="switch-state bg-info"></span>
                                                    @endif
                                            </label>
                                        </div>
                                    </div>
                                        <?php }   ?>
                                    @endforeach 
                                    @if($errors->has('sidebarid'))<strong class="text-danger">{{ $errors->first('sidebarid') }}</strong>@endif
                                </div>
                                @endif
                                
                                <button class="btn btn-primary pull-right" type="submit">Update</button>
                            </form>
                        </div>
                        <!-- End Invoice Holder-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('#role').on('change', function() {

        var slug =jQuery(this).val();
        // console.log(slug);
        var head='';

            $.ajax({
            method: 'GET',
            url: '/designation-slug/'+slug,
            success: function(response){
                console.log(response);
                
                if (response.length>0) 
                {
                    for(var i=0;i<response.length;i++)
                    {
                        head+='<option name="head" value="'+response[i]["id"]+'">'+response[i]["post"]+'</option>';
                        // console.log(response[i]['service_id']);
                    }
                }else
                {
                    head = '<option value="">No service</option>';
                }
                $('#head').html(head);
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                console.log("error");
            }
        });
    }); 
</script>
@endsection