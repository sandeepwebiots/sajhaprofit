@extends('layouts.dashboard.master')

@section('title') Designation-Create | Dashboard @endsection

@section('content')
@php $slug = Sentinel::getUser()->roles()->first()->slug @endphp
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Designation</h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item">Designation</li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="invoice">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="text-md">
                                        <h3>Designation</h3> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="b-b-info"/>
                                
                        <div>
                            <!-- form start -->
                            <form method="post" action="{{ route('designation.store') }}">
                                {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="hedding">Position</label>
                                        <select class="form-control" name="role" id="role">
                                                <option value="">Select</option>
                                            @foreach($roles as $role)
                                                <option class="role" name="role" value="{{ $role->slug }}">{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('role'))<span class="text-danger">{{ $errors->first('role') }}</span>@endif
                                    </div>
                                </div>
                                 <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="hedding">Department</label>
                                        <input type="text" name="post" class="form-control" placeholder="Post">
                                        @if($errors->has('post'))<span class="text-danger">{{ $errors->first('post') }}</span>@endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="hedding">Head</label>
                                        <select class="form-control" name="head" id="head">
                                                <option value="">Select</option>
                                           <!--  @foreach($designation as $desig)
                                                <option class="role" name="head" value="{{ $desig->id }}">{{ $desig->post }}</option>
                                            @endforeach -->
                                        </select>
                                        @if($errors->has('head'))<span class="text-danger">{{ $errors->first('head') }}</span>@endif
                                    </div>
                                </div>
                                <div class="hedding">
                                    <label>Sidebar Permission</label> 
                                </div>
                                <div class="form-row col-md-12 mb-3">
                                   @foreach($sidebar  as $side)
                                   
                                    <?php $per = Sentinel::getUser()->permissions; 

                                        if(in_array($side->id, $per) || $slug == 'super_admin'){
                                     ?>
                                    <div class="media">
                                        <label class="col-form-label m-r-10 m-l-10">{{ $side->name }}</label>
                                        <div class="media-body text-right icon-state switch-outline">
                                            <label class="switch">
                                                <input name="sidebarid[]" type="checkbox" value="{{ $side->id }}" 
                                                {{ old('sidebarid[]') == $side->id  ? 'checked' : ''}}>
                                                <span class="switch-state bg-info"></span>
                                            </label>
                                        </div>
                                    </div>
                                        <?php }   ?>
                                    @endforeach 
                                    @if($errors->has('sidebarid'))<strong class="text-danger">{{ $errors->first('sidebarid') }}</strong>@endif
                                </div>
                                
                                <button class="btn btn-primary pull-right" onclick="return confirm('Are you sure you want to add Designation ?');" type="submit">Submit form</button>
                            </form>
                        </div>
                        <!-- End Invoice Holder-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('#role').on('change', function() {
        var slug =jQuery(this).val();
        console.log(slug);
        var head='';

            $.ajax({
            method: 'GET',
            url: '/designation-slug/'+slug,
            success: function(response){
                // console.log(response);
                
                if (response.length>0) 
                {
                    for(var i=0;i<response.length;i++)
                    {
                        head+='<option name="head" value="'+response[i]["id"]+'">'+response[i]["post"]+'</option>';
                        // console.log(response[i]['service_id']);
                    }
                }else
                {
                    head = '<option value="">No service</option>';
                }
                $('#head').html(head);
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                // console.log("error");
            }
        });
    }); 
</script>
@endsection