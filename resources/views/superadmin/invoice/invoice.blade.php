<!DOCTYPE html>
<html>
<head>
    <title>Invoice</title>
    <style type="text/css" media="all">
        
        /*invoice page start*/
        .invoice_section caption{
            font-size:28px;
            margin-bottom:15px;
        }
        .invoice_section table{
            border:1px solid #333;
            border-collapse:collapse;
            margin:0 auto;
        }
        .invoice_section table td, tr, th{
            padding:5px!important;
            border:1px solid #333;
            width:100px;
            color: black;
        }
        .invoice_section th{
            background-color: #f0f0f0;
        }
        .invoice_section h4, p{
            margin:0px;
        }
        .table tbody tr td {
            border: 1px solid #ddd !important;
            text-align: center;
        }
        .table_body_modify .heading_2 {
        }
        .page-break {
            page-break-after: always;
        }
        .
        .table_body_modify .heading_2 {
            font-size: 35px;
            font-weight: 700;
            background-color: white;
        }
    </style>
</head>
<body>
    <section class="invoice_section">
        <div class="col-md-8 text-center" data-aos="zoom-out" data-aos-duration="2000">
            <div class="container">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        <div class="table-responsive-md">
                            <table class="table table-striped">
                                <tbody class="table_body_modify">
                                <tr>
                                    <td  colspan="12" class="center border-bottom-set heading_1">TAX INVOICE</td>
                                </tr>
                                <tr>
                                    <td  colspan="12" class="center heading_2">SajhaProfit Investment Advisors</td>
                                </tr>
                                <tr>
                                    <td  colspan="12" class="center bold_title">Address: {{ $company_detail->address }}</td>
                                </tr>
                                <tr>
                                    <td  colspan="12" class="center bold_title">Mb:- {{ $company_detail->phone_no }}, Email: {{ $company_detail->mail_info }}</td>
                                </tr>
                                <tr>
                                    <td colspan="12" class="center heading_3">CIN: {{ $company_detail->cin }}<br>
                                        GST No: {{ $company_detail->gst_no }}
                                    </td>
                                </tr>
                                <tr>
                                    <td class="w25 td_heading_bold" colspan="4">Bill to</td>
                                    <td class="td_heading_bold" colspan="4">Place of Supply</td>
                                    <td class="td_heading_bold" rowspan="2" colspan="2">INVOICE NO : {{ $invoice_year }}{{ str_pad($invoice_no,6,'0',STR_PAD_LEFT) }}</td>
                                    <td class="td_heading_bold" rowspan="2" colspan="2">Dated <br> {{ $payment->created_at->format('d-m-Y') }}</td>
                                </tr>
                                <tr>
                                    <td class="td_heading_bold" colspan="4">Name:</td>
                                    <td colspan="4">{{ $user->user_name }}</td>
                                </tr>
                                <tr>
                                    <td class="td_heading_bold" colspan="4">Address:</td>
                                    <td  colspan="4">Address: {{ $user->address }},{{ $user->city }},{{ $user->state }},{{ $user->zip_code }} </td>
                                    <td colspan="2"></td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td class="td_heading_bold" colspan="4">Description of Service </td>
                                    <td colspan="4">{{ $payment->package->sub_service->name }}</td>
                                    <td class="td_heading_bold" colspan="4">Amount</td>
                                </tr>
                                <tr>
                                    <td class="td_heading_bold" colspan="4">CONSULATNCY SERVICES</td>
                                    <td colspan="4">{{ strtoupper($payment->pack) }}</td>
                                    <td colspan="4">Rs/- {{ $payment->pack_amount }}</td>
                                </tr>
                                <tr>
                                    <td colspan="12" class="td_heading_bold">TAXABLE VALUE</td>
                                </tr>
                                <tr>
                                    <td class="td_heading_bold" colspan="4"> IGST {{ $gst }}%</td>
                                    <td colspan="4" rowspan="3"></td>
                                    @if($user->state == 'GUJARAT')
                                    <td colspan="4" ></td>
                                    @else
                                    <td colspan="4" >Rs/- {{ $payment->gst_amount }}</td>
                                    @endif
                                </tr>
                                @php $cgst = $payment->gst_amount / 2; $per_gst = $gst / 2; @endphp
                                <tr>
                                    <td class="td_heading_bold" colspan="4"> CGST {{ $per_gst }}%</td>
                                    @if($user->state == 'GUJARAT')
                                    <td colspan="4">Rs/- {{ $cgst }}</td>
                                    @else
                                    <td colspan="4"></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td class="td_heading_bold" colspan="4">SGST {{ $per_gst }}%</td>
                                    @if($user->state == 'GUJARAT')
                                    <td colspan="4">Rs/- {{ $cgst }}</td>
                                    @else
                                    <td colspan="4"></td>
                                    @endif
                                </tr>
                                <tr class="page-break">
                                    <td class="td_heading_bold" colspan="4">Total</td>
                                    <td colspan="4"></td>
                                    <td colspan="4">Rs/- {{ $payment->total_amount }}</td>
                                </tr>
                                <tr>
                                    <td class="td_heading_bold" colspan="4">Amount Chargeable (in words)</td>
                                    <td colspan="8"></td>
                                </tr>
                                <tr>
                                    <td colspan="12" class="td_heading_bold">For SajhaProfit Investment Advisors</td>
                                </tr>
                                <tr>
                                    <td class="td_heading_bold" colspan="4">Company's PAN :</td>
                                    <td colspan="8"> {{ $company_detail->pan_no }}</td>
                                </tr>
                                <tr>
                                    <td class="td_heading_bold" colspan="4">Note-Please make cheques in favor of:</td>
                                    <td colspan="3"></td>
                                    <td  colspan="5" rowspan="2" class="td_heading_bold">Authorized Signatory:<br><img src="{{ public_path('assets/home/images/signature.png') }}" alt="signature_img" class="img-fluid" height="50px" width="50px"></td>
                                </tr>
                                <tr>
                                    <td class="td_heading_bold" rowspan="" colspan="4">"SajhaProfit Investment Advisors"</td>
                                    <td colspan="3"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
</html>