<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>CRM</title>
	<link rel="stylesheet" href="">

</head>
<body>
	@if (Session::has('sweet_alert.alert'))
    <script>
        swal({!! Session::get('sweet_alert.alert') !!});
    </script>
	@endif
	<div class="container">
		<div class="row">
			<h2>Hello Sweet Alert Testing..</h2>
		</div>
	</div>
	<script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
	@include('sweet::alert')
</body>
</html>