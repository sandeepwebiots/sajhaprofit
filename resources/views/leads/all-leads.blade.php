@extends('layouts.dashboard.master')

@section('title') All Lead | Dashboard @endsection

@section('style')    
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/dashboard/css/datatable-extension.css') }}" />
@endsection

@section('content')
<div class="page-body user-management">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>All Lead
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>      
                        <li class="breadcrumb-item active">All Lead</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>All Lead</h5>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="example" class="display">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="date-picker">
                                                <form method="post" action="{{ url('filter-leads') }}">
                                                    {{ csrf_field() }}
                                                    <div class="form-group row">
                                                        <div class="col-sm-4">
                                                            <input type="text" id="fromdate" name="from" class="datepicker-here form-control digits" data-language='en' data-multiple-dates-separator=", " data-position='bottom left' placeholder="From" autocomplete="off"/>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <input type="text" name="to" class="datepicker-here form-control digits" data-language='en' data-multiple-dates-separator=", " data-position='bottom left' placeholder="To" autocomplete="off" id="todate" />
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <button type="submit" class="btn btn-primary">Filter</button>
                                                        </div>
                                                    </div>
                                                        
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Mobile no</th>
                                    <th>Email</th>
                                    <th>State</th>
                                    <th>City</th>
                                    <th>Inquiry</th>
                                    <th>Comments</th>
                                    <th style="width: 75px;">Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @php $i=1; @endphp
                                  @foreach($lead  as $lea)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $lea->name }}</td>
                                        <td>{{ $lea->mobile_no }}</td>
                                        <td>{{ $lea->email }}</td>
                                        <td>{{ $lea->state }}</td>
                                        <td>{{ $lea->city }}</td>
                                        <td>{{ $lea->details }}</td>
                                        <td>@if($lea->status == 0)
                                            <button type="buuton" class="btn btn-info" data-toggle="modal" data-target="#commnet{{ $lea->id }}"><i class="fa fa-comments" aria-hidden="true"></i> {{ count($lea->commentcount) }}</button>
                                            @elseif($lea->status == 1)
                                                <span class="badge badge-success">Converted</span>
                                            @elseif($lea->status == 2)
                                                <span class="badge badge-warning">Not Interested </span>
                                            @endif
                                        </td>
                                        <td>{{ $lea->created_at->format('d-m-Y') }}</td>

                                        <div class="modal fade" id="commnet{{ $lea->id }}" role="dialog">
                                          <div class="modal-dialog">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              </div>
                                              <div class="modal-body">
                                                @if(count($lea->commentcount) == 0)
                                                    <span>No Comments</span>
                                                @else
                                                @foreach($lea->commentcount as $comment)
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <span>{{ $comment->comment }}</span>
                                                    </div>
                                                    <div class="col-md-6">
                                                       <span>{{  $comment->created_at->format('Y-m-d H:i:s') }}</span>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @endif
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="{{ URL::asset('assets/dashboard/js/date-picker/datepicker.js') }}"></script>
<script src="{{ URL::asset('assets/dashboard/js/date-picker/datepicker.en.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    } );
    var dateToday = new Date();
    var dates = $("#fromdate, #todate").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        // minDate: dateToday,
        maxDate: new Date(),
        onSelect: function(selectedDate) {
            var option = this.id == "fromdate" ? "minDate" : "maxDate",
                instance = $(this).data("datepicker"),
                date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
            dates.not(this).datepicker("option", option, date);
        }
    });

</script>
@endsection

