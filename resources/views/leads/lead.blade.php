@extends('layouts.dashboard.master')

@section('title') Today Lead | Dashboard @endsection

@section('style')
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}
.btn-secondary{
    margin: 5px;
}   
</style>
@endsection

@section('content')
@php $slug = Sentinel::getUser()->roles()->first()->slug; @endphp
 <div class="page-body user-management">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Lead
                    </h5>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>                                
                        <li class="breadcrumb-item active">Today Lead</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Today Lead</h5>
                    </div>
                    <div class="card-body">
                        <div class="dt-ext table-responsive">
                            <table id="lead-table" class="display">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Mobile no</th>
                                    <th>Email</th>
                                    <th>State</th>
                                    <th>City</th>
                                    <th>Inquiry</th>
                                    <th>Comments</th>
                                    @if($slug == 'm11')
                                    <th>Message</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                  @php $i=1; @endphp
                                  @foreach($lead  as $lea)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $lea->name }}</td>
                                        <td>{{ $lea->mobile_no }}</td>
                                        <td>{{ $lea->email }}</td>
                                        <td>{{ $lea->state }}</td>
                                        <td>{{ $lea->city }}</td>
                                        <td>{{ $lea->details }}</td>
                                        <td>@if($lea->status == 0)
                                            <button type="buuton" class="btn btn-info" data-toggle="modal" data-target="#commnet{{ $lea->id }}"><i class="fa fa-comments" aria-hidden="true"></i> {{ count($lea->commentcount) }}</button>
                                            @elseif($lea->status == 1)
                                                <span class="badge badge-success">Converted</span>
                                            @elseif($lea->status == 2)
                                                <span class="badge badge-warning">Not Interested </span>
                                            @endif
                                        </td>
                                        @if($slug == 'm11')
                                        <td>
                                            @if($lea->status == 0)
                                            <button type="button" class="btn btn-success" onclick="modelpopop({{$lea->id}})" data-toggle="modal" data-target="#addMyModal">Add Comment <i class="fa fa-comments" aria-hidden="true"></i></button>
                                            @endif
                                        </td>
                                        @endif
                                        <div class="modal fade" id="commnet{{ $lea->id }}" role="dialog">
                                          <div class="modal-dialog">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              </div>
                                              <div class="modal-body">
                                                @if(count($lea->commentcount) == 0)
                                                    <span>No Comments</span>
                                                @else
                                                @foreach($lea->commentcount as $comment)
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <span>{{ $comment->comment }}</span>
                                                    </div>
                                                    <div class="col-md-6">
                                                       <span>{{  $comment->created_at->format('Y-m-d H:i:s') }}</span>
                                                    </div>

                                                </div>
                                                @endforeach
                                                @endif
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <div class="modal fade" id="addMyModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form role="form" id="newModalForm" action="{{ url('comment-lead') }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="lead_id" id="lead_id">
              <div class="form-group">
                <label class="control-label col-md-3" for="updatework">Give Update</label>
                <div class="col-md-9">
                    <select class="form-control" id="updatework" name="updatework" onchange="comment(this.value)">
                        <option value="">Select</option>}
                        <option value="Call you later">Call you later</option>
                        <option value="Call Waiting">Call Waiting</option>
                        <option value="Convert">Convert</option>
                        <option value="Not Interested">Not Interested</option>
                        <option value="other">Other</option>
                    </select>
                </div>
              </div>
              <div class="form-group" id="otherMessage" style="display: none;">
                <label class="control-label col-md-3" for="message">Message</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="message" name="message" placeholder="Enter and Message" require>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="btnSaveIt">Save</button>
                <button type="button" class="btn btn-danger" id="btnCloseIt" data-dismiss="modal">Close</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>

@endsection

@section('script')

<script>
$(document).ready(function() {
    $('#lead-table').DataTable();
} );
</script>
<script>
function modelpopop(id)
{
    $("#lead_id").val(id);
}
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
<script>

    $(function() {
      $("#newModalForm").validate({
        rules: {
          updatework: {
            required: true,
          },
          message: "required"
        },
        messages: {
          updatework: {
            required: "Please Select option.",
          },
          message: "Please drop message."
        }
      });
    });

    function comment(val){
        console.log(val);
        if (val == 'other') {
            $('#otherMessage').css("display", "block");
        }else{
            $('#otherMessage').css("display", "none");
        }
    }
</script>
@endsection

