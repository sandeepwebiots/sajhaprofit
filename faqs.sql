-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 01, 2019 at 07:47 AM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm_profit`
--

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation` text,
  `answer` text,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `quotation`, `answer`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Are you registered with SEBI ?', 'Yes we are registered with SEBI as Research Analyst and our SEBI registration number is...', 1, '2018-12-20 18:30:00', NULL),
(2, 'What is Stock Market ?', 'A stock market (also called as share market) is the aggregation of buyers and sellers, where seller can sell their shares or stock and buyer can buy it. Or in other words, we can say that it is a market place where shares of pubic listed companies are traded.', 1, '2018-12-20 18:30:00', NULL),
(3, 'What is a Stock Exchange ?', 'Stock exchange is a body which act as a mediator and facilitates buying and selling of shares for execution of their respective orders. Some of the well known exchanges are BSE, NSE, NYSE, NASDAQ etc. For trading any stocks on exchange the company must first get listed on that exchange.', 1, '2018-12-20 18:30:00', NULL),
(4, 'What is a Share ?', 'Shares are the smallest unit of ownership in any enterprise that provides proportional distribution of any profit (if declared) in the form of Dividend.  The two main types of shares are common shares and preferred shares. Preferred shareholders have a bigger claim on company’s earnings and assets. Also, they get preference over common shareholders when dividends are distributed. Generally, dividend for preference shareholders is more than that for the common shareholders. Earlier, shares were issued in physical format. However, now physical paper stock certificates have been replaced with electronic recording of stock shares, just as mutual fund shares are recorded electronically.', 1, '2018-12-20 18:30:00', NULL),
(5, 'Why do companies issue Shares ?', 'Companies issue shares to meet their financial requirements for expansion of business and other purposes. By issuing the shares company gets funds from public and other financial institutions. In return of investment, shareholder gets ownership of the company in the form of share.', 1, '2018-12-20 18:30:00', NULL),
(6, 'Why do people buy shares ?', 'People buy shares because they are looking for better returns on their investments as compared to traditional products like bank FDs, PPF etc. They believe in growth prospect of a company in which they invest.', 1, '2018-12-20 18:30:00', NULL),
(7, 'How many ways we can trade in the market ?', 'You can trade in following ways in the market:\r\nIntraday trading: In Intraday trading, the position must be squared-off on the same day on which it’s created.\r\nPositional Trading/Investing: In Positional trading/investment, the trader/investor carry the trade overnight. He can hold the trade/investment for a couple of days or stay invested for long period.\r\nAt SP in this segment we have only two products for positional trading, BTST and STBT\r\nBTST stands for Buy Today Sell Tomorrow, this means that an individual can buy a share today and sell it tomorrow when its price moves up. STBT stands for Sell Today Buy Tomorrow (STBT) individual can earn a profit by selling the share today and purchasing the same tomorrow when prices moves down. STBT is possible in future segment only.', 1, '2018-12-20 18:30:00', NULL),
(8, 'What do you understand by Security Market? What are it\'s different types ?', 'Security market is where securities are issued and traded. It is the market for different types of securities namely: Debt, Equity and Derivatives.\r\nDebt market is divided into three parts: • Government securities market • Money market • Corporate Debt market\r\nEquity market is divided into two parts: • Primary market • Secondary market\r\nDerivatives market is also divided into two parts: • Options market • Futures market', 1, '2018-12-20 18:30:00', NULL),
(9, 'What is the difference between Bombay Stock Exchange and National Stock Exchange ?', 'Bombay Stock Exchange (BSE Sensex) was started in 1986 whereas National Stock Exchange (NSE Nifty) started in 1995. The base year for Sensex is 1978-79 and base value is 100 whereas the base year for nifty is 1994 and base value is 1000. • BSE consists of 30 scrips whereas NSE consists of 50 scrips • BSE is screen based trading whereas NSE is national, computerized exchange • BSE has adopted both quote driven system and order driven system whereas NSE has opted for an order driven system', 1, NULL, NULL),
(10, 'What are the types of Risks ?', 'Generally, there are two types of risk: Systematic risk and Unsystematic risk.\r\nSystematic risks are uncontrollable risk. It creates impact on the overall market and not just a stock or industry. Below mentioned are the major types of systematic risks: • Market risk. • Purchasing power risk. • Interest rate risk.\r\nUnsystematic risks are controllable risk. It creates impact on a organization or sector. Below mentioned are the major types of unsystematic risks: • Business risk. • Financial risk. • Liquidity risk. • Default risk', 1, NULL, NULL),
(11, 'Explain what is the role of equity analyst ?', 'Equity analysts perform research and analyse financial data & trends for a company. Equity analyst writes reports on company finances and assign them financial ratings. Apart from this they also help companies to overcome financial crisis by giving them plan to get out of debt.', 1, NULL, NULL),
(12, 'Explain what is Net Asset Value (NAV) ?', 'The value of one unit of a fund is referred as NAV or Net Asset Value. It is calculated by adding the current market values of all securities held by the fund, adding in cash and accumulated income and then subtracting liabilities, expenses and dividing the result by the number of units outstanding.', 1, NULL, NULL),
(13, 'What is hedging and speculating ?', 'Hedging is a strategy for price protection or to offset a potential loss of one market by taking an equal, but opposite position in another market. In speculation, traders trade according to their belief to earn profit and are ready to take high risk for high return. Speculators try to earn profit from the fluctuation in the market.', 1, NULL, NULL),
(14, 'Should I trade in ‘Futures and Options’ segment ?', 'Yes, you can trade in F&O segment if you have sufficient capital and risk appetite for the same. F&O segment has many benefits which equity cash segment doesn\'t have, so we always prefer trading in F&O segment and our services are designed also to serve in F&O segment. We provide only accurate and genuine trading calls in F & O segment in Indian Stock Market with good accuracy from which one can earn decent profits. SP is best investment advisory company where we focus on long term association and wealth building.', 1, NULL, NULL),
(15, 'Do you provide Intraday tips ?', 'Yes, SP provides Intraday tips in Indian Stock Market and Commodity Market where we focus on limited and quality trading. Our limited trades give accuracy of 90% - 95%. Our experts and analysts provide Sure Shot Intraday call only. SP provides Intraday Sureshot and Genuine calls and there will be full follow-up of this call till our call is open. We are best intraday tips provider in India & provide only quality calls.', 1, NULL, NULL),
(16, 'Do you provide Options Trading tips ?', 'Yes, we provide options trading tips. We provide options services in Stock options and index options. We focus on limited and quality trading so we provide only 7 to 10 calls in a month and they are multi bagger calls which give very good return on small capital. Our Best Options tips gives best results to small traders who have very limited capital. Trades will be in Call Option (CE) or Put Option (PE) and full guidance will be provided for entry to exit. You can get further details about Option Multi bagger Service in our Service page.', 1, NULL, NULL),
(17, 'Will SP provide full support to trade if I am new to Stock Market ?', 'Yes, we provide full support to new entrants who don\'t have basic knowledge about stock market. Our primary goal is to educate more and more people about equity investments and provide them decent results. We believe to provide knowledge & skills to all our clients so that they can invest and get good returns. SP is no. 1 Wealth Management Company in India and its main objective is to create wealth for clients and keep them into the comfort zone to trade in the Indian Stock Market.', 1, NULL, NULL),
(18, 'What if I wish to invest a certain amount and cannot trade from my side due to lack of time ?', 'We have Account Management facility (Wealth Management Services) in which we manage your existing account in case you don\'t have time to trade. We appoint a special RM (Relationship Manager) for you to trade on your behalf and he/she will be in touch with you to provide updates.So if you only invest money in market, you just have to keep it in your trading account of any broker based in India and we will manage that account.', 1, NULL, NULL),
(19, 'Are you', 'Are you registered with SEBI ?', NULL, '2018-12-22 07:40:46', '2018-12-22 09:08:21'),
(20, 'Are you registered with SEBI ?', 'Are you registered with SEBI ?', 0, '2018-12-22 08:20:47', '2018-12-22 09:10:05');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
