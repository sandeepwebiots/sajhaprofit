<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/','HomeController@index');
Route::get('about','HomeController@about');
Route::get('faq','HomeController@faq');
Route::get('contact','HomeController@contact');
Route::get('service','HomeController@service');
Route::get('chart','HomeController@chart');
Route::get('pricing','HomeController@pricing');
Route::get('technical-analysis','HomeController@technicalAnalysis');
Route::get('stock-chart','HomeController@stockChart');
Route::get('news','HomeController@news');
Route::get('explore','HomeController@explore');
Route::get('complain','HomeController@complain');
Route::get('refund-policy','HomeController@refundPolice');
Route::get('risk-discloser','HomeController@riskdiscloser');
Route::get('disclaimer','HomeController@disclaimer');
Route::get('terms_condition','HomeController@terms_condition');
Route::get('reviews','HomeController@reviews');
Route::get('risk-profile','HomeController@risk_profile');
Route::get('kyc','HomeController@kyc');
Route::get('kyc-individual','HomeController@kycIndividual');
Route::post('/kyc-Post-individual','KycController@Individual');
Route::post('/kyc-Post-Non-Individual','Kyc_Non_Individual_Controller@Non_Individual');
Route::get('kyc-non-individual','HomeController@kycNonIndividual');
Route::get('download_individual','KycController@getDownload');
Route::get('download_non_individual','Kyc_Non_Individual_Controller@getDownload');
Route::post('/upload_kyc_form','Upload_Kyc_Form_Controller@upload_kyc_form');
Route::get('risk-profile-form','HomeController@riskProfileForm');
Route::get('/blog','HomeController@blog');
Route::get('blog-detail/{id}','HomeController@blogDetail');
Route::get('news-detail/{id}','HomeController@newsDetail');
Route::get('service/{service}','HomeController@serviceList')->name('list.service');
Route::get('state/{id}','HomeController@stateId');
Route::get('city/{id}','HomeController@cityId');
Route::post('quickinquiry','HomeController@quickinquiry');
Route::post('upload-risk-profile-form','HomeController@uploadriskProfileForm');

Route::get('login',function(){
    return view('home_pages.login');
});
Route::get('registration',function(){
	return view('home_pages.register');
});

Route::get('invoice',function(){
    return view('home_pages.invoice');
});

Route::get('/service-detial/{id}','HomeController@subservice');

Route::get('forgot-password','ForgotPasswordController@forgotPassword');
Route::post('forgot-password','ForgotPasswordController@postForgotPassword');
Route::get('reset-password/{email}/{resetcode}','ForgotPasswordController@restetPassword');
Route::post('reset-password/{email}/{resetcode}','ForgotPasswordController@postResetPassword');

//Registration
Route::post('register-success','UserRegisterController@postRegister');
Route::get('activation/{email}/{code}','UserRegisterController@userActivation');
//Login
Route::post('gologin','LoginController@goLogin');
Route::get('validate','ValidationController@index');
Route::post('validate','ValidationController@validate2FA');
Route::get('logout','LoginController@logout');
//User
Route::group(['middleware' => ['User']], function () {

    // Route::get('dashboard','UserDashboardController@dashboard');
    // Route::get('profile_edit_user', 'ProfileController@edit');
    // Route::post('profile_details/{id}', 'ProfileController@update');
    // Route::post('change_password/{id}','ProfileController@password_update');
    // // profile
    // Route::post('profile_image/{id}', 'ProfileController@upload');
    Route::get('history','UserDashboardController@history');

});

// Super Admin
Route::group(['middleware' => ['SuperAdmin']], function () {
     Route::get('user-list','SuperAdminController@userList');
     Route::get('pack_act_de_act','SuperAdminController@pack_act_de_act');
    // profile
    Route::get('status-change/{id}','SuperAdminController@statusChange');
    Route::post('delete-user/{id}','SuperAdminController@deleteUser');
    //services
    Route::resource('services-list','ServiceController');
    Route::get('service-edit/{id}','ServiceController@edit');
    Route::post('service-update/{id}','ServiceController@update');
    Route::get('sub-services-list/{id}','ServiceController@subServiceList');
    //sub-services
    Route::resource('sub-service','SubServiceController');
    //blog
    Route::resource('super-admin-blog-section','BlogController');
    //news
    Route::resource('super-admin-news-section','NewsController');
    //Role Management section
    Route::resource('roles','RoleManagementController');
    //package section
    Route::resource('packages','PackageController');
    Route::get('sub-service-list/{id}','PackageController@subServiceList');
    //home-section
    Route::resource('manage-home-section','HomeSectionController');
    // Route::post('franchies/details/upload','StaffController@uploadFranchies')->name('upload.details');
    // Dsignation
    Route::resource('designation','DesignationController');
    
    Route::get('designation-slug/{slug}','StaffController@designationId');
    //Slider
    Route::get('slider-section','HomeSectionController@sliderSection');
    Route::get('add-slider','HomeSectionController@addSlider')->name('add-new-slider');
    Route::post('add-slider','HomeSectionController@postaddSlider')->name('added-slider');
    Route::get('edit-slider/{id}','HomeSectionController@editSlider')->name('edit-slider');
    Route::post('update-slider/{id}','HomeSectionController@updateSlider')->name('update-slider');
    Route::get('delete-slider/{id}','HomeSectionController@deleteSlider');
    //faq
    Route::resource('faq-list','FaqController');
    Route::get('faq-delete/{id}','FaqController@destroy');
    Route::post('faq-update/{id}','FaqController@update');
    Route::get('faq/{id}','FaqController@faqFind');
    
    //about us
    Route::resource('about-us','AboutUsController');
    //seeting
    Route::resource('setting','SettingController');
    Route::get('edit-limit','SettingController@edit_limit');
    Route::post('update-limit','SettingController@update_limit');
    Route::get('website-details','SettingController@edit_website_detials');
    Route::post('update-details','SettingController@update_details');
    Route::get('level-active','SettingController@levelActive');
    Route::get('update-level-active/{id}','SettingController@updateLevelActive');
    Route::get('staff-commission','SettingController@staffCommission');
    Route::post('update-commission','SettingController@updateCommission');
    Route::get('/franchise','FranchiseController@franchise');
    Route::get('promotion-staff','SuperAdminController@promtionStaff');
    Route::get('promoted-staff-name/{slug}','SuperAdminController@promotedStaff');
    Route::get('designation-role/{slug}','SuperAdminController@designationRole');
    Route::post('give-promotion-staff','SuperAdminController@givePromotionStaff');
    Route::get('invoice/{id}/{type}','PaymentController@invoice');
    Route::get('all-invoice','PaymentController@allInvioce');
    Route::post('all-invoice','PaymentController@allInvioce');
    Route::get('export-excel','PaymentController@exportExcel');
    Route::post('export-excel','PaymentController@exportExcel');
    Route::match(['get', 'post'],'filter-id-proof','StaffController@filterIdProof');
    //Deactivete
    Route::get('deactivate-staff/{id}','SuperAdminController@deactivateStaff');
    Route::get('role-level/{id}','SuperAdminController@rolelevel');
    Route::post('filter-role-level','SuperAdminController@filterRoleLevel');

    
});

Route::group(['middleware' => ['Telecaller']],function(){

    Route::get('lead','LeadController@genrateLead');
    Route::get('all-leads','LeadController@allLeads');
    Route::get('assigned-areas','FranchiseController@assignedAreas');
    Route::post('comment-lead','LeadController@commentLead');
});

Route::group(['middleware' => ['Franchise']],function(){
    Route::get('quick-inquiry','FranchiseController@quick_inquiry');
    Route::get('lead-assigned','FranchiseController@leadAssigned');
    Route::get('assigned-lead-staff/{id}','FranchiseController@leadAssignedStaff');
    Route::post('assigned-lead-staff','FranchiseController@postleadAssignedStaff');
    Route::get('lead/{id}','LeadController@staffLead');
    Route::get('assign-staff/{id}','FranchiseController@assignStaff');
    Route::post('assign-area','FranchiseController@assignArea');    
    Route::get('leads','FranchiseController@leads');
});

Route::group(['middleware' => ['Dashboard']],function(){

    Route::get('dashboard','SuperAdminController@superDashboard');
    //staff
    Route::resource('staff-manage','StaffController');
    Route::post('upload-details','StaffController@uploadstaffdetails');
    Route::get('staff-edit/{id}','StaffController@staffEdit');
    Route::get('approved/{id}','SuperAdminController@approvedByUpper');
    Route::get('staff-active/{id}','SuperAdminController@staffActivation');
    Route::get('staff-block/{id}','SuperAdminController@staffBlock');
    Route::get('staff-detial/{id}','StaffController@staffDetials');
    Route::post('staff-update/{id}','StaffController@staffUpdate');
    Route::get('child-staff/{id}','SuperAdminController@childStaff')->name('superadmin-childstaff');
    Route::get('chek-position/{id}','SuperAdminController@checkPosition');
    Route::get('payment-history','SuperAdminController@paymentHistory');
    Route::get('franchise-tc/{id}','StaffController@franchiseTc');
    //profile
    Route::get('profile-edit', 'ProfileController@edit');
    Route::post('profile-details/{id}', 'ProfileController@update');
    Route::post('profile-image/{id}', 'ProfileController@upload');
    Route::post('change-password/{id}','ProfileController@password_update');
    //delete staff
    Route::get('delete-staff/{id}','SuperAdminController@deletestaff');
    Route::get('delete-approve/{id}','SuperAdminController@approveDeleteReviewer');
    Route::get('delete-cancel/{id}','SuperAdminController@deleteCancel');
    Route::get('designation-id/{slug}','StaffController@designationfind');

    Route::get('staff-level/{level}','SuperAdminController@staffLevel');
    Route::get('revenue-list','SuperAdminController@revenuelist');
    Route::post('filter-payment-history','PaymentController@filterPaymentHistory');
    Route::post('filter-leads','LeadController@filterleads');
    
});

Route::get('pay/{pack_id}/{package}','PaymentController@pay');
Route::post('payment','PaymentController@payment');
Route::get('pay-now/{id}','PaymentController@paymentComplete');

Route::get('/pay_form', function () {
    return view('home_pages.pay_form');
});
// Newsletter
Route::post('email-subscribe','SubscriptionController@subscribe');