<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Individual extends Model
{
       protected $table = 'kyc_individual';
        protected $fillable = ['email','Contact_No','gender','married_status','state','resident_status','dob','pincode','pan_no','adhar','address','file_signature','file_photo','file_doc'];
}
