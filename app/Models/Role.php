<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	public function staff()
    {
    	 // return $this->belongsToMany('App\Models\Role');
    	return $this->belongsToMany('App\User', 'App\Models\RoleUser', 'role_id','user_id','user_id');
    }
}
