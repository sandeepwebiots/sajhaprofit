<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
    public function designation()
    {
    	return $this->hasOne('App\Models\Designation','id','user_desig_id');
    }
}
