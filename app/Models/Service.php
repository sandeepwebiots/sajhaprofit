<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function sub_service()
    {
    	return $this->hasMany('App\Models\SubService','service_id','id')->where('is_delete',0);
    }
}
