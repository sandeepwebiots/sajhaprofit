<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Packge extends Model
{
    public function sub_service()
    {
    	return $this->hasOne('App\Models\SubService','id','sub_service_id');
    }


    public function subservice()
    {
    	return $this->hasMany('App\Models\SubService','service_id','service_id');
    }
}
