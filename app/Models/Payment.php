<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
   public function package()
   {
   		return $this->hasOne('App\Models\Packge','id','package_id');
   }

   public function user()
   {
   		return $this->hasOne('App\User','id','user_id');
   }

}
