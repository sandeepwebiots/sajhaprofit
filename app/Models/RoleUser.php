<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    public function staffList()
    {
    	return $this->belongsTo('App\User','user_id','id');
    }

	public function user()
    {
    	return $this->belongsTo('App\User','user_id','id')->orderBy('id','desc')->where('is_delete',0);
    }
}
