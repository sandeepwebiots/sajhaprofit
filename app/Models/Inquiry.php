<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    public function commentcount()
    {
    	return $this->hasMany('App\Models\CommentLead','lead_id','id')->orderBy('id','desc');
    }
}
