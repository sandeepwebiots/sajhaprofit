<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    public function desig_staff()
    {
    	return $this->hasOne('App\User','designation_id','desig_id');
    }

    public function childstaff()
    {
    	return $this->hasMany('App\User','designation_id','id');
    }

    public function designation()
    {
    	return $this->hasone('App\Models\Designation','id','desig_id');
    }

    public function downlinestaff()
    {
        return $this->hasOne('App\User','designation_id','id');
    }
    
    public function tcLeads()
    {
        $this->hasMany('App\Models\Inquiry','tc_id','id');
    }

}
