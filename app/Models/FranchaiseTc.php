<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FranchaiseTc extends Model
{
    public function tclead()
    {
        return $this->hasMany('App\Models\Inquiry','tc_id','tc_id')->whereDate('created_at','=',date('Y-m-d'));
    }
}
