<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubService extends Model
{
    public function service()
    {
    	return $this->hasOne('App\Models\Service','id','service_id');
    }

    public function package()
    {
    	return $this->hasOne('App\Models\Packge','sub_service_id','id')->where('is_delete',0);
    }
}
