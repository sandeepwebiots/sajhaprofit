<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes; 
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name', 'email', 'password','first_name','last_name','gender','date_birth','mobile_no','adhaar_no','pan_no','driving_li','address','city','state','zip_code','education','school','profile','resume','ref_1','ref_2','experience','company','permissions','designation_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function designation()
    {
        return $this->belongsTo('App\Models\Designation','designation_id','id');
    }

    public  function Payment() {
        return $this->hasOne('App\Models\Payment');
    }

    public function roleuser()
    {
        return $this->hasMany('App\Models\RoleUser','user_id','id')->where('role_id',12);
    }
    public function payment_date()
   {
     return $this->hasMany('App\Models\Payment','user_id','id')->where('status',1);
   }
   
}
