<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\HomeSection;
use App\Models\SidebarModule;
use App\Models\FranchaiseTc;
use App\Models\Designation;
use Sentinel;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['layouts.home.footer'], function($view) {
        $view
            ->with('mail', HomeSection::where('slug','mail')->first())
            ->with('contact', HomeSection::where('slug','contact')->first())
            ->with('address', HomeSection::where('slug','address')->first());
        });

        view()->composer(['layouts.dashboard.side_bar'], function($view) {
        $view
            ->with('modules', SidebarModule::where('is_active',1)->get());

            $designation = Sentinel::getUser()->designation_id;
            $permission = Designation::where('id',$designation)->first();
            $franch_tc = FranchaiseTc::where('tc_id',$permission->id)->where('is_other',1)->first();
            $view->with('permission',$permission)->with('franch_tc',$franch_tc);
        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    
}
