<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kyc_Non_Individual extends Model
{
     protected $table = 'kyc_non_individual';
        protected $fillable = ['email1','Contact_No1','resident_status','city','state1','Resistation_No','date_bussiness','date_commencement','pincode1','pan_no1','address','file_signature','file_photo','file_doc'];
}
