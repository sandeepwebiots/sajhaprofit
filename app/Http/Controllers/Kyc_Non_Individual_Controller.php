<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kyc_Non_Individual;
use Redirect;
use Sentinel;
use Validator;
use Response;

class Kyc_Non_Individual_Controller extends Controller
{
       public function Non_Individual(Request $request)
   {
   
   	// return $request->all();
   	 $request->validate([
   	 	  'email1'			              => 'required|email|unique:kyc_non_individual',
   	 	  'Contact_No1'		          => 'required|max:11|unique:kyc_non_individual',   
        'resident_status'         =>'required',
        'city'                    =>'required',
        'state1'			              =>'required|max:50',
        'Company_Resistation_No'  => 'required', 
        'date_bussiness'         => 'required',
        'date_commencement'		    =>'required',
        'pincode1'		              =>'required',
	      'pan_no1'		              =>'required|max:10|unique:kyc_non_individual',
	      'address'                 =>'required',
        'file_signature'          =>'required',	
        'file_photo'		          =>'required',
        'file_doc'		            =>'required',
        'g-recaptcha-response'    => 'required'
     	 ],
      	[
      	'email1.email'=>'Please enter a valid email address.',
      	'email1.unique'=>'The email has already been taken.',
      	 'Contact_No1.regex'	=>'Please enter valid phone number 07xxxxxxxxx',
      	 'Contact_No1.unique'=>'The Contact No has already been taken.',
         'g-recaptcha-response.required'=>'Please Select This',
         'pan_no1.unique'=>'The Pan no has already been taken.',
     	]);

		$non_Individual = new Kyc_Non_Individual;
          $non_Individual->email1					= $request->email1;
            $non_Individual->Contact_No1			= $request->Contact_No1;
            $non_Individual->resident_status		=$request->resident_status;
            $non_Individual->city 					= $request->city;
       		$non_Individual->state1					=$request->state1;
            $non_Individual->Company_Resistation_No	=$request->Company_Resistation_No;
            $non_Individual->date_bussiness  		= $request->date_bussiness;
            $non_Individual->date_commencement		= $request->date_commencement;
            $non_Individual->pincode1				= $request->pincode1;
            $non_Individual->pan_no1				=$request->pan_no1;
            $non_Individual->address 				= $request->address;

           
            if ($request->hasFile('file_signature')) 
            {
	    	    $file_signature = $request->file('file_signature');
	    		$pic_name = time().'.'.$file_signature->getClientOriginalExtension();
		    	$path = public_path('../assets/non_Individual/file_signature/');
		    	$file_signature->move($path, $pic_name);
		    	$non_Individual->file_signature = $pic_name;
	    	}
	    	   if ($request->hasFile('file_photo')) 
            {
	    	 	$file_photo = $request->file('file_photo');
	    		$pic_name = time().'.'.$file_photo->getClientOriginalExtension();
		    	$path = public_path('../assets/non_Individual/file_photo/');
		    	$file_photo->move($path, $pic_name);
		    	$non_Individual->file_photo = $pic_name;
	    	}
	    		 if ($request->hasFile('file_doc')) 
            {
	    	 	$file_doc = $request->file('file_doc');
	    		$doc =time().'.'.$file_doc->getClientOriginalExtension();
		    	$path = public_path('../assets/non_Individual/file_doc/');
		    	$file_doc->move($path, $doc);
		    	$non_Individual->file_doc = $doc;
	    	}
            $non_Individual->save();
            alert()->success('your kyc successful uploaded !');
   			return Redirect::to('/');
   }
        public function getDownload()
      {
          //PDF file is stored under project/public/download/info.pdf
          $file= public_path(). "/assets/kyc_form/kyc_non_individual.pdf";

          $headers = array(
                    'Content-Type: application/pdf',
                  );

          return Response::download($file, ' for non individual.pdf', $headers);
      }
}
