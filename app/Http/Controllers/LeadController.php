<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inquiry;
use App\Models\Assign;
use App\Models\CommentLead;
use App\Models\Designation;
use App\User;
use Sentinel;
use DB;

class LeadController extends Controller
{
    public function genrateLead()
    {
    	$user = Sentinel::getUser();
    	$lead = Inquiry::with('commentcount')->where('tc_id',$user->designation_id)->where('assign_id',null)->whereDate('created_at',date('Y-m-d'))->orWhere('assign_id',$user->designation_id)->orderBy('id','desc')->get();
    	return view('leads.lead',compact('lead'));
    }

    public function staffLead($id)
    {
        // $user = Sentinel::getUser();
        $lead = Inquiry::where('tc_id',$id)->where('assign_id',null)->orWhere('assign_id',$id)->orderBy('id','desc')->get();
        
        return view('leads.lead',compact('lead'));
    }

    public function allLeads()
    {
        $user = Sentinel::getUser();
        $lead = Inquiry::with('commentcount')->where('tc_id',$user->designation_id)->where('assign_id',null)->orWhere('assign_id',$user->designation_id)->orderBy('id','desc')->get();
        return view('leads.all-leads',compact('lead'));
    }

    public function commentLead(Request $request)
    {
        $comment = new CommentLead;
        $comment->lead_id = $request->lead_id;
        if ($request->updatework == 'other') {
            $comment->comment = $request->message;
        }else{
            $comment->comment = $request->updatework;
        }
        $comment->save();

        $lead = Inquiry::where('id',$request->lead_id)->first();
        if ($request->updatework == 'Convert') {
            $lead->status = 1;
        }elseif ($request->updatework == 'Not Interested') {
            $lead->status = 2;
        }
        $lead->update();

        alert()->success('Comment posted.');
        return redirect()->back();
    }

    public function filterleads(Request $request)
    {
        $user = Sentinel::getUser();
        $from = $request->from;
        $to = $request->to;
        if ($user->inRole('m11')) {
            $lead = Inquiry::with('commentcount')->whereBetween(DB::raw('date(created_at)'), [$from, $to])->where('tc_id',$user->designation_id)->where('assign_id',null)->orWhere('assign_id',$user->designation_id)->orderBy('id','desc')->get();
        }else{ 
            $designation = Designation::where('desig_id',$user->designation_id)->get();
            $j = 0;
            foreach ($designation as $desig) {
                if ($desig) {
                    $leads = Inquiry::with('commentcount')->whereBetween(DB::raw('date(created_at)'), [$from, $to])->where('tc_id',$desig->id)->where('assign_id',null)->orWhere('assign_id',$desig->id)->orderBy('id','desc')->get();

                    foreach ($leads as $value) {
                        $lead[$j] = $value;
                        $j++;
                    }
                }
            }
        }
        
        return view('superadmin.leads',compact('lead'));
    }
}
