<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subscription;
use Sentinel;
use Mail;
use View;
use Validator;

class SubscriptionController extends Controller
{
    public function subscribe(Request $request)
    {
       $validator = Validator::make($request->all(), [
           'email' => 'required|email|max:255|unique:subscriptions',
       ]);

       if ($validator->fails()) {
           return 0;
       }

      $is_present=Subscription::where("email" , $request->email)->first();
      if($is_present){
          return 0;
      }
      $subscribe = new Subscription;
      $subscribe->email = $request->email;
      $subscribe->save();
      $user=$subscribe->email;
      try {
        // Sending an Email to the Subscriber
        // return view('mails.subscription', compact('user'));
         Mail::send('mails.subscription', [
          'user' => $request->email,
        ], function ($message) use($request) {
          $message->to($request->email);
          $message->subject("$this->Subscription");
        });
        // Adding Subscriber to the mailchimp
        // Newsletter::subscribe($request->email);
      } catch (\Exception $e) {
        \Log::error($e->getMessage());
      }     
      return 1;
    } 
}
