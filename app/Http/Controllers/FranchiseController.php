<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State;
use App\Models\Assign;
use App\Models\FranchaiseTc;
use App\Models\Inquiry;
use App\Models\Designation;
use App\User;
use Sentinel;

class FranchiseController extends Controller
{
    public function franchise()
    {
        return view('superadmin.franchise');
    }
    public function assignStaff($id)
    {
        $designation = Designation::where('id',$id)->first();
        if ($designation) {
        	$state = State::get();
        	return view('franchise.assign',compact('state','id'));
        }else{
            alert()->error('Something wrong.');
            return redirect('staff-manage');
        }
    }

    public function assignArea(Request $request)
    {
    	$this->validate($request,[
    		'state'	=> 'required',
    	]);
        $city = $request->city;
    	$alredy_assign = Assign::where('state',$request->state)->get();

        foreach ($alredy_assign as $assign) {
                
            if ($assign->user_id == $request->staff_id) {
                return redirect()->back()->with('error','Already assign this city same person.');
            }
        }
        if ($city == 'All') {
            $assign = new Assign;
            $assign->user_id = $request->staff_id;
            $assign->state  = $request->state;
            $assign->city = $city;
            $assign->save();

            alert()->success('Staff assigned area.');
            return redirect('staff-manage');
        }
    	
    	foreach ($city as $value) {
    		
    		$assign = new Assign;
    		$assign->user_id = $request->staff_id;
    		$assign->state  = $request->state;
    		$assign->city = $value;
    		$assign->save();
    	}
    	alert()->success('Staff assigned area.');
    	return redirect('staff-manage');
    }

    public function assignedAreas()
    {
    	$staff_id = Sentinel::getUser()->designation_id;
    	$assign = Assign::where('user_id',$staff_id)->get();
    	return view('assign.assigned-areas', compact('assign'));	
    }

    public function quick_inquiry()
    {
        return view('franchise.quick-inquiry');
    }

    public function leadAssigned()
    {
        $user = Sentinel::getUser();
        $k = 0;
        $franch_user = FranchaiseTc::where('franch_id',$user->designation_id)->get();

        foreach ($franch_user as $value) {
           $stafflist[$k] = User::with('designation')->where('designation_id',$value->tc_id)->first();
           $k++;
        }
        $stafflist = array_filter($stafflist);
        if (empty($stafflist)) {
            $stafflist = [];
        }
        return view('franchise.lead-assigned',compact('stafflist'));
    }

    public function leadAssignedStaff($id)
    {
        $user = Sentinel::getUser();
        $k = 0;
        $franch_user = FranchaiseTc::where('franch_id',$user->designation_id)->get();
        foreach ($franch_user as $value) {
            if ($value->tc_id != $id) {               
                   $stafflist[$k] = User::with('designation')->where('designation_id',$value->tc_id)->first();
                   $k++;
            }
        }

        $leads = Inquiry::where('tc_id',$id)->where('assign_id',null)->whereDate('created_at','=',date('Y-m-d'))->get();
        $stafflist = array_filter($stafflist);
        $arr['users'] = $stafflist;
        $arr['lead'] = $leads;

        return $arr;
    }

    public function postleadAssignedStaff(Request $request)
    {
        $this->validate($request,[
            'absent'    =>  'required',
            'assigned'  =>  'required',
            'leads'  =>  'required',
        ]);
        
        $inquiry = Inquiry::where('tc_id',$request->absent)->whereIn('id',$request->leads)->update(['assign_id' => $request->assigned]);
                    
        alert()->success('Lead assigned by antoher staff.');
        return redirect()->back();
    }

    public function leads()
    {
        $user = Sentinel::getUser();

        $designation = Designation::where('desig_id',$user->designation_id)->get();
        $j = 0;
        if ($designation) {
            foreach ($designation as $desig) {
                if ($desig) {
                    $leads = Inquiry::with('commentcount')->where('tc_id',$desig->id)->where('assign_id',null)->orWhere('assign_id',$desig->id)->orderBy('created_at','asc')->get();

                    foreach ($leads as $value) {
                        $lead[$j] = $value;
                        $j++;
                    }
                }
            }
        }
        if (empty($lead)) {
            $lead = [];
        }
        return view('superadmin.leads',compact('lead'));
    }
}
