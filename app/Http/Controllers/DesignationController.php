<?php

namespace App\Http\Controllers;

use App\Models\Designation;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\SidebarModule;
use App\User;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $designation = Designation::with('designation')->whereNotIn('role_id',['super_admin','m10','m11'])->orderBy('role_id','asc')->get();
        foreach ($designation as $desig) {
            $head = Designation::where('id',$desig->desig_id)->first();
        }
        return view('superadmin.designation.index',compact('designation','head'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::whereNotIn('id',array(1,10,11,12))->where('status',1)->get();
        $designation = Designation::get();
        $sidebar = SidebarModule::whereIn('id',array(1,7))->get();
        return view('superadmin.designation.create',compact('roles','designation','sidebar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $this->validate($request,[
            'role'  =>  'required',
            'post'  =>  'required|max:150',
            'head'  =>  'required',
            'sidebarid' =>  'required',
        ]);
        // return $request->all();
        $design = new Designation;
        $design->role_id = $request->role;
        $design->post = $request->post;
        $design->permissions = json_encode($request->sidebarid);
        $design->save();

        if ($request->role == 'm2' || $request->role == 'm5' || $request->role == 'm7' || $request->role == 'm9' ) {
            $alredy_add = Designation::where('desig_id',$request->head)->where('role_id',$request->role)->count();
            if ($alredy_add < 1) {
                $head_change = Designation::where('desig_id',$request->head)->update(['desig_id' => $design->id]);
            } 
        }
        $design->desig_id = $request->head;
        $design->update();

        alert()->success('Designation created successfully.');
        return redirect('designation');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function show(Designation $designation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $designation = Designation::find($id);
        $head_desig = Designation::where('id',$designation->desig_id)->first();
        $roles = Role::whereNotIn('id',array(1,10,11,12))->where('status',1)->get();
        $sidebar = SidebarModule::whereIn('id',array(1,7))->get();
        return view('superadmin.designation.edit',compact('roles','designation','head_desig','sidebar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'role'  =>  'required',
            'post'  =>  'required|max:150',
            'head'  =>  'required',
            'sidebarid' =>  'required',
        ]);

        $design = Designation::find($id);
        $design->role_id = $request->role;
        $design->post = $request->post;
        $design->desig_id = $request->head;
        $design->permissions = json_encode($request->sidebarid);
        // $design->update();

        return $staff = User::where('designation_id',$id)->first();
        if ($staff) {
            $role_user = RoleUser::where('user_id',$staff->id)->delete();
            $role = Sentinel::findRoleBySlug($request->level);
            $role->users()->syncWithoutDetaching($staff);
        }

        alert()->success('Designation updated successfully.');
        return redirect('designation');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Designation $designation)
    {
        //
    }
}
