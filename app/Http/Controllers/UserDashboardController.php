<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use Sentinel;

class UserDashboardController extends Controller
{
    public function dashboard()
    {
    	return view('user.dashboard');
    }

    public function history()
    {
    	$user = Sentinel::getUser();
    	$purchase = Payment::with('package.sub_service')->where('user_id',$user->id)->get();
    	return view('user.history',compact('purchase'));
    }
}
