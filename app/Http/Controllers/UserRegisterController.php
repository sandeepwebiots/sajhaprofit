<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Alert;
use Mail;
use Activation;
use App\User;

class UserRegisterController extends Controller
{
    public function postRegister(Request $request)
    {
    	$this->validate($request,[
    		'email' => 'required|email|unique:users',
    		'first_name' => 'required',
    		'last_name'	=>	'required',
    		'user_name'	=>	'required|unique:users',
    		'password'	=>	'required|min:8',
    		're_password' =>'required|same:password'
    	]);

    	$user = Sentinel::register(array(
    		'email'	=> $request->email,
    		'first_name' =>	$request->first_name,	
            'last_name' =>  $request->last_name,    
    		'user_name' =>	$request->last_name,	
    		'password' =>	$request->password,	
    	));

    	$role = Sentinel::findRoleBySlug('m12');
    	$role->users()->attach($user);

    	$activation = Activation::create($user);
    	$email = urldecode($user->email);

    	$text = 'Activate to account..!';
    	$link = url('').'/activation/'.$email.'/'.$activation->code;

    	// return view('mails.activation',compact('user','link','text'));

    	$this->sendactivation($user,$text,$link);

    	alert()->success('Registration success','Your registration successfully.');
    	return redirect()->back();

    }

    private function sendactivation($user,$text,$link)
    {
    	Mail::send('mails.activation', ['user' => $user,'text' => $text,'link' => $link], function ($m) use ($user) {

            $m->to($user->email, $user->name)->subject('Activate Your Acount!');
        });
    }

    public function userActivation($email,$code)
    {
        $user = User::whereEmail($email)->first();
        $user_id = Sentinel::findById($user->id);

        if(Activation::complete($user_id,$code))
        {
            $user->status = 1;
            $user->save();

            // return view('mails.welcome', compact('user'));
            Mail::send('mails.welcome', ['user' => $user], function ($m) use ($user) {
                  $m->to($user->email);
                  $m->Subject("Hello $user->user_name, your account is activated now");
                });
            alert()->success('success','Successfull activation your account.');
            return redirect('login');
        }
        alert()->error('error','This link is expires. please try to login !!!');
        return redirect('login');
    }
}
