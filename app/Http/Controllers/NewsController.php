<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news_list = News::orderBy('id','desc')->get();
        return view('superadmin.news.news',compact('news_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.news.news_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'sub_title' => 'required',
            'content'   =>  'required',
            'img'   =>  'required|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $image = $request->file('img');
        $img_name = time().'.'.$image->getClientOriginalExtension();
        $path = public_path().'\assets\dashboard\upload\news';
        $image->move($path, $img_name);

        $news = new News;
        $news->title = $request->title;
        $news->sub_title = $request->sub_title;
        $news->content = $request->content;
        $news->img = $img_name;
        $news->save();

        alert()->success('News Created','News created successfully.');
        return redirect('super-admin-news-section');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        return view('superadmin.news.news_edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
         $this->validate($request,[
            'title' =>  'required',
            'sub_title' =>  'required',
            'content'   =>  'required',
        ]);

        $news = News::find($id);

        if ($request->hasFile('img')) {
            try{
                $image_path = public_path('assets/dashboard/upload/news/').$news->img;
                unlink($image_path);
            }catch(\ErrorException  $e){

            }

            $image = $request->file('img');
            $path = public_path('assets/dashboard/upload/news/');
            $img_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($path,$img_name); 

            $news->img = $img_name;         
        }

        $news->title = $request->title;
        $news->sub_title = $request->sub_title;
        $news->content = $request->content;
        $news->update();

        alert()->success('Blog Edit','Blog Edit successfully.');
        return redirect('super-admin-news-section');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);
        $news->delete();

        alert()->success('News Delete','News Delted successfully.');
        return redirect('super-admin-news-section');
    }
}
