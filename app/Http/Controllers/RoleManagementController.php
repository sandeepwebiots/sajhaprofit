<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\SubRole;

class RoleManagementController extends Controller
{
    public function index(){
    	$role = Role::get();
    	return view('superadmin.roles.index',compact('role'));
    }

    public function show($id){
    	$subrole = SubRole::where('role_id',$id)->get();
    	return view('superadmin.roles.subrole',compact('subrole'));
    }

    public function create(){
    	return view('superadmin.roles.subrole_create');
    }
}
