<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Role;
use App\Models\Designation;
use App\Models\SidebarModule;
use App\Models\State;
use App\Models\RoleUser;
use App\Models\FranchaiseTc;
use App\User;
use Sentinel;
use Activation;
use Alert;
use Mail;
use Validator;

class StaffController extends Controller
{
    public function index()
    {
        $user = Sentinel::getUser();
        $designation = Designation::where('desig_id',$user->designation_id)->get();
        $stafflist = [];
        $i=0;
        $j=0;
        $k=0;
        foreach ($designation as $desig) {
            $slug = Sentinel::getUser()->roles()->first()->slug;
            if (in_array($slug, ['m9','m8'])) {
                $staff_list = User::with('designation')->where('designation_id',$desig->id)->get();
                foreach ($staff_list as $value) {
                   $stafflist[$i] = $value;
                   $i++;
                }
                $stafflist = array_filter($stafflist);
            }elseif ($slug == 'm10') {
                // $franch_user = FranchaiseTc::where('franch_id',$desig->id)->get();
                // foreach ($franch_user as $value) {
                   // $stafflist = User::with('designation')->where('designation_id',$desig->id)->first();
                //    $k++;
                // }
                 $staff_list = User::with('designation')->where('designation_id',$desig->id)->get();
                foreach ($staff_list as $value) {
                   $stafflist[$i] = $value;
                   $i++;
                }
                $stafflist = array_filter($stafflist);
                // $stafflist = array_filter($stafflist);
            }
            else{
               $staff_list = User::with('designation')->where('designation_id',$desig->id)->first();
                
                $stafflist[$j] = $staff_list;
                $j++;
                $stafflist = array_filter($stafflist);
            }
        }
        // if (!is_array($stafflist)) {
        //     return $stafflist = $stafflist;
        // }
        // return $stafflist;
        $bussiness = Designation::where('desig_id',$user->designation_id)->where('role_id','m3')->get();

        return view('superadmin.staff.staff',compact('stafflist','bussiness'));
       
    }

    public function designationfind($slug)
    {
        $user = Sentinel::getUser();
        $staff_designation_id = $user->designation_id;
        $role = $user->roles()->first()->slug;
        if ($role == 'super_admin') {
            return $desig = Designation::where('role_id',$slug)->get();
        }else{
            return $desig = Designation::where('role_id',$slug)->where('desig_id',$staff_designation_id)->get();
        }
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'user_name'	=>	'required|unique:users',
    		'email'	=>	'required|email|unique:users',
    		'role'	=>	'required',
    		'sidebarid'	=>	'required',
            'designation_id'  =>  'required|unique:users'
    	]);

    	$password =str_random(12) ;
    	$hash_password = Hash::make($password);
    	$user = Sentinel::register(array(
    		'email'	=> $request->email,
    		'user_name' =>	$request->name,	
    		'password' =>	$hash_password,
    		'permissions' =>  $request->sidebarid,
            'designation_id' => $request->designation_id,	
    	));

    	$role = Sentinel::findRoleBySlug($request->role);
    	$role->users()->attach($user);

    	$activation = Activation::create($user);
    	$email = urldecode($user->email);

    	$text = 'Activate to account..!';
    	$link = url('activation').'/'.$email.'/'.$activation->code;

    	// return view('mails.staff_activation',compact('user','link','text','password'));

    	// $this->sendactivation($user,$text,$link);

    	alert()->info('Staff add successfully.');
    	return redirect('staff-manage');
    }

    

    public function create()
    {
        $state = State::get();
        $user = Sentinel::getUser();
        if ($user->inRole('super_admin')) { 
           $roles = Role::whereIn('id',[2,3,4,5,6,7,8,9])->where('status',1)->get();
        }elseif ($user->inRole('m2')) {
            $roles = array(Role::where('id',[3])->where('status',1)->first());
        }elseif ($user->inRole('m3')) {
          $roles = array(Role::where('id',[4])->where('status',1)->first());
          // $roles = array($roles);
        }elseif ($user->inRole('m4')) {
            $roles = Role::whereIn('id',[5,6])->where('status',1)->get();
        }elseif ($user->inRole('m5')) {
            $roles = array(Role::where('id',6)->where('status',1)->first());
        }elseif ($user->inRole('m6')) {
            $roles = Role::whereIn('id',[7,8])->where('status',1)->get();
        }elseif ($user->inRole('m7')) {
            $roles = array(Role::where('id',8)->where('status',1)->first());
        }elseif ($user->inRole('m8')) {
            $roles = Role::whereIn('id',[9,10])->where('status',1)->get();
        }elseif ($user->inRole('m9')) {
            $roles = array(Role::where('id',10)->where('status',1)->first());
        }elseif ($user->inRole('m10')) {
            $roles = array(Role::where('id',11)->where('status',1)->first());
        }
        $sidebar = SidebarModule::get();
        return view('staff.join_form',compact('roles','sidebar','state'));
    }

    public function uploadstaffdetails(Request $request)
    {
        // return $request->all();
        $this->validate($request,[
            'name'  =>  'required|max:255',
            'last_name' =>  'required|max:255',
            'email'     =>  'required|email|unique:users',
            'gender'    =>  'required',
            'birth'     =>  'required',
            'mobile_no' =>  'required|min:10|max:10',
            'adhaar_no' =>  'required|min:12|max:12',
            'pan_no' =>  'required|min:10|max:10',
            'driving_li' =>  'required',
            'address' =>  'required',
            'city' =>  'required',
            'state' =>  'required',
            'zip_code' =>  'required|max:6|min:6',
            'education' =>  'required',
            'school' =>  'required',
            'profile_pic' =>  'required|mimes:jpeg,jpg,png|max:10000',
            'resume' =>  'required|mimes:pdf|max:10000',       
        ]);
        $role = $request->role;
        $slug = Sentinel::getUser()->roles()->first()->slug;
        if (in_array($slug, ['m10','m9','m8'])) {
            $head = Sentinel::getUser()->designation_id;
            $set_designation = Designation::where('desig_id',$head)->count();
            $count = $set_designation + 1;
            $designation = new Designation;
            if ($slug == 'm10') {
                $post = 'TC'.$count;
                $designation->permissions = '["1"]';
                $role = 'm11';
            }else{
                $post = 'FA'.$count;
                $designation->permissions = '["1","7"]';
                $role = 'm10';
            }

            $designation->role_id = $role;
            $designation->post = $post;
            $designation->desig_id = $head;
            $designation->save();

           $designation_id = $designation->id;
        }else{
           
            $this->validate($request,[
                'designation_id'  =>  'required|unique:users',
                'user_name' =>  'required|unique:users',
                'role'  =>  'required',
            ]);
            $designation_id = $request->designation_id;
        }
        
        $resume = $request->file('resume');
        $resume_name = time().'.'.$resume->getClientOriginalExtension();
        $path = public_path().'/assets/resume/';
        $resume->move($path, $resume_name);

        $profile_pic = $request->file('profile_pic');
        $pic_name = time().'.'.$profile_pic->getClientOriginalExtension();
        $path = public_path().'/assets/upload/images/';
        $profile_pic->move($path, $pic_name);

        $password =str_random(12) ;

        $hash_password = Hash::make($password);
        $user = Sentinel::register(array(
            'email' =>  $request->email,
            'first_name' => $request->name,
            'last_name' => $request->last_name,
            'gender' => $request->gender,
            'date_birth' => $request->birth,
            'mobile_no' => $request->mobile_no,
            'adhaar_no' => $request->adhaar_no,
            'pan_no' => $request->pan_no,
            'driving_li' => $request->driving_li,
            'address' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'zip_code' => $request->zip_code,
            'education' => $request->education,
            'school' => $request->school,
            'profile' => $pic_name,
            'resume' => $resume_name,
            'ref_1' => $request->ref_1,
            'ref_2' => $request->ref_2,
            'experience' => $request->experience,
            'company' => $request->company,
            'password' =>   $hash_password,
            'user_name' =>  $request->name,
            'designation_id' => $designation_id,
        ));

        $role = Sentinel::findRoleBySlug($role);
        $role->users()->attach($user);

        $activation = Activation::create($user);
        $email = urldecode($user->email);

        $slug = Sentinel::getUser()->roles()->first()->slug;
        if ($slug == 'm10') {
            $franch_id = Sentinel::getUser()->designation_id;
            $franch_tc = new FranchaiseTc;
            $franch_tc->franch_id = $franch_id;
            $franch_tc->tc_id = $designation_id;
            $franch_tc->is_other = $request->staff;
            $franch_tc->save();

            $user->code = str_random(10);
        }
        $text = 'Activate to account..!';
        $link = url('activation').'/'.$email.'/'.$activation->code;

        // return view('mails.staff_activation',compact('user','text','password'));

        $this->sendactivation($user,$link);
        $user->form_complete = 1;
        $user->update();

        alert()->success('Successfully add staff details.');
        return redirect('staff-manage');
    }


    public function sendactivation($user,$text,$link)
    {
        Mail::send('mails.staff_activation', ['user' => $user,'text' => $text,'link' => $link, 'password' => $password], function ($text) use ($user) {

            $m->to($user->email, $user->name)->subject('Activate Your Acount!');
        });
    }

    public function staffDetials($id)
    {
        $staff = User::with('designation')->find($id);
        $head_desig = Designation::where('id',$staff->designation->desig_id)->first();
        return view('superadmin.staff.staff_detials',compact('staff','head_desig'));
    }

    public function staffEdit($id)
    {
        $staff = Sentinel::findById($id);
        $user = Sentinel::getUser();
        if ($user->inRole('m2')) {
            $roles = array(Role::where('id',[3])->first());
        }elseif ($user->inRole('m3')) {
          $roles = array(Role::where('id',[4])->first());
          // $roles = array($roles);
        }elseif ($user->inRole('m4')) {
           $roles = Role::whereIn('id',[5,6])->get();
        }elseif ($user->inRole('m5')) {
            $roles = array(Role::where('id',6)->first());
        }elseif ($user->inRole('m6')) {
            $roles = Role::whereIn('id',[7,8])->get();
        }elseif ($user->inRole('m7')) {
            $roles = array(Role::where('id',8)->first());
        }elseif ($user->inRole('m8')) {
            $roles = Role::whereIn('id',[9,10])->get();
        }elseif ($user->inRole('m9')) {
            $roles = array(Role::where('id',10)->first());
        }elseif ($user->inRole('m10')) {
            $roles = array(Role::where('id',11)->first());
        }
        $sidebar = SidebarModule::get();
        $designation = Designation::get();
        return view('staff.staff_edit',compact('staff','roles','sidebar', 'designation'));
    }

    public function staffUpdate(Request $request,$id)
    {
        // return $request->all();
        $this->validate($request,[
            'name'  =>  'required|max:255',
            'last_name' =>  'required|max:255',
            'email'     =>  'required|email',
            'gender'    =>  'required',
            'birth'     =>  'required',
            'mobile_no' =>  'required|min:10|max:10',
            'adhaar_no' =>  'required|min:12|max:12',
            'pan_no' =>  'required',
            'driving_li' =>  'required',
            'address' =>  'required',
            'city' =>  'required',
            'state' =>  'required',
            'zip_code' =>  'required|max:6|min:6',
            'education' =>  'required',
            'school' =>  'required',
        ]);
        $staff = User::find($id);
        if ($request->file('resume')) {
            try{
                $resume_path = public_path('assets/resume/').$staff->resume;
                unlink($resume_path);
            }catch(\ErrorException  $e){

            }
            $resume = $request->file('resume');
            $resume_name = time().'.'.$resume->getClientOriginalExtension();
            $path = public_path().'\assets\resume';
            $resume->move($path, $resume_name);
            $staff->resume = $resume_name;
        }
        
        if ($request->file('profile_pic')) {
            try{
                $profile_path = public_path('assets/upload/images/').$staff->profile;
                unlink($profile_path);
            }catch(\ErrorException  $e){

            }
            $profile_pic = $request->file('profile_pic');
            $pic_name = time().'.'.$profile_pic->getClientOriginalExtension();
            $path = public_path().'\assets\upload\images';
            $profile_pic->move($path, $pic_name);
            $staff->profile = $pic_name;
        }

        $staff->email =  $request->email;
        $staff->first_name = $request->name;
        $staff->last_name = $request->last_name;
        $staff->gender = $request->gender;
        $staff->date_birth = $request->birth;
        $staff->mobile_no = $request->mobile_no;
        $staff->adhaar_no = $request->adhaar_no;
        $staff->pan_no = $request->pan_no;
        $staff->driving_li = $request->driving_li;
        $staff->address = $request->address;
        $staff->city = $request->city;
        $staff->state = $request->state;
        $staff->zip_code = $request->zip_code;
        $staff->education = $request->education;
        $staff->school = $request->school;
        $staff->ref_1 = $request->ref_1;
        $staff->ref_2 = $request->ref_2;
        $staff->experience = $request->experience;
        $staff->company = $request->company;
        $staff->update();

        alert()->success('Successfully updated staff details.');
        return redirect('staff-manage'); 
    }

    public function franchiseTc($id)
    {
        return $franchise_tc = FranchaiseTc::where('franch_id',$id)->where('is_other',1)->first();
    }

    public function designationId($slug)
    {
        $role = Role::where('slug',$slug)->first();
        
        $data = Role::where('id', $role->id - 1)->where('status', 1)->first();
        if(!isset($data))
        {
            $data = Role::where('id', $role->id - 2)->where('status', 1)->first();
        }
        return $designation = Designation::where('role_id',$data->slug)->get();
    }

    public function filterIdProof(Request $request)
    {        

        $stafflist = collect();
        $validator = collect();

            $validator = Validator::make($request->all(), [
                'id_proof'  =>  'required',
                'id_no'  =>  'required',
            ]);    

            if ($validator->fails()) {
                 return view('superadmin.filter_staff_id',compact('stafflist'))->withErrors($validator);
            }       

        if ($request->id_proof == 'adhaar_no') {
            $validator = Validator::make($request->all(), [
                'id_no'  =>  'required|digits:12|numeric',
                'id_proof'  =>  'required',
            ]);    

            if ($validator->fails()) {
                 return view('superadmin.filter_staff_id',compact('stafflist'))->withErrors($validator);
            }        
            $stafflist = User::with('designation')->where('adhaar_no',$request->id_no)->get();
        }elseif ($request->id_proof == 'pan_no') {
            $validator = Validator::make($request->all(), [
                'id_no'  =>  'required|digits:10',
                'id_proof'  =>  'required',
            ]);
             if ($validator->fails()) {
                 return view('superadmin.filter_staff_id',compact('stafflist'))->withErrors($validator);
            }    
            $stafflist = User::with('designation')->where('pan_no',$request->id_no)->get();
        }


        return view('superadmin.filter_staff_id',compact('stafflist'));
    }
}
