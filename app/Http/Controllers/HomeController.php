<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use App\Models\SubService;
use App\Models\HomeSection;
use App\Models\Packge;
use App\Models\Faq;
use App\Models\AboutUs;
use App\Models\State;
use App\Models\City;
use App\Models\Inquiry;
use App\Models\Assign;
use App\Models\FranchaiseTc;
use App\Models\Setting;
use App\Models\RiskProfile;
use App\Blog;
use App\News;

class HomeController extends Controller
{
    public function index()
    {
        $about_us = AboutUs::get();
        $faqs = Faq::orderBy('id','asc')->limit(5)->get();
        $service = Service::orderBy('id','asc')->limit(6)->get();
        $blogs = Blog::orderBy('id','desc')->limit(4)->get();
        $news = News::orderBy('id','desc')->limit(3)->get();
        $slider = HomeSection::where('slug','slider')->orderBy('position','asc')->where('status',1)->get();
    	return view('home_pages.index',compact('about_us','blogs','news','service','slider','faqs'));
    }

    public function about()
    {
        $about_us = AboutUs::get();
    	return view('home_pages.about',compact('about_us'));
    }

    public function faq()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        $faqs = Faq::where('status',1)->get();
    	return view('home_pages.faq',compact('faqs','service'));
    }

    public function contact()
    {
    	return view('home_pages.contact');
    }

    public function service()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
    	return view('home_pages.service',compact('service'));
    }

    public function chart()
    {
    	return view('home_pages.chart');
    }

    public function pricing()
    {
        $service = Service::with('sub_service.package')->get();
    	return view('home_pages.pricing',compact('service'));
    }

    public function technicalAnalysis()
    {
    	return view('home_pages.technical-analysis');
    }
    public function news()
    {
        $news = News::orderBy('id','desc')->Paginate(5);
        return view('home_pages.news',compact('news'));
    }

    public function subservice($id)
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        $subservice = SubService::with('service')->find($id);
        return view('home_pages.service-details',compact('subservice','service'));
    }

    public function stockChart()
    {
        $stock = file_get_contents('https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&outputsize=full&apikey=3PE8CIQ8UG7WM4TD');
        
        $data = json_decode($stock, true);
        $data = $data["Time Series (Daily)"];
        return $data;
        print_r($data["2018-08-09"]["1. open"]);
        die();
    }

    public function explore()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.explore',compact('service'));
    }

    public function blog()
    {
        $blogs = Blog::orderBy('id','desc')->Paginate(5);
        return view('home_pages.blog',compact('blogs'));
    }

    public function blogDetail($id)
    {
        $blog_details = Blog::find($id);
        $blogs = Blog::orderBy('id','desc')->limit(4)->get();
        return view('home_pages.sub_blog',compact('blog_details','blogs'));
    }

    public function newsDetail($id)
    {
        $news_details = News::find($id);
        $news = News::orderBy('id','desc')->limit(4)->get();
        return view('home_pages.news_detial',compact('news_details','news'));
    }

    public function serviceList($id)
    {
        $service = Service::get();
        $service_name = Service::find($id)->name;
        $subservice = SubService::where('service_id',$id)->get();
        return view('home_pages.service-list',compact('service','subservice','service_name'));
    }

    public function stateId($id)
    {
       return $state = State::where('country_id',$id)->get();
    }

    public function cityId($id)
    {
        return $state = City::where('city_state',$id)->get();   
    }

    public function complain()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.complain',compact('service'));
    }

    public function refundPolice()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.refund_policy',compact('service'));
    }

    public function riskdiscloser()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.risk_discloser',compact('service'));
    }

    public function  disclaimer()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.disclaimer',compact('service'));
    }

    public function  terms_condition()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.terms_condition',compact('service'));
    }

    public function  reviews()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.reviews',compact('service'));
    }

    public function  risk_profile()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.risk_profile',compact('service'));
    }

    public function  riskProfileForm()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.upload_risk_profile',compact('service'));
    }

    public function  kyc()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.kyc',compact('service'));
    }

    public function  kycIndividual()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.kycindividualform',compact('service'));
    }

    public function  kycNonIndividual()
    {
        $service = Service::with('sub_service')->orderBy('id','asc')->get();
        return view('home_pages.kycnonindividualform',compact('service'));
    }


    public function quickinquiry(Request $request)
    {
        $this->validate($request,[
            'name'  =>  'required',
            'mobile_no'  =>  'required|min:10',
            'email'  =>  'required|email',
            'country'  =>  'required',
            'state'  =>  'required',
            'city'  =>  'required',
            'details'  =>  'required',
            'g-recaptcha-response' => 'required',
        ]);

        $assign = Assign::where('state', $request->state)->get();
        
        
        $inquiry = new Inquiry;
        // $assign = Assign::get();
        $tc_limit = Setting::find(2);
        if ($assign->count() > 0) {
            foreach ($assign as $assig) {
                    // return $assig;
                $tc_inquiry = Inquiry::where('tc_id',$assig->user_id)->whereDate('created_at',date('Y-m-d'))->count();
                if ($assig->state == $request->state && $assig->city == $request->city) {
                       if ($tc_inquiry < $tc_limit->value) {
                           $inquiry->tc_id =  $assig->user_id;
                           break;
                       }
                }else if($assig->state == $request->state){
                    if ($tc_inquiry < $tc_limit->value) {
                        $inquiry->tc_id =  $assig->user_id;
                        break;
                    }
                }
            }
        }else{
            $franch_tc = FranchaiseTc::where('is_other',1)->get();
            $seting = Setting::first();
            foreach ($franch_tc as $tc) {
                $tc_inquiry = Inquiry::where('tc_id',$tc->tc_id)->whereDate('created_at',date('Y-m-d'))->count();  
                if ($tc_inquiry < $seting->value) {
                    $inquiry->tc_id = $tc->tc_id;
                    break;
                }
            }
            if (empty($inquiry->tc_id)) {
                alert()->error("Sorry Sending inquiries from this area is not allowed yet.");
                return redirect()->back();
            }
        }
        if ($request->manual == 1) {
            $inquiry->manual = $request->manual;
            $inquiry->franchies_id = $request->franchies_id;   
        }
        $inquiry->name = $request->name;
        $inquiry->mobile_no = $request->mobile_no;
        $inquiry->email = $request->email;
        $inquiry->country = $request->country;
        $inquiry->state = $request->state;
        $inquiry->city = $request->city;
        $inquiry->details = $request->details;
        $inquiry->save();

        alert()->success('Thank you we are contact Quickly.');
        return redirect()->back();
    }

    public function uploadriskProfileForm(Request $request)
    {
        $this->validate($request,[
            'name'  =>  'required',
            'mobile_no'  =>  'required|max:10|min:10',
            'email'  =>  'required|email',
            'risk_form' =>  'required|mimes:pdf|max:10000'
        ]);

        // return $request->all();
        $risk = $request->file('risk_form');
        $risk_form_name = time().'.'.$risk->getClientOriginalExtension();
        $path = public_path().'/assets/risk_profile/';
        $risk->move($path, $risk_form_name);

        $risk_profile = new RiskProfile;
        $risk_profile->name   =  $request->name;
        $risk_profile->mobile_no =  $request->mobile_no;
        $risk_profile->email  = $request->email;
        $risk_profile->form   = $risk_form_name;
        $risk_profile->save();

        alert()->success('Thank you we are contact Quickly.');
        return redirect()->back();
    }
}
