<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Alert;
use Sentinel;
use Mail;
use View;
class LoginController extends Controller
{
    public function goLogin(Request $request)
    {
    	$this->validate($request,[
    		'email'	=> 'required|email',
    		'password'	=> 'required|min:8'
    	]);

    	$credentials = [
		    'email'    => $request->email,
		    'password' => $request->password,
		];

		$user = User::where('email',$request->email)->first();

		if($user && $user->status == 0)
		{
			alert()->warning('Deactivated','Your account not activated.');
			return redirect()->back();
		}
		elseif ($user && $user->status == 2) 
		{
			alert()->warning('Blocked','Your account is blocked.');
			return redirect()->back();	
		}
		elseif ($user && $user->is_delete == 2) 
		{
			alert()->warning('Deleted','Your account is blocked.');
			return redirect()->back();	
		}
		elseif ($user && $user->is_delete == 3) 
		{
			alert()->warning('Deleted','Your account is deleted.');
			return redirect()->back();	
		}
		elseif(!$user){
			Alert::error('Error', 'Email does not match.')->autoclose(18000);
			return redirect()->back();
		} 

		$user = Sentinel::authenticate($credentials);

		if ($user) 
		{
			$otp = rand(100000 , 999999);
			$slug = Sentinel::getUser()->roles()->first()->slug;
			if ($slug == 'm12') 
			{
				return redirect('/');
			}
			elseif($slug == 'super_admin') 
			{
				// User::where('id',Sentinel::getUser()->id)->update(['otp'=>$otp]);
				// $request->session()->put('logged_user_id', Sentinel::getUser()->id);
				// $this->sendOtpMail(Sentinel::getUser()->id,$otp); //Send mail for 2fa code 

    			//$this->logout();
                return redirect('dashboard');
                // return redirect('validate');
			}else{
				return redirect('/dashboard');
			}
		}
		else{
			alert()->error('Please enter correct password.','Password does not match.');
			return redirect('login');
		}
    }

    public function logout()
    {
    	if (Sentinel::check()) 
    	{
    		if (Sentinel::getUser()->roles()->first()->slug == 'm12') {
    			Sentinel::logout();
    			return redirect('/');
    		}
    		elseif (Sentinel::getUser()->roles()->first()->slug == 'super_admin') {
    			Sentinel::logout();
    			return redirect('/');
    		}
    		else{
				Sentinel::logout();
				return redirect('/');
			}					
    	}
		else{
			Sentinel::logout();
			return redirect('/');
		}
    }

    public function sendOtpMail($user_id,$otp){
    	$user = User::where('id', $user_id)->first();
    	Mail::send('mails.send-otp',[
            'user' => $user,
            'otp' => $otp
        ],function ($message) use ($user) {
            $message->to($user->email);
            $message->subject("Hello $user->first_name, 2FA code to validate account. ");
        });
    }
}
