<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use App\User;
use Session;
use Hash;
use Validator;
use Alert;

class ProfileController extends Controller
{
    public function edit()
    {
      $user = Sentinel::getUser();
      return view('layouts.dashboard.profile', compact('user'));
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());  
        $user = User::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->save();
        // Session::flash('success', 'Your profile was updated.');
        alert()->success('Success','Update your profile details.');
       return redirect()->back();
        
    }

    public function password_update(Request $request, $id)
    {
        if(Sentinel::check()){
         $user_id = Sentinel::getUser()->id;
          if(!$request->current_password)
          {
            return redirect()->back()->with('error-enter-oldpassword', 'Please Enter Current Password.');
          }

            $validator = Validator::make($request->only('current_password', 'new_password', 'confirm_password'),[
              'current_password' => 'required|min:8',
              'new_password' => 'required|min:8|different:current_password',
              'confirm_password' => 'required|required_with:new_password|min:8|same:new_password',
            ]);
            if($validator->fails()){
              return redirect()->back()->withErrors($validator)->withInput()->with(['validator' => '1']);  
            }  

            $input_password = $request->current_password;
            $current_password=Sentinel::getUser()->password;
            if(Hash::check($input_password, $current_password))
            {
                $user = User::find($user_id);
                $user->password =bcrypt($request->new_password);
                $user->save();
                alert()->success('Success','Changed your password.');
                return redirect()->back();
             }else{
                  alert()->error('Error','Invalid Current Password. Please try again.');
                  return redirect()->back();    
            }
        }else{
            alert()->error('Error','Session is Expired. Please try again.');
            return redirect()->back()->with('error-password', 'Session is Expired. Please Login to change password.');
        }
    }

    public function upload(Request $request, $id)
    {
        if(!$request->profile)
        {
          alert()->error('Opps','Please select profile pic after upload.');
          return redirect()->back();
        }
        if(Sentinel::check()){
            $user =User::find($id);
            $this->validate($request, [
              'profile' => 'mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            
            if(!is_null($request->profile)){
              $image = $request->file('profile');
              $path = 'assets/upload/images/';
              $imagename = time().'.'.$image->getClientOriginalExtension();
              $image->move($path, $imagename);
              $user->profile = $imagename;
            }  
            $user->save();
            alert()->success('Success','Your profile pic uploaded.');
            return redirect()->back();
        }else{
            alert()->error('Error','Session is Expired. Please try again.');
            return redirect()->back();
        }
    }
}
