<?php

namespace App\Http\Controllers;

use App\Models\SubService;
use App\Models\Service;
use Illuminate\Http\Request;

class SubServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service = Service::get();
        return view('superadmin.sub_service.sub-service-create',compact('service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' =>  'required',
            'services'  =>  'required',
            'capital'   =>  'required|numeric',
            'profit'    =>  'required|numeric',
            'service_details' => 'required',
            'duration_from' =>  'required|numeric',
            'duration_to' =>  'required|numeric',
            'sample_call'  => 'required', 
            'updates'   => 'required',
            'img'   =>  'required'
        ]);

        $img = $request->file('img');
        $path = public_path().'/assets/home/images/service/';
        $img_name = time().'.'.$img->getClientOriginalExtension();
        $image = $img->move($path,$img_name);

        $sub_service = new SubService;
        $sub_service->service_id = $request->services;
        $sub_service->name = $request->title;
        $sub_service->capital = $request->capital;
        $sub_service->profit = $request->profit;
        $sub_service->duration_from = $request->duration_from;
        $sub_service->duration_to = $request->duration_to;
        $sub_service->service_details = $request->service_details;
        $sub_service->genuine = $request->genuine;
        $sub_service->sample_call = $request->sample_call;
        $sub_service->updates = $request->updates;
        $sub_service->image = $img_name;
        $sub_service->save();

        alert()->success('Service created successfully.');
        return redirect('services-list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubService  $subService
     * @return \Illuminate\Http\Response
     */
    public function show(SubService $subService)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubService  $subService
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sub_service = SubService::find($id);
        $service = Service::get();
        return view('superadmin.sub_service.sub_service_edit',compact('sub_service','service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubService  $subService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' =>  'required',
            'services'  =>  'required',
            'capital'   =>  'required|numeric',
            'profit'    =>  'required|numeric',
            'duration_from' =>  'required|numeric',
            'duration_to' =>  'required|numeric',
            'service_details' => 'required',
            'sample_call'  => 'required', 
            'updates'   => 'required'
        ]);

        $sub_service = SubService::find($id);
        if($request->file('img')){
            try{
                $img = public_path().'/assets/home/images/service/'.$sub_service->image;
                unlink($img);
            }catch(\ErrorException  $e){

            }

            $img = $request->file('img');
            $path = public_path().'/assets/home/images/service/';
            $img_name = time().'.'.$img->getClientOriginalExtension();
            $image = $img->move($path,$img_name);

            $sub_service->image = $img_name;
        }

       

        $sub_service->service_id = $request->services;
        $sub_service->name = $request->title;
        $sub_service->capital = $request->capital;
        $sub_service->profit = $request->profit;
        $sub_service->duration_from = $request->duration_from;
        $sub_service->duration_to = $request->duration_to;
        $sub_service->service_details = $request->service_details;
        $sub_service->sample_call = $request->sample_call;
        $sub_service->updates = $request->updates;
        $sub_service->save();

        alert()->success('Service updated successfully.');
        return redirect('services-list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubService  $subService
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sub_service = SubService::find($id);
        $sub_service->is_delete = 1;
        $sub_service->update();

        alert()->success('Service deleted successfully.');
        return redirect()->back();
    }
}
