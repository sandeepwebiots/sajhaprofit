<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use App\Models\Packge;
use App\Models\Payment;
use App\Models\FranchaiseTc;
use App\Models\Setting;
use App\Models\CompanyDetail;
use App\Models\State;
use App\User;
use Activation;
use Sentinel;
use Excel;
use DB;
use PDF;
use App;
use Mail;
use Zipper;
use File;


class PaymentController extends Controller
{
    // public function pay()
    // {
    // 	$order = Indipay::gateway('PayUMoney')->prepare($parameters);
    //   	return Indipay::process($order);
    // }

    // public function payInformation($id)
    // {
    // 	$package = Package::find($id);
    // }

    public function pay($pack_id,$package)
    {	
    	$package = $package;
    	$payment = Packge::find($pack_id);

    	$state = State::get();
    	return view('home_pages.pay_form',compact('payment','package','state'));
    }

    public function payment(Request $request)
    {
    	$this->validate($request,[
    		'name'	=>	'required',
    		'email'	=> 'required|email',
    		'password'	=> 'required',
    		'mobile_no'	=> 'required|min:10',
    		'sponser_code'	=> 'required|min:10|max:10',
    		'address'	=> 'required',
    		'state'	=> 'required',
    		'city'	=> 'required',
    		'zipcode'	=> 'required|max:6|min:6',
    	]);

    	$credentials = [
		    'email'    => $request->email,
		    'password' => $request->password,
		];

		$user = User::where('email',$request->email)->first();
		if($user){
			$check_credentials = Sentinel::authenticate($credentials);
			if ($check_credentials) {
				$user_id = $user->id;
			}else{
				alert()->error('Password does not match.');
				return redirect()->back();
			}
		}else{
			$user = Sentinel::register(array(
	    		'user_name' =>	$request->name,	
	            'mobile_no' =>  $request->mobile_no,    
	    		'email'	=> $request->email,	
	    		'password' =>	$request->password,
	    		'address'	=> $request->address,
	    		'state'	=> $request->state,
	    		'city'	=> $request->city,
	    		'zip_code'	=> $request->zipcode,
	    	));
	    	$role = Sentinel::findRoleBySlug('m12');
    		$role->users()->attach($user);

	    	$activation = Activation::create($user);
	    	Activation::complete($user,$activation->code);
	    	$user_id = $user->id; 
	    	$user = User::where('id',$user->id)->update(['status' => 1]);
		}

		$sponser = User::where('code',$request->sponser_code)->first();
		if ($sponser) {
			$sponser_id = $sponser->designation_id;
		}else{
			$franch_tc = FranchaiseTc::where('is_other',1)->get();
            $seting = Setting::first();
            foreach ($franch_tc as $tc) {
                $tc_inquiry = Payment::where('tc_id',$tc->tc_id)->whereDate('created_at',date('Y-m-d'))->count();  
                if ($tc_inquiry < $seting->value) {
                    $sponser_id = $tc->tc_id;
                    break;
                }
            }
            if (empty($sponser_id)) {
                alert()->error("Sorry this Service is currently not available in this area.");
                return redirect()->back();
            }
			// $sponser_id = Null;
		}
		$gst_percentage = Setting::where('key','gst_percentage')->first();

		$amount = 'price_'.$request->package;
		$package_amount = Packge::where('id',$request->package_id)->first()->$amount;
		$gst_amount = ($package_amount * $gst_percentage->value) / 100;
		if ($request->state == 'GUJARAT') {
			$cgst_amount = $gst_amount / 2;
		}
		$total_amount = $package_amount +  $gst_amount;
		$gst = Setting::where('key','gst_percentage')->first()->value;

		$company_detail = CompanyDetail::first();

		$record = Payment::latest()->first();
		if (empty($record)) {
			$invoice_year = date('Y');
			$invoice_no = '0';
		}else{
			$invoice_year = $record->year;
		    $invoice_no = $record->invoice_no + 1;
		}

		$payment = new Payment;
		$payment->package_id = $request->package_id;
		$payment->pack = $request->package;
		$payment->pack_amount = $package_amount;
		$payment->gst_amount = $gst_amount;
		$payment->total_amount = $total_amount;
		$payment->tc_id = $sponser_id;
		$payment->user_id = $user_id;
		$payment->mobile_no = $request->mobile_no;
		$payment->name = $request->name;
		$payment->year = $invoice_year;
		$payment->invoice_no = $invoice_no;
		$payment->save();

		
    	$company_detail = CompanyDetail::first();
    	$user = User::where('id',$payment->user_id)->first();
    	$gst = Setting::where('key','gst_percentage')->first()->value;
//         return $data = view('superadmin.invoice.invoice',compact('payment','user','company_detail'));
  		PDF::loadView('superadmin.invoice.invoice',compact('payment','company_detail','user','gst','invoice_no','invoice_year'))->setPaper('a4')->save(public_path('assets/invoice/'.$invoice_no.'.pdf'));
        
		
		return view('home_pages.payment',compact('payment','user','company_detail','cgst_amount','gst','invoice_no','invoice_year'));
    }

    public function paymentComplete($id)
    {
    	$payment = Payment::find($id);
    	$date = Carbon::now();
    	$payment->start_date = $date->format('Y-m-d');
    	if ($payment->pack == 'half_yearly') {
    		$end_date = $date->add(CarbonInterval::months(6));
    	}elseif($payment->pack == 'yearly'){
    		$end_date = $date->add(CarbonInterval::year());
    	}elseif ($payment->pack == 'quarterly') {
    		$end_date = $date->add(CarbonInterval::months(3));
    	}elseif ($payment->pack == 'monthly') {
    		$end_date = $date->add(CarbonInterval::months());
    	}

    	$payment->end_date = $end_date->format('Y-m-d');
    	$payment->status = 1;
    	$payment->update();

    	$user = User::where('id',$payment->user_id)->first();
    	$company_detail = CompanyDetail::first();

    	Mail::send('mails.welcome', ['user' => $user,'company_detail' => $company_detail], function ($m) use ($user,$company_detail) {
            $m->to($user->email, $user->user_name)->subject('Your Reminder!');
            $m->to($company_detail->mail_contact)->subject('Your Reminder!');
        });

    	alert()->success('successfully payment complete.');
    	return redirect('/');
    }

    public function allInvioce(Request $request)
    {

        $files = glob(public_path('/assets/invoice/zip/*')); // get all file names

        foreach($files as $file){ // iterate files
            if(is_file($file)){
                unlink($file); // delete file
            }
        }

        // return 'success';

    	$from = $request->from;
    	$to = $request->to;
    	if (!$from && !$to) {
    		 $data = Payment::get()->toArray();
    		 $files = glob('assets/invoice/*');

    	}elseif ($from && $to) {
    		$data = Payment::whereBetween(DB::raw('date(created_at)'), [$request->from, $request->to])->whereNotNull('invoice')->get();
    		if ($data) {


    			// unlink();
    			// File::deleteDirectory(public_path('/assets/invoice/zip/payment-28330.zip'));
       //          File::cleanDirectory(public_path() . '/assets/invoice/zip'); 
	    		
    			$files =[];
				foreach ($data as $key => $value) {
					$file = public_path().'/assets/invoice/'.$value->invoice.'.pdf';
						if(file_exists($file)) {
					 		 $files[] = $file;
					 	}
					}
	    		}
    	}
    	$zip_file = "payment-".rand(10000,99999);
        Zipper::make(public_path('/assets/invoice/zip/'.$zip_file.'.zip'))->add($files)->close();
        return response()->download(public_path('/assets/invoice/zip/'.$zip_file.'.zip'));
    }

    public function exportExcel(Request $request)
    {
    	// return $request->all();
    	$from = $request->from;
    	$to = $request->to;
    	if (!$from && !$to) {
    		$data = Payment::with('package.sub_service')->get()->toArray();
    	}elseif ($from && $to) {
    		$data = Payment::with('package.sub_service')->whereBetween(DB::raw('date(created_at)'), [$request->from, $request->to])->get()->toArray();
    	}
		return Excel::create('payments', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
	        	foreach ($data as $key => $value) {
	        		if ($value['status'] == 0) {$status = 'Pending';}
	        		elseif($value['status'] == 1) { $status = 'Complete'; }
	                $payload[] = array('Invoice No' => $value['year'].str_pad($value['invoice_no'],6,'0',STR_PAD_LEFT),'Name' => $value['name'], 'Mobile No' => $value['mobile_no'], 'Service' => $value['package']['sub_service']['name'], 'Package' => $value['pack'], 'Package Amount' => $value['pack_amount'], 'GST Amount' => $value['gst_amount'], 'Total Amount' => $value['total_amount'], 'Date' => $value['created_at'],'Payment Status' => $status, 'Date' => $value['created_at']);
	            }
	            $sheet->fromArray($payload);
	        });
		})->download();

    }

    public function filterPaymentHistory(Request $request)
    { 
		$from = $request->from;
		$to = $request->to;
		$history = Payment::with('package.sub_service')->whereBetween(DB::raw('date(created_at)'), [$from, $to])->get();
		
		return view('superadmin.filter_history',compact('history','from','to'));
    }
}
