<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Models\Activation;
use Alert;
use Sentinel;
use Reminder;
use Mail;
use Carbon\Carbon;
use Hash;

class ForgotPasswordController extends Controller
{
	public function forgotPassword()
	{
		return view('home_pages.forgot_password');
	}
    public function postForgotPassword(Request $request)
    {
    	$this->validate($request,[
    		'email'	=>	'required|email'
    	]);

    	$user = User::whereEmail($request->email)->first();
    	
    	if ($user && $user->status == 3) {
    		alert()->info('Your account is Deleted!');
    		return redirect()->back();
    	}
    	elseif (isset($user)) {
    		$user_active = Activation::where('user_id',$user->id)->where('completed',1)->first();

    		if (empty($user_active) || $user->status == 0) {
                alert()->info('Email has not been validate.');
    			return redirect()->back();
    		}

             $sentinelUser = Sentinel::findById($user->id);
            if(!isset($user)){
                alert()->info('Reset Code already sended to your email.');
                return redirect()->back();
            }
            $alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            $reset_string = substr(str_shuffle(str_repeat($alpha_numeric, 8)), 0, 8);

            $reminder = Reminder::exists($sentinelUser) ? : Reminder::create($sentinelUser);
            $credentials = [
                'code' => $reset_string
            ];

            Reminder::where('id', $reminder->id)->update($credentials);
            $code = $reminder->code;
            $reset_code = $reset_string;

            // return view("mails.forgot_password",compact('user','code','reset_code'));
            $this->sendForgotPasswordMail($user,$reminder->code,$reset_string);

            alert()->warning('Reset code is send to your email.');
            return redirect('/');
    	}
    	else{
	    	alert()->warning('This email not valid.');
	    	return redirect()->back();
    	}
    }

    public function sendForgotPasswordMail($user, $code, $reset_string)
    {
        Mail::send('mails.forgot_password',[
            'user' => $user,
            'code' => $code,
            'reset_code' => $reset_string
        ],function ($message) use ($user) {
            $message->to($user->email);
            $message->subject("mails.forgot_password, Password reset request recieved. ");
        });
    }

    public function restetPassword($email,$resetCode)
    {
        $user = User::where('email',$email)->first();


        $sentinelUser = Sentinel::findById($user->id);
        if($reminder = Reminder::exists($sentinelUser) ? : Reminder::create($sentinelUser)){
            $now = Carbon::now();
            $date = $reminder->created_at;
            $created = new Carbon($date);
            $totalDuration = $now->diffInSeconds($created);
            if($totalDuration>480){
                $credentials = [
                    'completed' => '1'
                ];
                $update = Reminder::where('id', $reminder->id)->update($credentials);
                alert()->warning('Your reset password link has been expired !!!');
                return redirect('/');
            } 
            if($resetCode==$reminder->code){
                // alert()->warning('The email is required');
                return view('home_pages.reset_password',compact('user','resetCode'));
            }
            else
                alert()->warning('Your reset password link has been expired !!!');
                return redirect('/');
        }
        else{
            alert()->warning('Something went wrong !!!');
            return redirect('/');
        }
    } 

    public function postResetPassword(Request $request,$email,$resetcode)
    {
        // return $request->all();
        $this->validate($request,[
            'new_password'  =>  'required|min:8',
            'confnewpassword'  => 'required|same:new_password'
        ]); 

        $user = Sentinel::findByCredentials(['email' => $request->email]);  

        if($reminder = Reminder::complete($user,$request->resetCode, $request->new_password)) {
            $new_password = Hash::make($request->new_password);
            $user->password = $new_password;
            $user->update();

            alert()->success('Please Login With Your New Password.');
            return redirect('login');
        }
        alert()->info('Link is Expired.');
        return redirect('/');
    }
}
