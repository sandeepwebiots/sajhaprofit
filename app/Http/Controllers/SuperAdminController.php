<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Activation;
use App\Models\Designation;
use App\Models\Payment;
use App\Models\FranchaiseTc;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Inquiry;
use App\Models\Revenue;
use App\Models\Setting;
use App\User;
use Carbon\Carbon;
use Alert;
use Sentinel;
use Validator;
use Mail;
use DB;
use Illuminate\Support\Arr;

class SuperAdminController extends Controller
{
    public function superDashboard()
    {
        $user = Sentinel::getUser();
        // return $this->getDownlevelStaff($user);

        $date = \Carbon\Carbon::now();
        $lastmonth_name = $date->subMonth()->format('F'); 
        
        $m10=0;
        $m11=0;
        $m2_m4=0;
        $m5_m7=0;
        $m8_m11=0;
        $total_staff=0;
        $revenueLastMonth=0;
        $revenueThisMonth=0;
        $commissionThisMonth=0;
        $commissionLastMonth=0;
        $open_leads=0;
        $direct_clients=0;
        $total_active_clients=0;
        $total_lead=0;

        $AllLevelStaff =  $this->refferalDownline($user);
        if (!empty($AllLevelStaff)) {
            
            $total_staff = count($AllLevelStaff);
            $revenue_this = 0;
            $revenue_last = 0;
            foreach ($AllLevelStaff as $key => $value) {
                if ($value['role_id'] == 'm10') {
                    $m10++;
                }elseif ($value['role_id'] == 'm11') {
                    $m11++;
                }elseif ($value['role_id'] == 'm2' || $value['role_id'] == 'm3' || $value['role_id'] == 'm4') {
                    $m2_m4++;  
                }elseif ($value['role_id'] == 'm5' || $value['role_id'] == 'm6' || $value['role_id'] == 'm7') {
                    $m5_m7++;
                }
                if ($value['role_id'] == 'm8' || $value['role_id'] == 'm9' || $value['role_id'] == 'm10' || $value['role_id'] == 'm11') {
                    $m8_m11++;
                } 
                if ($value['role_id'] == 'm11') {
                    $fromDate = Carbon::now()->subMonth()->startOfMonth()->toDateString();
                    $tillDate = Carbon::now()->subMonth()->endOfMonth()->toDateString();
                        
                        $revenueLastMonth = Payment::where('tc_id',$value['id'])->whereBetween(DB::raw('date(created_at)'), [$fromDate, $tillDate])->where('status',1)->get()->sum('total_amount');
                        $revenue_last = $revenue_last + $revenueLastMonth; 
                        $revenueThisMonth = Payment::where('tc_id',$value['id'])->whereMonth('created_at', '=', Carbon::now()->month)->where('status',1)->get()->sum('total_amount');
                        $revenue_this = $revenue_this + $revenueThisMonth;

                    $revenueLastMonth = $revenue_last;
                    $revenueThisMonth = $revenue_this;


                    $leads = Inquiry::where('tc_id',$value['id'])->whereDate('created_at',date('Y-m-d'))->orderBy('id','desc')->get();
                    $open_leads = $open_leads + $leads->where('manual',null)->count();
                    $direct_clients = $direct_clients + $leads->where('manual',1)->count();
                    $total_active_clients = $total_active_clients + $leads->where('status',1)->count();
                    $total_lead = $total_lead + $leads->count();
                }
            }
            // return $m8_m11;
               
        }
        $staff_list = $this->getStaffList($user);
        //staff delete
        $staff_delete = $this->staffDelete($user);

        if ($user->inRole('m11')) {
            $leads = Inquiry::where('tc_id',$user->designation_id)->whereDate('created_at',date('Y-m-d'))->orderBy('id','desc')->get();
            $open_leads = $leads->where('manual',null)->count();
            $direct_clients = $leads->where('manual',1)->count();
            $total_active_clients = $leads->where('status',1)->count();
            $total_lead = $leads->count();

            $fromDate = Carbon::now()->subMonth()->startOfMonth()->toDateString();
            $tillDate = Carbon::now()->subMonth()->endOfMonth()->toDateString();
            if ($user->inRole('m11')) {

                $revenueLastMonth = Payment::where('tc_id',$user->designation_id)->whereBetween(DB::raw('date(created_at)'), [$fromDate, $tillDate])->where('status',1)->get()->sum('total_amount');
                $revenueThisMonth = Payment::where('tc_id',$user->designation_id)->whereMonth('created_at', '=', Carbon::now()->month)->where('status',1)->get()->sum('total_amount');
            }
            // elseif ($user->inRole('m10')) {
            //     $designation = Designation::where('desig_id',$user->designation_id)->get();
            //     $revenue_this = 0;
            //     $revenue_last = 0;
            //     foreach ($designation as $desig) {
            //         if ($desig) {
            //             $revenueLastMonth = Payment::where('tc_id',$desig->id)->whereBetween(DB::raw('date(created_at)'), [$fromDate, $tillDate])->where('status',1)->get()->sum('total_amount');
            //             $revenue_last = $revenue_last + $revenueLastMonth;
                        
            //             $revenueThisMonth = Payment::where('tc_id',$desig->id)->whereMonth('created_at', '=', Carbon::now()->month)->where('status',1)->get()->sum('total_amount');
            //             $revenue_this = $revenue_this + $revenueThisMonth;
            //         }
            //     }
            //     $revenueLastMonth = $revenue_last;
            //     $revenueThisMonth = $revenue_this;
            // }
            
                // $commissionThisMonth;
                // $commissionLastMonth;  
        }
            // $commission = Setting::where('list','commission')->get();
            // foreach ($commission as $commiss) {

            //     if ($revenueThisMonth < $commiss->key) {
            //         $commissionThisMonth = ($revenueThisMonth * $commiss->value) / 100;
            //     }
                
            //     if ($revenueLastMonth < $commiss->key) {
            //         $commissionLastMonth = ($revenueLastMonth * $commiss->value) / 100;
            //     }
            //     if ($commiss->id == 9) {
            //           if ($revenueThisMonth > $commiss->key) {
            //             $commissionThisMonth = ($revenueThisMonth * $commiss->value) / 100;
            //             $commissionLastMonth = ($revenueLastMonth * $commiss->value) / 100;
            //         }      
            //     }              
            // }
            $first_commission = Setting::where('id','4')->first();
            $second_commission = Setting::where('id','5')->first();
            $third_commission = Setting::where('id','6')->first();
            $fourth_commission = Setting::where('id','7')->first();
            $fifth_commission = Setting::where('id','8')->first();
            $sixth_commission = Setting::where('id','9')->first();

            if ($revenueThisMonth < $first_commission->key) {
                $commissionThisMonth = ($revenueThisMonth * $first_commission->value) / 100;
            }elseif ($revenueThisMonth < $second_commission->key) {
                $commissionThisMonth = ($revenueThisMonth * $second_commission->value) / 100;
            }elseif ($revenueThisMonth < $third_commission->key) {
                $commissionThisMonth = ($revenueThisMonth * $third_commission->value) / 100;
            }elseif ($revenueThisMonth < $fourth_commission->key) {
                $commissionThisMonth = ($revenueThisMonth * $fourth_commission->value) / 100;
            }elseif ($revenueThisMonth < $fifth_commission->key) {
                $commissionThisMonth = ($revenueThisMonth * $fifth_commission->value) / 100;
            }elseif ($revenueThisMonth > $sixth_commission->key) {
                $commissionThisMonth = ($revenueThisMonth * $sixth_commission->value) / 100;
            }
            
            if ($revenueLastMonth < $first_commission->key) {
                $commissionLastMonth = ($revenueLastMonth * $first_commission->value) / 100;
            }elseif ($revenueLastMonth < $second_commission->key) {
                $commissionLastMonth = ($revenueLastMonth * $second_commission->value) / 100;
            }elseif ($revenueLastMonth < $third_commission->key) {
                $commissionLastMonth = ($revenueLastMonth * $third_commission->value) / 100;
            }elseif ($revenueLastMonth < $fourth_commission->key) {
                $commissionLastMonth = ($revenueLastMonth * $fourth_commission->value) / 100;
            }elseif ($revenueLastMonth < $fifth_commission->key) {
                $commissionLastMonth = ($revenueLastMonth * $fifth_commission->value) / 100;
            }elseif ($revenueLastMonth > $sixth_commission->key) {
                $commissionLastMonth = ($revenueLastMonth * $sixth_commission->value) / 100;
            }

            $revenue = Revenue::where('user_desig_id',$user->designation_id)->where('month',$lastmonth_name)->first();
            if (!$revenue) {
                $revenue = new Revenue;
                $revenue->user_desig_id = $user->designation_id;
                $revenue->revenue = $revenueLastMonth;
                $revenue->commission = $commissionLastMonth;
                $revenue->month = $lastmonth_name;
                $revenue->save();
            }

    	return view('superadmin.dashboard',  compact('staff_list','staff_delete','commissionThisMonth','commissionLastMonth','revenueLastMonth','revenueThisMonth','AllLevelStaff','total_staff','m10','m11','total_active_clients','direct_clients','open_leads','m2_m4','m5_m7','m8_m11','total_lead'));
    }

    public function getStaffList($user)
    {
        $designation = Designation::where('desig_id',$user->designation_id)->get();
        $staff_list = [];
        foreach ($designation as $design) {
            if($design){ 
                $staff_desig = Designation::where('desig_id',$design->id)->get();
                    foreach ($staff_desig as $designation) {
                        if($designation){
                            if(count($staff_list) > 0){
                                $staffdata = User::with('designation')->where('designation_id',$designation->id)->where('status',0)->get(); 
                                $staff_list = $staff_list->merge($staffdata);
                            }else{
                                $staff_list = User::with('designation')->where('designation_id',$designation->id)->where('status',0)->get();
                            }
                        }
                    }  
                if($user->inRole('super_admin')){
                    $staff = User::with('designation')->where('designation_id',$design->id)->where('status',0)->orwhere('status',4)->get();
                    if ($staff) {
                         if($staff_list){
                            $staff_list = $staff_list->merge($staff);
                        }else{
                             $staff_list = $staff;
                        }
                     } 
                }
            }
        }
        // return $staff_list;
        // if(!isset($staff_list)){
        //     return $staff_list = [];
        // }else{
            return $staff_list;
        // }
    }


    public function refferalDownline($user)
    {
        $childStaff = Designation::with('downlinestaff')->has('downlinestaff')->where('desig_id',$user->designation_id)->get()->toArray();
        if (!empty($childStaff)) {
            
            foreach ($childStaff as $row) {
                
                $child_id = $row['downlinestaff']['designation_id'];
                $children[$child_id] = $row;
                $children = array_merge($children, $this->getAllDownlines($child_id));
            }
        return $children;
        return $children = json_encode($children);
        }
        
    }

    public function getAllDownlines($fathers) {
        if(is_array($fathers)){
            $childStaff = Designation::with('downlinestaff')->has('downlinestaff')->whereIn('desig_id',$fathers)->get()->toArray();
        }else{
            $childStaff = Designation::with('downlinestaff')->has('downlinestaff')->where('desig_id',$fathers)->get()->toArray();
        }
            
        $new_father_ids = array();
        $children = array();
        foreach ($childStaff as $child) {
            $child_id = $child['downlinestaff']['designation_id'];
            $children[$child_id] = $child; // etc

            $new_father_ids[] = $child_id;
        }

        if($new_father_ids)
            $children = array_merge($children, $this->getAllDownlines($new_father_ids));
        
        return $children;
    }

    public function staffDelete($user)
    {
        $designations = Designation::where('desig_id',$user->designation_id)->get();
        $staff_delete = [];
        foreach ($designations as $design) {
            if($design){
                $staff_desig = Designation::where('desig_id',$design->id)->get();
                foreach ($staff_desig as $designation) {
                    if($designation){
                        if(count($staff_delete) > 0){
                            $staffdelete = User::with('designation')->where('designation_id',$designation->id)->where('is_delete',1)->get(); 
                            $staff_delete = $staff_delete->merge($staffdelete);
                        }else{
                           $staff_delete = User::with('designation')->where('designation_id',$designation->id)->where('is_delete',1)->get();
                        }
                    }
                }    

                if($user->inRole('super_admin')){
                    $staff = User::with('designation')->where('designation_id',$design->id)->whereIn('is_delete',[1,2])->orwhere('is_delete',2)->get(); 
                    if ($staff) {
                         if($staff_delete){
                            $staff_delete = $staff_delete->merge($staff);
                        }else{
                            $staff_delete = $staff;
                        }
                     } 
                }
            }
        }
        // return $staff_delete;
        if(!isset($staff_delete)){
            return $staff_delete = [];
        }else{
            return $staff_delete;
        }
    }

    public function userList()
    {
        $user = Payment::with('user')->where('status',1)->get();    
        $users = $user->groupBy('user_id');
        // $users = RoleUser::with('user.payment_date')->where('role_id',12)->orderBy('user_id','desc')->get();
        // return $users = User::with('roleuser','payment')->where(function ($query) {
        //           $query->where('role_id', 12) ;
        //       })->where('id','<>',15)->orderBy('id','desc')->where('is_delete',0)->get();
        return view('superadmin.user_list',compact('users'));
    }
    public function pack_act_de_act(Request $request)
    {
        if($request->status=='Active')
        {
            User::where('id', $request->id)->update(['status' => 1]);
            
            return 'Active';
        }else
        {
            User::where('id', $request->id)->update(['status' => 0]);
            return 'Deactive';
        }
       
    }

    public function statusChange($id)
    {
    	$user = User::find($id);

    	if ($user->status == 1) {

    		$user->status = 2;
    		$user->update();
    	}
    	elseif ($user->status == 2) {

    		$user->status = 1;
    		$user->update();
    	}
    	alert()->info('Successfully change user status.');
    	return redirect()->back();
    }

    public function deleteUser($id)
    {
    	$user = User::find($id);

    	$user->status = 3;
    	$user->is_delete = 2;
    	$user->update();

    	return redirect()->back();
    }

    public function staffActivation($id)
    {
        $staff = Sentinel::findById($id);
        $activation = Activation::where('user_id',$id)->first();
        $activation->completed = 1;
        $activation->completed_at = date("Y-m-d");
        $activation->update();
        
        $password =str_random(12) ;
        $hash_password = Hash::make($password);
        $staff->status = 1;
        $staff->password = $hash_password;
        $staff->save();

        // return view('mails.staff_activation',compact('staff','password'));
        Mail::send('mails.staff_activation', ['staff' => $staff,'password' => $password], function ($m) use ($staff) {
              $m->to($staff->email);
              $m->Subject("Hello $staff->user_name, your account is activated now");
            });
        alert()->success('Successfull activate staff account.');
        return redirect()->back();
                
    }

    public function staffBlock($id)
    {
        $staff = Sentinel::findById($id);
        $staff->delete();

        alert()->success('success','Canceled staff account.');
        return redirect()->back();
    }

    public function childStaff($id)
    {
        $designation = Designation::where('desig_id',$id)->pluck('id')->toArray();
        $stafflist = User::with('designation')->whereIn('designation_id',$designation)->get();
        
        return view('superadmin.staff.sub_staff',compact('stafflist'));
    }

    public function approvedByUpper($id)
    {
        $staff = Sentinel::findById($id);
        $staff->status = 4;
        $staff->save();

        alert()->success('Successfull activate staff account.');
        return redirect()->back();
    }

    public function paymentHistory()
    {
        $user = Sentinel::getUser();
        
        if($user->inRole('super_admin')){
            $history = Payment::with('package.sub_service')->get();
        }elseif ($user->inRole('m10')) {
            $tc_list = FranchaiseTc::where('franch_id',$user->designation_id)->get();
            $k = 0;
            foreach ($tc_list as $tc) {
               
                $historys = Payment::with('package.sub_service')->where('tc_id',$tc->tc_id)->get();
                if($historys->count() > 0){
                foreach ($historys as $hs) {
                    $history_temp[$k] = $hs;
                    $k++;
                }
                $history = $history_temp; 
                }else{
                    $history = '';
                }
            }
        }
        elseif ($user->inRole('m11')) {
           $history = Payment::with('package.sub_service')->where('tc_id',$user->designation_id)->get();
        }
        if (empty($history)) {
            $history = [];
        }

        return view('superadmin.history',compact('history'));
    }

    public function deletestaff($id)
    {
        $user = Sentinel::getUser();
        if($user->inRole('super_admin')){
            $delte_staff = User::find($id);
            $delte_staff->is_delete = 3;
            $delte_staff->designation_id = Null;
            $delte_staff->deleted_at = date('Y-m-d H:i:s');
            $delte_staff->update();
        }else{
             $delte_staff = User::find($id);
             $delte_staff->is_delete = 1;
             $delte_staff->update();
        }

        alert()->success('Succesfully deleted staff.');
        return redirect()->back();
    }

    public function approveDeleteReviewer($id)
    {
        $user = Sentinel::getUser();
        if($user->inRole('super_admin')){
            $delte_staff = User::find($id);
            $delte_staff->is_delete = 3;
            $delte_staff->designation_id = Null;
            $delte_staff->deleted_at = date('Y-m-d H:i:s');
            $delte_staff->update();
        }else{
            $delte_staff = User::find($id);
            $delte_staff->is_delete = 2;
            $delte_staff->update();
        }
        

        alert()->success('Succesfully deleted staff.');
        return redirect()->back();
    }

    public function deleteCancel($id)
    {
        $delte_staff = User::find($id);
        $delte_staff->is_delete = 0;
        $delte_staff->update();

        alert()->success('Succesfully staff deleted request Cancel.');
        return redirect()->back();
    }

    public function promtionStaff()
    {
        $roles = Role::whereNotIn('id',[1,12])->where('status',1)->get();
        return view('superadmin.promotion.promotion-staff',compact('roles'));
    }

    public function promotedStaff($slug)
    {
        $designation = Designation::where('role_id',$slug)->get();
        $k = 0;
        if ($slug == 'm10' || $slug == 'm11') {
            foreach ($designation as $design) {
                $staffs = User::where('designation_id',$design->id)->get();
                $j = 0;
                foreach ($staffs as $value) {
                    $staff[$j] = $value;
                    $j++;
                }
                $k++;
            }
        }else{
            foreach ($designation as $design) {
                $staff[$k] = User::where('designation_id',$design->id)->first();
                $k++;
            }
        }

        return $staff;
    }

    public function designationRole($slug)
    {
        return $designation = Designation::where('role_id',$slug)->get();
    }

    public function checkPosition($id)
    {
        $staff = User::where('designation_id',$id)->first();
        if (is_null($staff)) {
            return 0;
        }else{
            return 1;
        }
    }

    public function givePromotionStaff(Request $request)
    {
        $this->validate($request,[
            'role' => 'required',
            'staff' =>  'required',
            'level' =>  'required',
            'department' => 'required',
        ]);

        // return $request->all();
        $staff = User::where('id',$request->staff)->first();
        $staff->designation_id = $request->department;
        $staff->update();

        $role_user = RoleUser::where('user_id',$staff->id)->delete();
        $role = Sentinel::findRoleBySlug($request->level);
        $role->users()->syncWithoutDetaching($staff);
        
        alert()->success('Successfully change staff postion.');
        return redirect()->back();

    }

    public function staffLevel($level)
    {
        $user = Sentinel::getUser();
        $AllLevelStaff =  $this->refferalDownline($user); 
        if (!empty($AllLevelStaff)) {
        
        $staff = [];
        $total_staff = count($AllLevelStaff);

            foreach ($AllLevelStaff as $key => $value) {

                if ($level == 'Top-Management') {
                    if ($value['role_id'] == 'm2' || $value['role_id'] == 'm3' || $value['role_id'] == 'm4') {
                         $staff[] = $value;
                    }    
                }elseif ($level == 'Middle-Management') {
                    if ($value['role_id'] == 'm5' || $value['role_id'] == 'm6' || $value['role_id'] == 'm7') {
                        $staff[] = $value;
                    }
                }elseif ($level == 'Junior-Management') {
                    if ($value['role_id'] == 'm8' || $value['role_id'] == 'm9' || $value['role_id'] == 'm10' || $value['role_id'] == 'm11') { 
                        $staff[] = $value; 
                    }
                }else{
                    return redirect()->back();
                }
            }   
            return view('superadmin.staff_level',compact('staff','level'));
        }
    }

    public function revenuelist()
    {

        $user = Sentinel::getUser();
        $AllLevelStaff =  $this->refferalDownline($user); 
        if (!empty($AllLevelStaff)) {
        
        $revenues = [];
            foreach ($AllLevelStaff as $key => $value) {

                if ($user->inRole('super_admin')) {
                    if ($value['role_id'] == 'm3') {     
                        $revenue = Revenue::with('designation')->where('user_desig_id',$value['id'])->get();
                        foreach ($revenue as $value) {
                                $revenues[] =  $value; 
                        }
                    }
                }
                if ($user->inRole('m10')) {
                    if ($value['role_id'] == 'm11') { 
                        $revenue = Revenue::with('designation')->where('user_desig_id',$value['id'])->get();
                        foreach ($revenue as $value) {
                                $revenues[] =  $value; 
                        }
                    }
                }  
            }   
        }
        if ($user->inRole('m11')) { 
            $revenues = Revenue::with('designation')->where('user_desig_id',$user->designation_id)->get();
        }
        return view('superadmin.revenue_list',compact('staff','level','revenues'));

    }

    public function refferalRevenueDownline($user)
    {
        $childStaff = Designation::with('downlinestaff')->has('downlinestaff')->where('desig_id',$user['designation_id'])->get()->toArray();
        if (!empty($childStaff)) {
            
            foreach ($childStaff as $row) {
                
                $child_id = $row['downlinestaff']['designation_id'];
                $children[$child_id] = $row;
                $children = array_merge($children, $this->getAllDownlines($child_id));
            }
        return $children;
        return $children = json_encode($children);
        }
        
    }  

    public function deactivateStaff($id)
    {
        $staff = User::where('id',$id)->first();
        $designation = Designation::where('id',$staff->designation_id)->first();
        $child = Designation::where('desig_id',$designation->id)->update(['desig_id' => $designation->desig_id]);
        // $staff->delete();
        $staff->is_delete = 3;
        $staff->designation_id = Null;
        $staff->deleted_at = date('Y-m-d H:i:s');
        $staff->update();

        alert()->success('Succesfully deactivate staff.');
        return redirect()->back();
    } 

    public function rolelevel($id)
    {
        $staff = User::where('designation_id',$id)->first();
        $LevelStaff =  $this->refferalDownline($staff);

        return $details = $this->unique_multidim_array($LevelStaff,'role_id'); 
        
    }

    public function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 
        
        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 

    public function filterRoleLevel(Request $request)
    {
        $admin = Sentinel::getUser(); 
        $staff = User::where('designation_id',$request->bussiness_id)->first();
        $LevelStaff =  $this->refferalDownline($staff);

        $stafflist = array();
        if (!empty($LevelStaff)) {
                
            foreach ($LevelStaff as $key => $value) {
                if ($value['role_id'] == $request->level_id) {
                    $stafflist[] = $value;
                }
            }
            // return $stafflist;

            $bussiness = Designation::where('desig_id',$admin->designation_id)->where('role_id','m3')->get();

            return view('superadmin.staff.role_level',compact('stafflist','bussiness'));

        }
    }
}
