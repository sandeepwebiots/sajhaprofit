<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\SubService;
use App\Models\Packge;

class PackageController extends Controller
{
    public function index()
    {
    	$packge = Packge::with('sub_service.service')->where('is_delete',0)->orderBy('id','desc')->get();
    	return view('superadmin.package.index',compact('packge'));
    }

    public function create()
    {
    	$service = Service::get();
        return view('superadmin.package.create',compact('service'));
    }

    public function subServiceList($id)
    {
        return $sub_service = SubService::where('service_id',$id)->where('is_delete',0)->orderBy('id','desc')->get(); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $this->validate($request,[
           
            'service'  =>  'required',
            'subserviceid'  =>  'required',
        ]);
        // return $request->all();
        $package = Packge::where('sub_service_id',$request->subserviceid)->first();
        if (!$package) {
            $packge = new Packge;
            if($request->monthly == '1'){ $packge->monthly_checked = 1; }
            if($request->quaterly == '1'){ $packge->quarterly_checked = 1; }
            if($request->half_yearly == '1'){ $packge->half_yearly_checked = 1; }
            if($request->yearly == '1'){ $packge->yearly_checked = 1; }
            if($request->price_monthly ){ $packge->price_monthly = $request->price_monthly; }
            if($request->price_quaterly ){ $packge->price_quarterly = $request->price_quaterly; }
            if($request->price_half_yearly ){ $packge->price_half_yearly = $request->price_half_yearly; }
            if($request->price_yearly ){ $packge->price_yearly = $request->price_yearly; }
            $packge->service_id = $request->service;
            $packge->sub_service_id = $request->subserviceid;
            $packge->save();
        }else{
            alert()->warning('This Service as already package crated.');
            return redirect()->back();
        }
        
        alert()->success('Packge created successfully.');
        return redirect()->route('packages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $service = Service::get();
        $packge = Packge::with('subservice')->find($id);
        return view('superadmin.package.edit',compact('packge','service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        // return $request->all();
        $this->validate($request,[
            'service'  =>  'required',
            'subserviceid'  =>  'required',
        ]);
        $packge = Packge::find($id);
        if($request->monthly == '1'){ $packge->monthly_checked = 1; }else{$packge->monthly_checked = 0;}
        if($request->quaterly == '1'){ $packge->quarterly_checked = 1; }else{$packge->quarterly_checked = 0;}
        if($request->half_yearly == '1'){ $packge->half_yearly_checked = 1; }else{$packge->half_yearly_checked = 0;}
        if($request->yearly == '1'){ $packge->yearly_checked = 1; }else{$packge->yearly_checked = 0;}
        if($request->price_monthly ){ $packge->price_monthly = $request->price_monthly; }else{$packge->price_monthly = '';}
        if($request->price_quaterly ){ $packge->price_quarterly = $request->price_quaterly; }else{$packge->price_quaterly = '';}
        if($request->price_half_yearly ){ $packge->price_half_yearly = $request->price_half_yearly; }else{$packge->price_half_yearly = '';}
        if($request->price_yearly ){ $packge->price_yearly = $request->price_yearly; }else{$packge->price_yearly = '';}
        
        $packge->service_id = $request->service;
        $packge->sub_service_id = $request->subserviceid;
        $packge->update();

        alert()->success('Packge updated successfully.');
        return redirect()->route('packages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $packge = Packge::find($id);
        $packge->is_delete = 1;
        $packge->update();

        alert()->success('Packge deleted successfully.');
        return redirect()->route('packages.index');
    }
}
