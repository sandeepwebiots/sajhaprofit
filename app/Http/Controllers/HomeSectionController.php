<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\HomeSection;
use App\Models\SidebarModule;

class HomeSectionController extends Controller
{
    public function index()
    {
    	$home_section = HomeSection::get();
        $modules = SidebarModule::first();
    	return view('superadmin.home-section.home_section',compact('home_section','modules'));
    }

    public function edit($id)
    {
    	$section = HomeSection::find($id);
    	return view('superadmin.home-section.home_section_edit',compact('section'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'email' =>  'required|email',
            'contact_no' => 'required|numeric',
            'address'   =>  'required',
            'city'      =>  'required',
            'state'     =>  'required'
        ]);

    	return $section = HomeSection::find($id);   	
    }

    public function sliderSection()
    {
        $slider = HomeSection::where('slug','slider')->orderBy('position','asc')->get();
        return view('superadmin.home-section.slider_section',compact('slider'));
    }

    public function addSlider()
    {
        return view('superadmin.home-section.add-slider');
    }

    public function postaddSlider(Request $request)
    {
        $this->validate($request,[
            'title' =>  'required|max:150',
            'image' =>  'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status'  => 'required', 
            'position'  => 'nullable|unique:home_sections', 
        ]);

        $img = $request->file('image');
        $path = public_path().'/assets/home/images/team/';
        $img_name = time().'.'.$img->getClientOriginalExtension();
        $image = $img->move($path,$img_name);
        
        $slider = new HomeSection;
        $slider->section = 'Slider';
        $slider->slug    = 'slider';
        $slider->title   = $request->title;
        $slider->sub_title   = $request->sub_title;
        $slider->image   = $img_name;
        $slider->status   = $request->status;
        $slider->position   = $request->position;
        $slider->save();

        alert()->success('Successfully add slider.');
        return redirect('slider-section');
    }

    public function editSlider($id)
    {
        $slider = HomeSection::where('slug','slider')->find($id);
        return view('superadmin.home-section.edit-slider',compact('slider'));
    }

    public function updateSlider(Request $request,$id)
    {
        $this->validate($request,[
            'title' =>  'required|max:150',
            'status'  => 'required', 
            'position'  => 'nullable', 
        ]);

        $present = HomeSection::where('position',$request->position)->where('id','!=',$id)->get();
        if(count($present)>0){
          alert()->error('Position already set for other picture');
           return redirect()->back(); 
        }
       $slider = HomeSection::find($id);

        if ($request->hasFile('image')) {
            $this->validate($request,[
                'image' =>  'mimes:jpeg,png,jpg,gif,svg|max:2048',
                
            ]);
            try{
                $image_path = public_path('/assets/home/images/team/').$slider->image;
                unlink($image_path);
            }catch(\ErrorException  $e){

            }

            $img = $request->file('image');
            $path = public_path().'/assets/home/images/team/';
            $img_name = time().'.'.$img->getClientOriginalExtension();
            $image = $img->move($path,$img_name); 

            $slider->image   = $img_name;
        }
        
        $slider->title   = $request->title;
        $slider->sub_title   = $request->sub_title;
        $slider->status   = $request->status;
        $slider->position   = $request->position;
        $slider->update();

        alert()->success('Successfully update slider.');
        return redirect('slider-section');
    }

    public function deleteSlider($id)
    {
        $total_slider = HomeSection::where('slug','slider')->where('status','1')->count();
        if ($total_slider < 4) {
            $slider = HomeSection::find($id);
            $slider->delete();

            alert()->success('Successfully delete slider.');
            return redirect('slider-section');
        }else{
            $slider = HomeSection::find($id);
            if ($slider->status == 0) {
                $slider->delete();
            }
            alert()->error("You don't delete.");
            return redirect('slider-section');
        }
    }
    
}
