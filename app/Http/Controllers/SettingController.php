<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\Models\HomeSection;
use App\Models\Role;
use App\Models\CompanyDetail;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('superadmin.setting.setting');
    }

    public function edit_limit()
    {
        $limit_tc = Setting::where('key','max_limit_for_other')->first()->value;
        $limit_other = Setting::where('key','max_limit_tc')->first()->value;
        $gst = Setting::where('key','gst_percentage')->first()->value;
        return view('superadmin.setting.edit_limit',compact('limit_tc','limit_other','gst'));
    }

    public function edit_website_detials()
    {
        $company_details = CompanyDetail::first();
        return view('superadmin.setting.edit_website_details',compact('company_details'));
    }

    public function update_limit(Request $request)
    {
        $this->validate($request,[
            'telecaller'  =>  'required|numeric',
            'other'  =>  'required|numeric',
            'gst'  =>  'required|numeric',
        ]);

        $limit_tc = Setting::where('key','max_limit_for_other')->update(array('value' => $request->telecaller ));
        $limit_other = Setting::where('key','max_limit_tc')->update(array('value' => $request->other));
        $gst = Setting::where('key','gst_percentage')->update(array('value' => $request->gst));

        alert()->success('successfully update limit setting.');
        return redirect('setting');

    }

    public function update_details(Request $request)
    {
        $this->validate($request,[
            'mail_contact'  =>  'required',
            'mail_info'  =>  'required',
            'phone_no'  =>  'required|min:10|max:10',
            'address'  =>  'required',
            'cin'  =>  'required',
            'gst_no'  =>  'required',
            'pan_no'  =>  'required',
        ]);

        $company_details = CompanyDetail::first();
        $company_details->mail_contact = $request->mail_contact;
        $company_details->mail_info = $request->mail_info;
        $company_details->phone_no = $request->phone_no;
        $company_details->address = $request->address;
        $company_details->cin = $request->cin;
        $company_details->gst_no = $request->gst_no;
        $company_details->pan_no = $request->pan_no;
        $company_details->update();

        alert()->success('Successfully update website details setting.');
        return redirect('setting');

    }

    public function levelActive()
    {
        $roles = Role::whereNotIn('slug', ['super_admin','m12'] )->get();
        return view('superadmin.setting.level_active',compact('roles'));
    }

    public function updateLevelActive($id)
    {
        $roles = Role::where('id',$id)->update(array('status'=> 1));

        alert()->success('Successfully active level.');
        return redirect()->back();
    }

    public function staffCommission()
    {
        $commission_setting = Setting::where('list','commission')->get();
        return view('superadmin.setting.commission_setting',compact('commission_setting'));
    }

    public function updateCommission(Request $request)
    {
        $commission_setting = Setting::where('id','4')->update(['key' => $request->key4,'value' => $request->value4]);
        $commission_setting = Setting::where('id','5')->update(['key' => $request->key5,'value' => $request->value5]);
        $commission_setting = Setting::where('id','6')->update(['key' => $request->key6,'value' => $request->value6]);
        $commission_setting = Setting::where('id','7')->update(['key' => $request->key7,'value' => $request->value7]);
        $commission_setting = Setting::where('id','8')->update(['key' => $request->key8,'value' => $request->value8]);
        $commission_setting = Setting::where('id','9')->update(['key' => $request->key9,'value' => $request->value9]);

        alert()->success('Successfully update commission setting.');
        return redirect('setting');
    }
}
