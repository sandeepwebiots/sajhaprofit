<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kyc_form_uoplad;
use Redirect;


class Upload_Kyc_Form_Controller extends Controller
{
    public function upload_kyc_form(Request $request)
    {
    	//return $request->all();
    	 	 $request->validate([
   	 	  'user_email'			     => 'required|email|unique:kyc_form_uoplad',
   	 	  'mobile'				 => 'required|max:11|unique:kyc_form_uoplad',    
          'name'			 	 => 'required',
          'utype'				 =>'required|max:50',
          'uploadfile'  		 =>'required',
          'g-recaptcha-response' => 'required',
      ],['user_email.required'=>'email filed is required',
      	'user_email.user_email'	=>'Please enter a valid email address.',
      	'user_email.unique'	=>'The email has already been taken.',
      	'mobile.regex'	=>'Please enter valid phone number 07xxxxxxxxx',
      	'mobile.unique'	=>'The mobile has already been taken.','g-recaptcha-response.required'=>'Please Select This']);

		$form_upload = new kyc_form_uoplad;
          
            $form_upload->mobile= $request->mobile;
            $form_upload->name=$request->name;
            $form_upload->user_email = $request->user_email;
            $form_upload->form_type = $request->utype;
       		$form_upload->uploadfile=$request->uploadfile;
            if ($request->hasFile('uploadfile')) 
            {
	    	 	$uploadfile = $request->file('uploadfile');
	    		$pic_name = time().'.'.$uploadfile->getClientOriginalExtension();
		    	$path = public_path('../assets/upload_kyc_form/');
		    	$uploadfile->move($path, $pic_name);
		    	$form_upload->uploadfile = $pic_name;
	    	}
	    	 $form_upload->save();
            alert()->success('your kyc successful uploaded !');
   			return Redirect::to('/');
    }
}
