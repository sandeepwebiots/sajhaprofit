<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use Alert;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs_list = Blog::orderBy('id','desc')->get(); 
        return view('superadmin.blog.blog',compact('blogs_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.blog.blog_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'title' => 'required',
            'sub_title' => 'required',
            'content'   =>  'required',
            'img'   =>  'required|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $image = $request->file('img');
        $img_name = time().'.'.$image->getClientOriginalExtension();
        $path = public_path().'\assets\dashboard\upload\blog';
        $image->move($path, $img_name);

        $blog = new Blog;
        $blog->title = $request->title;
        $blog->sub_title = $request->sub_title;
        $blog->content = $request->content;
        $blog->img = $img_name;
        $blog->save();

        alert()->success('Blog Created','Blog created successfully.');
        return redirect('super-admin-blog-section');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('superadmin.blog.blog_edit',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' =>  'required',
            'sub_title' =>  'required',
            'content'   =>  'required',
        ]);

        $blog = Blog::find($id);

        if ($request->hasFile('img')) {
            try{
                $image_path = public_path('assets/dashboard/upload/blog/').$blog->img;
                unlink($image_path);
            }catch(\ErrorException  $e){

            }

            $image = $request->file('img');
            $path = public_path('assets/dashboard/upload/blog/');
            $img_name = time().'.'.$image->getClientOriginalExtension();
            $image->move($path,$img_name); 

            $blog->img = $img_name;         
        }

        $blog->title = $request->title;
        $blog->sub_title = $request->sub_title;
        $blog->content = $request->content;
        $blog->update();

        alert()->success('Blog Edit','Blog Edit successfully.');
        return redirect('super-admin-blog-section');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blog->delete();

        alert()->success('Blog Delete','Blog Delted successfully.');
        return redirect('super-admin-blog-section');
    }
}
