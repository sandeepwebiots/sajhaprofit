<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Validator;
use App\Individual;
use Redirect;
use Response;

class KycController extends Controller
{
   public function Individual(Request $request)
   {
   
   		//return $request->all();
   	 $request->validate([
   	 	  'email'			=> 'required|email|unique:kyc_individual',
   	 	  'Contact_No'		=> 'required|max:11|unique:kyc_individual',    
          'gender'			=> 'required',
          'state'			=>'required|max:50',
          'married_status'  => 'required', 
          'resident_status' => 'required',
          'dob'		    =>'required',
          'pincode'		    =>'required|max:6',
	      'pan_no'		    =>'required|max:10|unique:kyc_individual',
	      'adhar'		    =>'required|max:18|unique:kyc_individual',
	      'address'         =>'required',
          'file_signature'  =>'required',	
          'file_photo'		=>'required',
          'file_doc'		=>'required',
          'g-recaptcha-response' => 'required'
      ],['Mobile_No.regex'	=>'Please enter valid phone number 07xxxxxxxxx','g-recaptcha-response.required'=>'Please Select This']);

		$Individual = new Individual;
          $Individual->email = $request->email;
            $Individual->Contact_No= $request->Contact_No;
            $Individual->gender=$request->gender;
            $Individual->state = $request->state;
       		$Individual->married_status=$request->married_status;
            $Individual->resident_status=$request->resident_status;
            $Individual->dob  = $request->dob;
            $Individual->pincode = $request->pincode;
            $Individual->pan_no	= $request->pan_no;
            $Individual->adhar	=$request->adhar;
            $Individual ->address = $request->address;
           
            if ($request->hasFile('file_signature')) 
            {
	    	 	$file_signature = $request->file('file_signature');
	    		$pic_name = time().'.'.$file_signature->getClientOriginalExtension();
		    	$path = public_path('../assets/Individual/file_signature/');
		    	$file_signature->move($path, $pic_name);
		    	$Individual->file_signature = $pic_name;
	    	}
	    	   if ($request->hasFile('file_photo')) 
            {
	    	 	$file_photo = $request->file('file_photo');
	    		$pic_name = time().'.'.$file_photo->getClientOriginalExtension();
		    	$path = public_path('../assets/Individual/file_photo/');
		    	$file_photo->move($path, $pic_name);
		    	$Individual->file_photo = $pic_name;
	    	}
	    		 if ($request->hasFile('file_doc')) 
          {
	    	 	$file_doc = $request->file('file_doc');
	    		$doc =time().'.'.$file_doc->getClientOriginalExtension();
		    	$path = public_path('../assets/Individual/file_doc/');
		    	$file_doc->move($path, $doc);
		    	$Individual->file_doc = $doc;
	    	  }
            $Individual->save();
            alert()->success('your kyc successful uploaded !');
   			return Redirect::to('/');
   }


         public function getDownload()
      {
          //PDF file is stored under project/public/download/info.pdf
          $file= public_path(). "/assets/kyc_form/kyc_individual.pdf";

          $headers = array(
                    'Content-Type: application/pdf',
                  );

          return Response::download($file, 'for individual.pdf', $headers);
      }
  
}
