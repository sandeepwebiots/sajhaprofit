<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Alert;
use Sentinel;

class ValidationController extends Controller
{
    public function index(){
    	return view('validate');
    }

    public function validate2FA(Request $request){
    	$user_id = $request->session()->pull('logged_user_id');
    	$user = User::where('id',$user_id)->where('otp',$request->code)->first();
    	if($user){
    		$sentinel = app('sentinel');
	        $user = $sentinel->findById($user_id);
	        $sentinel->login($user);
    		// Alert::message('Welcome.')->autoclose(2000);
			return redirect('dashboard');
    	}
    	else{
    		alert()->warning('Error','2FA not match.');
			return redirect()->back();
    	}
    }
}
