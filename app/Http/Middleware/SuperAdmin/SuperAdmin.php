<?php

namespace App\Http\Middleware\SuperAdmin;

use Closure;
use Sentinel;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check()  && (Sentinel::getUser()->roles()->first()->slug == 'super_admin')) 
        {
            return $next($request);
        }else{
            Sentinel::logout();
            return redirect('/login');
        }
    }
}
