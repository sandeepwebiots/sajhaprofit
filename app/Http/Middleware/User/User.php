<?php

namespace App\Http\Middleware\User;

use Closure;
use Sentinel;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check()  && Sentinel::getUser()->roles()->first()->slug == 'm12') {
            return $next($request);
        }
        else{
            Sentinel::logout();
            return redirect('/login');
        }
    }
}
