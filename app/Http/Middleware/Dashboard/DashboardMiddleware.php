<?php

namespace App\Http\Middleware\Dashboard;

use Closure;
use Sentinel;

class DashboardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check()  && (Sentinel::getUser()->roles()->first()->slug == 'super_admin' || Sentinel::getUser()->roles()->first()->slug == 'm2' || Sentinel::getUser()->roles()->first()->slug == 'm3' || Sentinel::getUser()->roles()->first()->slug == 'm4' || Sentinel::getUser()->roles()->first()->slug == 'm5' || Sentinel::getUser()->roles()->first()->slug == 'm6' || Sentinel::getUser()->roles()->first()->slug == 'm7' || Sentinel::getUser()->roles()->first()->slug == 'm8' || Sentinel::getUser()->roles()->first()->slug == 'm9' || Sentinel::getUser()->roles()->first()->slug == 'm10' || Sentinel::getUser()->roles()->first()->slug == 'm11' )) 
        {
            return $next($request);
        }else{
            Sentinel::logout();
            return redirect('/login');
        }
    }
}
