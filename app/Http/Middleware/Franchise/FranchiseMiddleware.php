<?php

namespace App\Http\Middleware\Franchise;

use Closure;
use Sentinel;

class FranchiseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check()  && (Sentinel::getUser()->roles()->first()->slug == 'm10')) 
        {
            return $next($request);
        }else{
            Sentinel::logout();
            return redirect('/dashboard');
        }
    }
}
