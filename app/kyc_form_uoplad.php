<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kyc_form_uoplad extends Model
{
    protected $table = 'kyc_form_uoplad';
    protected $fillable = ['name','mobile','email','form_type','uploadfile'];
}
