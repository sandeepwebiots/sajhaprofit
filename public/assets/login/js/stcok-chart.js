

function stockChart()
{ 
    var coinname='BTC';
    
        $.getJSON('stock-chart/', function (result) {
            console.log(result);
            console.log('testing');
        // Exchange Data on Exchange Header
           

            var change = result['exchange'][coinname]['USD']['CHANGEPCT24HOUR'];
            var change_val=parseFloat(change).toFixed(2) ;  
          

           

        var data = result['data'];

        

        // split the data set into ohlc and volume
        var datatemp = [],
            
            dataLength = data.length,
          

            i = 0;

        for (i; i < dataLength; i += 1) {
            datatemp.push([
                data[i]['time']*1000, // the date
                data[i]['open'], // open
                data[i]['high'], // high
                data[i]['low'], // low
                data[i]['close'] // close
            ]);
        }

        if(coinname=='BTC'){ var currency = 'Bitcoin'; }else if(coinname=='BCH'){ var currency = 'Bitcoin Cash';   }
       

        var title = "<span style='color: #fff;'>"+currency+"</span><br><span style='color: #fff;'>"+coinname+"</span>"; 
        // var subtitle = "R";
         // create the chart
        $('#loading').fadeIn();
    Highcharts.stockChart('stock-chartcontainer', {
          title: {
                text: title,
            },


            // subtitle: {
            //     text: subtitle
            // }, 
            xAxis: {
                    gapGridLineWidth: 20,
                    type: 'datetime'
            },

            rangeSelector: {
                buttons: [  {
                //     type: 'day',
                //     count: 1,
                //     text: '1d'
                // }
                // ,{
                    type: 'week',
                    count: 1,
                    text: '1w'
                }, {
                    type: 'month',
                    count: 1,
                    text: '1m'
                }, {
                    type: 'all',
                    count: 1,
                    text: 'All'
                }
                ],
                selected: 0,
                inputEnabled: false
            },

            series: [{
                name: coinname,
                type: 'candlestick',
                data: datatemp,
                gapSize: 5,
                tooltip: {
                    valueDecimals: 2
                },
                fillColor:  "#1868dd",
                lineColor: "#1868dd",
                negativeFillColors: "#228B22",
                negativeLineColor: "#1868dd",
                threshold: null
            }]
        });
       $('#loading').fadeOut();
    });
}